let generatorInterface = (function(){
    let mails = [];



    function getMails(){
        return mails;
    }

    // let defaultSettings = {
    //     "date": new Date().toString(),
    //     // "phishing": true,
    //     "help": {
            
    //     },
    //     "isEncrypted": false,
    //     "tags": ["MailAccount"],
    //     "service": {},
    //     "generateMailAddress": true,
    //     "types": {
    //         // "required": "test1",
    //         "forbidden": []
    //     },
    //     "pieceGeneration": {
    //         "numberOfPieces": 5,
    //         "pieceSettings": {
                
    //         }    
    //     }
    // }

    function resetMails(){
        mails = []
    }

    function addMail(){
        let service = {
            name: "Gmail Support",
            url: "https://www.gmail.com/",
            email: "support@gmail.com"
        }
        let newMail;


        if(Math.random() >= 0.3){
            newMail = MTLG.mailGenerator.generateMail({phishing: true, generateMailAddress: true, service: service});
        }
        else {        
            newMail = MTLG.mailGenerator.generateMail({phishing: false, generateMailAddress: false, service: service});
            newMail.from = service;
            if("phishingData" in newMail){
                delete newMail.phishingData;
                delete newMail.mark;
                delete newMail.types;
                newMail.isFish = false;
            }
        }
        
        if("header" in newMail){
            delete newMail.header;
        }
        mails.push(newMail);
    }

    return {
        getMails : getMails,
        addMail : addMail,
        resetMails : resetMails
    }
})();