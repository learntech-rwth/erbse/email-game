
/**
 * View for the settings menu
 * @class
 * @param {number} x The x coordinate where the view starts
 * @param {number} y The y coordinate where the view starts
 * @param {number} width The width of the view
 * @param {number} height The height of the view
 * @param {stage} stage The stage on which the elements of the view are drawn
 */
function settingsView(x, y, width, height, stage) {
    this.x = x; //position of new email pane
    this.y = y;
    this.width = width; //size of new email pane
    this.height = height;
    this.stage = stage;

    //alignment variables
    let padEdgeBorderX = 20;
    let padEdgeBorderY = 20;
    let padBetweenOptionsY = 50;
    let btnWidth = 150;
    let btnHeight = 70;


    //Container
    this.settingsViewContainer = new createjs.Container();

    //BG
    this.bg = new createjs.Shape(); //create BG of E-Mail creation Pane
    this.bg.graphics.setStrokeStyle(1,"").beginStroke("black");
    this.bg.graphics.beginFill("#f2f2f2").drawRect(this.x,this.y,this.width,this.height);
    this.settingsViewContainer.addChild(this.bg);
    this.settingsViewContainer.visible = false;

    //caption
    this.caption = new createjs.Text(l('settings-caption'), "bold 40px Arial", "Black");
    this.caption.x = this.x + padEdgeBorderX;
    this.caption.y = this.y + padEdgeBorderY;

    this.captionBorder = new createjs.Shape();
    this.captionBorder.graphics.setStrokeStyle(1,"").beginStroke("black");
    this.captionBorder.graphics.beginFill("").drawRect(this.x, this.y + 2*padEdgeBorderY + this.caption.getMeasuredHeight(), this.width, 0);
    this.settingsViewContainer.addChild(this.captionBorder);


    this.closeButton = new Button(this.x + this.width - btnWidth/4 - 1, this.y + 1,btnWidth/4,btnHeight/2,"","#ffae3b",this.stage,imgPaths['phishing'],"","","",0.07,0.07);
    this.settingsViewContainer.addChild(this.caption,this.closeButton);

    if(MTLG.getOptions().debug == true){
      console.log("Reference img button",this.closeButton);
    }

    //audio settings
    this.sound = new createjs.Text(l('settings-sound'), "40px Arial", "Black");
    this.sound.x = this.x + padEdgeBorderX;
    this.sound.y = this.caption.y + 100;
    this.soundOnButton = new Button(this.x + 9*this.width/10 - btnWidth/2 - padEdgeBorderY, this.sound.y -15,btnWidth,btnHeight,"","white",this.stage,imgPaths['sound-on']);
    this.soundOffButton = new Button(this.soundOnButton.children[0].x,this.soundOnButton.children[0].y,this.soundOnButton.children[0].graphics.command.w,this.soundOnButton.children[0].graphics.command.h,"","white",this.stage,imgPaths['sound-off']);
    if (MTLG.getOptions().soundEnabled == true){
      this.soundOffButton.visible = false;
    }
    else{
      this.soundOnButton.visible = false;
    }
    this.settingsViewContainer.addChild(this.sound,this.soundOffButton,this.soundOnButton);

    //click selection setting
    this.click = new createjs.Text(l('settings-click'), "40px Arial", "Black");
    this.click.x = this.x + padEdgeBorderX;
    this.click.y = this.sound.y + padBetweenOptionsY + this.sound.getMeasuredHeight();
    this.clickOnButton = new Button(this.x + 9*this.width/10 - btnWidth/2 - padEdgeBorderY, this.click.y - 15,btnWidth,btnHeight,'settings-button-click',"white",this.stage,"","bold 30px Arial");
    this.clickOffButton = new Button(this.clickOnButton.children[0].x,this.clickOnButton.children[0].y,this.clickOnButton.children[0].graphics.command.w,this.clickOnButton.children[0].graphics.command.h,'settings-button-touch',"white",this.stage,"","bold 30px Arial");
    if (MTLG.getOptions().clicksEnabled == true){
      this.clickOffButton.visible = false;
    }
    else{
      this.clickOnButton.visible = false;
    }
    this.settingsViewContainer.addChild(this.click,this.clickOnButton,this.clickOffButton);

    //difficulty settings
    this.difficulty = new createjs.Text(l('settings-difficulty'), "40px Arial", "Black");
    this.difficulty.x = this.x + padEdgeBorderX;
    this.difficulty.y = this.click.y + padBetweenOptionsY + this.click.getMeasuredHeight();
    this.difficultyEasyButton = new Button(this.x + 9*this.width/10 - btnWidth/2 - padEdgeBorderY, this.difficulty.y - 15,btnWidth,btnHeight,'settings-button-easy',"white",this.stage,"","bold 30px Arial");
    this.difficultyMediumButton = new Button(this.difficultyEasyButton.children[0].x,this.difficultyEasyButton.children[0].y,this.difficultyEasyButton.children[0].graphics.command.w,this.difficultyEasyButton.children[0].graphics.command.h,'settings-button-medium',"white",this.stage,"","bold 30px Arial");
    this.difficultyHardButton = new Button(this.difficultyEasyButton.children[0].x,this.difficultyEasyButton.children[0].y,this.difficultyEasyButton.children[0].graphics.command.w,this.difficultyEasyButton.children[0].graphics.command.h,'settings-button-hard',"white",this.stage,"","bold 30px Arial");
    if (MTLG.getOptions().difficulty == "easy"){
      this.difficultyMediumButton.visible = false;
      this.difficultyHardButton.visible = false;
    }
    else if (MTLG.getOptions().difficulty == "medium"){
      this.difficultyEasyButton.visible = false;
      this.difficultyHardButton.visible = false;
    }
    else{
      this.difficultyEasyButton.visible = false;
      this.difficultyMediumButton.visible = false;
    }
    this.settingsViewContainer.addChild(this.difficulty,this.difficultyEasyButton,this.difficultyMediumButton,this.difficultyHardButton);

    this.toMenuButton = new Button(this.x + this.width - padEdgeBorderX - 250, this.difficulty.y + this.difficulty.getMeasuredHeight() + padBetweenOptionsY, 250, 75, 'toMenu', "#FFFFFF", this.stage);
    this.bigCloseButton = new Button(this.x + padEdgeBorderX, this.difficulty.y + this.difficulty.getMeasuredHeight() + padBetweenOptionsY, 250, 75, 'close', "#FFFFFF", this.stage);
    this.settingsViewContainer.addChild(this.toMenuButton, this.bigCloseButton);
    this.stage.addChild(this.settingsViewContainer);
}
