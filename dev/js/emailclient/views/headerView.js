/**
 * View for Header (Subject + Name)
 */
function headerView(x, y, width, height, stage, email, targets, header) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.stage = stage;
    this.email = email;
    this.targets = targets; //contains suspicious targets
    this.header = header; //true if header should be displayed

    var headerHeight = this.height * 0.75; //210
    var subjectHeight = 70;
    var senderHeight = headerHeight - subjectHeight;
    var attachmentsHeight = this.height * 0.25; //70

    //Container
    this._headerContainer = new createjs.Container();

    this._Bg = new createjs.Shape();
    this._Bg.graphics.beginFill('#f2f2f2').beginStroke("black").drawRect(this.x, this.y, this.width, this.height);

    this._subjectView = new subjectView(this.x, this.y, this.width, subjectHeight, this.stage, this.email, this.targets);
    this._senderView = new senderView(this.x, this.y + subjectHeight, this.width, senderHeight, this.stage, this.email, this.targets);
    this._attachmentsView = new attachmentsView(this.x, this.y + headerHeight, this.width, attachmentsHeight, this.stage, this.email, this.targets);
    this._functionsView = new functionsView(this.x, this.y + subjectHeight, this.width, senderHeight, this.stage, this.email, this.header);

    this._headerContainer.addChild(this._Bg, this._subjectView._subjContainer, this._senderView._senderContainer, this._attachmentsView.attachmentsContainer, this._functionsView._functionsContainer);

}