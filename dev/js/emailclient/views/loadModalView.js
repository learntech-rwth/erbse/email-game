function loadModalView (stage) {
    this.stage = stage;

    this.loadModalViewContainer = new createjs.Container();

    this.bg = new createjs.Shape();
    this.bg.graphics.setStrokeStyle(2,"").beginStroke("");
    this.bg.graphics.beginFill("#545350").drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
    this.bg.alpha = 0.7;
    this.loadModalViewContainer.addChild(this.bg);

    this.hackerImg = MTLG.assets.getBitmap(imgPaths['avatar1']);//hacker
    this.hackerImg.x = 1200;
    this.hackerImg.y = 550;
    this.hackerImg.scaleX = 1;//0.5;
    this.hackerImg.scaleY = 1;//0.5;

    this.loadModalViewContainer.addChild(this.hackerImg);

    //speech bubble
    this.speech = MTLG.assets.getBitmap(imgPaths['speech']);//'img/speech-bubble.png');
    this.speech.x = 400;
    this.speech.y = 30;
    this.speech.scaleX = 0.80;
    this.speech.scaleY = 0.6;
    this.loadModalViewContainer.addChild(this.speech)

    //text inside speech bubble
    this.speechTxt = new createjs.Text(l('load'), "35px Arial", "black");
    this.speechTxt.lineWidth = 890;
    this.speechTxt.x = this.speech.x + 95;
    this.speechTxt.y = this.speech.y + 55;
    this.loadModalViewContainer.addChild(this.speechTxt);

    //Notes imagePaths['correct'], imagePaths['incorrect']  

    this.loadButton = new Button(1250, 430, 100, 60, "", "white", this.stage, imgPaths['correct'], "", "", "", 0.05, 0.05);
    this.loadButton.on("click", function(){
        this.stage.removeChild(this.loadModalViewContainer)
        xapiGameLoaded();
    }.bind(this))

    this.noLoadButton = new Button(550, 430, 100, 60, "", "white", this.stage, imgPaths['incorrect'], "", "", "", 0.05, 0.05);
    this.noLoadButton.on("click", function(){
        _saver.reset()
        xapiGameStart()
        this.stage.removeChild(this.loadModalViewContainer)
    }.bind(this))

    this.loadModalViewContainer.addChild(this.loadButton);
    this.loadModalViewContainer.addChild(this.noLoadButton);

    this.stage.addChild(this.loadModalViewContainer);
}