/**
 * View for functions in Header (3 dots icon, header pop up, lock icon, signed icon)
 */
function functionsView(x, y, width, height, stage, email, header) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.stage = stage;
    this.email = email;
    this.header = header; //should header be displayed?

    this._functionsContainer = new createjs.Container();
    this._dropdownContainer = new createjs.Container();
    this._headerPopupContainer = new createjs.Container();

    var dotsObj = {
        text: "...",
        font: "60px Arial",
        x: this.x + this.width - 65,
        y: this.y - 10,
        pad: 10,
        colorBg: '#f2f2f2',
    };
    this._threeDots = new Textbox(dotsObj);

    this.dropdown = new Textbox({
        text: l('dropdownText'),
        x: dotsObj.x - 125,
        y: dotsObj.y + 60,
        stroke: "black",
        fixwidth: 190,
        fixheight: 270,
    });
    this.dropdown.textbox.visible = false; //show dropdown menu when player clicks on three dots

    this._dropdownContainer.addChild(this._threeDots.textbox, this.dropdown.textbox);

    this._smimeSymbols = new createjs.Container();


    var distance = 35; //for positioning of mime symbols
    //signed icon
    if (this.email.isSigned) { //add signed icon, if email is signed
        signedIcon = MTLG.assets.getBitmap(imgPaths['signed']);//'img/ribbon-icon.png');
        signedIcon.x = dotsObj.x - 10 + distance;
        signedIcon.y = dotsObj.y + 80;
        signedIcon.scaleX = 0.06;
        signedIcon.scaleY = 0.06;

        var box = new Textbox({ //explanation shown on mouseover
            text: l("signed"),
            x: signedIcon.x - 20,
            y: signedIcon.y + 40,
            stroke: "black",
            font: "20px Arial",
        });
        signedIcon.box = box.textbox;
        signedIcon.box.visible = false;

        distance = 0;

        this._smimeSymbols.addChild(signedIcon, signedIcon.box);

    }

    //encrypted icon
    if (this.email.isEncrypted) { //add encrypted icon, if email is encrypted
        var lockIcon = MTLG.assets.getBitmap(imgPaths['encrypted']);//'img/lock-icon.png');
        lockIcon.x = dotsObj.x - 10 + distance;
        lockIcon.y = dotsObj.y + 80;
        lockIcon.scaleX = 0.06;
        lockIcon.scaleY = 0.06;

        var box = new Textbox({ //explanation shown on mouseover
            text: l("encrypted"),
            x: lockIcon.x - 65,
            y: lockIcon.y - 5,
            stroke: "black",
            font: "20px Arial",
        });
        lockIcon.box = box.textbox;
        lockIcon.box.visible = false;

        this._smimeSymbols.addChild(lockIcon, lockIcon.box);

    }

    if (this._smimeSymbols.children.length > 0) { //if email is signed or encrypted, show explanation on mouseover
        createjs.Ticker.on("tick", displaySMIME.bind(this)); //Ticker for displaying explanation
    }

    this._functionsContainer.addChild(this._smimeSymbols, this._dropdownContainer);

    // if (this.header) { //if header (Button and Pop-Up) should be shown in email client

    //     this._headerButtonContainer = new Button(this.x + this.width - 200, this.y, 100, 50, "Header", "white");

    //     var header = this.email.headers;
    //     var headerKeys = Object.keys(header);
    //     var popupTxt = "";

    //     for (let key of headerKeys) {
    //         if (key !== 'X-UI-Filterresults') { //Most Clients don't display this result
    //             popupTxt += key + ": " + header[key] + "\n";
    //         }
    //     }

    //     var popupOptions = {
    //         text: popupTxt,
    //         x: 680,
    //         y: 20,
    //         colorTxt: "black",
    //         colorBg: "white",
    //         stroke: "black",
    //         pad: 10,
    //         font: "20px Arial",
    //         rightaligned: false,
    //         fixwidth: 1240, //preview width
    //         fixheight: 950, //preview height
    //     };
    //     var popup = new Textbox(popupOptions)

    //     var _closeIcon = MTLG.assets.getBitmap(imgPaths['close']);//'img/close-icon.png');
    //     _closeIcon.x = popupOptions.x + popupOptions.fixwidth - 70;
    //     _closeIcon.y = popupOptions.y - 30;
    //     _closeIcon.scaleX = 0.1;
    //     _closeIcon.scaleY = 0.1;

    //     this._headerPopupContainer.addChild(popup.textbox, _closeIcon);
    //     this._headerPopupContainer.visible = false; //visible when player clicks on header button

    //     this._functionsContainer.addChild(this._headerButtonContainer, this._headerPopupContainer);

    //     this._headerButtonContainer.on("click", function (evt) {
    //         this.stage.setChildIndex(this._headerPopupContainer, this.stage.numChildren - 1);
    //         this._headerPopupContainer.visible = true;
    //     }.bind(this));

    //     _closeIcon.on("click", function (evt) {
    //         if (this._headerPopupContainer.visible) {
    //             this._headerPopupContainer.visible = false;
    //         }
    //     }.bind(this));


    // }

    this._threeDots.textbox.on("click", function (evt) {

        if (this.dropdown.textbox.visible) {
            this.dropdown.textbox.visible = false;
        }
        else {
            this.stage.setChildIndex(this.dropdown, this.stage.numChildren - 1);
            this.dropdown.textbox.visible = true;
        }
    }.bind(this));


    //display "signed"/"encrypted" on mousover over symbols
    function displaySMIME() {
        var symbols = this._smimeSymbols.children; //of the form: [symbol1, explanation1, symbol2,..]

        for (var i = 0; i < symbols.length / 2; i++) {
            let point = symbols[i * 2].globalToLocal(MTLG.getStage().mouseX, MTLG.getStage().mouseY); //get mousepostion

            if (symbols[i * 2].hitTest(point.x, point.y)) { //if mouse hovers over symbol, display corresponding explanation
                symbols[i * 2 + 1].visible = true;
                this.stage.setChildIndex(symbols[i * 2 + 1], this.stage.numChildren - 1);
            }
            else {
                symbols[i * 2 + 1].visible = false;
            }
        }


    }


    //notification, that click on dropdown functions is not supported
    var notsupported = new notSupportedNotification(this.stage, this.dropdown.textbox, true);

}
