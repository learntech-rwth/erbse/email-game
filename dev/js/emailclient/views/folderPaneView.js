/**
 * View for Folder Pane
 */
function folderPaneView(x, y, width, height, stage) {
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.stage = stage;

  //Container
  this._newButtonContainer = new createjs.Container();
  this._folderContainer = new createjs.Container();
  this._folderPaneContainer = new createjs.Container();
  this.soundButtonContainer = new createjs.Container();
  this.clickButtonContainer = new createjs.Container();

  //"NEW"-Button
  //this._buttonBgView = new createjs.Shape();
  this._buttonBgHeight = 100;
  //this._buttonBgView.graphics.beginFill("#ffae3b").drawRect(this.x, this.y, this.width, this._buttonBgHeight);
  this.newButton = new Button(this.x+5,this.y,this.width-10,this._buttonBgHeight,"","#ffae3b",this.stage,"");
  this.levelView = new levelView(250,0,1920-250,970,this.stage);
  this.settingsView = new settingsView(250+(1920-250)/4,970/4,(1920-250)/2,970/2,this.stage);
  this.finishNewGameView = new finishNewGameView(250+1*(1920-250)/10,970/10,8*(1920-250)/10,8*970/10,this.stage);
  this.tutorialModalView = new tutorialModalView(this.stage);

  if(MTLG.getOptions().debug == true){
    console.log("Reference text button",this.newButton);
  }

  this.buttonTextView = new createjs.Text(l('menu'), "bold 40px Arial", "#FFFFFF");
  this.buttonTextView.textAlign = "center";
  this.buttonTextView.textBaseline = "middle";
  this.buttonTextView.x = this.x + this.width / 2;
  this.buttonTextView.y = this.y + this._buttonBgHeight / 2;

  //this._newButtonContainer.addChild(this._buttonBgView, this.newButton,this.buttonTextView);
  this._newButtonContainer.addChild(this.newButton,this.buttonTextView);

  let btnWidth = this.width-10;
  let btnHeight = 50;

  //settings button
  this.settingsButton = new Button(this.x + this.width/2 - btnWidth/2,this.y + 520,btnWidth,btnHeight,"settings","white",this.stage,imgPaths['gear'],"26px arial","black","",0.055,0.055, {x: 5}, 10);
  this.settingsButton.visible = true;

  //clock
  this.clockImg = MTLG.assets.getBitmap(imgPaths['clock']);
  this.clockImg.scaleX = btnHeight/this.clockImg.image.width;
  this.clockImg.scaleY = btnHeight/this.clockImg.image.height;
  this.clockImg.x = this.x + this.width/4 + 10 - this.clockImg.image.height*this.clockImg.scaleX/2,
  this.clockImg.y = this.y + 600;
  this.clockImg.visible = false;
  this.clockText = new Textbox({
    text: " ",
    x: this.x + 3*this.width/4 - 10,
    y: this.clockImg.y + this.clockImg.image.height*this.clockImg.scaleY/2 - 15, // -13
    centered: true,
    font: "40px Arial", //35
    colorBg: null,
  });
  this.clockText.textbox.visible = false;

  //score  & highscore texts
  this.scoreText = new Textbox({
    text: l('score'),
    x: this.x + this.width/2 + 5,
    y: this.y + 600,
    centered: true,
    font: "35px Arial",
    colorBg: null,
  });
  this.scoreText.textbox.visible = false;

  this.scoreNumber = new Textbox({
    text: "0",
    x: this.x + this.width/2 + 5,
    y: this.scoreText.txt.y+60,
    centered: true,
    font: "35px Arial",
    colorBg: null,
  });
  this.scoreNumber.textbox.visible = false;

  this.highscoreText = new Textbox({
    text: l('highscore'),
    x: this.x + this.width/2 + 5,
    y: this.scoreNumber.txt.y+60,
    centered: true,
    font: "35px Arial",
    colorBg: null,
  });
  this.highscoreText.textbox.visible = false;

  this.highscoreNumber = new Textbox({
    text: "0",
    x: this.x + this.width/2 + 5,
    y: this.highscoreText.txt.y+60,
    centered: true,
    font: "35px Arial",
    colorBg: null,
  });
  this.highscoreNumber.textbox.visible = false;

  let emailLengthY = MTLG.getOptions().creationTimerEnabled? this.scoreNumber.txt.y+20 : this.clockImg.y;

  //email length
  this.emailLengthText = new Textbox({
    text: l('emailLength'),
    x: this.x + this.width/2 + 5,
    y: emailLengthY,
    centered: true,
    font: "35px Arial",
    colorBg: null,
  });
  this.emailLengthText.textbox.visible = false;

  this.emailLengthNumber = new Textbox({
    text: "0",
    x: this.x + this.width/2 + 5,
    y: this.emailLengthText.txt.y+60,
    centered: true,
    font: "35px Arial",
    colorBg: null,
  });
  this.emailLengthNumber.textbox.visible = false;

  //Folderlist

  //Bg needed for onclick event. Bg is not visible

  this._folderBg = new createjs.Shape();
  this._folderBg.graphics.drawRect(0, 0, this.width, 5 * 70);
  this._folderBg.x = this.x;
  this._folderBg.y = this.y + 150;
  var hit = new createjs.Shape();
  hit.graphics.beginFill("#000").rect(0, 0, this.width, 5 * 70);
  this._folderBg.hitArea = hit;

  this._folderContainer.addChild(this._folderBg);

  //folder labels
  this._folders = l('folders'); //["Posteingang", "Entwurf", "Gelöscht", "Spam", "Gesendet"];
  this._folder = [];

  for (i = 0; i < this._folders.length; i++) {
    this._folder[i] = new createjs.Text(this._folders[i], "35px Arial", "black");
    this._folder[i].x = this.x + 10;
    this._folder[i].y = this.y + 150 + i * 70; //position folders among each other

    this._folderContainer.addChild(this._folder[i]);
  }


  //BG for 'inbox' folder
  this._inboxBg = new createjs.Shape();
  this._inboxBg.graphics.beginFill("#f2f2f2").drawRect(0, 0, this.width, 70); //#D3D3D3
  this._inboxBg.x = this.x;
  this._inboxBg.y = this._folder[0].y - 18;

  //this._folderContainer.addChildAt(this._inboxBg, 0);

  this.numUnread = emails.length; //coounter for number of unread emails
  this.numUnreadBox = new Textbox({
    text: this.numUnread,
    x: this.x + this.width - 10, //position next to 'inbox' text
    y: this._folder[0].y + 3,
    colorTxt: "blue",
    colorBg: "#f2f2f2", //D3D3D3
    rightaligned: true,
  });

  //changed so that the not implemented notification wouldn't show up when pressing the New-Button
  //this._folderPaneContainer.addChild(this._newButtonContainer, this._folderContainer, this.numUnreadBox.textbox);
  this._folderPaneContainer.addChild(this._folderContainer, this.numUnreadBox.textbox);
  this.stage.addChild(this._folderPaneContainer,this._newButtonContainer,this.soundButtonContainer,this.clickButtonContainer,this.levelView.levelViewContainer,this.clockImg,this.clockText.textbox,this.settingsButton,this.scoreText.textbox,this.scoreNumber.textbox,this.highscoreText.textbox,this.highscoreNumber.textbox,this.emailLengthText.textbox,this.emailLengthNumber.textbox);

  //notification, that click on folders and 'new'-Button is not supported
  var notsupported = new notSupportedNotification(this.stage, this._folderContainer);

  this.moveSettingsButton = function (onTop = true) {
    if (!onTop) {
      this.settingsButton.children[0].set({ y: this.y + 520});
    } else {
      this.settingsButton.children[0].set({ y: this.y + 125});
    }
    this.settingsButton.moveImg();
    this.settingsButton.children[1].realignButtonText();
  }.bind(this);
}
