/* View when no email is selected*/

function noSelectionView(x, y, stage) {
    this.x = x;
    this.y = y;
    this.stage = stage;

    this.emptyScreen = new createjs.Text(l('noSelec'), "50px Arial", "black"); //Text 'Select a Message'
    this.emptyScreen.x = this.x + 50; 
    this.emptyScreen.y = this.y; 
    this.stage.addChild(this.emptyScreen);

    this.letterImg = MTLG.assets.getBitmap(imgPaths['letter']);//'img/letter.png');
    this.letterImg.x = this.x + 130;
    this.letterImg.y = this.y + 100;
    this.letterImg.scaleX = 0.4;
    this.letterImg.scaleY = 0.4;
    this.stage.addChild(this.letterImg);
}