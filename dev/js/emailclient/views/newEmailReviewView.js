
/**
 * View for the review after the other two games
 * @class
 * @param {number} x The x coordinate where the view starts
 * @param {number} y The y coordinate where the view starts
 * @param {number} width The width of the view
 * @param {number} height The height of the view
 * @param {stage} stage The stage on which the elements of the view are drawn
 * @param {number} padding The padding for some UI elements
 */
function newEmailReviewView(x, y, width, height, stage, padding) {
  this.x = x; //position of new email pane
  this.y = y;
  this.width = width;
  this.height = height;
  this.stage = stage;
  this.padding = padding;

  var paddingBoxes = 30;
  var paddingText = 10;
  var boxHeight = (this.height - this.padding*3)/2;
  var boxWidth = this.width - this.padding*2;
  var boxStartX = this.x+this.padding;

  //Container
  this.newEmailReviewContainer = new createjs.Container();

  //border for the review boxes
  this.borderReviewEmail = new createjs.Shape();
  this.borderReviewEmail.graphics.setStrokeStyle(1,"").beginStroke("black");
  this.borderReviewEmail.graphics.beginFill("#f2f2f2").drawRect(boxStartX,this.y+this.padding,boxWidth,boxHeight);
  this.borderReviewMachting = new createjs.Shape();
  this.borderReviewMachting.graphics.setStrokeStyle(1,"").beginStroke("black");
  this.borderReviewMachting.graphics.beginFill("#f2f2f2").drawRect(boxStartX,this.y+this.padding*2+boxHeight,boxWidth,boxHeight);

  this.bgReviewEmail = new createjs.Shape(); //create BG of email review box
  this.bgReviewEmail.graphics.beginFill("#f2f2f2").drawRect(boxStartX,this.y+this.padding,boxWidth,boxHeight);
  this.bgReviewEmail.alpha = 0.65;
  this.bgReviewMatching = new createjs.Shape(); //create BG of matching game review box
  this.bgReviewMatching.graphics.beginFill("#f2f2f2").drawRect(boxStartX,this.y+this.padding*2+boxHeight,boxWidth,boxHeight);
  this.bgReviewMatching.alpha = 0.65;

  //get glyph size
  var textSizeTextbox = new createjs.Text("I", "30px Arial");
  this.glyphHeight = textSizeTextbox.getBounds().height;

  //create PE review textbox
  this.newEmailReviewEmailTextbox = new Textbox({
    x: boxStartX + paddingText*2,
    y: this.y + this.padding + paddingText*2,
    text: "Text goes here and here",
    fixwidth: boxWidth - (paddingText*2)*2,
    font: "30px Arial",
    colorBg: null,
    lineHeight: this.glyphHeight*1.25,
  });

  //creation icon
  this.creationImg = MTLG.assets.getBitmap(imgPaths['letter-border']);
  this.creationImg.scaleX = 0.35;
  this.creationImg.scaleY = 0.35;
  this.creationImg.x = this.borderReviewEmail.graphics.command.x + this.borderReviewEmail.graphics.command.w - this.creationImg.image.width*this.creationImg.scaleX - 10; //-10 to get the image away from the border
  this.creationImg.y = this.borderReviewEmail.graphics.command.y + this.borderReviewEmail.graphics.command.h - this.creationImg.image.height*this.creationImg.scaleY - 10;

  //create matching review textbox
  this.newEmailReviewMatchingTextbox = new Textbox({
    x: boxStartX + paddingText*2,
    y: this.y + this.padding*2 + boxHeight + paddingText*2,
    text: "Text goes here",
    fixwidth: boxWidth - (paddingText*2)*2,
    font: "30px Arial",
    colorBg: null,
    lineHeight: this.glyphHeight*1.25,
  });

  //matching icon
  this.matchingImg = MTLG.assets.getBitmap(imgPaths['matching']);
  this.matchingImg.scaleX = 0.35;
  this.matchingImg.scaleY = 0.35;
  this.matchingImg.x = this.borderReviewMachting.graphics.command.x + this.borderReviewMachting.graphics.command.w - this.matchingImg.image.width*this.matchingImg.scaleX -5;
  this.matchingImg.y = this.borderReviewMachting.graphics.command.y + this.borderReviewMachting.graphics.command.h - this.matchingImg.image.height*this.matchingImg.scaleY -5;


  this.newEmailReviewContainer.addChild(this.borderReviewEmail,this.borderReviewMachting,this.bgReviewEmail,this.bgReviewMatching,this.creationImg,this.matchingImg,this.newEmailReviewEmailTextbox.textbox,this.newEmailReviewMatchingTextbox.textbox);
  this.newEmailReviewContainer.visible = false;

  this.stage.addChild(this.newEmailReviewContainer);
}
