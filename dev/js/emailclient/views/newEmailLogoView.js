
/**
 * View for the filler logo that fills the void while the email body hasn't been chosen (yet)
 * @class
 * @param {number} x The x coordinate where the view starts
 * @param {number} y The y coordinate where the view starts
 * @param {number} width The width of the view
 * @param {number} height The height of the view
 * @param {stage} stage The stage on which the elements of the view are drawn
 */
function newEmailLogoView(x, y, width, height, stage) {
    this.x = x; //position of new email pane
    this.y = y;
    this.width = width;
    this.height = height;
    this.stage = stage;

    //Container
    this.newEmailLogoContainer = new createjs.Container();

    //Letter picture to fill up the space when the new e-mail preview is still empty
    this.letterImg = MTLG.assets.getBitmap(imgPaths['letter']);
    this.letterImg.scaleX = 0.15;
    this.letterImg.scaleY = 0.15;
    this.letterImg.x = this.x + this.width/2  - this.letterImg.image.width*this.letterImg.scaleX/2;
    this.letterImg.y = this.y + this.height/2 - this.letterImg.image.height*this.letterImg.scaleY/2;

    this.newEmailLogoContainer.addChild(this.letterImg);
    this.stage.addChild(this.newEmailLogoContainer);

}
