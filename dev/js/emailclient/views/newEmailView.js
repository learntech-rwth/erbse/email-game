
/**
 * General view for the creation game (and partially for the review game). Also contains the choice options for the creation game
 * @class
 * @param {number} x The x coordinate where the view starts
 * @param {number} y The y coordinate where the view starts
 * @param {number} width The width of the view
 * @param {number} height The height of the view
 * @param {stage} stage The stage on which the elements of the view are drawn
 */
function newEmailView(x, y, width, height, stage) {
    this.x = x; //position of new email pane
    this.y = y;
    this.width = width; //size of new email pane
    this.height = height;
    this.stage = stage;

    //alignment variables
    var padBetweenButtons = 40;
    var padBorderButtons = 40;
    var padEdgeBorderX = 10;
    var padEdgeBorderY = 10;
    var borderWidth = (this.width-3*padEdgeBorderY)/2;
    var borderChoiceButtonsX = this.x + borderWidth + padEdgeBorderX*2;//1090;
    var borderChoiceButtonsY = padEdgeBorderY;
    this.buttonHeight = (this.height-2*padEdgeBorderY-4*padBetweenButtons)/3;
    this.buttonWidth = borderWidth-padBorderButtons*2; //20 from left border and 20 from the left side of right border of the choice buttons +10 from the border being only 10 (not 20) from the side of the screen
    this.gapBetweenContentAndButtons = borderChoiceButtonsX + padBorderButtons - (this.x + borderWidth); //used for click events
    var participantsHeight = 180;

    // this.bg2 = new createjs.Shape(); //create BG of E-Mail creation Pane
    // this.bg2.graphics.beginFill("red").drawRect(this.x,this.y,borderWidth+this.gapBetweenContentAndButtons + this.buttonWidth/2,this.height);

    //Container
    this.newEmailViewContainer = new createjs.Container();
    this.newEmailPreviewViewContainer = new createjs.Container();
    this.creationTutorialContainer = new createjs.Container();

    //BG
    this.bg = new createjs.Shape(); //create BG of E-Mail creation Pane
    this.bg.graphics.beginFill("#f2f2f2").drawRect(this.x,this.y,this.width,this.height);

    if(MTLG.getOptions().debug == true){
      console.log("Reference shape",this.bg);
    }

    this.newEmailViewContainer.addChild(this.bg);
    this.newEmailViewContainer.visible = false;

    //border for the choice buttons
    this.borderChoiceButtons = new createjs.Shape();
    this.borderChoiceButtons.graphics.setStrokeStyle(1,"").beginStroke("black");
    this.borderChoiceButtons.graphics.beginFill("white").drawRect(borderChoiceButtonsX,borderChoiceButtonsY,borderWidth, this.height-padEdgeBorderY*2); //"#f2f2f2" grey

    //border participants
    this.borderParticipants = new createjs.Shape();
    this.borderParticipants.graphics.setStrokeStyle(1,"").beginStroke("black");
    this.borderParticipants.graphics.beginFill("white").drawRect(this.x,borderChoiceButtonsY,borderWidth, participantsHeight);

    //border email content
    this.borderEmailContent = new createjs.Shape();
    this.borderEmailContent.graphics.setStrokeStyle(1,"").beginStroke("black");
    this.borderEmailContent.graphics.beginFill("white").drawRect(this.x,padEdgeBorderY*3+participantsHeight,borderWidth, this.height-participantsHeight-4*padEdgeBorderY); // - 10px from the top - 20px between the two boxes - 10px from the controlbar

    //participants content
    this.newEmailParticipantsView = new newEmailParticipantsView(this.x,padEdgeBorderY,borderWidth,participantsHeight,this.stage,padBetweenButtons-10);

    //new email content
    this.newEmailContentView = new newEmailContentView(this.x,participantsHeight+3*padEdgeBorderY,borderWidth,this.height-4*padEdgeBorderY-participantsHeight,this.stage,padBetweenButtons);

    //new email match minigame
    this.newEmailMatchView = new newEmailMatchView(this.x,this.y+padEdgeBorderY,this.width-padEdgeBorderX,this.height-2*padEdgeBorderY,this.stage,padBetweenButtons);

    //safe choice button positions so we can reset them if need be
    this.choiceOptions = [];
    this.CONSTchoiceOptionsPositions = [];

    //choice Buttons
    this.firstChoiceButton = new Button(borderChoiceButtonsX+padBorderButtons,padEdgeBorderY+padBetweenButtons,this.buttonWidth,this.buttonHeight," ","#4da6ff",this.stage,"","30px Arial","white"); //Courier New
    this.choiceOptions.push(this.firstChoiceButton);
    this.choiceOptions[this.choiceOptions.length-1].visible = false;
    this.CONSTchoiceOptionsPositions.push([this.firstChoiceButton.children[0].x,this.firstChoiceButton.children[0].y,this.firstChoiceButton.id]);
    this.secondChoiceButton = new Button(borderChoiceButtonsX+padBorderButtons,padEdgeBorderY+2*padBetweenButtons+this.buttonHeight,this.buttonWidth,this.buttonHeight," ","#4da6ff",this.stage,"","30px Arial","white");
    this.choiceOptions.push(this.secondChoiceButton);
    this.choiceOptions[this.choiceOptions.length-1].visible = false;
    this.CONSTchoiceOptionsPositions.push([this.secondChoiceButton.children[0].x,this.secondChoiceButton.children[0].y,this.secondChoiceButton.id]);
    this.thirdChoiceButton = new Button(borderChoiceButtonsX+padBorderButtons,padEdgeBorderY+3*padBetweenButtons+this.buttonHeight*2,this.buttonWidth,this.buttonHeight," ","#4da6ff",this.stage,"","30px Arial","white");
    this.choiceOptions.push(this.thirdChoiceButton);
    this.choiceOptions[this.choiceOptions.length-1].visible = false;
    this.CONSTchoiceOptionsPositions.push([this.thirdChoiceButton.children[0].x,this.thirdChoiceButton.children[0].y,this.thirdChoiceButton.id]);

    //new email review view
    this.newEmailReviewView = new newEmailReviewView(borderChoiceButtonsX, padEdgeBorderY,borderWidth,this.height-2*padEdgeBorderY,this.stage,padBetweenButtons);

    //tutorial shapes that indicate where the options go (will only be visible in the first level the chreation game is played in)
    this.creationTutorialShape = new createjs.Shape();
    this.creationTutorialShape.graphics.setStrokeStyle(4,"").beginStroke("#ffae3b");
    this.creationTutorialShape.graphics.beginFill("").drawRect(this.firstChoiceButton.children[0].x-10,this.firstChoiceButton.children[0].y-10,this.firstChoiceButton.children[0].graphics.command.w+20, this.thirdChoiceButton.children[0].graphics.command.h + this.thirdChoiceButton.children[0].y - this.firstChoiceButton.children[0].y +20);

    //tutorial lines
    this.creationTutorialLine = new createjs.Shape();
    this.creationTutorialLine.graphics.setStrokeStyle(4).beginStroke("#ffae3b");
    this.creationTutorialLine.graphics.moveTo(this.newEmailParticipantsView.contentSenderTextbox.textbox.children[0].graphics.command.w + this.newEmailParticipantsView.contentSenderTextbox.textbox.children[0].x - 20, this.newEmailParticipantsView.contentSenderTextbox.textbox.children[0].y + this.newEmailParticipantsView.contentSenderTextbox.textbox.children[0].graphics.command.h/2);
    this.creationTutorialLine.graphics.lineTo(this.creationTutorialShape.graphics.command.x, this.newEmailParticipantsView.contentSenderTextbox.textbox.children[0].y + this.newEmailParticipantsView.contentSenderTextbox.textbox.children[0].graphics.command.h/2);
    this.creationTutorialLine.graphics.endStroke();
    this.creationTutorialContainer.addChild(this.creationTutorialShape,this.creationTutorialLine);
    this.creationTutorialContainer.visible = false;

    //this.choiceButtonsContainer.addChild(this.firstChoiceButton,this.secondChoiceButton,this.thirdChoiceButton);
    this.newEmailViewContainer.addChild(this.borderChoiceButtons, this.borderParticipants, this.borderEmailContent, this.newEmailParticipantsView.newEmailParticipantsLabelContainer,this.newEmailContentView.newEmailContentBgContainer, this.newEmailContentView.newEmailContentContainer, this.newEmailReviewView.newEmailReviewContainer, this.newEmailParticipantsView.newEmailParticipantsContentContainer)//,this.newEmailParticipantsView.newEmailParticipantsHoverContainer);

    this.stage.addChild(this.newEmailViewContainer,this.creationTutorialContainer,this.firstChoiceButton,this.secondChoiceButton,this.thirdChoiceButton);

    /**
     * This function highlights which part of the email the player is currently supposed to alter
     * @function
     * @param {number} activePartIndex The part of the email that the players are currently filling in (sender, subject, first email text, other email text)
     * @param {object} currentPhishingEmail Object of the current phishing email / level (see phishingEmailModel.js)
     */
    this.highlightCurrentChoosingOption = function (activePartIndex, currentPhishingEmail){
      let blue = "#4da6ff";
      let lightBlue = "#AED6FF";
      let white = "white";
      let black = "black";

      if (activePartIndex == 1){ //sender
        setTextboxColors(this.newEmailParticipantsView.contentSenderTextbox,lightBlue,blue);
        setTextboxColors(this.newEmailParticipantsView.contentSubjectTextbox);
        setShapeColors(this.borderEmailContent);
      }
      else if (activePartIndex == 2){ //subject
        setTextboxColors(this.newEmailParticipantsView.contentSenderTextbox);
        setTextboxColors(this.newEmailParticipantsView.contentSubjectTextbox,lightBlue,blue);
      }
      else if (activePartIndex == 3){ //first email text
        setTextboxColors(this.newEmailParticipantsView.contentSubjectTextbox);
        setShapeColors(this.borderEmailContent,lightBlue,blue);
      }
      else if (activePartIndex < Object.keys(currentPhishingEmail).length){ //email text
        setShapeColors(this.borderEmailContent,white,blue);
      }
      else{ //email is complete
        setTextboxColors(this.newEmailParticipantsView.contentSenderTextbox);
        setTextboxColors(this.newEmailParticipantsView.contentSubjectTextbox);
        setShapeColors(this.borderEmailContent);
      }
    }.bind(this);

    /**
     * This helper function sets the color of a textbox
     * @function
     * @param {textbox} box The textbox of which we want to change the background and border
     * @param {string} colorBg The color the background of the textbox is supposed to have
     * @param {string} colorBorder The color the border of the textbox is supposed to have
     */
    function setTextboxColors(box,colorBg="white",colorBorder="black"){
      box.bg.graphics._stroke.style = colorBorder;
      box.bg.bgColor.style = colorBg;
    }

    /**
     * This helper function sets the color of a shape
     * @function
     * @param {shape} shape The shape of which we want to change the background and border
     * @param {string} colorBg The color the background of the shape is supposed to have
     * @param {string} colorBorder The color the border of the shape is supposed to have
     */
    function setShapeColors(shape,colorBg="white",colorBorder="black"){
      shape.graphics._stroke.style = colorBorder;
      shape.graphics._fill.style = colorBg;
    }
}
