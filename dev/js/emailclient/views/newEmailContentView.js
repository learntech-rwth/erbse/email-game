
/**
 * View for the email body of the creation game
 * @class
 * @param {number} x The x coordinate where the view starts
 * @param {number} y The y coordinate where the view starts
 * @param {number} width The width of the view
 * @param {number} height The height of the view
 * @param {stage} stage The stage on which the elements of the view are drawn
 * @param {number} padding The padding for the emailTextboxes (not used in this class directly but by addTextboxWithEmailContent() in creationGame.js)
 */
function newEmailContentView(x, y, width, height, stage, padding) {
    this.x = x; //position of new email pane
    this.y = y;
    this.width = width;
    this.height = height;
    this.stage = stage;
    this.padding = padding;

    //Container
    this.newEmailContentContainer = new createjs.Container();
    this.newEmailContentBgContainer = new createjs.Container();

    //this.bg = new createjs.Shape(); //create BG of E-Mail creation Pane
    //this.bg.graphics.beginFill("white").drawRect(this.x,this.y,this.width,this.height);
    //this.newEmailContentBgContainer.addChild(this.bg);

    //get glyph size
    var textSizeTextbox = new createjs.Text("I", "30px Arial");
    this.glyphHeight = textSizeTextbox.getBounds().height;
    this.lineMultiplicator = 1.25;
    this.emailTextboxes = [];

    //textbox for URL depiction
    let padURLBox = 5;
    this.newEmailURLTextbox = new Textbox({
      x: this.x + padURLBox,
      y: this.y + this.height,
      text: "URL BOX OX OXq",
      fixwidth: this.width - 2*padURLBox,
      font: "30px Arial",
      colorBg: "white",
      lineHeight: this.glyphHeight*this.lineMultiplicator,
      stroke: "black",
      pad: padURLBox,
    });
    this.newEmailURLTextbox.textbox.visible = false;
    this.newEmailURLTextbox.textbox.y -= this.newEmailURLTextbox.bg.height - padURLBox;

    //attachment image
    this.attachmentImg = MTLG.assets.getBitmap(imgPaths['attach']);
    this.attachmentImg.scaleX = 0.10;
    this.attachmentImg.scaleY = 0.10;
    this.attachmentImg.x = this.x + this.width - this.attachmentImg.image.width*this.attachmentImg.scaleX - 10;
    this.attachmentImg.y = this.y + 10;
    this.attachmentImg.visible = false;

    //Letter logo to show in case there is nothing in the textbox yet
    this.newLogoView = new newEmailLogoView(this.x, this.y, this.width, this.height, this.stage);

    this.newEmailContentContainer.addChild(this.newLogoView.newEmailLogoContainer,this.newEmailURLTextbox.textbox,this.attachmentImg); //this.newEmailContentTextbox.textbox

    this.stage.addChild(this.newEmailContentContainer);
}
