
/**
 * General view for the matching game that contains the options / buttons
 * @class
 * @param {number} x The x coordinate where the view starts
 * @param {number} y The y coordinate where the view starts
 * @param {number} width The width of the view
 * @param {number} height The height of the view
 * @param {stage} stage The stage on which the elements of the view are drawn
 */
function newEmailMatchView(x, y, width, height, stage) {
    this.x = x; //position of new email pane
    this.y = y;
    this.width = width; //size of new email pane
    this.height = height;
    this.stage = stage;

    //alignment variables
    var padBetweenButtons = 40;
    var padBorderButtons = 40;
    var padEdgeBorderX = 10;
    var padEdgeBorderY = 10;
    this.numberButtons = 4;
    this.buttonHeight = (this.height-(this.numberButtons-1)*padBetweenButtons-2*padBorderButtons)/this.numberButtons;
    this.buttonWidth = 400;

    //Container
    this.newEmailMatchViewContainer = new createjs.Container();
    this.matchingTutorialContainer = new createjs.Container();

    //BG
    this.bg = new createjs.Shape(); //create BG of E-Mail creation Pane
    this.bg.graphics.beginFill("#f2f2f2").drawRect(this.x,this.y,this.width,this.height);
    this.newEmailMatchViewContainer.addChild(this.bg);
    this.newEmailMatchViewContainer.visible = false;

    //border
    this.border = new createjs.Shape();
    this.border.graphics.setStrokeStyle(1,"").beginStroke("black");
    this.border.graphics.beginFill("white").drawRect(this.x,this.y,this.width, this.height); //#f2f2f2
    this.newEmailMatchViewContainer.addChild(this.border);

    var layout = [[150,padBorderButtons],
                  [50,padBorderButtons+this.buttonHeight+padBetweenButtons],
                  [50,padBorderButtons+this.buttonHeight*2+padBetweenButtons*2],
                  [150,padBorderButtons+this.buttonHeight*3+padBetweenButtons*3],
                  ];

    //safe matching button positions so we can reset them if need be
    this.CONSTmatchTextOptionsPositions = [];
    this.CONSTmatchFeatureOptionsPositions = [];

    //text passage options
    this.textOptions = [];
    for (let i = 0; i < this.numberButtons; i++){
      this.textOptions.push(new Button(this.x+layout[i][0],this.y+layout[i][1],this.buttonWidth,this.buttonHeight," ","#4da6ff",this.stage,"","24px Arial","white"));//this.x+(1+i)*padEdgeBorderX,this.y+(1+i)*padEdgeBorderY,this.buttonWidth,this.buttonHeight,label[i],"#4da6ff",this.stage,"","30px Arial","white"));
      this.textOptions[i].visible = false;
      this.CONSTmatchTextOptionsPositions.push([this.textOptions[this.textOptions.length-1].children[0].x,this.textOptions[this.textOptions.length-1].children[0].y,this.textOptions[this.textOptions.length-1].id]); //safe original positions for realignment
    }

    //feature options
    this.featureOptions = [];
    for (let i = 0; i < this.numberButtons; i++){
      this.featureOptions.push(new Button(this.x+this.width-layout[i][0]-this.buttonWidth,this.y+layout[i][1],this.buttonWidth,this.buttonHeight," ","#4da6ff",this.stage,"","24px Arial","white"));//this.x+(1+i)*padEdgeBorderX+500,this.y+(1+i)*padEdgeBorderY,this.buttonWidth,this.buttonHeight,"to be implemented","#4da6ff",this.stage,"","30px Arial","white"));
      this.featureOptions[i].visible = false;
      this.CONSTmatchFeatureOptionsPositions.push([this.featureOptions[this.featureOptions.length-1].children[0].x,this.featureOptions[this.featureOptions.length-1].children[0].y,this.featureOptions[this.featureOptions.length-1].id]); //safe original positions for realignment
    }

    //logo view where the options should be combined
    this.newEmailMatchLogoView = new newEmailMatchLogoView(this.x+layout[0][0]+this.buttonWidth,this.y+layout[0][1]+this.buttonHeight,this.width-layout[0][0]*2-this.buttonWidth*2,this.buttonHeight*2+3*padBetweenButtons,this.stage);

    //live view
    this.newEmailMatchLiveView = new newEmailMatchLiveView(this.x - 260,this.height - 260,260,260,this.stage);

    //tutorial shapes that indicate where the options have to go (will only be visible in the first level the matching game is played in)
    this.matchingTutorialShapeTexts = new createjs.Shape();
    this.matchingTutorialShapeTexts.graphics.setStrokeStyle(4,"").beginStroke("#ffae3b");
    this.matchingTutorialShapeTexts.graphics.beginFill("").drawRect(this.textOptions[1].children[0].x-10,this.textOptions[0].children[0].y-10,this.textOptions[0].children[0].x+this.textOptions[0].children[0].graphics.command.w+20-this.textOptions[1].children[0].x, this.textOptions[3].children[0].y+this.textOptions[3].children[0].graphics.command.h+20-this.textOptions[0].children[0].y);
    this.matchingTutorialShapeFeatures = new createjs.Shape();
    this.matchingTutorialShapeFeatures.graphics.setStrokeStyle(4,"").beginStroke("#ffae3b");
    this.matchingTutorialShapeFeatures.graphics.beginFill("").drawRect(this.featureOptions[0].children[0].x-10,this.featureOptions[0].children[0].y-10,this.featureOptions[1].children[0].x+this.featureOptions[1].children[0].graphics.command.w+20-this.featureOptions[0].children[0].x, this.featureOptions[3].children[0].y+this.featureOptions[3].children[0].graphics.command.h+20-this.featureOptions[0].children[0].y);

    //tutorial lines
    this.matchingTutorialLineTexts = new createjs.Shape();
    this.matchingTutorialLineTexts.graphics.setStrokeStyle(4).beginStroke("#ffae3b");
    this.matchingTutorialLineTexts.graphics.moveTo(this.newEmailMatchLogoView.letterImg.x - 10, this.newEmailMatchLogoView.letterImg.y + this.newEmailMatchLogoView.letterImg.scaleY * this.newEmailMatchLogoView.letterImg.image.height / 2);
    this.matchingTutorialLineTexts.graphics.lineTo(this.matchingTutorialShapeTexts.graphics.command.x + this.matchingTutorialShapeTexts.graphics.command.w, this.newEmailMatchLogoView.letterImg.y + this.newEmailMatchLogoView.letterImg.scaleY * this.newEmailMatchLogoView.letterImg.image.height / 2);
    this.matchingTutorialLineTexts.graphics.endStroke();
    this.matchingTutorialLineFeatures = new createjs.Shape();
    this.matchingTutorialLineFeatures.graphics.setStrokeStyle(4).beginStroke("#ffae3b");
    this.matchingTutorialLineFeatures.graphics.moveTo(this.newEmailMatchLogoView.questionImg.x + 10 + this.newEmailMatchLogoView.questionImg.scaleX * this.newEmailMatchLogoView.questionImg.image.width, this.newEmailMatchLogoView.questionImg.y + this.newEmailMatchLogoView.questionImg.scaleY * this.newEmailMatchLogoView.questionImg.image.height / 2);
    this.matchingTutorialLineFeatures.graphics.lineTo(this.matchingTutorialShapeFeatures.graphics.command.x, this.newEmailMatchLogoView.questionImg.y + this.newEmailMatchLogoView.questionImg.scaleY * this.newEmailMatchLogoView.questionImg.image.height / 2);
    this.matchingTutorialLineFeatures.graphics.endStroke();

    this.matchingTutorialContainer.addChild(this.matchingTutorialShapeTexts,this.matchingTutorialShapeFeatures,this.matchingTutorialLineTexts,this.matchingTutorialLineFeatures);
    this.matchingTutorialContainer.visible = false;

    this.stage.addChild(this.newEmailMatchViewContainer,this.newEmailMatchLogoView.newEmailMatchLogoContainer,this.newEmailMatchLiveView.livesCaption.textbox,this.newEmailMatchLiveView.newEmailMatchLiveContainer,this.matchingTutorialContainer,this.textOptions[0],this.textOptions[1],this.textOptions[2],this.textOptions[3],this.featureOptions[0],this.featureOptions[1],this.featureOptions[2],this.featureOptions[3]);

    //make the matching game buttons either visible of invisible
    this.setMatchingButtonsVisibility = function (status){
      this.textOptions.forEach((item, i) => {
        item.visible = status;
      });
      this.featureOptions.forEach((item, i) => {
        item.visible = status;
      });
    }.bind(this);


}
