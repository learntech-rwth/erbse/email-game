
/**
 * View for the center logo of the matching game
 * @class
 * @param {number} x The x coordinate where the view starts
 * @param {number} y The y coordinate where the view starts
 * @param {number} width The width of the view
 * @param {number} height The height of the view
 * @param {stage} stage The stage on which the elements of the view are drawn
 */
function newEmailMatchLogoView(x, y, width, height, stage) {
    this.x = x; //position of new email pane
    this.y = y;
    this.width = width;
    this.height = height;
    this.stage = stage;

    //Container
    this.newEmailMatchLogoContainer = new createjs.Container();

    //background to test alignments, invisible during the game
    this.bg = new createjs.Shape(); //create BG of E-Mail creation Pane
    this.bg.graphics.beginFill("red").drawRect(this.x,this.y,this.width,this.height);

    //Letter picture to indicate where the email part has to go when the part hasn't been selected yet
    this.letterImg = MTLG.assets.getBitmap(imgPaths['letter']);
    this.letterImg.scaleX = 0.14;
    this.letterImg.scaleY = 0.14;
    this.letterImg.x = this.x + this.width/2  - this.letterImg.image.width*this.letterImg.scaleX/2;
    this.letterImg.y = this.y //- this.letterImg.image.height*this.letterImg.scaleY/2;

    //questionmark picture to indicate where the phishing feature has to go when the feature hasn't been selected yet
    this.questionImg = MTLG.assets.getBitmap(imgPaths['question']);
    this.questionImg.scaleX = 0.28;
    this.questionImg.scaleY = 0.28;
    this.questionImg.x = this.x + this.width/2  - this.questionImg.image.width*this.questionImg.scaleX/2;
    this.questionImg.y = this.y + this.height - this.questionImg.image.height*this.questionImg.scaleY;

    //create binar phishing textbox
    var testTextBox = new createjs.Text("I", "bold 24px Courier", "grey");
    this.newEmailMatchBinaryTextbox = new createjs.Text("01010000 01101000 01101001 01110011\n01101000 01101001 01101110 01100111", "bold 24px Courier", "grey"); //binary depiction of the word phishing
    this.newEmailMatchBinaryTextbox.textAlign = "center";
    this.newEmailMatchBinaryTextbox.textBaseline = 'hanging';
    this.newEmailMatchBinaryTextbox.x = this.letterImg.x + this.letterImg.image.width*this.letterImg.scaleX/2;
    this.newEmailMatchBinaryTextbox.y = this.letterImg.y + this.letterImg.image.height*this.letterImg.scaleY + (this.questionImg.y-(this.letterImg.image.height*this.letterImg.scaleY+this.letterImg.y))/2 - testTextBox.getBounds().height;

    //connecting lines between the 2 logos
    this.newEmailMatchBinaryLine1 = new createjs.Shape();
    this.newEmailMatchBinaryLine1.graphics.setStrokeStyle(4).beginStroke("grey");
    this.newEmailMatchBinaryLine1.graphics.moveTo(this.newEmailMatchBinaryTextbox.x, this.letterImg.y+this.letterImg.image.height*this.letterImg.scaleY);
    this.newEmailMatchBinaryLine1.graphics.lineTo(this.newEmailMatchBinaryTextbox.x, this.newEmailMatchBinaryTextbox.y);
    this.newEmailMatchBinaryLine1.graphics.endStroke();
    this.newEmailMatchBinaryLine2 = new createjs.Shape();
    this.newEmailMatchBinaryLine2.graphics.setStrokeStyle(4).beginStroke("grey");
    this.newEmailMatchBinaryLine2.graphics.moveTo(this.newEmailMatchBinaryTextbox.x, this.newEmailMatchBinaryTextbox.y + testTextBox.getBounds().height*2);
    this.newEmailMatchBinaryLine2.graphics.lineTo(this.newEmailMatchBinaryTextbox.x, this.questionImg.y);
    this.newEmailMatchBinaryLine2.graphics.endStroke();


    this.newEmailMatchLogoContainer.addChild(this.newEmailMatchBinaryLine1,this.newEmailMatchBinaryLine2,this.letterImg,this.questionImg,this.newEmailMatchBinaryTextbox); //this.bg,
    this.newEmailMatchLogoContainer.visible = false;
    //this.stage.addChild(this.newEmailMatchLogoContainer);

}
