
/**
 * View for the lives of the matching game
 * @class
 * @param {number} x The x coordinate where the view starts
 * @param {number} y The y coordinate where the view starts
 * @param {number} width The width of the view
 * @param {number} height The height of the view
 * @param {stage} stage The stage on which the elements of the view are drawn
 */
function newEmailMatchLiveView(x, y, width, height, stage) {
    this.x = x; //position of new email pane
    this.y = y;
    this.width = width;
    this.height = height;
    this.stage = stage;

    //Container
    this.newEmailMatchLiveContainer = new createjs.Container();

    //create live caption
    this.livesCaption = new Textbox({
          x: this.x+10,
          y: this.y+200,
          text: l("lives"),
          font: "35px Arial",
          colorBg: null,
    });
    this.livesCaption.textbox.visible = false;

    //create live icons
    this.lives = [];
    var gapMinimizer = 80;

    //left live
    this.lives.push(MTLG.assets.getBitmap(imgPaths['live']));
    this.lives[this.lives.length-1].scaleX = 0.06;
    this.lives[this.lives.length-1].scaleY = 0.06;
    //this.lives[this.lives.length-1].x = this.x + this.width/2  - this.lives[this.lives.length-1].image.width*this.lives[this.lives.length-1].scaleX + gapMinimizer;
    this.lives[this.lives.length-1].x = this.livesCaption.txt.x + 10 + this.livesCaption.textbox.children[1].getMeasuredWidth();
    //this.lives[this.lives.length-1].y = this.y + this.height - this.lives[this.lives.length-1].image.height*this.lives[this.lives.length-1].scaleY;//- this.letterImg.image.height*this.letterImg.scaleY/2;
    this.lives[this.lives.length-1].y = this.livesCaption.txt.y - 5;
    this.lives[this.lives.length-1].visible = false;
    this.newEmailMatchLiveContainer.addChild(this.lives[this.lives.length-1]);

    //right live
    this.lives.push(MTLG.assets.getBitmap(imgPaths['live']));
    this.lives[this.lives.length-1].scaleX = 0.06;
    this.lives[this.lives.length-1].scaleY = 0.06;
    //this.lives[this.lives.length-1].x = this.x + this.width/2 - gapMinimizer;
    this.lives[this.lives.length-1].x = this.lives[this.lives.length-2].x + this.lives[this.lives.length-2].image.width * this.lives[this.lives.length-2].scaleX + 10;
    //this.lives[this.lives.length-1].y = this.y + this.height - this.lives[this.lives.length-1].image.height*this.lives[this.lives.length-1].scaleY;//- this.letterImg.image.height*this.letterImg.scaleY/2;
    this.lives[this.lives.length-1].y = this.lives[this.lives.length-2].y;
    this.lives[this.lives.length-1].visible = false;
    this.newEmailMatchLiveContainer.addChild(this.lives[this.lives.length-1]);

}
