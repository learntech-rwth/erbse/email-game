/**
 * View for Preview Pane
 */
function emailView(x, y, width, height, stage, email, header) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.stage = stage;
    this.email = email;
    this.header = header;

    this.targets = []; //contains suspicous targets
    this.wasSeen = false;  //true email was seen already by the player

    var headerHeight = 280;
    var gap = 10;
    var previewHeight = this.height - headerHeight - gap; //660

    this.emailContainer = new createjs.Container();

    this.headerView = new headerView(this.x, this.y, this.width, headerHeight, this.stage, this.email, this.targets, this.header);
    this.previewPaneView = new previewPaneView(this.x, this.y + headerHeight + gap, this.width, previewHeight, this.stage, this.email, this.targets);

    this.emailContainer.addChild(this.previewPaneView.previewContainer, this.headerView._headerContainer);
    this.emailContainer.visible = false; //visible after player clicked on corresponding messagebox
    this.stage.addChild(this.emailContainer);

    this.remove = function(){
        this.stage.removeChild(this.emailContainer);
    }

    // console.log(this.targets);
}