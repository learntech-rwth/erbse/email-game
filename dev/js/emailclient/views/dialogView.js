
/**
 * View for the dialog textbox in the controlPaneView
 * @class
 * @param {number} x The x coordinate were the view starts
 * @param {number} y The y coordinate were the view starts
 * @param {number} width The width of the view
 * @param {number} height The height of the view
 * @param {stage} stage The stage on which the elements of the view are drawn
 */
function dialogView(x, y, width, height, stage) {
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.stage = stage;

  var font = "30px Arial"

  //get height of dialogBox's text
  var testTextBox = new createjs.Text("I", font, "grey");

  //create textbox to communicate with the players
  this.dialogBox = new createjs.Text(" ", font, "white");
  this.dialogBox.x = this.x;
  this.dialogBox.y = this.y+this.height/2-testTextBox.getMeasuredLineHeight()/2 + 2;
  this.dialogBox.lineWidth = this.width;
  this.dialogBox.lineHeight = testTextBox.getMeasuredLineHeight()*1.25;
  // this.dialogBox = new Textbox({
  //   text: " ", //if we don't insert anything here the text won't be created
  //   x: this.x,
  //   y: this.y+this.height/2-testTextBox.getBounds().height/2 + 2,
  //   font: font,
  //   colorBg: "", //set to red if you want to test if the empty textbox is visible
  //   colorTxt: "white",
  //   fixwidth: this.width,
  // });


  this.dialogBoxContainer = new createjs.Container();
  this.dialogBoxContainer.addChild(this.dialogBox);
  this.dialogBoxContainer.visible = false;
  this.stage.addChild(this.dialogBoxContainer);

  /**
   * function that aligns (vertically centered in the controlPaneView) the dialog textbox according to its lines
   * @function
   */
  this.fixDialogBoxHeight = function(){
    this.dialogBox.y = this.y + this.height/2 - this.dialogBox.getBounds().height/2 + (this.dialogBox.lineHeight - testTextBox.getMeasuredLineHeight())/2 + 2;
  }.bind(this);

}
