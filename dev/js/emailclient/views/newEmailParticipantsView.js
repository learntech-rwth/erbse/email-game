
/**
 * View for the email creation game that contains the paricipants such as sender
 * @class
 * @param {number} x The x coordinate where the view starts
 * @param {number} y The y coordinate where the view starts
 * @param {number} width The width of the view
 * @param {number} height The height of the view
 * @param {stage} stage The stage on which the elements of the view are drawn
 * @param {number} padding The padding for some UI elements
 */
function newEmailParticipantsView(x, y, width, height, stage, padding) {
    this.x = x; //position of new email pane
    this.y = y;
    this.width = width;
    this.height = height;
    this.stage = stage;
    this.padding = padding;

    this.font = "30px Arial";
    var color = "black";
    var paddingBorderX = 180;
    this.paddingLabelContent = 20;
    var paddingLabels = 50;

    //this.bg = new createjs.Shape(); //create BG of E-Mail creation Pane
    //this.bg.graphics.beginFill("white").drawRect(this.x+2,this.y+2,this.width-2,this.height-2);

    //Container
    this.newEmailParticipantsLabelContainer = new createjs.Container();
    this.newEmailParticipantsContentContainer = new createjs.Container();
    //this.newEmailParticipantsHoverContainer = new createjs.Container();

    //Subject-textboxes for sender, receiver and email subject
    this.labelReceiverTextbox = new createjs.Text(l('receiver')+":",this.font,color);
    this.labelReceiverTextbox.textAlign = "right";
    this.labelReceiverTextbox.x = this.x + paddingBorderX;
    this.labelReceiverTextbox.y = this.y + this.padding;
    this.labelSenderTextbox = new createjs.Text(l('sender')+":",this.font,color);
    this.labelSenderTextbox.textAlign = "right";
    this.labelSenderTextbox.x = this.x + paddingBorderX;
    this.labelSenderTextbox.y = this.labelReceiverTextbox.y + paddingLabels;
    this.labelSubjectTextbox = new createjs.Text(l('subject')+":",this.font,color);
    this.labelSubjectTextbox.textAlign = "right";
    this.labelSubjectTextbox.x = this.x + paddingBorderX;
    this.labelSubjectTextbox.y = this.labelSenderTextbox.y + paddingLabels;

    this.contentReceiverTextbox = new Textbox({
      text: " ",
      x: this.labelReceiverTextbox.x + this.paddingLabelContent,
      y: this.labelReceiverTextbox.y,
      fixwidth: this.width - (this.labelReceiverTextbox.x + this.paddingLabelContent*2 - this.x),
      font: this.font,
      colorBg: "white",
      pad: 5,
      stroke: "black",
    });

    this.contentSenderTextbox = new Textbox({
      text: " ",
      x: this.labelSenderTextbox.x + this.paddingLabelContent,
      y: this.labelSenderTextbox.y,
      fixwidth: this.width - (this.labelSenderTextbox.x + this.paddingLabelContent*2 - this.x),
      font: this.font,
      colorBg: "white",
      pad: 5,
      stroke: "black",
    });

    this.contentSubjectTextbox = new Textbox({
      text: " ",
      x: this.labelSubjectTextbox.x + this.paddingLabelContent,
      y: this.labelSubjectTextbox.y,
      fixwidth: this.width - (this.labelSubjectTextbox.x + this.paddingLabelContent*2 - this.x),
      font: this.font,
      colorBg: "white",
      pad: 5,
      stroke: "black",
    });

    if(MTLG.getOptions().debug == true){
      console.log("Reference textbox",this.contentReceiverTextbox);
    }

    this.newEmailParticipantsLabelContainer.addChild(this.labelReceiverTextbox,this.labelSenderTextbox,this.labelSubjectTextbox);//,
    this.newEmailParticipantsContentContainer.addChild(this.contentReceiverTextbox.textbox,this.contentSenderTextbox.textbox,this.contentSubjectTextbox.textbox);

    this.stage.addChild(this.newEmailParticipantsLabelContainer,this.newEmailParticipantsContentContainer);
}
