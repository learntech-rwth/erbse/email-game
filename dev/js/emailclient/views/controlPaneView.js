/* View for control Pane */

function controlPaneView(x, y, width, height, stage) {
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.stage = stage;

  let xPos = 670; //xPos of preview
  var continueX = this.width - 180; //x Position of help and Continue Button
  var legitX = xPos;  //x Position of legitimate button
  var phishX = legitX + (170 + 10); //x Position of phishing button
  var helpX = phishX + (170 + 70);
  var infoX = helpX + (170 + 20); //legitX - 100; //x Position of info 'i' button

  this.bg = new createjs.Shape(); //BG of control pane
  this.bg.graphics.beginFill("#454545").drawRect(this.x, this.y, this.width, this.height);

  this.stage.addChild(this.bg);

  this.phishButton = new Button(phishX, this.y + 5, 170, 100, "", "#ffae3b", this.stage, imgPaths['phishing']);//"img/phishing.png"); //#fe53bb
  this.legitButton = new Button(legitX, this.y + 5, 170, 100, "", "#4da6ff", this.stage, imgPaths['legit']);//"img/legit.png"); //#09fbd3
  this.continueButton = new Button(continueX, this.y + 5, 170, 100, "", "#4da6ff", this.stage, imgPaths['continue']);//"img/continue-arrow.png"); //#00FFFF
  this.inactiveContinueButton = new Button(continueX, this.y + 5, 170, 100, "", "grey", this.stage, imgPaths['continue']);

  this.continueButton.visible = false; //visible after classification of an email
  this.inactiveContinueButton.visible = false; //visible when user can't continue because there has been no mouseover over all passages

  //this.stage.addChild(this.phishButton, this.legitButton, this.continueButton);
  this.stage.addChild(this.continueButton, this.inactiveContinueButton);


  this.printContainer = new createjs.Container(); //contains stamp prints
  this.stage.addChild(this.printContainer);
  this.correctIconContainer = new createjs.Container(); //contains correct/incorrect icon


  //let xPos = 670; //xPos of preview
  this.verifyButton = new Button(xPos, this.y + 30, 170, 50, 'verify', "white", this.stage);
  this.verifyButton.visible = false;
  this.stage.addChild(this.verifyButton);

  this.stamp = MTLG.assets.getBitmap(imgPaths['stamp']);//'img/stamp.png');
  this.stamp.scaleX = 0.3;
  this.stamp.scaleY = 0.3;
  this.stamp.x = xPos + 170 + 10; //10px right from verify button
  this.stamp.y = this.y + 10;
  this.stamp.alpha = 0.8; //make a bit transparent so that player can see what passage is under the stamp

  this.stamp.imgW = this.stamp.image.width * 0.3;
  this.stamp.imgH = this.stamp.image.height * 0.3;

  this.stamp.visible = false;
  this.stage.addChild(this.stamp);

  this.points = new Textbox({//View for displaying the number of correct classified passages in realtion to existing suspicious passages
    text: "points",
    x: xPos + 5, //+5 because of padding
    y: this.y + 35,
    pad: 5,
    font: "40px Arial",
  });
  this.points.textbox.visible = false;
  this.stage.addChild(this.points.textbox);

  this.helpModalView = new helpModalView(0, 0, 661, 950, helpX, this.y + 5, this.stage); //help button and help Pop-Up
  this.infoBox = new infoBoxView(infoX, this.y + 30, legitX, phishX, helpX, 800, this.stage); //info Button and three info boxes

  this.task = new createjs.Container(); //contains hacker img with task 'is the E-Mail legitimate?'

  /*this.avatarImg = MTLG.assets.getBitmap(imgPaths['avatar1']);
  this.avatarImg.x = 5;
  this.avatarImg.y = this.y+10;
  this.avatarImg.scaleX = 0.25;
  this.avatarImg.scaleY = 0.25;*/
  this.avatarImg = new Button(5, this.y+5, 100, 100, "", "", this.stage, imgPaths['avatar1'], "", "", [], 0.25, 0.25);

  var question = new createjs.Text(l('questionTask'), '50px Arial', 'white');
  question.x = this.avatarImg.x + 140;
  question.y = this.y + 30;

  this.task.addChild(question);

  console.log(this.avatarImg.x);
  console.log(this.avatarImg.width);
  this.dialogView = new dialogView(this.x + this.avatarImg.x + 110 + 30, this.y, this.width-20-(this.avatarImg.x + 110 + 30), this.height, this.stage); //create dialog textbox to communicate with users

  this.classification = new createjs.Container(); //contains buttons needed for classication
  this.classification.addChild(this.legitButton, this.phishButton, this.helpModalView.helpButtonContainer, this.infoBox.info, this.task);
  this.stage.addChild(this.classification, this.avatarImg);
}
