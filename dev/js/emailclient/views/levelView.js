
/**
 * View that contains the level selection for tutorials and gameplay levels
 * @class
 * @param {number} x The x coordinate where the view starts
 * @param {number} y The y coordinate where the view starts
 * @param {number} width The width of the view
 * @param {number} height The height of the view
 * @param {stage} stage The stage on which the elements of the view are drawn
 */
function levelView(x, y, width, height, stage) {
    this.x = x; //position of level pane
    this.y = y;
    this.width = width; //size of level pane
    this.height = height;
    this.stage = stage;

    //alignment variables
    //var numberOfLevels = 25;
    var levelsPerRow = 6;//Math.sqrt(numberOfLevels);
    var levelsPerCol = 5;//Math.sqrt(numberOfLevels);
    var padEdgeBorderX = 20;
    var padEdgeBorderY = 10;
    var padBetweenButtonsX = 20;
    var padBetweenButtonsY = 20;
    var buttonHeight = (this.height-2*padEdgeBorderY-(levelsPerCol-1)*padBetweenButtonsY)/levelsPerCol;
    var buttonWidth = (this.width-2*padEdgeBorderX-(levelsPerRow-1)*padBetweenButtonsX)/levelsPerRow;


    //Container
    this.levelViewContainer = new createjs.Container();

    //BG
    this.bg = new createjs.Shape(); //create BG of E-Mail creation Pane
    this.bg.graphics.beginFill("#f2f2f2").drawRect(this.x,this.y,this.width,this.height);

    this.levelViewContainer.addChild(this.bg);
    this.levelViewContainer.visible = false;

    //level Buttons
    this.levelButtons = [];
    var numberButton = 0;
    for (let i = 0; i < levelsPerCol; i++){
      for (let j = 0; j < levelsPerRow; j++){
        let infos = findCorrespondingPhishingEmail(numberButton); //get the name of level that corresponds with the button id and if it's a tutorial or a creation level
        let icon = infos[1] == "tutorial"? imgPaths['book']:imgPaths['controller']; //get the correct icon for the level
        this.levelButtons.push(new Button(this.x + j*buttonWidth+padEdgeBorderX+j*padBetweenButtonsX,this.y + i*buttonHeight+padEdgeBorderY+i*padBetweenButtonsY,buttonWidth,buttonHeight,infos[0],"#4da6ff",this.stage,"","30px Arial","white",[[icon,true,0.2,0.2,0.6],"",[imgPaths['signed'],false,0.15,0.3,0.9],""]));
        this.levelButtons[this.levelButtons.length-1].children[0].alpha = 0.5;
        this.levelViewContainer.addChild(this.levelButtons[this.levelButtons.length-1]);
        numberButton++;
      }
    }

    //create newEmailPaneView for the levels
    this.newEmailPaneView = new newEmailView(260,0,1920-260,970,this.stage);

    //create tutorialView
    this.tutorialView = new tutorialView(260,0,1920-260,970,this.stage);

    this.stage.addChild(this.levelViewContainer,this.newEmailPaneView.newEmailViewContainer,this.tutorialView.tutorialViewContainer);

    /**
     * This function is used to find the phishing level whose id fits to the id of the level button
     * @function
     * @param {number} id The ID of the current level we want to get from the list of phishing emails (phishingEmailModel.js)
     */
    function findCorrespondingPhishingEmail(id){
      for (let i = 0; i < phishingLevels.length; i++){
        if (phishingLevels[i].creation == undefined){ //tutorial
          if (phishingLevels[i].id == id){
            return [l(phishingLevels[i].name),"tutorial"]; //l('button-tutorial')+"\n"+
          }
        }
        else{ //creation game
          if (id == phishingLevels[i].creation.infos.id){
            return [l(phishingLevels[i].creation.infos.name),"creation"]; //l('button-creation')+"\n"+
          }
      }
    }
    return "TODO";
  }
}
