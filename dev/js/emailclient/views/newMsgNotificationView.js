// View for "New Message" Pop Up

function newMsgNotificationView(stage) {
  this.stage = stage;

  this.box = new Textbox({
    text: l('newMsg'),
    x: MTLG.getOptions().width + 15, //currently not visible as it's to far right
    y: 15,
    colorTxt: "blue",
    stroke: "blue",
    pad: 5,
    font: "45px Arial",
  });

  this.stage.addChild(this.box.textbox);
}