// View for "This Function is not supported" Pop Up

function notSupportedNotification(stage, clickObject, rightaligned=false) {
    this.stage = stage;
    this.clickObject = clickObject; //the display object that leads to this notification when clicked
    this.rightaligned = rightaligned;

    //notification, that this function is not supported
    var notsupported = new Textbox({
        text: l("notSupported"),
        fixwidth: 180,
        font: "35px Arial",
        stroke: "black",
        pad: 5,
        rightaligned: this.rightaligned,
    });
    notsupported.fixHeight();
    notsupported.textbox.visible = false;
    this.stage.addChild(notsupported.textbox);

    var xPos = (this.rightaligned)? -20 : 20; //needed for positioning of notification box

    //Click on display Objects leads to display of 'this function is not supported'
    this.clickObject.on('click', function (evt) {

        this.stage.setChildIndex(notsupported.textbox, this.stage.numChildren - 1);
        var pt = this.stage.globalToLocal(evt.stageX, evt.stageY);
        createjs.Tween.get(notsupported.textbox, { override: true })
            .to({ visible: true, x: pt.x + xPos, y: pt.y }) //notification will appear near to click position
            .wait(1500) //wait 1,5 seconds before pop up disappears again
            .to({ visible: false, x: 0, y: 0 }); //again invisible

    }.bind(this));
}