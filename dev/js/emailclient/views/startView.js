/* Start Screen */

function startView(stage) {
    this.stage = stage;

    this.container = new createjs.Container();

    //BG
    this.bg = new createjs.Shape();
    this.bg.graphics.beginFill("#D9D9D9").drawRect(0, 0, 1920, 1080);

    this.container.addChild(this.bg);
    
    //image of a hacker
    this.hackerImg = MTLG.assets.getBitmap(imgPaths['avatar1']);//hacker
    this.hackerImg.x = 1200;
    this.hackerImg.y = 550;
    this.hackerImg.scaleX = 1;//0.5;
    this.hackerImg.scaleY = 1;//0.5;

    this.container.addChild(this.hackerImg);

    this.alignX = 200;
    this.alignY = 10;

    //speech bubble
    this.speech = MTLG.assets.getBitmap(imgPaths['speech']);//'img/speech-bubble.png');
    this.speech.x = 400;
    this.speech.y = 30;
    this.speech.scaleX = 0.80;
    this.speech.scaleY = 0.6;

    this.container.addChild(this.speech);

    //text inside speech bubble
    this.speechTxt = new createjs.Text(l('intro-creation-game1'), "35px Arial", "black");
    this.speechTxt.lineWidth = 890;
    this.speechTxt.x = this.speech.x + 95;
    this.speechTxt.y = this.speech.y + 55;

    this.container.addChild(this.speechTxt);

    //continue button for text advancement
    //this.continueButton = new Button(this.speech.x+this.speech.image.width*this.speech.scaleX - 200,this.speech.y + this.speech.image.height*this.speech.scaleY - 280,100,60,"", "#4da6ff", this.stage, imgPaths['continue'],"","","",0.1,0.09);
    //this.container.addChild(this.continueButton);

    this.creationGameButton = new Button(MTLG.getOptions().width / 2 - 200, MTLG.getOptions().height - 300, 400, 100, "creationGame", "#4da6ff", this.stage, null, "35px Arial");

    this.container.addChild(this.creationGameButton);
    
    this.decideGameButton = new Button(MTLG.getOptions().width / 2 - 200, MTLG.getOptions().height - 150, 400, 100, "decisionGame", "#4da6ff", this.stage, null, "35px Arial");

    this.container.addChild(this.decideGameButton);

    this.stage.addChild(this.container);

    this.login = new loginView(50, 720, this.container);

    // //language selection
    // this.language = MTLG.getOptions().language;

    // //flag images
    // this.engImg = MTLG.assets.getBitmap(imgPaths['flag-eng']);
    // this.engImg.x = 1500;
    // this.engImg.y = 50;
    // this.engImg.scaleX = 0.12;
    // this.engImg.scaleY = 0.12;
    // this.gerImg = MTLG.assets.getBitmap(imgPaths['flag-ger']);
    // this.gerImg.x = 1700;
    // this.gerImg.y = 50;
    // this.gerImg.scaleX = 0.12;
    // this.gerImg.scaleY = 0.12;

    // if (this.language == 'de'){
    //   this.engImg.alpha = 0.25;
    // }
    // else{
    //   this.gerImg.alpha = 0.25;
    // }

    // this.container.addChild(this.engImg,this.gerImg);

    //create random user id
    this.userId = createRandomId(16);
    this.phase = 1;
    /*this.continueButton.on('click', function () { //click changes intro text to
        this.speechTxt.text = l('intro-creation-game2');
        xapiButton({en:"introText button", de:"Einführungstext-Button"},this.userId); //xapi call
        this.continueButton.visible = false;
        this.phase = 2;
    }.bind(this));*/

    // this.engImg.on('click', function () { //click changes the games language and flag alpha
    //     this.engImg.alpha = 1;
    //     this.gerImg.alpha = 0.25;
    //     MTLG.lang.setLanguage('en');
    //     // this.creationGameButton.children[1].text = l('creationGame');
    //     // this.creationGameButton.children[1].realignButtonText();
    //     // this.decideGameButton.children[1].text = l('decisionGame');
    //     // this.decideGameButton.children[1].realignButtonText();
    //     MTLG.emailClient.updateLang();
  
    //     this.speechTxt.text = this.phase == 2? l('intro-creation-game2') : l('intro-creation-game1');
    //     console.log('Language set to ' + MTLG.lang.getLanguage());
    //     this.login.updateLanguage();
    //     this.login.hide();
    //     xapiButton({en:"english button", de:"Englisch-Button"},this.userId); //xapi call
    // }.bind(this));

    // this.gerImg.on('click', function () { //click changes the games language and flag alpha
    //   this.gerImg.alpha = 1;
    //   this.engImg.alpha = 0.25;
    //   MTLG.lang.setLanguage('de');
    //   // this.creationGameButton.children[1].text = l('creationGame');
    //   // this.creationGameButton.children[1].realignButtonText();

    //   // this.decideGameButton.children[1].text = l('decisionGame');
    //   // this.decideGameButton.children[1].realignButtonText();
    //   MTLG.emailClient.updateLang();
    //   this.speechTxt.text = this.phase == 2? l('intro-creation-game2') : l('intro-creation-game1');
    //   console.log('Language set to ' + MTLG.lang.getLanguage());
    //   this.login.updateLanguage();
    //   this.login.hide();
    //   xapiButton({en:"german button", de:"Deutsch-Button"},this.userId); //xapi call
    // }.bind(this));

    //create a string of given length
    function createRandomId(length = 16){
      let availableChars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
      // Pick characers randomly
      let id = '';
      for (let i = 0; i < length; i++) {
          id += availableChars.charAt(Math.floor(Math.random() * availableChars.length));
      }
      return id;
    }

    if(_saver.hasSavedGameState()){
      this.loadModalView = new loadModalView(this.stage);
    }
    else {
      xapiGameStart();
    }
}
