function tutorialModalView (stage) {
    this.stage = stage;

    this.tutorialModalViewContainer = new createjs.Container();

    this.bg = new createjs.Shape();
    this.bg.graphics.setStrokeStyle(2,"").beginStroke("");
    this.bg.graphics.beginFill("#545350").drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
    this.bg.alpha = 0.7;
    this.tutorialModalViewContainer.addChild(this.bg);

    this.avatarImg = MTLG.assets.getBitmap(imgPaths['avatar1']);
    this.avatarImg.x = 1490;
    this.avatarImg.y = 670;
    this.avatarImg.scaleX = 1;
    this.avatarImg.scaleY = 1;
    this.tutorialModalViewContainer.addChild(this.avatarImg);

    //speech bubble
    this.speech = MTLG.assets.getBitmap(imgPaths['speech']);//'img/speech-bubble.png');
    this.speech.x = 690;
    this.speech.y = 150;
    this.speech.scaleX = 0.80;
    this.speech.scaleY = 0.6;
    this.tutorialModalViewContainer.addChild(this.speech);

    //text inside speech bubble
    this.speechTxt = new createjs.Text(l('tutorial-modal-creation-1'), "35px Arial", "black");
    this.speechTxt.lineWidth = 890;
    this.speechTxt.x = this.speech.x + 95;
    this.speechTxt.y = this.speech.y + 55;
    this.tutorialModalViewContainer.addChild(this.speechTxt);

    this.gameVisible = 2;

    this.nextButton = new Button(1525, 560, 75, 60, "", "white", this.stage, imgPaths['continue'], "", "", "", 0.05, 0.05);
    this.nextButton.on("click", function() {
        this.speechTxt.text = (this.gameVisible == 2) ? l("tutorial-modal-creation-2") : l("tutorial-modal-decision-2");
        this.nextButton.visible = false;
        this.startButton.visible = true;
    }.bind(this));


    this.startButton = new Button(1400, 560, 200, 60, l("tutorial-modal-start-button"), "#4da6ff", this.stage, null, "30px arial", "black");
    this.startButton.visible = false;
    this.startButton.on("click", function() {
        this.tutorialModalViewContainer.visible = false;
    }.bind(this));

    this.tutorialModalViewContainer.addChild(this.nextButton);
    this.tutorialModalViewContainer.addChild(this.startButton);

    this.pageBack = function() {
        this.speechTxt.text = (this.gameVisible == 2) ? l("tutorial-modal-creation-1") : l("tutorial-modal-decision-1");
        this.nextButton.visible = true;
        this.startButton.visible = false;
    }.bind(this);

    this.switchGame = function (gameVisible) {
        this.gameVisible = gameVisible;
        this.pageBack();
    }

    this.stage.addChild(this.tutorialModalViewContainer);
}