/* View for displaying the results (left side)*/

function resultBoxView(x, y, width, height, stage) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.stage = stage;

    //Container
    this.resultContainer = new createjs.Container();

    // BG
    this.resultBox = new createjs.Shape();
    this.resultBox.graphics.beginFill('#D9D9D9'); 
    this.resultBox.graphics.drawRect(this.x, this.y, this.width, this.height); 
    this.resultBox.graphics.endFill();

    //Headline of resultbox, e.g. 'Correct. This is a phishing E-Mail'
    this.resultTxt = new createjs.Text("", "bold 30px Arial", "black");
    this.resultTxt.x = this.x + 10;
    this.resultTxt.y = this.y + 20;
    this.resultTxt.lineWidth = 640;

    //reason for the classification
    this.reasonTxt = new createjs.Text("", "30px Arial", "black");  
    this.reasonTxt.x = this.resultTxt.x;
    this.reasonTxt.y = this.resultTxt.y + 120;
    this.reasonTxt.lineWidth = 640;

    this.resultContainer.addChild(this.resultBox, this.resultTxt, this.reasonTxt);
    this.resultContainer.visible = false;

    this.stage.addChild(this.resultContainer);

    //Prompt to hover first over all marked passages before continueing
    this.firstMouseoverTxt = new createjs.Text("", "27px Arial", "black"); 
    this.firstMouseoverTxt.text = l('PromptToMouseover');
    this.firstMouseoverTxt.lineWidth = 640;
    this.firstMouseoverTxt.x = this.resultTxt.x;
    this.firstMouseoverTxt.y = this.y + this.height - 140 + 5;

    this.firstMouseoverBg = new createjs.Shape();
    this.firstMouseoverBgColor = this.firstMouseoverBg.graphics.beginFill("#fe97d6").command;
    this.firstMouseoverBg.graphics.drawRect(0, 0, this.width, 140);
    this.firstMouseoverBg.x = this.x;
    this.firstMouseoverBg.y = this.firstMouseoverTxt.y - 5;

    this.firstMouseover = new createjs.Container();
    this.firstMouseover.addChild(this.firstMouseoverBg, this.firstMouseoverTxt);
    this.firstMouseover.visible = false;
    this.stage.addChild(this.firstMouseover);

    //image of a hacker to fill empty space
    this.hackerImg = MTLG.assets.getBitmap(imgPaths['hacker']);//'img/hacker.jpg');
    this.hackerImg.x = this.x;
    this.hackerImg.y = this.reasonTxt.y;
    this.hackerImg.scaleX = 0.34;
    this.hackerImg.scaleY = 0.34;
    this.hackerImg.visible = false;
    this.stage.addChild(this.hackerImg);


    //Tutorial of how to use the stamp to mark susupicious passages
    this.tutImg = (MTLG.lang.getLanguage() == 'en') ? MTLG.assets.getBitmap(imgPaths['tut_stamp_en']) : MTLG.assets.getBitmap(imgPaths['tut_stamp_de']);
    this.tutImg.x = this.x;
    this.tutImg.y = this.reasonTxt.y + 90;
    this.tutImg.scaleX = 0.65;
    this.tutImg.scaleY = 0.65;
    this.tutImg.visible = false;
    this.stage.addChild(this.tutImg);

    //yellow legend to indicate that yellow marked passages are correctly classified
    this.legendCont = new createjs.Container();

    this.yellow = new createjs.Shape();
    this.yellow.graphics.beginFill("yellow").beginStroke("black").drawRect(0, 0, 50, 50);
    this.yellow.x = this.x + 10;
    this.yellow.y = this.resultTxt.y + 40;

    this.yellowTxt = new createjs.Text(l('correct'), "30px Arial", "black");
    this.yellowTxt.x = this.yellow.x + 60;
    this.yellowTxt.y = this.yellow.y + 10;

    this.legendCont.addChild(this.yellow, this.yellowTxt);
    this.legendCont.visible = false;
    this.stage.addChild(this.legendCont);



}