
/**
 * View for the tutorial levels
 * @class
 * @param {number} x The x coordinate where the view starts
 * @param {number} y The y coordinate where the view starts
 * @param {number} width The width of the view
 * @param {number} height The height of the view
 * @param {stage} stage The stage on which the elements of the view are drawn
 */
function tutorialView(x, y, width, height, stage) {
    this.x = x; //position of new email pane
    this.y = y;
    this.width = width; //size of new email pane
    this.height = height;
    this.stage = stage;

    //alignment variables
    var padEdgeBorderX = 10;
    var padEdgeBorderY = 10;

    //Container
    this.tutorialViewContainer = new createjs.Container();

    //BG
    this.bg = new createjs.Shape(); //create BG of E-Mail creation Pane
    this.bg.graphics.setStrokeStyle(1,"").beginStroke("black");
    this.bg.graphics.beginFill("#f2f2f2").drawRect(this.x,this.y+padEdgeBorderY,this.width-padEdgeBorderX,this.height-2*padEdgeBorderY);
    this.tutorialViewContainer.addChild(this.bg);
    this.tutorialViewContainer.visible = false;

    //picture of the supervisor to showcase who is talking
    this.supervisor = MTLG.assets.getBitmap(imgPaths['avatar1']);
    this.supervisor.scaleX = 0.5;
    this.supervisor.scaleY = 0.5;
    this.supervisor.x = this.x + this.width/8  - this.supervisor.image.width*this.supervisor.scaleX/2;
    this.supervisor.y = this.y + 2*padEdgeBorderY;

    //picture of the intern to showcase who is talking
    this.intern = MTLG.assets.getBitmap(imgPaths['avatar2']);
    this.intern.scaleX = 0.5;
    this.intern.scaleY = 0.5;
    this.intern.x = this.x + 7*this.width/8  - this.intern.image.width*this.intern.scaleX/2;
    this.intern.y = this.y + 2*padEdgeBorderY;

    //will hold the dialog textboxes
    this.tutorialDialogTextboxes = [];
    this.textStartY = this.supervisor.y + this.supervisor.image.width*this.supervisor.scaleY;

    this.tutorialViewContainer.addChild(this.supervisor, this.intern);
    this.stage.addChild(this.tutorialViewContainer);
}
