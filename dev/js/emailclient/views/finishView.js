/*View when all emails are classified*/

function finishView(stage) {
    this.stage = stage;

    var BG = MTLG.assets.getBitmap(imgPaths['hacker']);//'img/hacker.jpg');
    BG.y = -300;

    this.stage.addChild(BG);

    this.toMenuButton = new Button(50, 100, 250, 100, 'toMenu', "white", this.stage);

    this.stage.addChild(this.toMenuButton);
}

/**
 * View for the gameOver screen
 * @class
 * @param {number} x The x coordinate where the view starts
 * @param {number} y The y coordinate where the view starts
 * @param {number} width The width of the view
 * @param {number} height The height of the view
 * @param {stage} stage The stage on which the elements of the view are drawn
 */
function finishNewGameView(x,y,width,height,stage){
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.stage = stage;
  this.padding = 20;

  //container
  this.finishNewGameViewContainer = new createjs.Container(); //contains the button, backgroudn and badge
  this.finishNewGameViewContainer.visible = false; //we only want to show this after the game is finished
  this.speechContainer = new createjs.Container(); //contains the speech bubble, speech text and the supervisor icon
  this.speechContainer.visible = false;
  this.speechContainer.alpha = 0;

  //BG
  this.bg = new createjs.Shape();
  this.bg.graphics.setStrokeStyle(1,"").beginStroke("black");
  this.bg.graphics.beginFill("white").drawRect(this.x,this.y,this.width,this.height); //"#f2f2f2"

  //close button
  this.closeButton = new Button(this.x + this.width - 150/4 - 1, this.y + 1,150/4,70/2,"","#ffae3b",this.stage,imgPaths['phishing'],"","","",0.07,0.07);

  //speech bubble
  this.speechImg = MTLG.assets.getBitmap(imgPaths['speech2']);//'img/speech-bubble.png');
  this.speechImg.x = this.x + this.padding*5;
  this.speechImg.y = this.y + this.padding*1.5;
  this.speechImg.scaleX = 0.85;
  this.speechImg.scaleY = 0.5;

  //image of supverisor
  this.supervisorImg = MTLG.assets.getBitmap(imgPaths['avatar1']);//hacker
  this.supervisorImg.scaleX = 0.8;//0.5;
  this.supervisorImg.scaleY = 0.8;//0.5;
  this.supervisorImg.x = this.x+this.width-this.padding*4-this.supervisorImg.image.width*this.supervisorImg.scaleX;
  this.supervisorImg.y = this.y+this.height-this.padding*1.5-this.supervisorImg.image.height*this.supervisorImg.scaleY;

  //badge
  this.badgeImg = MTLG.assets.getBitmap(imgPaths['award']);
  this.badgeImg.scaleX = this.width*(5/10)/this.badgeImg.image.width;
  this.badgeImg.scaleY = this.height*(7/10)/this.badgeImg.image.height;
  this.badgeImg.x = this.x + this.width/2 - this.badgeImg.image.width*this.badgeImg.scaleX/2;
  this.badgeImg.y = -this.badgeImg.image.height*this.badgeImg.scaleY;
  this.badgeImgFinalY = this.y + this.height/2 - this.badgeImg.image.height*this.badgeImg.scaleY/2;

  //game over text
  this.gameOverText = new Textbox({
    text: l('gameOver'),
    x: this.speechImg.x + this.padding*5,
    y: this.speechImg.y + this.padding*2.5,
    fixwidth: 940,
    font: "35px Arial",
    colorBg: null,
  });

  this.speechContainer.addChild(this.supervisorImg,this.speechImg,this.gameOverText.textbox);
  this.finishNewGameViewContainer.addChild(this.bg,this.closeButton,this.badgeImg);
  this.stage.addChild(this.finishNewGameViewContainer,this.speechContainer);

  /**
   * This function lets the content of the view appear
   * @function
   * @param {number} duration Amount of time until the badge will reach its destination
   */
  this.moveInContent = function (duration){
    createjs.Tween.get(this.badgeImg,{ override: true } )
      .to({ y: this.badgeImgFinalY }, duration)
      .to({alpha: 0.075}, duration*0.5)
    createjs.Tween.get(this.speechContainer)
      .wait(duration*1.75)
      .to({alpha: 1}, duration*0.5)
  }

}
