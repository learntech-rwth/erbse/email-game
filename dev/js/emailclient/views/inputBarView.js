/**
 * View for input bar
 */
function inputBarView (pInputBar, x, y, width, height, prefill="Enter name...", stage, otherInputBar = null) {
    this.stage = stage;
    this.inputBar = pInputBar; // sets model
    this.width = width;
    this.height = height;
    this.x = x;
    this.y = y;
  
    this._view = createBox(this.x, this.y, this.width, this.height); // sets view
    this.stage.addChild(this._view);
  
    this._textView = createText(l(prefill));
    this._textView.x = this._view.x;
    this._textView.y = this._view.y;
    this.stage.addChild(this._textView);
  
    this.addedViews = [];
    this.active = false;
  
    this.keysPressed = [];
  
    /**
     * Returns text of input bar
     */
    this.getText = () => {
      return this._textView.getText();
    }

    this.setText = (text) => {
      this._textView.setText(text);
    }

    this.remove = () => {
      this.stage.removeChild(this._textView);
      this.stage.removeChild(this._view);
    }
  
    /**
     * Resets text in input bar
     */
    this.resetText = () => {
      this._textView.resetText();
    }
  
    this.otherInputBars = []
    this.addOtherInputBar = (otherInputBar) => {
      this.otherInputBars.push(otherInputBar);
    }

    this.activate = () => {
      this._view.shadow = new createjs.Shadow('#ffae3b', 0, 0, 50);
      this.active = true;
      for(var otherInputBar of this.otherInputBars){
        otherInputBar.deactivate();
      }
    }
  
    this.deactivate = () => {
      this._view.shadow = null;
      this.active = false;
    }

  
    // Activates/Deactivates the inputBarView for key press events
    this._view.addEventListener('pressup', function() {
      if (!this.active) {
        this.activate();
        if(this.getText().trim() == l(prefill).toString()){
          this.resetText();
        }
      } else {
        this.deactivate();
      }
    }.bind(this));
  
    // Handles all key up events to either add or remove character
    document.addEventListener('keyup', function (e) {
  
      // all invalid keys that might be pressed and now released
      let invalidKeys = [
        " ","Meta","F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11",
        "F12","NumLock","ArrowUp","ArrowLeft","ArrowDown","ArrowRight",
        "Home","End","PageUp","PageDown","Clear","Insert","Delete","Enter",
        "PrintScreen","ScrollLock","Pause","Dead","Alt","Escape",
        "Shift","CapsLock","Control","AltGraph","Backspace","NumLock"
      ];
  
      // all character mappings due to AltGraph or Control+Alt
      let charMapping = new Map([
        ["<","|"],
        ["+","~"],
        ["7","{"],
        ["8","["],
        ["9","]"],
        ["0","}"],
        ["ß","\\"],
        ["e","€"],
        ["q","@"],
        ["m","µ"]]);
  
      let key = e.key;
  
      if (this.active) {
          // checks for Ctrl+Alt or Ctrl+AltGr pressed
          if (charMapping.has(key) &&
                (this.keysPressed.includes("Control") &&
                  (this.keysPressed.includes("AltGraph") ||
                    this.keysPressed.includes("Alt"))
                  )
                ) {
            key = charMapping.get(key);
          }
  
          if (key == 'Enter') {
            // this.addGeneratedUrlPart();
            return;
          }
          // console.log(key);
          if (!invalidKeys.includes(key)){
            this._textView.addChar(key);
          } else if (key == "Backspace") {
            this._textView.removeChar();
          }
      }
  
      if (this.keysPressed.includes(key)){
        this.keysPressed = this.keysPressed.filter((v,i,arr) => {
          return v != key;
        });
        //console.log(this.keysPressed);
      }
      // console.log(key);
    }.bind(this));
  
    // Stores all keys pressed
    document.addEventListener('keydown', function (e) {
      // console.log(e.key);
      // all forbidden chars/multi-key-presses
      let forbiddenChars = [
        " ","\\","{","[","]","}","~","|","³","²","€","@","µ"
      ];
      // all invalid keys which will not be logged
      let invalidKeys = [
        " ","Meta","NumLock","ArrowUp","Clear","ArrowLeft",
        "ArrowDown","ArrowRight","Home","End","PageUp","PageDown","Insert",
        "Delete","Enter","PrintScreen","ScrollLock","Pause","F1","F2","F3",
        "F4","F5","F6","F7","F8","F9","F10","F11","F12"
      ];
  
      if (!this.keysPressed.includes(e.key) && !forbiddenChars.includes(e.key) && !invalidKeys.includes(e.key)){
        this.keysPressed.push(e.key);
        // console.log("key " + e.key + " pressed");
      } else {
        // console.log("key " + e.key + " already pressed");
      }
      // console.log(this.keysPressed);
  
      // Maybe use preventDefault in production mode
      // e.preventDefault();
      return;
    }.bind(this));
  
}

/**
 * Container for input bar and 'generate' button
 */
function inputAreaView(inputBar, x, y, width, height, title="Name", prefill="Enter name...", stage) {
    this.inputBar = inputBar;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.stage = stage;
  
    this._container = new createjs.Container();
    this._container._stage = this.stage;
    this._container.x = this.x;
    this._container.y = this.y;
  
    this._labelView = createText(l(title), {txtColor:"#000000"});
    this._labelView.x = 0;
    this._labelView.y = 0;

    this._container.addChild(this._labelView);
  
    this._inputBarView = new inputBarView(inputBar, 0, 50, width, 50, prefill, this._container);
  
    this.addGeneratedUrlPart = () => {
      this._inputBarView.addGeneratedUrlPart();
    }
  
    this.activateInputBar = () => {
      this._inputBarView.activate();
    }
  
    this.deactivateInputBar = () => {
      this._inputBarView.deactivate();
    }

    this.getText = () => {
      return this._inputBarView.getText();
    }

    this.setText = (text) => {
      return this._inputBarView.setText(text);
    }
  
    // this._generateButtonView = new button(this.width + 20, 50, l('button_Generate'), this.addGeneratedUrlPart, this._container, {bgColor:"#56bca4"});
  
    stage.addChild(this._container);

    this.addOtherInputBar = (otherInputBar) => {
      this._inputBarView.addOtherInputBar(otherInputBar);
    }

    this.remove = () => {
      this._inputBarView.remove();
      this._container.removeAllChildren();
    }
}
  


function inputBar () {
    this.keys_pressed = [];
}

  
/**
 * Creates Box
 */
function createBox(x, y, width, height, options = null) {
    let defaultOptions = {
      bgColor: "#4da6ff",
      alpha: 1.0,
    }
    options = Object.assign(defaultOptions, options);
  
    let retCont = new createjs.Container();
    let shape = new createjs.Shape();
    shape.graphics.beginFill(options.bgColor).drawRect(0, 0, width, height);
    shape.myType = "Rect";
    shape.alpha = options.alpha;
    retCont.addChild(shape);
    retCont.x = x;
    retCont.y = y;
    return retCont;
}


/**
 * Creates Text
 */
function createText(label, options = null) {

    let defaultOptions = {
      txtColor: "#000000",
      bgColor: "#ffffff",
      padding: {
        "lr": 15,
        "tb": 10
      },
      fontSize: "30px",
      font: "Arial",
      lineWidth: 900
    }
    options = Object.assign(defaultOptions, options);
  
    let retCont = new createjs.Container();
    let text = new createjs.Text(label+" ", options.fontSize+" "+options.font, options.txtColor);
    text.set({lineWidth: (options.lineWidth-2*options.padding.lr)});
    text.textBaseline = "top";
    text.x = options.padding.lr; //TODO
    text.y = options.padding.tb;
    text.myType = "Text";
    //text.outline = 1;
    retCont.text = text;
    let bounds = text.getBounds();
    bounds.width += options.padding.lr*2;
    bounds.height += options.padding.tb*2; 
  
    retCont.addChild(text);
    retCont.setBounds(bounds.x, bounds.y, bounds.width, bounds.height);
  
    /**
     * Appends character to text in input bar
     */
    retCont.addChar = function (newChar) {
      // console.log("add char "+newChar);
      this.text = this.text.trim()+newChar.trim();
      // logging event
    }.bind(text);
  
    retCont.setText = function (newText) {
      this.text = newText;
    }.bind(text);
  
    /**
     * Removes last character of text in input bar
     */
    retCont.removeChar = function () {
      // console.log("remove last char");
      this.text = this.text.slice(0, -1);
      // logging event
    }.bind(text);
  
    /**
     * Returns text in text element
     */
    retCont.getText = function () {
      return this.text;
    }.bind(text);
  
    /**
     * Resets text in text element
     */
    retCont.resetText = function () {
      this.text = "";
      this.color = "#000000"
    }.bind(text);
  
    return retCont;
}
  