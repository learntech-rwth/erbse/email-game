/**
 * View for Preview Pane (E-Mail Body)
 */
function previewPaneView(x, y, width, height, stage, email, targets) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.stage = stage;
    this.email = email;
    this.targets = targets; //contains suspicious targets
    //console.log("Preview Pane View")

    //Container
    this.previewContainer = new createjs.Container();

    var bg = new createjs.Shape();
    bg.graphics.beginFill("white").beginStroke('black').drawRect(this.x, this.y, this.width, this.height);

    this.previewContainer.addChild(bg);


    if(this.email.html) { //if email contains html
        var str = this.email.html[MTLG.lang.getLanguage()]; //get email body
        str = str.replace(/(?:\r\n|\r|\n)/g, ''); //remove '\n' and '\r' that parser adds automatically
        str = str.replace(/<br[^>]*>/gi, '\n'); //replace <br> with \n
        var linkRx = /<a[\s]+([^>]+)>((?:.(?!\<\/a\>))*.)<\/a>/g; //Regex fot extracting anchor tag
        var links = str.match(linkRx); //array containing all anchor tags
        str = str.replace(/<(.|\n)*?>/g, ''); //convert html to plain text. Remove all html tags
        var arrayStr = str.split("\n"); //array where each element corresponds to one line
        var emailLines = addLineBreaks(arrayStr); //add line breaks if one line is too long to display in preview as one line


        if (links) { // if email contains links (anchor tags)
            textWithLink.bind(this)(emailLines); //display text and add mouseover event to links
            //console.log("textWithLink");
        }
        else {
            //console.log("textWithoutLink");
            textWithoutLink.bind(this)(emailLines); //display text
        }


    }
    else if (this.email.text) { //if email consists of plain text

        var str = this.email.text; //get email body text
        var arrayStr = str.split("\n"); //array where each element corresponds to one line
        var emailLines = addLineBreaks(arrayStr); //add line breaks if one line is too long to display in preview as one line

        textWithoutLink.bind(this)(emailLines); //display text
    }



    //add Line Breaks if an element of 'arrayStr' is too long to display in one line in the preview pane
    // TODO: check for placeholders + phishing features
    function addLineBreaks(arrayStr) {
        //console.log("LineBreaks")
        var arrayLB = [];
        arrayStr.forEach(function (string) {
            var newString = string;
            while (newString.length > 86) { //string is too long. A Split is needed
                var index = 86;
                while (newString[index] !== " " && index >= 0) { //find index where to split (don't split between a word)
                    index--;
                }
                arrayLB.push(newString.slice(0, index)); //add shortened string to array
                newString = newString.slice(index + 1); //test whether the string after split is still to long
            }
            arrayLB.push(newString); //add string after split to array
        });
        return arrayLB;
    }

    //display text containing links
    function textWithLink(emailLines) {

        var yPos = this.y + 10; //for positioning of text passages
        var xPos = this.x + 10;
        this.urlCont = []; //contains the view of the urls that should be displayed on mouseover
        this.numLinks = links.length;
        this.hyperlinkCont = []; //contains the view of the links with mouseover event
        var hrefs = []; //contains the url in anchor tags (<a href = 'url'> Linktext </a>)
        var linkTxts = []; //contains the Linktext between anchor tags
        var bodyIsSusp = true; //body has suspicious passages

        for (var i = 0; i < this.numLinks; i++) { //fill arrays 'hrefs' and 'linkTxts'
            var link = links[i];

            var href = (link.match(/href=(["'])(.*?)\1/))[2]; //extract URL. href.match returns: ["href='url'", "'", "url"], therefore we need only index 2
            var linkTxt = link.replace(/<[^>]*>/g, ""); //Text between anchor Tags

            hrefs.push(href);
            linkTxts.push(linkTxt);
        }

        var isPhish = this.email.isPhish;
        var notSuspLinks = new Set(); //set containg Linktxt that are not suspicious and therefore don't need to be added into 'targets' array

        if (isPhish && this.email.mark.body) { //email has sth. to mark
            var markArray = this.email.mark.body; //markArray: [{markTxt: '...', text: '...'}]
            markArray = markArray.map(obj => obj.markTxt); //get passages to mark

            linkTxts.forEach(function (elem) { //add also links that are not suspicious, bc needed for mousseover event
                if (markArray.indexOf(elem) === -1) { //link(elem) was not in markArray -> link is not suspicious
                    notSuspLinks.add(elem); //note that it is not suspicious
                    markArray.push(elem); //add to markArray. Needed for mouseover event
                }
            });
        }
        else { //email is not phishing or has no supisious passages in body
            var markArray = linkTxts; //add only LinkTxt. Needed for mouseover event
            bodyIsSusp = false; //body has no supicious passages

        }



        for (var k = 0; k < emailLines.length; k++) { //iterate over each line
            var match = false; //true if line contains a word to be marked
            var line = emailLines[k]; //get first line

            for (var l = 0; l < markArray.length; l++) { //iterate over each passage to be marked

                if (line.match(markArray[l][MTLG.lang.getLanguage()])) { //test whether line contains a passage to be marked
                    match = true;
                   // var textArray = line.split(markArray[l]); //textArray: [text before markTxt, text after markTxt]

                    var ind = line.indexOf(markArray[l]);
                    var textArray = [line.substr(0, ind), line.substr(ind+markArray[l][MTLG.lang.getLanguage()].length)]; //textArray: [text before markTxt, text after markTxt]

                    if (textArray[0] !== "") { //text before the passage to be marked
                        var textBefore = new Textbox({
                            text: textArray[0],
                            x: xPos,
                            y: yPos,
                        });
                        this.previewContainer.addChild(textBefore.textbox);

                        xPos += textBefore.txt.getBounds().width; //following Text should be in same line, but further right
                    }

                    var marktxt = new Textbox({ //passage to be marked
                        text: markArray[l][MTLG.lang.getLanguage()],
                        x: xPos,
                        y: yPos,
                    });

                    var linkIndex = linkTxts.indexOf(markArray[l]); //Test whether the text to be marked is a link and therefore requires a mouse-over event
                    if (linkIndex > -1) { //it is a link
                        marktxt.txt.color = "blue"; //links are displayed blue

                        this.hyperlinkCont[linkIndex] = marktxt.textbox; //add View of Linktext to array

                        var url = new Textbox({ //create url-box to the corresponding Linktxt
                            text: hrefs[linkIndex],
                            x: xPos,
                            y: yPos - 40, //url is displayed over Linktext
                            stroke: "black",
                            pad: 5,
                            font: "25px Arial",
                        });

                        this.urlCont[linkIndex] = url.textbox; //add View of url to array
                        this.urlCont[linkIndex].visible = false; //visible on mouseover

                        this.previewContainer.addChild(this.urlCont[linkIndex]);
                    }

                    this.previewContainer.addChild(marktxt.textbox);

                    if (!notSuspLinks.has(markArray[l][MTLG.lang.getLanguage()]) && bodyIsSusp) { //not suspicious link should not be a target
                        this.targets.push(marktxt.textbox);
                        marktxt.textbox.reason = new Textbox({ //reason why it's supicious. Will be shown on mouseover
                            text: this.email.mark.body[l].text[MTLG.getLanguage()],
                            x: marktxt.txt.x,
                            y: marktxt.txt.y + 40,
                            colorTxt: "yellow",
                            colorBg: "#454545",
                            pad: 5,
                            font: "25px Arial",
                            fixwidth: 300,
                        });
                        marktxt.textbox.reason.fixHeight();
                        marktxt.textbox.reason.textbox.visible = false;
                        this.stage.addChild(marktxt.textbox.reason.textbox);
                    }

                    xPos += marktxt.txt.getBounds().width; //following Text should be in same line, but further right

                    if (textArray[1] && textArray[1] !== "") { //test if following text contains a passage to marked
                        line = textArray[1];
                        match = false;
                    }

                }
            }

            if (!match) { //line contains no passage to be marked. display this line
                var newtext = "";
                if (line !== "") {
                    newtext = new Textbox({
                        text: line,
                        x: xPos,
                        y: yPos,
                    });
                    this.previewContainer.addChild(newtext.textbox);
                }
                else {
                    newtext = new createjs.Text("\n");
                    newtext.x = xPos;
                    newtext.y = yPos;
                }
            }

            yPos += 30; //set to next line
            xPos = this.x + 10;//reset x Position

        }


        createjs.Ticker.on("tick", tick.bind(this)); //Ticker just needed if email contains Links

    }

    //display text containing no links
    function textWithoutLink(emailLines) {
        //console.log("textWithoutLink");
        var yPos = this.y + 10; //for positioning of text passages
        var xPos = this.x + 10;

        let previousMatched = false;
        for (var k = 0; k < emailLines.length; k++) { //iterate over each line
            var match = false; //true if line contains a word to be marked
            var line = emailLines[k]; //get first line

            if (this.email.isPhish && this.email.mark.body) { //is phish and has sth to mark

                var markArray = this.email.mark.body; //markArray: [{markTxt: '...', text: '...'}]
                
                for (var l = 0; l < markArray.length; l++) { //iterate over each passage to be marked
                    let textToMatch = markArray[l].markTxt[MTLG.lang.getLanguage()];
                    if (line.match(textToMatch)) { //test if line conatains a passage to be marked
                        //console.log("Single-line match");
                        match = true;
                        //var textArray = line.split(markArray[l].markTxt); //textArray: [text before markTxt, text after markTxt]

                        var ind = line.indexOf(markArray[l].markTxt[MTLG.lang.getLanguage()]);
                        var textArray = [line.substr(0, ind), line.substr(ind+markArray[l].markTxt[MTLG.lang.getLanguage()].length)]; //textArray: [text before markTxt, text after markTxt]

                        if (textArray[0] !== "") { //text before the passage to be marked
                            var textBefore = new Textbox({
                                text: textArray[0],
                                x: xPos,
                                y: yPos,
                            });
                            this.previewContainer.addChild(textBefore.textbox);

                            xPos += textBefore.txt.getBounds().width; //following Text should be in same line, but further right

                        }
                        var marktxt = new Textbox({ //passage to be marked
                            text: markArray[l].markTxt[MTLG.lang.getLanguage()],
                            x: xPos,
                            y: yPos,
                        });


                        this.previewContainer.addChild(marktxt.textbox);

                        xPos += marktxt.txt.getBounds().width; //following Text should be in same line, but further right

                        this.targets.push(marktxt.textbox); //add to array containing suspicious passages (targets)
                        
                        marktxt.textbox.reason = new Textbox({ //reason why it's supicious. Will be shown on mouseover
                            text: markArray[l].text[MTLG.lang.getLanguage()],
                            x: marktxt.txt.x,
                            y: marktxt.txt.y + 40,
                            colorTxt: "yellow",
                            colorBg: "#454545",
                            pad: 5,
                            font: "25px Arial",
                            fixwidth: 300,
                        });
                        marktxt.textbox.reason.fixHeight();
                        marktxt.textbox.reason.textbox.visible = false;
                        this.stage.addChild(marktxt.textbox.reason.textbox);

                        if (textArray[1] && textArray[1] !== "") { //test if following text contains a passage to marked
                            line = textArray[1];
                            match = false;
                        }

                    }        
                    else if(k < emailLines.length - 1){
                        //console.log("Checking multiline");
                        let nextLine = emailLines[k+1];
                        let combinedLine = line + " " + nextLine;
                        //console.log(combinedLine);

                        if(!nextLine.match(textToMatch) && combinedLine.match(textToMatch)){ // text to match is only contained in the combined lines
                            match = true;
                            previousMatched = true;
                            
                            var ind = combinedLine.indexOf(markArray[l].markTxt[MTLG.lang.getLanguage()]);
                            var textArrayLine = [line.substr(0, ind), line.substr(ind)]; //textArray: [text before markTxt, text after markTxt]
                            var textArrayNextLine = [nextLine.substr(0, textToMatch.length - (line.length - ind)), line.substr(textToMatch.length - ind)]; //textArray: [text before markTxt, text after markTxt]

                            if (textArrayLine[0] !== "") { //text before the passage to be marked
                                var textBefore = new Textbox({
                                    text: textArrayLine[0],
                                    x: xPos,
                                    y: yPos,
                                });
                                this.previewContainer.addChild(textBefore.textbox);

                                xPos += textBefore.txt.getBounds().width; //following Text should be in same line, but further right

                            }
                            var marktxt = new Textbox({ //passage to be marked
                                text: textArrayLine[1],
                                x: xPos,
                                y: yPos,
                            });
                            //console.log(textArrayNextLine[0])
                            var marktxt2 = new Textbox({ //passage to be marked
                                text: textArrayNextLine[0],
                                x: this.x+10,
                                y: yPos+30,
                            });
                            //console.log("marktxt2:")
                            //console.log(marktxt2);


                            this.previewContainer.addChild(marktxt.textbox);
                            this.previewContainer.addChild(marktxt2.textbox);

                            xPos += marktxt2.txt.getBounds().width; //following Text should be in same line, but further right

                            if (textArrayNextLine[1] !== "") { //text before the passage to be marked
                                var textAfter = new Textbox({
                                    text: textArrayNextLine[1],
                                    x: xPos,
                                    y: yPos,
                                });
                                this.previewContainer.addChild(textAfter.textbox);
                            }

                            this.targets.push(new MultiTextbox([marktxt.textbox, marktxt2.textbox])); //add to array containing suspicious passages (targets)
                            // this.targets.push(marktxt.textbox); //add to array containing suspicious passages (targets)
                            
                            marktxt.textbox.reason = new Textbox({ //reason why it's supicious. Will be shown on mouseover
                                text: markArray[l].text[MTLG.lang.getLanguage()],
                                x: marktxt.txt.x,
                                y: marktxt.txt.y + 40,
                                colorTxt: "yellow",
                                colorBg: "#454545",
                                pad: 5,
                                font: "25px Arial",
                                fixwidth: 300,
                            });
                            marktxt.textbox.reason.fixHeight();
                            marktxt.textbox.reason.textbox.visible = false;
                            this.stage.addChild(marktxt.textbox.reason.textbox);

                            marktxt2.textbox.reason = new Textbox({ //reason why it's supicious. Will be shown on mouseover
                                text: markArray[l].text[MTLG.lang.getLanguage()],
                                x: marktxt2.txt.x,
                                y: marktxt2.txt.y + 40,
                                colorTxt: "yellow",
                                colorBg: "#454545",
                                pad: 5,
                                font: "25px Arial",
                                fixwidth: 300,
                            });
                            marktxt2.textbox.reason.fixHeight();
                            marktxt2.textbox.reason.textbox.visible = false;
                            this.stage.addChild(marktxt2.textbox.reason.textbox);
                        }
                    }
                }
            }

            if (!match && !previousMatched) { //line contains no passage to be marked. display this line
                var newtext = "";
                if (line !== "") {

                    newtext = new Textbox({
                        text: line,
                        x: xPos,
                        y: yPos,
                    });
                    this.previewContainer.addChild(newtext.textbox);
                }
                else {
                    newtext = new createjs.Text("\n");
                    newtext.x = xPos;
                    newtext.y = yPos;
                }
            }
            else if(!match && previousMatched) {
                previousMatched = false;
            }

            yPos += 30; //set to next line
            xPos = this.x + 10;//reset width

        }
    }

    //display url on mouseover
    function tick() {
        for (var i = 0; i < this.numLinks; i++) {
            let point = this.hyperlinkCont[i].globalToLocal(MTLG.getStage().mouseX, MTLG.getStage().mouseY); //get mouse position

            if (this.hyperlinkCont[i].hitTest(point.x, point.y)) { //if mouse hovers over Linktext show url
                this.urlCont[i].visible = true;
                this.stage.setChildIndex(this.urlCont[i], this.stage.numChildren - 1);

            }
            else {
                this.urlCont[i].visible = false;
            }
        }

    }

}
