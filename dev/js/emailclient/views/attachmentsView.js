/**
 * View for Attachments
 */
function attachmentsView(x, y, width, height, stage, email, targets) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.stage = stage;
    this.email = email;
    this.targets = targets; //contains suspicious targets

    //Container
    this.attachmentsContainer = new createjs.Container();



    var xPos = 0; //for the positioning of attachments next to each other

    var attachments = this.email.attachments;
    if (attachments) { //if email has attachments
        for (var i = 0; i < attachments.length; i++) { //create for each attachment a View consisting of type-icon and name

            var container = new createjs.Container(); //View of one attachments

            var _attTxt = new Textbox({
                text: attachments[i].name,
                x: this.x + 53 + xPos,
                y: this.y + 25,
                stroke: "black",
                pad: 5,
            });

            container.addChild(_attTxt.textbox);

            if (this.email.isPhish && this.email.mark.attachments[i].susp) { //if this attachment is suspicious

                _attTxt.textbox.reason = new Textbox({ //add a box with the reason why it's suspicious. Will be shown on mouseover
                    text: this.email.mark.attachments[i].text[MTLG.lang.getLanguage()],
                    x: _attTxt.txt.x - 45,
                    y: _attTxt.txt.y + 40, //display reason box under (+40) suspicious passage on mouseover
                    colorTxt: "yellow",
                    colorBg: "#454545",
                    pad: 5,
                    font: "25px Arial",
                    fixwidth: 300,
                });
                _attTxt.textbox.reason.fixHeight(); //adjust Height of box
                _attTxt.textbox.reason.textbox.visible = false;
                this.stage.addChild(_attTxt.textbox.reason.textbox);

                this.targets.push(_attTxt.textbox); //add attachment View to Array of suspicious targets
            }

            var width = _attTxt.txt.getBounds().width;
            xPos += width + 70;


            var type = attachments[i].contentType;
            type = type.slice(type.indexOf("/") + 1, type.indexOf(";")); //type looks like 'image/jpg; name="Bild1.png" but we need just 'jpg''

            switch (type) { //which mime type? get corresponding icon to mime type
                case 'pdf':
                    _attIcon = MTLG.assets.getBitmap(imgPaths['pdf']); //'img/pdf-icon.png');
                    break;
                case 'msword':
                case 'vnd.openxmlformats-officedocument.wordprocessingml.document':
                    _attIcon = MTLG.assets.getBitmap(imgPaths['doc']);//'img/doc-icon.png');
                    break;
                case 'jpeg':
                case 'jpg':
                    _attIcon = MTLG.assets.getBitmap(imgPaths['jpg']);//'img/jpg-icon.png');
                    break;
                case 'vnd.microsoft.portable-executable':
                    _attIcon = MTLG.assets.getBitmap(imgPaths['exe']);//'img/exe-icon.png');
                    break;
                case 'x-zip-compressed':
                    _attIcon = MTLG.assets.getBitmap(imgPaths['zip']);//'img/zip-icon.png');
                    break;
                default:
                    _attIcon = MTLG.assets.getBitmap(imgPaths['noType']);
            }

            _attIcon.x = _attTxt.txt.x - 50;
            _attIcon.y = _attTxt.txt.y - 20;
            _attIcon.scaleX = 0.1;
            _attIcon.scaleY = 0.1;



            container.addChild(_attIcon);

            this.attachmentsContainer.addChild(container);

        }
    }

}