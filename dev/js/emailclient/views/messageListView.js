/**
 * View for Message List
 */
function messageListView(x, y, width, height, stage, displayedEmails) {
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.stage = stage;
  this.emails = displayedEmails;


  //Container
  this._searchContainer = new createjs.Container();

  //Search Box
  this._searchBoxView = new createjs.Shape();
  this._searchBoxView.graphics.beginStroke('black').beginFill("white").drawRect(0, 0, this.width, 100);
  this._searchBoxView.x = this.x;
  this._searchBoxView.y = this.y;

  this._searchTextView = new createjs.Text(l('search'), "40px Arial", "black");
  this._searchTextView.x = this.x + 10;
  this._searchTextView.y = this.y + 30;

  this._searchContainer.addChild(this._searchBoxView, this._searchTextView);

  this.stage.addChildAt(this._searchContainer, 2);

  this._messageContainer = []; //contains View of each Messagebox
  this._msgBoxHeight = 150;

  this.createMsgBox = function (newMail, i) { //create Messagebox for email 'newMail' at index 'i'
    var container = new createjs.Container();

    //Background
    this.msgBoxBg = new createjs.Shape();
    this.msgBoxBg.graphics.beginStroke('#b9a8f6').beginFill("#d0c6f5").drawRect(0, 0, this.width, this._msgBoxHeight);
    this.msgBoxBg.x = this.x; //same as Search
    this.msgBoxBg.y = this._searchBoxView.y + 100 + 20 + i * (this._msgBoxHeight + 20);

    //Checkbox
    let checkSize = 30;
    this.msgCheckbox = new createjs.Shape();
    this.msgCheckbox.graphics.beginStroke('black').beginFill("white").drawRect(0, 0, checkSize, checkSize);
    this.msgCheckbox.x = this.msgBoxBg.x + 10;
    this.msgCheckbox.y = this.msgBoxBg.y + 10;

    //Text
    this._msgName = new createjs.Text(newMail.from.name.substring(0, 18), "35px Arial", "blue"); //Names longer than 19 characters are cut
    
    let trimmedSubject = newMail.subject[MTLG.lang.getLanguage()];
    trimmedSubject = (trimmedSubject.length > 35) ? trimmedSubject.substring(0, 35) + "..." : trimmedSubject;
    this.msgSubj = new createjs.Text(trimmedSubject, "30px Arial", "black");
    this._msgName.lineWidth = this.msgSubj.lineWidth = 350;
    this._msgName.textAlign = this.msgSubj.textAlign = "left";
    this._msgName.y = this.msgBoxBg.y + 10;
    this._msgName.x = this.msgSubj.x = this.x + 10 + checkSize + 10;
    this.msgSubj.y = this._msgName.y + 40;

    //Date
    var date = new Date(newMail.date);
    var lang = (MTLG.lang.getLanguage() == 'en') ? "en-US" : "de-DE"; //display date differently depending on language
    var dateString = date.toLocaleString(lang, { 'weekday': "short", 'day': "2-digit", 'month': "2-digit", 'year': "numeric", 'hour': "numeric", 'minute': "numeric" });

    this.msgDate = new createjs.Text(dateString, "20px Arial", "black");
    this.msgDate.textAlign = "right";
    this.msgDate.x = this.msgBoxBg.x + this.width - 5;
    this.msgDate.y = this.msgBoxBg.y + this._msgBoxHeight - 20;

    container.addChild(this.msgBoxBg, this._msgName, this.msgSubj, this.msgDate, this.msgCheckbox);

    var distance = 5; //for positioning of icons (attachment, smime)

    //attachment icon
    if (newMail.attachments) {
      this.attIcon = MTLG.assets.getBitmap(imgPaths['attach']);//'img/attach-icon.png');
      this.attIcon.x = this.msgBoxBg.x + distance;
      this.attIcon.y = this.msgDate.y - 20;
      this.attIcon.scaleX = 0.06;
      this.attIcon.scaleY = 0.06;

      distance += 45;

      container.addChild(this.attIcon);
    }
    //encrypted icon
    if (newMail.isEncrypted) {
      this.lockIcon = MTLG.assets.getBitmap(imgPaths['encrypted']);//'img/lock-icon.png');
      this.lockIcon.x = this.msgBoxBg.x + distance;
      this.lockIcon.y = this.msgDate.y - 20;
      this.lockIcon.scaleX = 0.06;
      this.lockIcon.scaleY = 0.06;

      distance += 35;

      container.addChild(this.lockIcon);
    }
    //signed icon
    if (newMail.isSigned) {
      this.signedIcon = MTLG.assets.getBitmap(imgPaths['signed']);//'img/ribbon-icon.png');
      this.signedIcon.x = this.msgBoxBg.x + distance;
      this.signedIcon.y = this.msgDate.y - 20;
      this.signedIcon.scaleX = 0.06;
      this.signedIcon.scaleY = 0.06;

      container.addChild(this.signedIcon);
    }

    return container;

  }.bind(this);

  for (var i = 0; i < this.emails.length; i++) { //create for each email a Messagebox View
    this._messageContainer[i] = this.createMsgBox(this.emails[i], i);
  }

  this.remove = function(){
    for(let messageContainer of this._messageContainer){
      this.stage.removeChild(messageContainer);
    }
    this.stage.removeChild(this._searchContainer);
  }


  //notification, that this click on search is not supported
  var notsupported = new notSupportedNotification(this.stage, this._searchContainer);
}