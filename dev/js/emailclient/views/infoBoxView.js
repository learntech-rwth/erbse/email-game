/* View fpr info Box */

function infoBoxView(x, y, legitX, phishX, helpX, boxesY, stage) {//
    this.x = x; //position of 'i' Button
    this.y = y;
    this.legitX = legitX; //x-Position of 'legitimate'-Button Infobox
    this.phishX = phishX; //x-Position of 'phishing'-Button Infobox
    this.helpX = helpX; //x-Position of 'help'-Button Infobox
    this.boxesY = boxesY; //y-Postion of three Infoboxes
    this.stage = stage;


    this.boxes = new createjs.Container();

    var legitBox = new Textbox({
        text: l('legitInfoBox'),
        x: this.legitX,
        y: this.boxesY,
        colorBg: "#09fbd3",
        pad: 5,
        fixwidth: 170,
    });
    legitBox.fixHeight();

    var phishBox = new Textbox({
        text: l('phishInfoBox'),
        x: this.phishX,
        y: this.boxesY,
        colorBg: "#fe53bb",
        pad: 5,
        fixwidth: 170,
    });
    phishBox.fixHeight();

    var helpBox = new Textbox({
        text: l('helpInfoBox'),
        x: this.helpX,
        y: this.boxesY,
        colorBg: "yellow",
        pad: 5,
        fixwidth: 170,
    });
    helpBox.fixHeight();

    this.boxes.addChild(phishBox.textbox, legitBox.textbox, helpBox.textbox);
    this.boxes.visible = false; //visible when player clicks on 'i' Button
    this.stage.addChild(this.boxes);

    this.info = new Button(this.x, this.y, 50, 50, "i", "white", this.stage);
    this.stage.addChild(this.info);
}
