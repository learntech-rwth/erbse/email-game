
/**
 * Container for input bar and 'generate' button
 */
 function loginView(x, y, stage) {
    this.x = x;
    this.y = y;
    this.stage = stage;

    this._container = new createjs.Container();
    this._container._stage = this.stage;
    this._container.x = this.x;
    this._container.y = this.y;

    this._container.addChild(this._labelView);

    let loginName = new inputAreaView(new inputBar(), 0, 0, 600, 70, "Name", "entername", this._container);    
    let loginMail = new inputAreaView(new inputBar(), 0, 0+120, 600, 70, "Mail", "entermail", this._container);
    loginName.addOtherInputBar(loginMail._inputBarView);
    loginMail.addOtherInputBar(loginName._inputBarView);

    let lastLangMail = l("entermail");
    let lastLangName = l("entername");
    
    let nameCorrect = MTLG.assets.getBitmap('img/correct.png');
    nameCorrect.x = 600 + 25;
    nameCorrect.y = 20 + 25;
    nameCorrect.scaleX = 0.08;
    nameCorrect.scaleY = 0.08;
    nameCorrect.visible = false;
    this._container.addChild(nameCorrect);
    
    let nameIncorrect = MTLG.assets.getBitmap('img/incorrect.png');
    nameIncorrect.x = 600 + 25;
    nameIncorrect.y = 20 + 25;
    nameIncorrect.scaleX = 0.08;
    nameIncorrect.scaleY = 0.08;
    this._container.addChild(nameIncorrect);
    
    let mailCorrect = MTLG.assets.getBitmap('img/correct.png');
    mailCorrect.x = 600 + 25;
    mailCorrect.y = 140 + 25;
    mailCorrect.scaleX = 0.08;
    mailCorrect.scaleY = 0.08;
    mailCorrect.visible = false;
    this._container.addChild(mailCorrect);
    
    let mailIncorrect = MTLG.assets.getBitmap('img/incorrect.png');
    mailIncorrect.x = 600 + 25;
    mailIncorrect.y = 140 + 25;
    mailIncorrect.scaleX = 0.08;
    mailIncorrect.scaleY = 0.08;
    this._container.addChild(mailIncorrect);

    let submitButton = new Button(350, 250, 250, 60, "save", "#4da6ff", this._container, null, "25px Arial")
    this._container.addChild(submitButton);

    this.submit = function () {
        if(loginName.getText().trim() !== l("entername") && loginName.getText().trim() != ""){
            nameIncorrect.visible = false;
            nameCorrect.visible = true;
            MTLG.mailGenerator.setReceiverName(loginName.getText().trim());
        }
        else {
            nameIncorrect.visible = true;
            nameCorrect.visible = false;
            MTLG.mailGenerator.resetReceiverName();
        }
        
        if(loginMail.getText().trim() !== l("entermail") && loginMail.getText().trim() != ""){
            mailIncorrect.visible = false;
            mailCorrect.visible = true;
            MTLG.mailGenerator.setReceiverMail(loginMail.getText().trim());
        }
        else {
            mailIncorrect.visible = true;
            mailCorrect.visible = false;
            MTLG.mailGenerator.resetReceiverMail();
        }

        generatorInterface.resetMails()
        MTLG.emailClient.setEmails([]);
        let numberOfMails = 10;
        for(let i = 0; i < numberOfMails; i++){
            generatorInterface.addMail();
            MTLG.emailClient.addEmail(generatorInterface.getMails()[i]);
        }
        loginName.deactivateInputBar();
        loginMail.deactivateInputBar();
    }.bind(this)
    submitButton.on('click', this.submit);

    this.updateLanguage = function(){
        this._container.removeChild(submitButton);
        submitButton = new Button(350, 250, 250, 60, "save", "#4da6ff", this._container, null, "25px Arial")
        submitButton.on('click', this.submit);
        this._container.addChild(submitButton);


        if(loginName.getText().trim() !== l("entername") && loginName.getText().trim() !== lastLangName && loginName.getText().trim() != ""){
            loginName.remove();
            loginName = new inputAreaView(new inputBar(), 0, 0, 600, 70, "Name", loginName.getText().trim(), this._container);    
        }
        else {
            loginName.remove();
            loginName = new inputAreaView(new inputBar(), 0, 0, 600, 70, "Name", "entername", this._container);    
        }
        if(loginMail.getText().trim() !== l("entermail") && loginMail.getText().trim() !== lastLangMail && loginMail.getText().trim() != ""){
            loginMail.remove();
            loginMail = new inputAreaView(new inputBar(), 0, 0+120, 600, 70, "Mail", loginMail.getText().trim(), this._container);
        }
        else {            
            loginMail.remove();
            loginMail = new inputAreaView(new inputBar(), 0, 0+120, 600, 70, "Mail", "entermail", this._container);
        }
        lastLangMail = l('entermail');
        lastLangName = l('entername');
        loginName.addOtherInputBar(loginMail._inputBarView);
        loginMail.addOtherInputBar(loginName._inputBarView);
    }

    this.stage.addChild(this._container);

    this.hide = function(){
        loginName.deactivateInputBar();
        loginMail.deactivateInputBar();
    }
}