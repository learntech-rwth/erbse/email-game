/**
 * View for Name + E-Mail-Address
 */
function senderView(x, y, width, height, stage, email, targets) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.stage = stage;
    this.email = email;
    this.targets = targets; //contains suspicious targets

    this._senderContainer = new createjs.Container();

    //icon on the left side of sendername
    this._userIcon = MTLG.assets.getBitmap(imgPaths['user']);//'img/user.png');
    this._userIcon.x = this.x + 10;
    this._userIcon.y = this.y;
    this._userIcon.rotation = 0;
    this._userIcon.scaleX = 0.2;
    this._userIcon.scaleY = 0.2;

    //sendername
    this._senderName = new Textbox({
        text: this.email.from.name,
        x: this._userIcon.x + 150,
        y: this._userIcon.y + 20,
        colorBg: "#f2f2f2",
        font: "35px Arial",
    });

    //add sendername to 'targets' if it is suspicious
    if (this.email.isPhish && this.email.mark.sender.susp) {
        this._senderName.textbox.name = "sender";

        this._senderName.textbox.reason = new Textbox({ //reason why it's suspicious. Will be shown on mouseover
            text: this.email.mark.sender.text[MTLG.lang.getLanguage()],
            x: this._senderName.txt.x,
            y: this._senderName.txt.y + 40,
            colorTxt: "yellow",
            colorBg: "#454545",
            pad: 5,
            font: "25px Arial",
            fixwidth: 300,
        });
        this._senderName.textbox.reason.fixHeight();
        this._senderName.textbox.reason.textbox.visible = false;
        this.stage.addChild(this._senderName.textbox.reason.textbox);

        this.targets.push(this._senderName.textbox);
    }

    //sender E-Mail-Address
    this._senderMail = new Textbox({
        text: this.email.from.email,
        x: this._senderName.txt.x,
        y: this._senderName.txt.y + 50,
        colorBg: "#f2f2f2",
    });
    this._senderMail.textbox.visible = false; //will be shown when player clicks on arrow near sendername or on icon

    //add sendermail to 'targets' if it is suspicious
    if (this.email.isPhish && this.email.mark.email.susp) {

        this._senderMail.textbox.reason = new Textbox({ //reason why it's supicious. Will be shown on mouseover
            text: this.email.mark.email.text[MTLG.lang.getLanguage()],
            x: this._senderMail.txt.x,
            y: this._senderMail.txt.y + 40,
            colorTxt: "yellow",
            colorBg: "#454545",
            pad: 5,
            font: "25px Arial",
            fixwidth: 300,
        });
        this._senderMail.textbox.reason.fixHeight();
        this._senderMail.textbox.reason.textbox.visible = false;
        this.stage.addChild(this._senderMail.textbox.reason.textbox);

        this.targets.push(this._senderMail.textbox);
    }

    //arrow down
    var _arrowD = MTLG.assets.getBitmap(imgPaths['down']);//'img/arrow-down.png');//
    _arrowD.x = this._senderName.txt.x + this._senderName.txt.getBounds().width;
    _arrowD.y = this._senderName.txt.y - 10;
    _arrowD.rotation = 0;
    _arrowD.scaleX = 0.1;
    _arrowD.scaleY = 0.1;

    //arrow up
    var _arrowU = MTLG.assets.getBitmap(imgPaths['up']);//'img/arrow-up.png');
    _arrowU.x = this._senderName.txt.x + this._senderName.txt.getBounds().width;
    _arrowU.y = this._senderName.txt.y - 10;
    _arrowU.rotation = 0;
    _arrowU.scaleX = 0.1;
    _arrowU.scaleY = 0.1;
    _arrowU.visible = false;


    this._senderContainer.addChild(this._userIcon, this._senderName.textbox, this._senderMail.textbox, _arrowD, _arrowU);


    this.hideMail = function () { //hide E-Mail-Address
        this._senderMail.textbox.visible = false;
        _arrowU.visible = false;
        _arrowD.visible = true;
    }.bind(this);

    this.showMail = function () { //show E-Mail-Address
        this._senderMail.textbox.visible = true;
        _arrowU.visible = true;
        _arrowD.visible = false;
    }.bind(this);

    this._senderContainer.on("click", function (evt) {
        if (this._senderMail.textbox.visible) {
            this.hideMail();

        }
        else {
            this.showMail();
        }
    }.bind(this));

}
