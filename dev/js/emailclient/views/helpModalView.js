function helpModalView(modalX, modalY, width, height, buttonX, buttonY, stage) {
    this.modalX = modalX; //position of pop-up
    this.modalY = modalY;
    this.width = width; //size of Pop-Up
    this.height = height;
    this.buttonX = buttonX; //position of 'help' Button
    this.buttonY = buttonY;
    this.stage = stage;

    //Container
    this.helpModalContainer = new createjs.Container();

    this.helpButtonContainer = new Button(this.buttonX, this.buttonY, 170, 100, "", "white", this.stage, imgPaths['help']);//"img/help.png");

    this.modal = new Textbox({
        text: "help",
        x: this.modalX +10,
        y: this.modalY +20,
        colorBg: "#FFFF66",
        stroke: "black",
        fixwidth: this.width,
        fixheight: this.height,
        pad: 10,
    });

    this.helpModalContainer.addChild(this.modal.textbox);
    this.helpModalContainer.visible = false; //visible when player clicks on 'help' Button

    this.stage.addChild(this.helpButtonContainer, this.helpModalContainer);
}
