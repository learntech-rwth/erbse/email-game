/**
 * View for Subject + Date
 */
function subjectView(x, y, width, height, stage, email, targets) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.stage = stage;
    this.email = email;
    this.targets = targets; //contains suspicious targets

    var subject = this.email.subject[MTLG.lang.getLanguage()];

    this._subjContainer = new createjs.Container();

    //Subject
    if (subject) {
        this._subjTxtView = new Textbox({
            text: subject,
            x: this.x + 10,
            y: this.y + 10,
            colorBg: "#f2f2f2",
            font: "40px Arial",
        });
        this._subjTxtView.lineWidth = this.width - 20;
        //this._subjTxtView.textbox.name = "subj";

        this._subjContainer.addChild(this._subjTxtView.textbox);

        //add subject to 'targets' if it is suspicious
        if (this.email.isPhish && this.email.mark.subj.susp) {

            this.targets.push(this._subjTxtView.textbox);

            this._subjTxtView.textbox.reason = new Textbox({ //reason why it's suspicious. Will be shown on mouseover
                text: this.email.mark.subj.text[MTLG.lang.getLanguage()],
                x: this._subjTxtView.txt.x,
                y: this._subjTxtView.txt.y + 40,
                colorTxt: "yellow",
                colorBg: "#454545",
                pad: 5,
                font: "25px Arial",
                fixwidth: 300,
            });
            this._subjTxtView.textbox.reason.fixHeight();
            this._subjTxtView.textbox.reason.textbox.visible = false;
            this.stage.addChild(this._subjTxtView.textbox.reason.textbox);
        }
    }

    //Date
    var date = new Date(this.email.date);
    var lang = (MTLG.lang.getLanguage() == 'en') ? "en-US" : "de-DE"; //display date differently depending on language
    var dateString = date.toLocaleString(lang, { 'weekday': "short", 'day': "2-digit", 'month': "2-digit", 'year': "numeric", 'hour': "numeric", 'minute': "numeric" });
    
    this._dateTxtView = new Textbox({
        text: dateString,
        x: this.x + this.width - 5,
        y: this.y + 10,
        colorBg: "#f2f2f2",
        font: "20px Arial",
        rightaligned: true,
    });
    //this._dateTxtView.textbox.name = "date";

    this._subjContainer.addChild(this._dateTxtView.textbox);

    //add date to 'targets' if it is suspicious
    if (this.email.isPhish && this.email.mark.date.susp) {
        this.targets.push(this._dateTxtView.textbox);

        this._dateTxtView.textbox.reason = new Textbox({ //reason why it's suspicious. Will be shown on mouseover
            text: this.email.mark.date.text,
            x: this._dateTxtView.txt.x - 295,
            y: this._dateTxtView.txt.y + 40,
            colorTxt: "yellow",
            colorBg: "#454545",
            pad: 5,
            font: "25px Arial",
            fixwidth: 300,
        });
        this._dateTxtView.textbox.reason.fixHeight();
        this._dateTxtView.textbox.reason.textbox.visible = false;
        this.stage.addChild(this._dateTxtView.textbox.reason.textbox);
    }

}