//on click events for buttons



/**
 * This function is used to add an on click event for the new button if it's pressed. Mostly xAPI calls are made, tickers are turned off and the level selection screen is made visible
 * @function
 */
function addEventListenerNewButton(){
  this.folderPaneView.newButton.on('click', function (evt) {
    this.playSounds("feedback/button-selected");
    if (this.gameVisible == 1) {
      MTLG.emailClient.goToMenu(this.stage);
      return;
    }

    if (this.gameVisible != 2) {
      return;
    }

    createjs.Ticker.off("tick", this.tickerWrapperEmailHighlights); //remove Tick event (results on Mouseover)
    createjs.Ticker.off("tick", this.tickerWrapperParticipantHovers); //remove Tick event (results on Mouseover)
    createjs.Ticker.off("tick", this.tickerWrapperURLHovers); //remove Tick event (results on Mouseover)

    //pressing the new button either does nothing (during level selection), cancels a finished level or just brings you back to the selection (during a positive review)
    if(this.progData.currently == "creation" || this.progData.currently == "matching"){
      xapiButton({en:"new button", de:"Neu-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
      xapiStageCancel(this.progData.currently, this.progData.userId, this.progData.currentPhishingEmail.infos.name);
      xapiLevelCancel(this.progData.currentPhishingEmail.infos.name, this.progData.userId);

      this.initLevelSelection();
      this.manageLevelProgression();

      this.folderPaneView.buttonTextView.text = l("menu");

      return;
    }
    else if (this.progData.currently == "review"){
      xapiButton({en:"new button", de:"Neu-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
      xapiStageEnd(this.progData.currently, this.progData.userId, this.progData.currentPhishingEmail.infos.name); //xapi call
      if (this.progData.activeLevelPassed == true){
        if (this.progData.score > 0){ //if we have a score, save it
          xapiLevelEnd(this.progData.currentPhishingEmail.infos.name, this.progData.userId, this.progData.score); //xapi call
        }
        else{
          xapiLevelEnd(this.progData.currentPhishingEmail.infos.name, this.progData.userId); //xapi call
        }
      }
      else{
        xapiLevelCancel(this.progData.currentPhishingEmail.infos.name, this.progData.userId);
      }

      this.initLevelSelection();
      this.manageLevelProgression();

      this.folderPaneView.buttonTextView.text = l("menu");

      return;
    } else {
      xapiButton({en:"new button", de:"Neu-Button"},this.progData.userId); //xapi call
      if (this.progData.currently == "tutorial" && this.progData.activeLevelPassed == true){
        xapiStageEnd(this.progData.currently, this.progData.userId, this.progData.activeTutorial.name); //xapi call
        this.initLevelSelection();
        this.manageLevelProgression();

        this.folderPaneView.buttonTextView.text = l("menu");

        return;
      }
      else if (this.progData.currently == "tutorial" && this.progData.activeLevelPassed == false){
        xapiStageCancel(this.progData.currently, this.progData.userId, this.progData.activeTutorial.name); //xapi call
        this.initLevelSelection();
        this.manageLevelProgression();

        this.folderPaneView.buttonTextView.text = l("menu");

        return;
      }
      MTLG.emailClient.goToMenu(this.stage);
    }
  }.bind(this));
}

function addEventListenerInboxButton(){
  this.folderPaneView.folderClickableContainer.on('click', function (evt) {
    this.setNewEmailGameInterface(false);
    //MTLG.emailClient.goToMenu(this.stage);
    
  }.bind(this));
}

function addEventListenerMenuButton() {
  this.folderPaneView.settingsView.toMenuButton.on('click', function (evt) {
    this.folderPaneView.settingsView.settingsViewContainer.visible = false;
    MTLG.emailClient.goToMenu(this.stage);
  }.bind(this));
}

/**
 * This function is used to add an on click event for the sound button to deactivate sound if it's pressed
 * @function
 */
function addEventListenerSoundOnButton(){
  this.folderPaneView.settingsView.soundOnButton.on('click', function (evt) {
    this.progData.sounds = false;
    this.folderPaneView.settingsView.soundOnButton.visible = false;
    this.folderPaneView.settingsView.soundOffButton.visible = true;
    if (this.progData.currently == "selecting"){ //no currently active level
      xapiButton({en:"soundOn button", de:"SoundAn-Button"},this.progData.userId,this.progData.currently); //xapi call
    }
    else{
      xapiButton({en:"soundOn button", de:"SoundAn-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
    }
  }.bind(this));
}

/**
 * This function is used to add an on click event for the sound button to activate sound if it's pressed
 * @function
 */
function addEventListenerSoundOffButton(){
  this.folderPaneView.settingsView.soundOffButton.on('click', function (evt) {
    this.progData.sounds = true;
    this.playSounds("feedback/button-selected");
    this.folderPaneView.settingsView.soundOnButton.visible = true;
    this.folderPaneView.settingsView.soundOffButton.visible = false;
    if (this.progData.currently == "selecting"){ //no currently active level
      xapiButton({en:"soundOff button", de:"SoundAus-Button"},this.progData.userId,this.progData.currently); //xapi call
    }
    else{
      xapiButton({en:"soundOff button", de:"SoundAus-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
    }
  }.bind(this));
}

/**
 * This function is used to add an on click event for the click button to deactivate clicks during email creation if it's pressed
 * @function
 */
function addEventListenerClickOnButton(){
  this.folderPaneView.settingsView.clickOnButton.on('click', function (evt) {
    this.progData.clicksEnabled = false;
    this.dragButtonListener(this.folderPaneView.levelView.newEmailPaneView.choiceOptions,false);
    this.playSounds("feedback/button-selected");
    this.folderPaneView.settingsView.clickOnButton.visible = false;
    this.folderPaneView.settingsView.clickOffButton.visible = true;
    if (this.progData.currently == "selecting"){ //no currently active level
      xapiButton({en:"clickOn button", de:"KlicksAn-Button"},this.progData.userId,this.progData.currently); //xapi call
    }
    else{
      xapiButton({en:"clickOn button", de:"KlicksAn-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
    }
  }.bind(this));
}

/**
 * This function is used to add an on click event for the click button to activate clicks during email creation if it's pressed
 * @function
 */
function addEventListenerClickOffButton(){
  this.folderPaneView.settingsView.clickOffButton.on('click', function (evt) {
    this.progData.clicksEnabled = true;
    this.dragButtonListener(this.folderPaneView.levelView.newEmailPaneView.choiceOptions,false);
    this.playSounds("feedback/button-selected");
    this.folderPaneView.settingsView.clickOnButton.visible = true;
    this.folderPaneView.settingsView.clickOffButton.visible = false;
    if (this.progData.currently == "selecting"){ //no currently active level
      xapiButton({en:"clickOff button", de:"KlicksAus-Button"},this.progData.userId,this.progData.currently); //xapi call
    }
    else{
      xapiButton({en:"clickOff button", de:"KlicksAus-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
    }
  }.bind(this));
}

/**
 * This function is used to add an on click event for the settings close button so that it closes the options window if it's pressed
 * @function
 */
function addEventListenerSettingsCloseButton(){
  const onClick = function (evt) {
    this.folderPaneView.settingsView.settingsViewContainer.visible = false;
    if (this.progData.currently == "selecting"){ //no currently active level
      xapiButton({en:"settingsClose button", de:"OptionenSchließen-Button"},this.progData.userId,this.progData.currently); //xapi call
    }
    else{
      xapiButton({en:"settingsClose button", de:"OptionenSchließen-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
    }
  }.bind(this);

  this.folderPaneView.settingsView.closeButton.on('click', onClick);
  this.folderPaneView.settingsView.bigCloseButton.on('click', onClick);
}

/**
 * This function is used to add an on click event for the finish close button so that it closes the options window if it's pressed
 * @function
 */
function addEventListenerFinishCloseButton(){
  this.folderPaneView.finishNewGameView.closeButton.on('click', function (evt) {
    this.folderPaneView.finishNewGameView.finishNewGameViewContainer.visible = false;
    this.folderPaneView.finishNewGameView.speechContainer.visible = false;
    if (this.progData.currently == "selecting"){ //no currently active level
      xapiButton({en:"finishClose button", de:"EndeSchließen-Button"},this.progData.userId,this.progData.currently); //xapi call
    }
    else{
      xapiButton({en:"finishClose button", de:"EndeSchließen-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
    }
  }.bind(this));
}

/**
 * This function is used to add an on click event for the easy difficulty button to change the difficulty to medium (when the next level is started) if it's pressed
 * @function
 */
function addEventListenerEasyButton(){
  this.folderPaneView.settingsView.difficultyEasyButton.on('click', function (evt) {
    this.playSounds("feedback/button-selected");
    this.folderPaneView.settingsView.difficultyMediumButton.visible = true;
    this.folderPaneView.settingsView.difficultyEasyButton.visible = false;
    this.progData.newDifficulty = "medium"; //we only want to change the difficulty at the start of each level not during a level (or the players could cheat by starting with easy and finishing with hard)
    if (this.progData.currently == "selecting"){ //no currently active level
      xapiButton({en:"difficultyEasy button", de:"SchwierigkeitsgradLeicht-Button"},this.progData.userId,this.progData.currently); //xapi call
    }
    else{
      xapiButton({en:"difficultyEasy button", de:"SchwierigkeitsgradLeicht-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
    }
  }.bind(this));
}

/**
 * This function is used to add an on click event for the medium difficulty button to change the difficulty to hard (when the next level is started) if it's pressed
 * @function
 */
function addEventListenerMediumButton(){
  this.folderPaneView.settingsView.difficultyMediumButton.on('click', function (evt) {
    this.playSounds("feedback/button-selected");
    this.folderPaneView.settingsView.difficultyHardButton.visible = true;
    this.folderPaneView.settingsView.difficultyMediumButton.visible = false;
    this.progData.newDifficulty = "hard"; //we only want to change the difficulty at the start of each level not during a level (or the players could cheat by starting with easy and finishing with hard)
    if (this.progData.currently == "selecting"){ //no currently active level
      xapiButton({en:"difficultyMedium button", de:"SchwierigkeitsgradMittel-Button"},this.progData.userId,this.progData.currently); //xapi call
    }
    else{
      xapiButton({en:"difficultyMedium button", de:"SchwierigkeitsgradMittel-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
    }
  }.bind(this));
}

/**
 * This function is used to add an on click event for the hard difficulty button to change the difficulty to easy (when the next level is started) if it's pressed
 * @function
 */
function addEventListenerHardButton(){
  this.folderPaneView.settingsView.difficultyHardButton.on('click', function (evt) {
    this.playSounds("feedback/button-selected");
    this.folderPaneView.settingsView.difficultyEasyButton.visible = true;
    this.folderPaneView.settingsView.difficultyHardButton.visible = false;
    this.progData.newDifficulty = "easy"; //we only want to change the difficulty at the start of each level not during a level (or the players could cheat by starting with easy and finishing with hard)
    if (this.progData.currently == "selecting"){ //no currently active level
      xapiButton({en:"difficultyHard button", de:"SchwierigkeitsgradSchwierig-Button"},this.progData.userId,this.progData.currently); //xapi call
    }
    else{
      xapiButton({en:"difficultyHard button", de:"SchwierigkeitsgradSchwierig-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
    }
  }.bind(this));
}

/**
 * This function is used to add an on click event for the settings button to make the settings menu visible if it's pressed
 * @function
 */
function addEventListenerSettingsButton(){
  this.folderPaneView.settingsButton.on('click', function (evt) {
    if (this.folderPaneView.finishNewGameView.finishNewGameViewContainer.visible == false){ //wait until the gameover screen is closed until you allow the level selection
      this.playSounds("feedback/button-selected");
      if (this.folderPaneView.settingsView.settingsViewContainer.visible == false){ //show settings menu
        this.folderPaneView.settingsView.settingsViewContainer.visible = true;
        this.stage.setChildIndex(this.folderPaneView.settingsView.settingsViewContainer, this.stage.numChildren - 1);
      }
      else{ //make settings menu invisible
        this.folderPaneView.settingsView.settingsViewContainer.visible = false;
      }
      if (this.progData.currently == "selecting"){ //no currently active level
        xapiButton({en:"settings button", de:"Optionen-Button"},this.progData.userId,this.progData.currently); //xapi call
      }
      else{
        xapiButton({en:"settings button", de:"Optionen-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
      }
    }
  }.bind(this));
}
