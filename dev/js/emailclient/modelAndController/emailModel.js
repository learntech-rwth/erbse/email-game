//Array containing email json objects and email properties

var emails = [];

//Studydrive
emails.push({

	date: 'Mon, 15 Jun 2020 14:29:46 +0000 (UTC)',
	subject: 'AirPods Pro für deine Hilfe bei Karriere-Umfrage',
	from: { name: 'Studydrive', email: 'info@studydrive.net' },
	to: { email: 'duygu98@web.de' },
	headers: {
		'Return-Path': '<bounces+36625-eb1f-duygu98=web.de@putsmail.litmus.com>',
		Received: [
			'from o1.putsmail.litmus.com ([50.31.42.65]) by mx-ha.web.de\r\n' +
			'(mxweb110 [212.227.17.8]) with ESMTPS (Nemesis) id 1MkGA7-1j4fgL0XPN-00kbyb\r\n' +
			'for <duygu98@web.de>; Mon, 15 Jun 2020 16:29:48 +0200',
			'by filter0073p3las1.sendgrid.net with SMTP id filter0073p3las1-19600-5EE785DA-58\r\n' +
			'2020-06-15 14:29:46.435541299 +0000 UTC m=+591587.850446486',
			'from putsmail.com (unknown)\r\n' +
			'by geopod-ismtpd-5-1 (SG) with ESMTP\r\n' +
			'id yXMAreQJTPmfuCl-b-e6JA\r\n' +
			'for <duygu98@web.de>;\r\n' +
			'Mon, 15 Jun 2020 14:29:46.259 +0000 (UTC)'
		],
		'DKIM-Signature': 'v=1; a=rsa-sha256; c=relaxed/relaxed; d=litmus.com;\r\n' +
			'h=from:subject:mime-version:to:content-type:content-transfer-encoding;\r\n' +
			's=s1; bh=ykSxt4dQjU/4RVsFE3MqtIHiKYn7K+aglnuVZf6Shek=;\r\n' +
			'b=coho5loGpYIhlGeM6cTj8f6EBnEfRbgLu5v6uWlLTl0iX7XFTIiL0maa2sN1VzloDM77\r\n' +
			'LAs8oMtzoew9WifcrrDDJoG4f0tw26xh30694e3hz2cIxEj8vN/zldKNhFbJ3/viYYeN4Q\r\n' +
			'UoT7W7NmkKwPLLCuU9u1VyAFo8o6rOmIM=',
		Date: 'Mon, 15 Jun 2020 14:29:46 +0000 (UTC)',
		From: 'info@studydrive.net',
		'Message-ID': '<5ee785da38b55_43fc31c002d543548bb@3764eba9-3e48-45fc-89c8-df649a18533a.mail>',
		Subject: 'AirPods Pro deine Hilfe bei Karriere-Umfrage',
		'Mime-Version': '1.0\r\n' +
			'=?us-ascii?Q?Rz3Ii86sRfqa7ZIB5m+y1K5F1txMnwcycjLBt5hUtfaNsHdE1o6V4ddHCrmKTe?=\r\n' +
			'=?us-ascii?Q?foZBn1QJq6nN2SW5ThzenJPo0iBotion0nzjvSV?=\r\n' +
			'=?us-ascii?Q?3bxbuJKpE6BI3KmI4FDfZbgeOxKtQ7CzsR=2Fe3z3?=\r\n' +
			'=?us-ascii?Q?+52EddRO8Lcxf5v8wV=2FnG8eBsIBzu93jDRk=2F+jN?=\r\n' +
			'=?us-ascii?Q?dUeNZkkpd8=2FeS9qfj181MjnlKTBXNlCT5pA=3D=3D?=',
		To: 'duygu98@web.de',
		'Content-Type': 'multipart/alternative;\r\n' +
			'boundary="--==_mimepart_5ee785da381b6_43fc31c002d543547e1";\r\n' +
			'charset=UTF-8',
		'Content-Transfer-Encoding': '7bit',
		'Envelope-To': '<duygu98@web.de>',
		'X-UI-Filterresults': 'unknown:5;V03:K0:ebjj+wgw63A=:GV22SdT3IqMExtQ1bEDp8lb+/g\r\n' +
			'WjRQKGUFFkjLcMRzKvjD9xjaJtO9TMaBIz1a6NBU03IpPjyqkKNe6p6CIQn6xofOY+Ey5bAmY\r\n' +
			'MxEOPHYGzcYQwRx1K2Qb5RgtyZ0IlomhKw7sWQ/sMyMiFASn/fmvetD73b5HGGitXAO7SWStz\r\n' +
			'Ya7HkQp75n0JMDci56gO6gWL4CjbbD+zp6LvS18zQjuIE050dAdULn++0nUqEB/Zw1OZbkSJG\r\n' +
			'AN47vfcsgAhnvgfD9Tw+FztuuuYQRwyrsRFy09pMFYX0hnXQ9NuKAMHXxMznIEMfAyiJZSmW+\r\n' +
			'FRyNjSwd/UBY8KJOLlAoDWRYvFu7F8XyLx7dNW7TVMy9RIZoD34mo7o1w/JulPHhdVTQQ/3fI\r\n' +
			'9M99p1U8npI9wIT6B1k9AMdVnTXqeXX8X/zGMTOS13iFcZ/qI779zayEGzJ3dbE+sHEk9iXlR\r\n' +
			'n708wGQRCt+HH0JjqP7O0vnoRzWCK5ilRVb8zf3JQWAoLxiM0b/Meni0KLtL+OcPgEz3x+cNv\r\n' +
			'QxjL5JaqkduyA80pBjH8nncY4QOnKVzk9GzmQgI3Br1ABmAGmvAKPr/olvHtyBWRkfXNHLiIU\r\n' +
			'Nn+2+JesTTbA07iMpjvTHblqz2qSHGa9w1sL1aD2KIiA2nFELbMxTKPhXnH0l4PvBb6lEbMsX\r\n' +
			'jEBbmBXP+qdn/rlUcgcaZmZe1wCm2vU1ipKApRVJofOcGcaUr0qKpNm7UF4k0s0CBGmz8n6UZ\r\n' +
			'uyWLyAkhAtsbsVn6cZLh31K5L4BrUqXfWEGYgR/x/2JlPr+Qg5DIDDGIsjMssuBXW+l6JG2NS\r\n' +
			'MRImxUew8EBJU+mFQzUQul7d98g6l7qaA5hlrANwVb6Wg6WAOUiFEE+igJQH25PyDpG9dkNuI\r\n' +
			'pgvM2ENRmgYvJPrTfAmla47Qff30UZeUPpqj9Ufg4I9SuxbjaRhJ+PA0e0FhuJ40Th+caWkk7\r\n' +
			'VUF4kc7IlxRNEwHKKfOIXltx+Axm4gr9/s2nKOk8jwYVxZTEGcamTb94EUJLxJ1Q8/Rkw+cL2\r\n' +
			'jb+Myn6gi7/qBbAAYfLFJZFkuF4PHrtsLmozWpabL5rzhKwzU3N3HaQ+eo3K77Dh8hHS3da5Z\r\n' +
			'G9H5y+vaDYTjev5eFtvdyrw1R6J9yY0AFimI9DlIllemBviVjsFy6t/cFYqorXeEv4TV73aTu\r\n' +
			'VYj43MsXblfju15jgb6xn0Ht9dvcC6EDjaaFfZSTR/gJD9HJf8B7n7Ci1TK6Iar9pz4pdBdnU\r\n' +
			'S6wbbb8vAA7B87Kd8uNohj3iSaNp1j/Uw3bD/rKSiqCgXVcNEjPtpimRsfC7Dh5fVl7nBnZiu\r\n' +
			't84CFUoM1U2L7DIEajPgaeD/WEiA2BxXOY5hqmUIgLVk9xjmsuHjQhjQvqymCdmGM+xfybgvI\r\n' +
			'FJYBPYxc3hYvqa3vLbM9Dor5jm41I4zUY2om76LFCQ5Ebm18yPSQfISOw7ZVM++hI5TXYbJVe\r\n' +
			'/OkIxeMOWwaVl5rnZ56yGIO2ccNBqZCcgg4VdoBmcd+93UBHzkzdQQ1eOJyCIkcPAhu0sEEQD\r\n' +
			'RSFn8Hi6uDceo9fsGpyBS8EesAKLNhBVbDSIrrnVgPi1spH5dEYPBXc/H+8IIoIFRdTgswbv1\r\n' +
			'BolN0AobqFzZ6JZDJFvH8IAM1IiWTkpDfbOl3EpiSziacaBXGy2Jp7UnnZNHWmE36uaDd8dX2\r\n' +
			'IrJhcGI4BndSPiZPX3wokMun52kNmpJPA5Qb7/o3uxiumji0CVJXR8HY7JWK2dlyFqAb5j9Jy\r\n' +
			'nwYnhiu+5wBfrTJtIngc5Hr56p+VwGuPCCUSLILrJdf/3ngO1ncQ/D8TPfYBZCny/NQhTzFAU\r\n' +
			'bk5SpdtxUEu3SZoGMMXqxewM7QVNyZ2hQ99sYY5qKPXrpL3xImT7N8Dyq7K/XZLEDAsW7Awdd\r\n' +
			'ZtdB61uvs6UVLCG8RxVwwUqI1+TyYinesRkLDDjLbGUstXv9t5dZlq2UyXkeeotFMbwOVQ+so\r\n' +
			'MJ5qKFqThsjqaOo57NAb3NbSNGrYJ4GgoiXUBepPhNmpNtpgUNXo3BmUo4Y2qr9lkmAgbl9Mp\r\n' +
			'Coz3wTjWiYDavv5GQ5vFiy5nUL5W+18l7tupkgJyxMSUd6pWlKG2oILqrLojg42aPlgTK7rP4\r\n' +
			'fiZ0DjaMMqySn/ST671SfiOBJzz6DvoaaINm+cKmNDnDlhnzNBH1LyOMfMBrXQ9XBZZZicMbD\r\n' +
			'gQLH1fPOxSmtVjffNh7HXb0s0eeSqfEvAvqGlc3vZJeNfh+HMkicV8dy+Gn3ao6YVSGRFmDx3\r\n' +
			'8koAwPfzouCLfIsxDmgIq4wUy1EeIzEgpl1ryt5EIK7eeThHv9cm6OUoMzIwsVFHPNMMCqJiQ\r\n' +
			'1MirJ4s1SutqyXxGZXh1veI8BhAwAD8mzoQ6vMRDavO9vpv66WlA7MWlBQz7hMH+7aZ0VnHek\r\n' +
			'sXZGdesXt5u2vUHDf2o/7xGpwMl/G3XDYZoFN12PviCpg1wS57u2/oD4uVvjADl5HHPms+xzt\r\n' +
			'uGbcgBMcaN/3j61+skXGhY6QYplONLlDypesCi0yhgtBU30Szwc91wodKCkuwHgmcQFrUymr7\r\n' +
			'9Org77NKyDEt0jtVr3zZQGz+DNU2EL2GU9J1hEN6gxQA/JYSB5Af6+ygGrpSfldy34IHJ8U7j\r\n' +
			'INGEksYDm4SESftH51CiZIDo2nQUi5cbRxlHqaS50FU6yfvLMtFEzu16X8dD63wYQPWC9h8JX\r\n' +
			'/5HFh2XNvnMrFAxyZ+hKCwwElSGeRatz8wSz7vCbt5XQvGyqgHuma393R17LvZHWFNz/Szx/S\r\n' +
			'+3zXsIBQyjpGtYV7srCfY9n02aUf61c495SrD+j/3YFLi8p+Imfn0Xs5O9ZMDJ/KqQa3WE113\r\n' +
			'PeC3d+a17+BsxZhiTfl0JSaMz4ylA=='
	},
	text: '',
	html: 'Hi!\r\n' +
		'<br><br>\r\n' +
		'Wir wollen dich bei der beruflichen Orientierung unterstützen - dafür brauchen wir deine Hilfe. Nimm an unserer 10-minütigen Umfrage teil und gewinne tolle Preise, wie z.B.Airpods Pro! Uns interessiert: \r\n' +
		'<br><br>\r\n' +
		'Wie gut fühlst du dich über deine beruflichen Möglichkeiten informiert?\r\n' +
		'Wie orientierst du dich und was ist dir für den ersten Job nach dem Studium am wichtigsten?\r\n' +
		'<br><br>\r\n' +
		'<a href="http://www.email.studydrive.com/umfrage">Teilnehmen & gewinnen!</a>\r\n' +
		'<br><br>\r\n' +
		'Unter allen Teilnehmenden verlosen wir tolle Preise. Investiere 10 Minuten und gewinne mit ein wenig Glück: 1x Apple AirPods Pro.\r\n' +
		'<br><br>\r\n' +
		'Wir freuen uns auf deine Teilnahme!\r\n' +
		'<br>\r\n' +
		'Dein Studydrive Team\r\n',

	'isPhish': false,
	'isEncrypted': false,
	'isSigned': false,
	'help': 'Führt der Link tatsächlich zu Studydrive?\nDas findest Du heraus, wenn du die Maus über dem Link bewegst.',
	'reason': "Es handelt sich hierbei um eine Werbe-Mail.\n" +
		"Diese Mail ist zwar, wie Phishing-Mails auch, unerwünscht, aber nicht gefährlich.\n\n" +
		"Es werden keine persönlichen Daten abgefragt oder sonstige böswillige Aktionen beabsichtigt.\n\n" +
		"Der Absender ist tatsächlich Studydrive und der Link führt zu 'studydrive.com'.",

});
//Dropbox
emails.push({

	date: 'Mon, 15 Jun 2020 13:21:54 +0000 (UTC)',
	subject: 'Ihre Dropbox ist voll',
	from: { name: 'Dropbox', email: 'no-reply@dropboxmail.com' },
	to: { email: 'duygu98@web.de' },
	headers: {
		'Return-Path': '<bounces+36625-eb1f-duygu98=web.de@putsmail.litmus.com>',
		Received: [
			'from o1.putsmail.litmus.com ([50.31.42.65]) by mx-ha.web.de\r\n' +
			'(mxweb010 [212.227.15.17]) with ESMTPS (Nemesis) id 1M9Xua-1jo6JL38bM-005b3g\r\n' +
			'for <duygu98@web.de>; Mon, 15 Jun 2020 15:21:56 +0200',
			'by filter0135p3las1.sendgrid.net with SMTP id filter0135p3las1-31150-5EE775F1-D3\r\n' +
			'2020-06-15 13:21:54.004083793 +0000 UTC m=+587194.992273888',
			'from putsmail.com (unknown)\r\n' +
			'by ismtpd0005p1iad1.sendgrid.net (SG)\r\n' +
			'with ESMTP id ctWwtz0qS5ui0WDGObUL7g\r\n' +
			'for <duygu98@web.de>;\r\n' +
			'Mon, 15 Jun 2020 13:21:53.782 +0000 (UTC)'
		],
		'DKIM-Signature': 'v=1; a=rsa-sha256; c=relaxed/relaxed; d=litmus.com;\r\n' +
			'h=from:subject:mime-version:to:content-type:content-transfer-encoding;\r\n' +
			's=s1; bh=xGvBVlWbychh2O8EVes5DnY43vADMqYmDCSdUGapYD4=;\r\n' +
			'b=NLUtHy4yrWUDTfzT9zGgnYOq3N+4yYP8YqOxBonQgCI83ljEy8XkFOURgmCqqqxnbZWp\r\n' +
			'D4zpVUOSX9pCGT+/+Sw5s9iIfHV+HQhWxyU/oAzzSKqXwW36PBcCCtms54NbJuMHtsMKWn\r\n' +
			'gkyYoKV7VbOX3B/ib7VzIUcE5/cxpKdpI=',
		Date: 'Mon, 15 Jun 2020 13:21:54 +0000 (UTC)',
		From: 'putsmail@putsmail.litmus.com',
		'Message-ID': '<5ee775f1b0eb8_43fc31c002d5425705e@3764eba9-3e48-45fc-89c8-df649a18533a.mail>',
		Subject: 'Ihre Dropbox ist voll',
		'Mime-Version': '1.0\r\n' +
			'=?us-ascii?Q?Rz3Ii86sRfqa7ZIB5m+y1K5F1txMnwcycjLBt5hUtfaNsHdE1o6V4ddHCrmKTe?=\r\n' +
			'=?us-ascii?Q?foz3zw7ZKhHp6ogXHyjS5dwjPVC=2FUMWjGnTR3RI?=\r\n' +
			'=?us-ascii?Q?GNZHPvey1hVFhsgKKWmgV14CBPCDrYmkscyQC+g?=\r\n' +
			'=?us-ascii?Q?FC=2FJ2cfhe+zJKXtEVQWtpEauyRJqL9Lfv40J+CB?=\r\n' +
			'=?us-ascii?Q?pv8R1AVj=2FpiB6EpWjrlBm5CJf43xKR3xpPw=3D=3D?=',
		To: 'duygu98@web.de',
		'Content-Type': 'multipart/alternative;\r\n' +
			'boundary="--==_mimepart_5ee775f1afeb1_43fc31c002d5425699d";\r\n' +
			'charset=UTF-8',
		'Content-Transfer-Encoding': '7bit',
		'Envelope-To': '<duygu98@web.de>',
		'X-UI-Filterresults': 'unknown:2;V03:K0:s7NKtEDR6aY=:TtdqI7wbYJufJuoLB6RYTjASK/\r\n' +
			'O3v4gsJSdNpbxsdJ3iOPJqWwzl/qWl6/pZNl82BhCNBznLzuXh+Bfu59xwLNUf54B/aL9NrzC\r\n' +
			'wUuZEKUivgyp8eANdsJp67UXZqhFFy+6zYtGeuMluM2UAz31WhEW/rTbRnMIp2Ju+6FDC7naK\r\n' +
			'6lGbzMXpaQn6tArg1KUndJJJ5TRJEbOm0mO0Idlkw2gsqEreggftOTenUpMaomDe/oZTGbTmR\r\n' +
			'QwSrGLUOQHh0tE/FWPgTURqO8TGfGfMu9g09epSB4BdNdKRPkgZgnl4sQwi4b2FaSSnZ6sAXp\r\n' +
			'7TmCFYp5Q/oBBjG7gzRkriK0btrL/+JWlBo1Lkm+MjgRxGF1jGVv4gUi8QfV2JgGwp7wZLtXQ\r\n' +
			'NYkcskwRwzg3P06OvAXsma1TynlVEFBO3sX+kl39SOs+0rmjoVNx5OhXugJQlrDnR5bolc6q/\r\n' +
			'vwBW//bR9X8zkaqGgfjYwppY53E++0jTGbiqV/OBYzau3wuwcUyu3ZLderb00DKv1mHWXdYJE\r\n' +
			'lMfcILEJvvZnHAFf7vIv9Cn0aAhYwMC8ylz+20o/7hE7/V+SCZdk+rZAUuV8G+A562dg1gslF\r\n' +
			'D6YZmSTS8Zj7zw7Is6cwLtnuBTdIxZAbdw/THq5xkfc8BqQke/MQnMyykCR4Mo8XmUKTTXdFv\r\n' +
			'UT6hUcVtKInVo2B2GvTMbyZjmIUAlkmLOqEjCNvCwjn6u+oyvibOSqgLOGBoAdsNkqxkyHEZv\r\n' +
			'YywT0JiXktnT7gg2zkvJCqY+Rnq/qJ6O9OvfUjYy8h8GwofEK983Ly7WkilVygqytuu3Xz6yh\r\n' +
			'fnA1Tgz+/GCa5anx1ohhD9PF0DCtO4hLA7/iBTzvllFm3xEyALGQtbOkBmZ9jD0LXLjLsDKKm\r\n' +
			'C7aqyjUGrPy3ma5DHEs+B6tYSQSAOV7XtTNX6/b3T1Q6oSNv07bsabVvm96J5iLjzYHOCGXrf\r\n' +
			'bD6jsgAkABNH2LN5IVLpKNC2L08itQlb3V0tRnkzsSYh/9JfNnrpf8ID0g4lFnY6snHIYgLkg\r\n' +
			'I9YlqKOPFDS7CjcAm5rRi4ZHUIdrcCyNm31uf1I07RYUeL7qVIAniomGU/GcBUADXulxL2wX7\r\n' +
			'gdnwlDbmMkZB0h+FqelJqaqAtgt6x37BVOqO540SOYzxueI4jDHQ4EWdn3WmV7UVvubSeUrHj\r\n' +
			'sdlShHsLCYWP3dhYFGEwZp3Ylw9Wn7eWdvIuPqraoaEshxTm3aw7Rcu6FmsHcNbNk3j3+qW9B\r\n' +
			'SjBJmpumWgN+3dtQl5CZ2kjw0wwL3xXESQtwo8010UgFnxg7sCBaWNVlksF8K2vbmf8SjumAT\r\n' +
			'0M81jddRMpV24Q059WXMTHGDPYs7XYY+tZogIKMb5nv5IFCkOCntj29hjUBY2NF8ftFZTk+QS\r\n' +
			'2mF8/TjVi+OeEI47wkARo1i26lpbaXIqUjjcykTrnx77i/0jN0vHpTodt8HOOWfjP7Z2DT7vY\r\n' +
			'jmkfmo/tk8AaUf3b20iGrkd4A0LkRmj4J7wfOIWIcpB1Qelowtsn2YCgrvbfROuoIg+cDcCcv\r\n' +
			'PLXkZtYV3Tc/h/hsFsVQMkimMVTKYrGdSa+epiv824H2Gn+eORbwnxrXU2kM5bcp04FLQ0KJ6\r\n' +
			'pjUHDhfrLpjO3HYudkNvBtApFjfZfIwr1n6iBHyOiZjBI8F7NvUbRsVBypFOlFDmrB++h3G0v\r\n' +
			'NCWDavcek7aRTx7Ll9Wsaz+SxzxvS9O6MgW5CJbWVQhntvd8o7zeX3CyZCkNl37vUOKVQow8R\r\n' +
			'ja5wCo/FowqbcynxdJGCcfxB3GgKjEMUkljjQNA2x12IaJYL8CfOHWZkZKUcgyZbBDK/99qMm\r\n' +
			'bOGr3Cd/hSKMbGQMYQgqlEjHvbBM17gzJPzZFgYn3CVIlNGAEfoYnt/Ri4AtOBB4yEEVA26do\r\n' +
			'CgV05UZNwhzDxbMdsXV/GNMAjW8zPg8kk1FUXtW20rRVmm4hyOLR7Y+ViADZJcIZ4k085qFJa\r\n' +
			'SjRZErjxGawLB+EJH/2GbZg3MLLybqWGyk4gZxfpQzxjTCaVlNpFPNm+6/jBwl28Vg870GYah\r\n' +
			'kGAXpbe9A/mwzaelHXMd4cqQOx0LlXDV7PBsTp9TsmXkFK1fMPZX6P//RWmkrPJb4Aq1lzsBX\r\n' +
			'2ttj1n7CAWTPVeuLP4prR/Jba+V8X17HRTvII+7r/h+pPOwNZTBUSy4e8L0/cujOyiOyOPYku\r\n' +
			'mNNwYllVq0T9iXJ8/YOSC+leEOY7hklfbQY1TLBziM61OpKitJDxrZtKnnuDFTds+Y8e/kYNR\r\n' +
			'uDFOrxDqDprAlcxoaMU5Svtaq1P7dg7T6BYSadmyFucv+1V7fUw+4vK7naLp4kC0WrhGKCm11\r\n' +
			'XuFmPk1ExyB8xxowhs8VuHDGf5EeLf7WQ5PhuS6BSu41fj2mfCsDLa1F2fh3Z+vntHgfMy2Mv\r\n' +
			'1GjfR5fXgKMNVAleOPTLrOOBMwMon8WaN0MNu2EYigQQgN2WdfV74WJ5r7mznasq9BxrdUaJ5\r\n' +
			'zkO9SoxU3fgrRnqL2y2ZBxF2MRtanlyciQKS4wTZlbaODztiWhaWtEvr6dJZ/am8HcDKFBzaO\r\n' +
			'YxivNGKLqsPs7NWmAmWe7o1Wtpv+chSaInueh+Bx2HwarhEsMNl7Ar265C7aSEalZcOeIh5+n\r\n' +
			'Qp/BcvzQERTBCFKnERvApuhWRYB9JND+NCOenKekcYq2YZgqMJMIEQnK6E8sxkQhOB3UCS9Kg\r\n' +
			'sJi2agfITpz3ZmmPkC6qQbJnaH9EB0TAEdhhcoWRzhlJWHL509I/X0KPrDOzB5Pc+waawRO1X\r\n' +
			'sZK1YG+C1QbBJ96e2sXbB7XmvZQLw65OWSOT2ZfrjyZcU9UUKcMEWH/yYpke8SnFyDxxuDZwW\r\n' +
			'AwAUtmWCivsOcMjS70jBO6K2iPYaABVe4QrUw1RrrihFCQM/J0='
	},
	text: '',
	html: 'Hallo,\r\n' +
		'<br><br>\r\n' +
		'Ihre Dropbox ist voll und die Dateien darin werden nicht mehr synchronisiert. Neu hinzugefügte Dateien sind nicht auf anderen Geräten verfügbar und werden auch nicht online gesichert.\r\n' +
		'<br><br>\r\n' +
		'Wenn Sie noch heute ein Upgrade für Ihre Dropbox ausführen, erhalten sie 1 TB Speicherplatz und Zugriff auf unsere effiziente Freigabefunktion.\r\n' +
		'<br><br>\r\n' +
		'<a href="https://www.dropbox.com/buy">Upgrade ausführen</a>\r\n' +
		'<br><br>\r\n' +
		'Weitere Möglichkeiten, den Speicherplatz zu erweitern, finden Sie auf der Seite <a href="https://www.dropbox.com/help/space/get-more-space">Speicherplatz erhöhen</a>.\r\n' +
		'<br><br>\r\n' +
		'Viel Spaß mit Dropbox!\r\n' +
		'<br>\r\n' +
		'Das Dropbox-Team\r\n',

	//props
	'isPhish': false,
	'isEncrypted': false,
	'isSigned': true,
	'help': "Führt der Link tatsächlich zu Dropbox?\nDas findest Du heraus, wenn du die Maus über dem Link bewegst.",
	'reason': "Eine kurze Suche nach 'dropboxmail.com' genügt, um herauszufinden, dass 'dropboxmail.com' eine offizielle Domain von Dropbox ist und diese E-Mail somit echt ist.\n\n" +
		"Die URLs beider Links führen zu 'dropbox.com', was ebenfalls eine offizielle Domain von Dropbox ist.",

});
//Sicherheitswarnung google
emails.push({
	date: 'Sat, 13 Jun 2020 14:08:57 +0000 (UTC)',
	subject: 'Sicherheitswarnung',
	from: { name: "Google", email: 'no-reply@accounts.google.com' },
	to: { email: 'duygu98@web.de' },
	headers: {
		'Return-Path': '<bounces+36625-eb1f-duygu98=web.de@putsmail.litmus.com>',
		Received: [
			'from o1.putsmail.litmus.com ([50.31.42.65]) by mx-ha.web.de\r\n' +
			'(mxweb112 [212.227.17.8]) with ESMTPS (Nemesis) id 1MtvUs-1iuC0c29qW-00uF0s\r\n' +
			'for <duygu98@web.de>; Sat, 13 Jun 2020 16:08:58 +0200',
			'by filterdrecv-p3iad2-784dbb6bd8-gkg68 with SMTP id filterdrecv-p3iad2-784dbb6bd8-gkg68-19-5EE4DDF9-10\r\n' +
			'2020-06-13 14:08:57.183189229 +0000 UTC m=+842528.996443841',
			'from putsmail.com (unknown)\r\n' +
			'by ismtpd0001p1iad1.sendgrid.net (SG)\r\n' +
			'with ESMTP id BOzq-T4cSoyGL8SsHqDDsw\r\n' +
			'for <duygu98@web.de>;\r\n' +
			'Sat, 13 Jun 2020 14:08:57.096 +0000 (UTC)'
		],
		'DKIM-Signature': 'v=1; a=rsa-sha256; c=relaxed/relaxed; d=litmus.com;\r\n' +
			'h=from:subject:mime-version:to:content-type:content-transfer-encoding;\r\n' +
			's=s1; bh=Zhar8Iuf8RJhnxalxAblYhkae9edmOU3J4E5zyF7KHQ=;\r\n' +
			'b=Y2NNKit07JSj1pQ63/vk/hT94zRGx4VKI0QxpAZWoeENPkLxROieN401a3cy2S+lBWwZ\r\n' +
			'hNtBddoxqyDu08Psu1nVVcVNP7O3JcUjG37Ce5hhfnzsDSThPsF7M+o2m4IGKIsWTKQ3JX\r\n' +
			'PpAu7zij8DYZsUuUbC58N/YXkfymqbf7g=',
		Date: 'Sat, 13 Jun 2020 14:08:57 +0000 (UTC)',
		From: 'no-reply@accounts.google.com',
		'Message-ID': '<5ee4ddf9f97e_43fd0ee01c8c81572b0@7687a9b0-8521-4408-9fdb-683b73c5c1da.mail>',
		Subject: 'Sicherheitswarnung',
		'Mime-Version': '1.0\r\n' +
			'=?us-ascii?Q?Rz3Ii86sRfqa7ZIB5m+y1K5F1txMnwcycjLBt5hUtfaNsHdE1o6V4ddHCrmKTe?=\r\n' +
			'=?us-ascii?Q?foJ7wcfTTbT59HEuRPm7QgPb3oFJG1HnhWCQxM5?=\r\n' +
			'=?us-ascii?Q?A7VKET7jkgCp=2FGgFz7DsUO6U2gaar9YEmstjdau?=\r\n' +
			'=?us-ascii?Q?8Ym1DBVhlcKUs4RBp6q=2FgE=2F30hKHkRzr0+yYn+o?=\r\n' +
			'=?us-ascii?Q?g7PI+4X+dUiQyRMTu5Dzuz9BPEzLoXu2x4kUqxv?=\r\n' +
			'=?us-ascii?Q?Ng+zBYblT=2FVONhtqc=3D?=',
		To: 'duygu98@web.de',
		'Content-Type': 'multipart/alternative;\r\n' +
			'boundary="--==_mimepart_5ee4ddf9f127_43fd0ee01c8c81571a4";\r\n' +
			'charset=UTF-8',
		'Content-Transfer-Encoding': '7bit',
		'Envelope-To': '<duygu98@web.de>',
		'X-Spam-Flag': 'YES',
		'X-UI-Filterresults': 'junk:10;V03:K0:6f0Z9vBLUBM=:fupcf08mSqEVJKZyxyy5Q6b4DvU8\r\n' +
			'XXdgUN6wCuTwu3KKPpqzcR6B9HbJGMZ6t+aTLfg2xr1HhPjlD4nooBDkP3jQuXdsXfWOr2D/I\r\n' +
			'Cw3iM2PBWkFpEKgn/89sK4x6JnTYgxphnYeYByQNtnPpBXw4NFlMXYD5J44DfMJpUCkp4OI7h\r\n' +
			'DpVcNo72cIPZerJq4ivpghJWpulQGfZAGgBfdd3CWVHOQO2x/RHO3sfgVqKKRrs4OrcMBjkUK\r\n' +
			'GfBhqdRaTlwEV8T026kWmPrJm8fdbXxh2rLfy4tG6slX908BR6TuSjCWb8/qVVcbyhQX7o9fq\r\n' +
			'nT92BtCN1BOJA6/X+a96CVa47dIbNggJQqFmWQ5yKqpVKtMdw40vAy/uC6tnsEsC2mmhYsy71\r\n' +
			'63LqIJKBflHw/NV3A2ps9dREYU/9khYGdDmnDxkxNEELI+WCBR1ImN41oZUyqaytqLCn7qJCJ\r\n' +
			'l6kU0FIg8y/NFi1zyOeQlkh36cS/1lEeFCy6dQWvbBqA4aQYkfuKO3BvdiaMg25zPVk2maiMm\r\n' +
			'eV04weUKPZz0qC1ruxNCjGQPunde59OzSOt/eNmlWPBQiBykSD45RqidLX4//szFIWeuXPyLs\r\n' +
			'k4sC2fmsISb5CnaD9cYJ/ccfMa8+dmxbohIKcBL8PXnANZQDtl3Yp4w3k0onLdKA07LDv1hlF\r\n' +
			'dfl5ck2dJCm9YsytIf4A+CdnqXExhGLJ1H4clsXCBtU1Rp1fuZQ1m8YXo1stPy6D1QtW1C1Pu\r\n' +
			'mO3KM+k/O7AyfTfTBtuWaLLvw3ghHgb2Blt3HR/3yMvP7GKClOvNMwzP/j1YjuHD6X26z6tr4\r\n' +
			'gIgwHBXg22dI/NhlUK4EcDvN3FPixYpH+IxS2aEoLw655yIhH7kqGrrmzY1p9TLrIbGudpuWs\r\n' +
			'DWFCfo7dzGLhQH7jZdsvse5XI26WKv9e7dk1/ktHv5pJYepO5iVmBLJoPQWVTzLSxUdf2RW0n\r\n' +
			'n9AoGzwilxaXVDplCw6RoSZMesHr3OFa1Om3ca3sJTu9TEVQUbdgP4Ub+kOabKEKpyLUedEK8\r\n' +
			'4TCTnR7mhTblF2m2SWFYuK13sOjZm6+A63U0h9rNU28cVPrhF0cHFbqAwsZqjz2AAi8c00QIb\r\n' +
			'JkPP8uwf06a5cAKwFx32vptXTjFgL3VAg0MxyAjQKZw7cUuLyWXMTED6K0/BfqxnOa93iYbUv\r\n' +
			'b4cRjaGwbEGIkml+pVYCQpJa5zn8II/4W+R/AEd2L7jrw1h0rx1xNiHu3Tnf3t+3MAWUizYcB\r\n' +
			'09hgB4WgYAwhdAICcr5ZBB9Xs1ILV/4DY/RDXEIYvsQELDQIYwqiSqY3mebUw9yUcF4l912U1\r\n' +
			'tJUT6sIpN115KHxFQz8MZ6tBCgsqxolH69svACkk9ofQSjsrD8uiElVKR1BCBGLDx6PXs7upf\r\n' +
			'qNJA9UMPxfLeOkEKwJS14lJOom1N/WEs8pIVWVJJrw7z+potTJwOOrn9SAwc8GkjlU4u3Z3PQ\r\n' +
			'eauCn3F1vPJCULXRn8WWqgPZ7E1CI+aPDj+oiFP67REMntD/3tRo+GUH/jIMtieJDWT6AjkfR\r\n' +
			'ONNAgdvvMHmjUmGIdxRhyQxNR2EzQIreXPx9Vc2jUqLDSt4Ud7qdVYcczqMswuZxycx8OCiVp\r\n' +
			'AOoB2prF+YiNaSWWrWTIdA3/dahc9NUcj7esgfoKpYXMy82tX0o9hNbv6PBijLSaQZlvzmwwc\r\n' +
			'l6WAGaGA8fCNdyYiVM25IQsTXSnqmeQbLQR9CukmRKmCuF6YTVPf9yjudsmLk+W6UK+tUMuiC\r\n' +
			'lC8nbLuvxIk0BSyYXCI5tc53gsJWzWlE84F/gSbk7M0uxdCVkjfWSOHIuIr9n//UBIg7do1dI\r\n' +
			'2//llmcR/cO6DaMWKoA5ssTreH9xjMnM6pliHVWMjMSqkMXFO65eqRGPst56/RIpsVuwWku5r\r\n' +
			'QN/TCFVxcvzfUnipPXtvTuxShfrd++/QNw/wb1ZZnLHZ+rx1wGcUpUSBeF5YAEK8Z6Vl+Msad\r\n' +
			'FUrErqxsregdCD34h+H9p1/UldvE28GnHuPfX31LyURkvN5ACq3kwJ0+bIkqp8qpfue/HsshX\r\n' +
			'kbiB85sJM3G8LR67Uo3TFLJCrl1h3qdpypzmasbqvg8COijDrGTdjYo0/qchKorQL/JFGRCLU\r\n' +
			'K29Pckg7WafBxT1P7fpiYDtlA+g6kAed+cNgSma4xeqrpW8u9hQ2OT3Tvw6VeYfJCweNqfOei\r\n' +
			'ZyB32QeFcNjnuttE1sLZ/VGhFLjQgK1DKpDeDXcLaT6eqqewbmb68nyWlWFUqNcMHUw+5hwQ+\r\n' +
			'So461UJ/lP0XvZErp/7bvxPgB0CzIGmyIN+nYhMyOlWhRE7bNIlaysxspF3zztfgBhaA3l0qt\r\n' +
			'poT9TEMjQEQ7AryBKlRy6TW5MUXzt3DC54UuDmzP0Hf/FbkN1DORgZHsLEZsqTxF01oeDFWhN\r\n' +
			'iQPOmpSkdSFTbNwSpWS3CSiKu0CWw1nn94HWYs+LRr+j2/eYqRGfl9jEoCG/UUxRRDX7PZ46F\r\n' +
			'Q0ZpOzNrfQmDTDyVs3mHZzoUTmVJDZFrU04wgQC99QoHtt7o6e7Be0l0ROeNP0fK57gMezFYJ\r\n' +
			'g0lHZoVfPySL2fjw+XOE6OTQs0UQQ/KR7FpoJXdvUdNb4kSW4K7Cyk87WNw/yYjSlJmNvEWZ2\r\n' +
			'x3Eta3uQG+EAjqfguk5zF2O5eBXpmHSKOrE2x3PC3YFQC5HTMJpxKCIKBYd08B3eEVOsDrGLa\r\n' +
			'EyuVoDNpbZeG2/razTmTJsNyutnyYftOEXRhtwL29LkRs+OuthDajC52Dt7+jX3hxwcR4+IU2\r\n' +
			'd6DHONY1l0dWCyT4BsETS+kgM9fZXrzST8GuD8gFQej+ylpQ3kj5GismdCvxxuyOTGdGL3ZoD\r\n' +
			'VAU/IwBYpsKWi4zjwCi8D78SIqsrhD6IK26O6npRVe9BPgjM='
	},
	text: '',
	html: 'Neue Anmeldung in Ihrem verknüpften Konto\r\n' +
		'<br><br>\r\n' +
		'Jemand hat sich über ein neues Windows-Gerät in Ihrem Google-Konto angemeldet. Sie haben diese E-Mail erhalten, weil wir uns vergewissern möchten, dass Sie das waren.\r\n' +
		'<br><br>\r\n' +
		'<a href="https://www.accounts.google.com">Aktivität prüfen</a>\r\n' +
		'\r\n',

	'isPhish': false,
	'isEncrypted': true,
	'isSigned': false,
	'help': 'Führt der Link tatsächlich zu Google?\nDas findest Du heraus, wenn du die Maus über dem Link bewegst.',
	'reason': "Der Link führt zu 'google.com' und die Absender E-Mail-Adresse verwendet auch die Domain 'google.com'. Diese E-Mail ist daher echt.",
});
//Covid
emails.push({
	date: 'Sat, 13 Jun 2020 12:03:56 +0000 (UTC)',
	subject: 'Umgang mit dem COVID-19-Erreger',
	from: { name: 'Sparkasse', email: 'service@sparkase.de' },
	to: { email: 'duygu98@web.de' },
	headers: {
		'Return-Path': '<bounces+36625-eb1f-duygu98=web.de@putsmail.litmus.com>',
		Received: [
			'from o1.putsmail.litmus.com ([50.31.42.65]) by mx-ha.web.de\r\n' +
			'(mxweb112 [212.227.17.8]) with ESMTPS (Nemesis) id 1N002m-1iwwqh3FJO-00x0qV\r\n' +
			'for <duygu98@web.de>; Sat, 13 Jun 2020 14:03:58 +0200',
			'by filter0135p3las1.sendgrid.net with SMTP id filter0135p3las1-31150-5EE4C0AC-B\r\n' +
			'2020-06-13 12:03:56.068705368 +0000 UTC m=+409717.056895630',
			'from putsmail.com (unknown)\r\n' +
			'by ismtpd0041p1mdw1.sendgrid.net (SG)\r\n' +
			'with ESMTP id Y_pfnS5mR-aSsYXiCwZUOA\r\n' +
			'for <duygu98@web.de>;\r\n' +
			'Sat, 13 Jun 2020 12:03:55.876 +0000 (UTC)'
		],
		'DKIM-Signature': 'v=1; a=rsa-sha256; c=relaxed/relaxed; d=litmus.com;\r\n' +
			'h=from:subject:mime-version:to:content-type:content-transfer-encoding;\r\n' +
			's=s1; bh=Zoj/xQnQf7xt8IcHmnjDG1y5nk1e8x0EXqBNe7XSkCs=;\r\n' +
			'b=HNDGgQL+iFAxFNiSxRa46YH4WrUiA/Tnm6BNTDX1GNnK93M1VCfHWoUbZ4vKNxPBoX3l\r\n' +
			'H0/CEOr95b+Id885ewqkJjyJBnA8NppoXy888jI955XG+H5vPm9VuySJZUQcMh9cAYgRKG\r\n' +
			'WJLhEPF7cOo066Gnl2iT2rSPNugvWzuyQ=',
		Date: 'Sat, 13 Jun 2020 12:03:56 +0000 (UTC)',
		From: 'service@sparkase.de',
		'Message-ID': '<5ee4c0ab9d132_43fd0ee01d9d01324bc@7687a9b0-8521-4408-9fdb-683b73c5c1da.mail>',
		Subject: 'Umgang mit dem COVID-19-Erreger',
		'Mime-Version': '1.0\r\n' +
			'=?us-ascii?Q?Rz3Ii86sRfqa7ZIB5m+y1K5F1txMnwcycjLBt5hUtfaNsHdE1o6V4ddHCrmKTe?=\r\n' +
			'=?us-ascii?Q?fopQh=2F84rHhtNAn3DJun3b0Iex38loIRBn=2FkYRS?=\r\n' +
			'=?us-ascii?Q?83dj1mjnHlUkKRtueYAixPZGstRdWa6JLjT4RT3?=\r\n' +
			'=?us-ascii?Q?MLA=2FOkA01LlFOCZvX6etAxpaL+7stA3YhJjItWm?=\r\n' +
			'=?us-ascii?Q?98p8fsgleY7=2FvTiLru7K1MLwoCvJ7vJZCEw=3D=3D?=',
		To: 'duygu98@web.de',
		'Content-Type': 'multipart/alternative;\r\n' +
			'boundary="--==_mimepart_5ee4c0ab9c66e_43fd0ee01d9d013232e";\r\n' +
			'charset=UTF-8',
		'Content-Transfer-Encoding': '7bit',
		'Envelope-To': '<duygu98@web.de>',
		'X-UI-Filterresults': 'unknown:2;V03:K0:Q9zklWoAVYM=:ocCpv8aiA26oy+woqKmRoJGu9C\r\n' +
			'Bb6/pmKs0/AomauZB9Mg3mp97+jv0BV8Z9Ut3TEFoDZMG6ZMIzOoY//9Y7lbkdsZt/xndh/mF\r\n' +
			'oVO1eU5tP6NIR4/bX6aYSOBH4UAZ8yEBFDQJYhbFk9o99cIKgxzXPlnfPV4Kh3Nhx59ilJkEZ\r\n' +
			'6ZIfDu/QLOuhsd2BUYpIY7uBnG0Wwe1dQuXp/A1piGniRei3glgPH3m0KW4n4cFfJSI4KTNP8\r\n' +
			'lG8+29+ZKUq/hlRL2pCZ8EPLSKxc6nrOltZBo/mLJeyDOG1Va8i5p+vpIB5/X9SPFWochP7Pu\r\n' +
			'9PyDibksAgNHrT5eI1P6iRByasZ4n5+oIyYx270zi1DJjp2YlutKzeAHX2/97MIOh9dQadCfS\r\n' +
			'Fd+21VKVktuxozWi94RyodYLdYi4LqdISPb4RYkb1UOKAYa5V50NgtguOnFVChdPAexiizvni\r\n' +
			'Jmv5K/xABt2+kbbc6FiHQfQDjpipqx7RmnLMQIDosj16wDGs152UvOdv1BUesC5P3+TxcHcuK\r\n' +
			'7QHnDWALr1kaDYMA+0NxvCZwLSp80W7Ja6eRpeKhElF70ptJ+dAydUs+643WAKyGhVB+wEs61\r\n' +
			'yswfujgPoHowrOO96kgVFqWiXkKO3y6Bfqlc+LbjVZZjXcbqHL+9hByU6VN4I4Zopkb3/yMG+\r\n' +
			'9g+UWN3FKbvUSmXaiTK9ABAFxTpWcvv3r0dEOYWeDEM0+BK/3Hiu1lXEE1GeDklS+2s5/X6/V\r\n' +
			'cPKLYtyYTvxTX5Tt7wDNM2e03vz/rJM8jW6Fttjv81IijvNgHbUXXNS1tU/6WbRyyEIwmX8hE\r\n' +
			'i450b8hjsnskHcpabg5uSYkMhbDbg9jvUbZwKEJ/LYL7eGu8iV3Ypv7UWpdQ4GbuPOGdjKUmw\r\n' +
			'YQ8QtNHT/ZURO726wrjBvkuowCFOxwb/Vege3GgMbCTpXIQIUjtz8O0FBNe8ZoTSGL9SQKnCQ\r\n' +
			'tx/e2/iEB50ECbNf7NQt6b1YsvDKBQEEMVJBLatsoDSP9INrWzUUG7h+TemkURnHzS8j1fIEn\r\n' +
			'fpA5c9suvdCS/kE2cMz1CT91o1zEGm5hj9SEMDUsJH1eDP4Jh8YluDTAZj2rIDMFOEMeAnfDI\r\n' +
			'L2LEAU31rXpVhyTklqqb33Y79/Z8dd0O1C/+UAHyqhH+GMH85576xUTuNaSXbEjlLDNTaPIAR\r\n' +
			'ItbiRvGwFqjyc441RmFLXd7IA4nqKuXeh8vMft4PHrah2mbLg/qH2WR1B3yg6dbxlCt/7XlLH\r\n' +
			'z/PzVhCXdWCtg5jrjSd1FSbkJaibEn+Lllhw6mB/q80Bz2nxwF8lrT3afaurAGXE0s7f6UCAh\r\n' +
			'xHY0nQ10WWdcMbQ7yfEO2Ty1hGTY2Ze1f+7e9pCleDAXODq6QqOm+0f0gUz1W4zVty63kpmf/\r\n' +
			'XGv8Rg20AqN6/ZdrTuBfRpj1x9MnW4oGA9t+RTCQvjhrPHLx7zWaKHzrnE1FSdUgdIrF0d/Zu\r\n' +
			'3ILu+lPxc0WEGNo+TxwK1p5S7r7wHVOM2t04MAAqtiRdW8qcxL/+c5fiiKZqYFjLHxxIdd0cx\r\n' +
			'8XOwyiO4mH+52CNDIFc+yKj/pgLAsaouddtKnJ3gbwCj6FmVJ4kPzJzWlNXBchqinQZbIgRJd\r\n' +
			'5krQLSuKHWFo1AZLr17nzt1zYWG0AmW7xhNXUYRLdAmB/E1vi1U66Y4ZaU1QvOkeJcDZKrlaz\r\n' +
			'NucIksZ3Ifx1HA09Xal+mw6yx/NMo4BKyNhfq+SixvFaxwKwgSjkpe+vCx2+jNI1gGqIb4Eei\r\n' +
			'wjuQccop7vJs4U070bwXGjnDnEFOvcjru9CpDdNL4F4qex3bPvFSoAYTKCCK2XddpaZGKy+/v\r\n' +
			'FVF1RMOvKycnC3Kt1ANDtQhrwRQyanAQt6+/w0qsSXkreVKnAHoUX6cq9jobJEBtrMaJ5ccUW\r\n' +
			'bDp4GkrpoKpqEQEwPMxxnNZnNqkLtit8cS/XRIbNSIr7kF1m1VMmaHDP654inPEUb3stP/deG\r\n' +
			'X6WkWhj9a1F65sM7XCicLiqRpAq/054MfOML4t2v9a089Oq4i9voB+XkSVIVlLITB8R76QWXs\r\n' +
			'It/vg90nF0tuIvXeyMsQZuUU0yVVYzAymLkGqktD9WFET7Zh4N23Dt2V8RfpFjZJotF5m+gOr\r\n' +
			'RV7r6wqEquxjn1iD+h66Md3xcu9T/k2dHieFQqMsJNtCBvPlD9qxtMcQiCHQH0QX9VIsBpaL0\r\n' +
			'qQdCAv2tiKHLua5KQowEabbSEJ9lITN6VJx888yq7pBSzCdBH9BaMALNRlht1IoO/XaddhX7C\r\n' +
			'totsx476BjnHcVZ2W6HGYxeWdGXpq4WjNJaR7DV+NPi7ltFGqeHwUdq69hJ7n1utSn160ztbY\r\n' +
			'jPxG2f9GwJhAxNW6HJR3W8UHycLYEtldu+vcNBGPfm4dfIAWnbfvegvlEL59MTrbGn8s4FT7Q\r\n' +
			'0xIOhAJrUKTKg8DoKjjRQEmUAsjseLZT3GgYpTbZOsZogZIkp8vi8084U1LYsSVeh9qzkJpsj\r\n' +
			'S0JP7bZomqfAs8wDTiCQmoIg67YA2K65/8qqcR4if9le88kin+lafUx6Xl/41GvVbAwiDulmP\r\n' +
			'8JLhVEF3w34K45UG8MfZOXIzrb7O13yEqc4d3H+SuPUI6pOYLNz43M4Rxnna2PSF3DbDxiFah\r\n' +
			'SsY1HawIE+31/Izx4qLQDyJV8fw3kC+KwEx8D4ubiQo3swWNsmyxlT8cThncl3k4zpfCpPkfc\r\n' +
			'bBskWyjTJ/pmNII4XpN2P5ACR+K9qSpDrOQdRDKNHo63Aih0WAoXV57VendutbfAPWm+HQRzc\r\n' +
			'uvP75TxM6qVK4zE5+6YzYUNIoFCyagfBI3nCARU9GbWf0k1dJRe8ZshinBfidKRbs+XukiYE2\r\n' +
			'Yuh0oOMTERFhQaoIvNRXgWDbDt9fo6EcG6q+jvf/htwfKDI6uE='
	},
	text: '',
	html: '<div>\r\n' +
		'Sehr geehrte Kundinnen und Kunden,\r\n' +
		'<br><br>\r\n' +
		'Ihre Sicherheit und Gesundheit und auch die unserer Mitarbeiter liegt uns sehr am Herzen. <br>\r\n' +
		'Vor diesem Hintergrund haben wir uns dafür entschieden, unsere kleineren Filialen bis auf weiteres zu schließen. <br>\r\n' +
		'Gerne stehen wir Ihnen telefonisch, per E-Mail und in unserem Online-Banking zur Verfügung. <br><br>\r\n' +
		'Bitte nehmen sich sich deshalb die 2 Minuten Zeit, um <br>\r\n' +
		'- Ihre Adresse(n)<br>\r\n' +
		'- Ihre Telefonnummer(n)<br>\r\n' +
		'- und Ihre E-Mail-Adressen(n)\r\n' +
		'<br><br>\r\n' +
		'zu überprüfen, um weiterhin eine reibungslose Kommunikation zu gewährleisten.<br>\r\n' +
		'Vielen Dank!\r\n' +
		'<br><br>\r\n' +
		'Mit freundlichen Grüßen<br>\r\n' +
		'Ihr Sparkassenverband.\r\n' +
		'<br><br>\r\n' +
		'<a href="https://www.sparkase.de/pruefen">Jetzt prüfen</a>' +
		'</div>\r\n',

	'isPhish': true,
	'isEncrypted': false,
	'isSigned': false,
	'help': 'Prüfe die Domains in der Absender E-Mail-Adresse und in der URL, die bei einem Mouseover über einen Link erscheint. Sind diese Domains offizielle Domains von der Sparkasse?',
	'mark': {
		'subj': {
			'susp': false, //not suspicious, therefore no text needed
		},
		'date': {
			'susp': false,
		},
		'sender': {
			'susp': false,
		},
		'email': {
			'susp': true,
			'text': "Die Domain in der E-Mail-Adresse ist 'sparkase.de' und nicht 'sparkasse.de'"
		},
		'body': [
			{
				'markTxt': "Sehr geehrte Kundinnen und Kunden",
				'text': "Die Anrede ist unpersönlich",
			},
			{
				'markTxt': "Jetzt prüfen", //Linktext
				'text': "Der Link führt zu 'sparkase.de' und nicht zu 'sparkasse.de'",
			},
		],
	}
});
//Sicherheithinweis paypal
emails.push({
	date: 'Sat, 13 Jun 2020 11:45:57 +0000 (UTC)',
	subject: 'Sicherheitshinweis',
	from: { name: 'PayPal', email: 'paypal@paypal-service.de' },
	to: { email: 'duygu98@web.de' },
	headers: {
		'Return-Path': '<bounces+36625-eb1f-duygu98=web.de@putsmail.litmus.com>',
		Received: [
			'from o1.putsmail.litmus.com ([50.31.42.65]) by mx-ha.web.de\r\n' +
			'(mxweb111 [212.227.17.8]) with ESMTPS (Nemesis) id 1MeBpd-1jASjw2P0p-00bHM9\r\n' +
			'for <duygu98@web.de>; Sat, 13 Jun 2020 13:45:59 +0200',
			'by filter0073p3las1.sendgrid.net with SMTP id filter0073p3las1-19600-5EE4BC75-75\r\n' +
			'2020-06-13 11:45:57.964561603 +0000 UTC m=+408959.379466740',
			'from putsmail.com (unknown)\r\n' +
			'by ismtpd0005p1iad1.sendgrid.net (SG)\r\n' +
			'with ESMTP id YC7RMXg6S5WWOkNaZGGg0w\r\n' +
			'for <duygu98@web.de>;\r\n' +
			'Sat, 13 Jun 2020 11:45:57.785 +0000 (UTC)'
		],
		'DKIM-Signature': 'v=1; a=rsa-sha256; c=relaxed/relaxed; d=litmus.com;\r\n' +
			'h=from:subject:mime-version:to:content-type:content-transfer-encoding;\r\n' +
			's=s1; bh=vs9oqNgbT6r+ejsOnPbCkq1H2PWDqNpLLShcg7PLWxY=;\r\n' +
			'b=knPGhc4pLgzqdOc1GHak+k1EJr+Lom2yv3+dhUD0aG4BWKFqxxb7XwYUGLNc2N3q4K0/\r\n' +
			'KplVO/OFh/I9r5cZ/kHIFKD1C7J3PLKq4EO+4FLrGpeeVi0MqCHg+A/HSptNiPfC6Wc98+\r\n' +
			'C6jmEOE0ZLxB0qCLfaCcQHRkofP1aUm2c=',
		Date: 'Sat, 13 Jun 2020 11:45:57 +0000 (UTC)',
		From: 'paypal@paypal-service.de',
		'Message-ID': '<5ee4bc75b7124_42b26b18b015c1316a1@7687a9b0-8521-4408-9fdb-683b73c5c1da.mail>',
		Subject: 'Sicherheitshinweis',
		'Mime-Version': '1.0\r\n' +
			'=?us-ascii?Q?Rz3Ii86sRfqa7ZIB5m+y1K5F1txMnwcycjLBt5hUtfaNsHdE1o6V4ddHCrmKTe?=\r\n' +
			'=?us-ascii?Q?foFmgedzvNhs=2FkdbY=2FJyZPh30U0TODdIRVEAD+G?=\r\n' +
			'=?us-ascii?Q?TwcQlxFY6Gnt=2FQ3b9L3a2T4DUTD3UXcn9E8BajE?=\r\n' +
			'=?us-ascii?Q?WIXDb744SMfOkzcM1h2KtunF7Yas2Tub0PBomY6?=\r\n' +
			'=?us-ascii?Q?F5shgkppxpZ38eMIevWQhyjAKVxJPT+LQCw=3D=3D?=',
		To: 'duygu98@web.de',
		'Content-Type': 'multipart/alternative;\r\n' +
			'boundary="--==_mimepart_5ee4bc75b689f_42b26b18b015c1315c3";\r\n' +
			'charset=UTF-8',
		'Content-Transfer-Encoding': '7bit',
		'Envelope-To': '<duygu98@web.de>',
		'X-UI-Filterresults': 'unknown:2;V03:K0:m2HADDl/ZYs=:8Kub9TSRHQx4j1CaSFujN7XHPM\r\n' +
			'jxLbF5tL0GYMSWv3GgKHhdhmvDuKvuHKBiqgajgG00Dat8t07rKnFuBBRHpI7RmEg9f9GFUhG\r\n' +
			'rAsGzJNtiHOh7dT5WoRDX9cKrUkb70zLwfxLURru/HCojEno34Vc+5CxxWHtB4rOkF1VWyyec\r\n' +
			'Mw3FSxQaGmNHA2wkMYeriQUKTwJD9Wuf+IcCYDHack6eZfjy7Nmhz4/rRAh3zPCNGxnapfNCH\r\n' +
			'tTOgTReKh8BOKZ/1dh3LqnYHt/GFmggPyYTmaFfBgPIkWA59XC9ATizNF+sKncvX8vA0Ip1ds\r\n' +
			'tsS9eWQBx3/6VYfCIPJ8fBSGvwgOPadBN7Vwi5mKqsxUykL0uzQArlXly1nh7Xxts02Wcz82p\r\n' +
			'SNZOvBplo4AVcnVa68VaxEKI8yza5GAhlMGuY0ckkdBLOi6NxBkPjWKEXmol8WAZD72IJzqrI\r\n' +
			'Xlgz2wzjbhxiEKwuoyK/I6gkM0BPxw9ASQsVddhySfV+oDx9aP7YYg3epYCtzsysECT+h1LEc\r\n' +
			'pM1cVmlY0MdbnWxAc/uaN/dYlLRJMC6wiGHHWPoKwTt+0GB4WzYQKrzPXQbjkYPduyXe+CHfM\r\n' +
			'Ezk8ZxtvaoaeUhuH0h9eFfq6I7BHdMYax9PxAlDEgyyeJpkcp9kkutrg0l5/SXX5rEpBxJonO\r\n' +
			'87k3bHrZgmP9O4a/j+bZbzv+n0bB3jT9Q9vVo58yuN/7VLYlIwbxPHgMOlhEMkb7RC8Sl/xng\r\n' +
			's+y27xO+1dDAFTFet6JYro9ynZ4iUvfM6NRX17pNs8D/EY+mQ+ZlGVSRfbjWynmkvgc1E9wy0\r\n' +
			'NuEN6N83YMldxjnJnoezFCmrWlNHl79AXxCBt91G/ooJOh9XrpthPTi47RbCGiMHimtHjkXTu\r\n' +
			'vuXwdBMYtYci0c+Wq3c8kMEyh2tz3s5PWHahsvvklC13/ij+7E+qe5YpSo1ip8Xf/P59wuF3O\r\n' +
			'/pc0YyCAuB4JcR0tO1uOhtJlpjROF+jx0prjVHOeYZIpuQrHzeimyQEJ3LbiUQN581CKiBnte\r\n' +
			'6pq6LqiShoVgWN01KzLrcU7ep1+RKBFkCqcuw8WnmNo6YagvQ/mIWEsfUCDdthYbTzfKeQcJL\r\n' +
			'Icr505YZ8LB4pLe013bm/7ZeJkZ+qwi/o+ia0fChFACLaEeg6P8ikjYoH5oR1VnWT1/4zWPc6\r\n' +
			'QBlsiZ7u8SAs7U08n7F2EuyJ6ZYOS6kYxC8EOVDIq66WHUpw9zl5HPX0WwLNO7JDx1P4jJx6+\r\n' +
			'jQSVQku+8qnOGiBunP2/UKV9ng2KmC51ZXArrBzmn3Mn41zB52zHPxwQjmWek8895kDFkeeLc\r\n' +
			'g0daglxxfv1YlBaIUJHXkANmwbbc/4n4UOL8ZUdCVWlN3lnV+JwXT2+2YRC9GosUdBCtTQhwH\r\n' +
			'Io73oAYj8ZYWtXAbfF5HYjY/UljtAoboVIQnZ1Ugx3wDjEobda3xO7n7IFwugaAKq1BlJZfGC\r\n' +
			'0CCSk6uF+GQxhY4r28vSCkERLOgVMT59z5Nd4zjuZ8JYnjFL0bfI7dWnF5U5Uua4mZuBqSvVL\r\n' +
			'3g5mBejtxAWsjecDCneQ4aFjy06THXczV8eu+FJ421NQJhaI7Gij6L5pBO7V/qv8n1JdSXXeg\r\n' +
			'+LosM9ZG62tYAtudrdUUmTMeOO9F88iUj+HbMj5EUwJeQ42ZRsm9a0em+8s4GHvxTsFmJSGmo\r\n' +
			'fFZMLLPTdlwuRj/SO6kybL1WMn/mFJ9nlRQB0r9cyYiagjrDz9b30+LRDWFnf08urbkMb59+9\r\n' +
			'7K/eOT12FqrXht5jKY8aR67ptQse0KuAsEaNt8mZxLOc/P9rG3x3H2etVS6YLnmmD70Bw6vuU\r\n' +
			'kHi/VPGAR8IgkT8i02NsATt+AiyPU3V8FEj8KQwtcIeiKuqE94b2UcPZ7kRoZwmNHuqqp8AzJ\r\n' +
			'Wm8VtkL7959sZ+zKLWV5oWYktbGFwCyZ8QnxIjRrVYdWGBL4KpkZ0wOOmFosY4Ioohb66sqj4\r\n' +
			'H5p7ggomq0Lc6TFnOeI6BtmvabgKE2DA0LBD6Y50X1KXLRby7K9QRPyJVbxujfuOKEX//AEPm\r\n' +
			'uxNfAaiQtjQnEESNLlFmvDJgJ4qyR4QlPX/p5rvgjQEuRxF0p7B6CXHqZp82ld8CDV6nosrfC\r\n' +
			'65HIqr4X0yTZJ3x27MFIsoQoIS6M4jAwXdRxVcZkuDBplHN/Ecmf9wq62ESMMJPZkXvbhgt85\r\n' +
			'hZTtXZa64mvWhyOnGHYt0++h1Ma6Q1l9BY7HtDAVczYzOT1d5Bv2e2HkRJTd+fG1NUQ7FEJmA\r\n' +
			'QjBBrbIqWRfn2/f9iCur7Po/GcoiVMVvx2NiENWMYUT0q+inYxdl7sWBrP+bkofynE7RUkDyw\r\n' +
			'8YZehQOrQv8J1Rimh3IwTYBDSzVeQDheYK7zw7iVv8zpju+DIebeqfyPo8uDtcyRahEQWGd2s\r\n' +
			'6iib6TafdQW8CYib1qx5SrK2TKnd2ImvZVO9PuAtYh3RM9zj5LFGhSEmyJADAJV3q0pK2C08j\r\n' +
			'8L5kSfNyA363ORkOb8CjyBunoY1yjaV7NQjYInr2SgAD9wj56zivVVJPEE0QQXDCFVLhFJbpg\r\n' +
			'DpzzDEgfoLtWghiDCfumFa+1In5r1mf2VzZIYYCO3hXAkbIAZvY2U2fP0oXnNyeqa+42Xxjjy\r\n' +
			'jvgh2RqVtG898bXyV4UOXtX3gBypuwg/HDLNl/kKEcSeFLHzXOgxJGA2X2So6yj7LXMXhOK5O\r\n' +
			'iG0Cy0ZZ3nFlzrQbhdWFTloQ+DwaO8JTOzWAATrOqaVLkxY06pfpLbS/J3ViTafc2HuoQOtIf\r\n' +
			'5pGWaLr12ScJt2KyrWD3Tf2WeIcKPBVyLxf1MXuh3qR7ACAb7V8CzE5UpuC5bg2ejqlB7PsmD\r\n' +
			'Ow0wwwk+riJsQACg/tmRyENG6Gn8bbLQ72g8ZomF4x/MGbY5jkqh2T5+AgwM/QVCIKVHbxM'
	},
	text: '',
	html: '<div>\r\n' +
		'<h1>Sehr geehrte Kundin, sehr geehrter Kunde, </h1>\r\n' +
		'<br><br>\r\n' +
		'durch unser Sicherheitssystem wurden ungewöhnliche Aktivitäten in Ihrem PayPal Konto festgestellt. Dies könnte verschiedene Ursachen haben. Ihr Konto mussten wir daher mit sofortiger Wirkung temporär einschränken. Haben Sie sich eventuell mit einem unbekannten Gerät oder einer unbekannten IP-Adresse angemeldet?\r\n' +
		'<br><br>\r\n' +
		'Diese Einschränkung ist nur temporär. Allerdings benötigen wir Ihre Mithilfe, um Ihr Konto wieder uneingeschränkt nutzbar zu machen. Dazu können Sie den unten stehenden Link nutzen.\r\n' +
		'<br><br>\r\n' +
		'<a href="https://www.paypal-service.de/verifizierung">Verifizierung jetzt durchführen</a>\r\n' +
		'<br><br>\r\n' +
		'Wir bitten um Verständnis und entschuldigen uns für eventuelle Unannehmlichkeiten.\r\n' +
		'<br><br>\r\n' +
		'Mit freundlichen Grüßen,\r\n' +
		'<br>\r\n' +
		'Ihr PayPal Servicecenter\r\n' +
		'\r\n' +
		'</div>\r\n',

	'isPhish': true,
	'isEncrypted': false,
	'isSigned': false,
	'help': 'Prüfe die Domains in der Absender E-Mail-Adresse und in der URL, die bei einem Mouseover über einen Link erscheint. Sind diese Domains offizielle Domains von PayPal?',
	'mark': {
		'subj': {
			'susp': false, //not suspicious, therefore no text needed
		},
		'date': {
			'susp': false,
		},
		'sender': {
			'susp': false,
		},
		'email': {
			'susp': true,
			'text': "Der Absender E-Mail-Adresse verwendet die Domain 'paypal-service.de'. Das ist keine offizielle Domain von Paypal."
		},
		'body': [
			{
				'markTxt': "Sehr geehrte Kundin, sehr geehrter Kunde",
				'text': "Die Anrede ist unpersönlich",
			},
			{
				'markTxt': "Verifizierung jetzt durchführen",
				'text': "Der Link führt zu 'paypal-service.de' und nicht zu 'paypal.de'",
			},

		],
	}
});
//Bewerbung
emails.push({
	date: 'Tue, 19 May 2020 13:59:10 +0000',
	subject: 'Bewerbung',
	from: { name: 'Siedler, Sarah', email: 'sarah.siedler@web.de' },
	to: { name: 'duygu98@web.de', email: 'duygu98@web.de' },
	headers: {
		'Return-Path': '<duygu.bayrak@rwth-aachen.de>',
		Received: [
			'from mail-out-2.itc.rwth-aachen.de ([134.130.5.47]) by mx-ha.web.de\r\n' +
			'(mxweb011 [212.227.15.17]) with ESMTPS (Nemesis) id 1MSJq8-1jULkF3gSG-00Sbvk\r\n' +
			'for <duygu98@web.de>; Tue, 19 May 2020 15:59:11 +0200',
			'from rwthex-s1-a.rwth-ad.de ([134.130.26.152])\r\n' +
			'by mail-in-2.itc.rwth-aachen.de with ESMTP; 19 May 2020 15:59:11 +0200',
			'from rwthex-w1-a.rwth-ad.de (2a00:8a60:1:e500::26:156) by\r\n' +
			'rwthex-s1-a.rwth-ad.de (2a00:8a60:1:e500::26:152) with Microsoft SMTP Server\r\n' +
			'(version=TLS1_2, cipher=TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256_P256) id\r\n' +
			'15.1.1979.3; Tue, 19 May 2020 15:59:10 +0200',
			'from rwthex-w1-a.rwth-ad.de ([fe80::fd01:de75:d684:fb97]) by\r\n' +
			'rwthex-w1-a.rwth-ad.de ([fe80::fd01:de75:d684:fb97%21]) with mapi id\r\n' +
			'15.01.1979.003; Tue, 19 May 2020 15:59:10 +0200'
		],
		'X-IronPort-Anti-Spam-Filtered': 'true',
		'X-IronPort-Anti-Spam-Result': '=?us-ascii?q?A2D8OwC85cNe/5gagoZmgQmBSYEjL1iBG?=\r\n' +
			'=?us-ascii?q?iwwNpMVkQKBVQuDP4UEVoF6CwEBAQEBAQEBAQMBAwEtAgQBAYZTJDoEDQIQAQE?=\r\n' +
			'=?us-ascii?q?GAQEBAQEFBG2FVwuHAwEMRDAnBBsGgxmCbQ8BA7IHgTSFUYRhEIE4gVOKb4FcP?=\r\n' +
			'=?us-ascii?q?oEyjW8El32BLA6VToQLAwQDgVCBAIQZglGKfIZZJZAijTSIGYgqnVMCAgICCQI?=\r\n' +
			'=?us-ascii?q?VgWwKFUoCAQESdnGCawEBTDwTGA2fCENoAgYBBwEBAwl8jQsBgQ8BAQ?=',
		'X-IPAS-Result': '=?us-ascii?q?A2D8OwC85cNe/5gagoZmgQmBSYEjL1iBGiwwNpMVkQKBVQu?=\r\n' +
			'=?us-ascii?q?DP4UEVoF6CwEBAQEBAQEBAQMBAwEtAgQBAYZTJDoEDQIQAQEGAQEBAQEFBG2FV?=\r\n' +
			'=?us-ascii?q?wuHAwEMRDAnBBsGgxmCbQ8BA7IHgTSFUYRhEIE4gVOKb4FcPoEyjW8El32BLA6?=\r\n' +
			'=?us-ascii?q?VToQLAwQDgVCBAIQZglGKfIZZJZAijTSIGYgqnVMCAgICCQIVgWwKFUoCAQESd?=\r\n' +
			'=?us-ascii?q?nGCawEBTDwTGA2fCENoAgYBBwEBAwl8jQsBgQ8BAQ?=',
		'X-IronPort-AV': 'E=Sophos;i="5.73,410,1583190000"; \r\n' +
			`d="xml'?rels'?docx'72,48,150?png'72,48,150,150?scan'72,48,150,150,208,217,48,150,72";a="109982798"`,
		From: '"Bayrak, Duygu" <duygu.bayrak@rwth-aachen.de>',
		To: '"duygu98@web.de" <duygu98@web.de>',
		Subject: 'Bewerbung',
		'Thread-Topic': 'Bewerbung',
		'Thread-Index': 'AQHWLeWjCF5aEHR76kGT1auKm7W9lQ==',
		Date: 'Tue, 19 May 2020 13:59:10 +0000',
		'Message-ID': '<b1e35e774f2c42fd84f5abab476c5326@rwth-aachen.de>',
		'Accept-Language': 'de-DE, en-US',
		'Content-Language': 'de-DE',
		'X-MS-Has-Attach': 'yes',
		'x-originating-ip': '[81.173.179.114]',
		'Content-Type': 'multipart/mixed;\r\n' +
			'boundary="_004_b1e35e774f2c42fd84f5abab476c5326rwthaachende_"',
		'MIME-Version': '1.0',
		'Envelope-To': '<duygu98@web.de>',
		'X-UI-Filterresults': 'unknown:2;V03:K0:Rpr6NqrqBjg=:AwyACn3znZrbGp/ocl4SxLn1WT\r\n' +
			'mk5TuEM3ePr7+6AwbKah0MqAZqXFXHlZv2cPE+Z5yKAsWu3RDrHGcrHakFzPLMp1MG0/+7sCb\r\n' +
			'WiCpQNfLEsCOTEoLPsGWed2x68jh9AaoEVgCoOmzjsflla6v7TExPIvhGua2YJhWPuqaDGBYR\r\n' +
			'v9WLzAqYLqv7VBSmJ009OVb6W1hIan94lW7xc1pEEwRj1zgnmC1rhmhTxkcuaKUHCvU+KuJzn\r\n' +
			'MON5MADXggDZP/zvpCh/PkZ/48ad326MUWlXs6xHSL4P9Y5Sp5zC0aM9Topjq4+rXeYq7OqYb\r\n' +
			'jTDiBoAcWh77J/2VE+jU7ZfyBD86e9YelqaxwX01EgQx7rFS6hJ0HB8ke8cEk2OOj8QQRvmuc\r\n' +
			'g6lhNNAs5l43SM86g3xIyE7xN+LVLb2iYrkz8tz5ZJe19HpSUGrn7t4az/F3wzrxTcBhHT3Wj\r\n' +
			'e20ElEYexhw1UGmPEXB/AfAAEYt6etE6P8VN2crT6vT+A2N8DwOXJC0C/ajJSjitq7Rj+9Cmp\r\n' +
			'Tv16KwJ/PBeJvjbWho9fLD8AJhtLF0/CNfr8ievj7dTP9HxiAKqewUg0NwEV0SPtt/8MVrnUU\r\n' +
			'/bO9MKkkde75JoWhVZASC+lm31h/XcWhRlCRLb0/Y9HqKNOLIYGeV/5ogQX7irCa5HOIYnF8K\r\n' +
			'P7H4NF90X3d4yarSK6fZUcDOdoNynAJzj6HVzMQW+lSPHcFnKMeoS8uGyq6+R52UJSXdeXXqh\r\n' +
			'tdt2LDIT10GTg7mpNdbA1qYHKlxRHLsNedLUmDHhTGwoBMYmigkzvvkH9o/6qeEKZUHVxxZYW\r\n' +
			'aJaHNd8TOYIZDYkh+Djrghw71CrniUIAY4XJ+cs6PqxoPPE+GsbxwYiB55KaYXBLpMbY5KZYA\r\n' +
			'GZNaquqLXiCKtxEq6EeIBZbU0cAmA3r/TRB2K3PwgTlCjvpuBIG32Mv1c28jO7DpgxlnZY5Z+\r\n' +
			'eK5beRjm173iElVJD3HtLe8Wk++PTqPrZKl9OVwMXdmKf69ACpXE+639f9euoSLs07vH4EKkm\r\n' +
			'5irGizPQh3cEW9yIogGyQvHyX8mI1qyzHpJB1roN5EhMwDwFTZJjOlJW8AD0vXfXRzm3IpHv9\r\n' +
			'WBFaQM2txBBoNE1ZMfZ3naDWv6+pd1SmRZiMR8DllXSnrGA3Vy5QuO/iq706nBYaKvm2DaANE\r\n' +
			'oIxab4uJC5EzBIU6vk/MplrZkUtT+xK8m7PiJXkKCFBVwhpqrAR4YGimsDuueSHt03QOmZoWa\r\n' +
			'A/w8P94hQEbm81lSfXmxc22GRLZyDQcXbm6ocQZDqJosILzQ7/2MeWPyhTH4VxkQKQiJsRq9N\r\n' +
			'QPpbj2pQkQlT/yjpNbNMWZFj94hji9u3ODuDjVLF1V5598AN8VYLf6nSaqloFeUTCFfb27Eq4\r\n' +
			'TT+Bu/M/x0NYSdpzQfqqDvPOJfle4/08TUUz4o5nG+JjIApLVSr60ms5MzuR8vISE6Io8IilA\r\n' +
			'G8SuhWe2oZWqEAxCJnVV1lT9hcY1pWi+V4gcbVano9vuknarGcrqJjROoV2GEHwd0Di8WPXve\r\n' +
			'pFXYDVkw57tFH80bWzWjtjFLLjyUkvnTHaKa5sI1HGjty5xMhnvgVGQqq78z4z/oTxmNw65Fd\r\n' +
			'PK+lFshciLhzqzuQRsMU8FMxBI+WoxW5VZpnvLsPWelpzubtZTWaOfjEPqrnI5cB/QW1rEZL9\r\n' +
			'+N/KOIV3vBEohOF3lrW5Wasge30kq8cnVmOtVUbwKdMK/EWnQbsEIJdCgmoagBNmmZlTums8z\r\n' +
			'qqhew695YG0zzz7l5/x3NGGgZRhJZeaOj253YwkOH1gwbRTY9XLCpV8zIKc7+CJ8jyIcjh022\r\n' +
			'p/uOThwvFBI5gaD3S+7lHXDHGapHdvDORqJy6Voz6B7XgOe0l1CgSotLIEUsK1nKQfy0PigYy\r\n' +
			'FWQobCZKNnZYS7l/hL0HR75qCxxDXnfLLf2vawZ4rv/8qxM6NVj0FRYkW4zmTWC1ryD/pcUrL\r\n' +
			'jxVNEUNLXn1jn34uEFu6aaewURcl6gRhUDCBDJpwaMgWZOXvPTNlTWQ4/ruAmiQ4AGwoxA4Sh\r\n' +
			'0ptnZA3QGgVQmfna1WpuCuEMs7weIB7tiiegPu25Zt1CvSrK1s9DkyPtuvK+y8HDZ4YRFCQt+\r\n' +
			'E7TSv54DNvntBXTxiVXQMN+eauailhUnq8a5yzdxyQ9OGMCJbJB//gW+RpYo6EeUn0eko2pUC\r\n' +
			'vUGebc+N8AxkKvlvSbUKM44Wr0HqUHBtF5P1EJouEfmQgRf6/FvOjgAN56kgIaDxSvxnNHHfJ\r\n' +
			'mBABsbAFWtyNkkFdyK3hpuAW3p7Id6KsJVej4fcMVwylC5aIm1gmFpUmJw+jwbn5v4Vhu/d4k\r\n' +
			'T1W2WpcqOQcLiwyDAm1MYV9k1xM6oFjbmrtQqQZkuEPjav6CIMzVvuVfH55aneXkCWgTopphz\r\n' +
			'bywu0c9yYW6mZ8tU0CpP9nr2x1x771i40QSk17Z/krXjbOoEY5Jr54mWb2PfsjONCSOmPCGc6\r\n' +
			'Z1pQWBOo2Q7whaUi6CoFQF026gJTXCoxoBIlNmrtH49qfs0edGSUPv0h1dac8NvSW+ZHCYwpa\r\n' +
			'mt59gTEqxNZeSKuB7la4lXQeeFB+QvH9zIEGsgRveOb8KiCbZJyZkdUtAEbohSgHtiHJ+KwZi\r\n' +
			'TcYVnFCk7LJxx++5C8YBuhSKaVUYLoxIYUGliI/yQHfD9LptHpIh8TChaVA+2jJTjmxH8zohi\r\n' +
			'yB6G9feNw9jeVaGXE/yATjDq4Ctz5ikNcE4wrXjS4UrCMqWR5Y2bFGFiH8FQSGq++f/YtgpFv\r\n' +
			'JELsEb1beZcxJbNSSEXarXRXmu0PQuD4oXlUzzwgX9b5bBdBIn0MMS207DVfeSgOGCrd82+A2\r\n' +
			'/AXWdTdjwaPCalM07mjC1Crp4RBLWGMiD9V+uU1bLJF68BGfze9Ukv8T/QWuYsFAwxb7g6BjG\r\n' +
			'PdVvPmKYf8farqved9ZIyga96ANRwFJLLGjXxaSrKM96F5rjiqUyuNfRwoeNFZZd2L0a4pN8N\r\n' +
			'tePBfIe+9bfniJT2AZ91aCe6ZS4YLOOikyJiW515zUqPgAtEdVHUzfK1hTdy+f1VN/nPnNvfH\r\n' +
			'Rg=='
	},
	text: 'Sehr geehrte Damen und Herren,\r\n' +
		'\r\n' +
		'Im Anhang finden Sie meine Bewerbungsunterlagen für die von Ihnen ausgeschriebene Stelle.\r\n' +
		'Da mein Profil und meine bisherigen Erfahrungen gut zu ihren Anforderungen passen, bin ich davon überzeugt, einen echten Mehrwert leisten zu können.\r\n' +
		'\r\n' +
		'Für Rückfragen stehe ich Ihnen gerne zur Verfügung und freue mich über die Möglichkeit eines persönlichen Gespächs.\r\n' +
		'\r\n' +
		'Mit freundlichen Grüßen\r\n' +
		'\r\n' +
		'Sarah Siedler\r\n' +
		'\r\n' +
		'Anlagen:\r\n' +
		'Bewerbungsunterlagen\r\n',
	html: '<html>\r\n' +
		'<head>\r\n' +
		'<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">\r\n' +
		'<style type="text/css" style="display:none;"><!-- P {margin-top:0;margin-bottom:0;} --></style>\r\n' +
		'</head>\r\n' +
		'<body dir="ltr">\r\n' +
		'<div id="divtagdefaultwrapper" style="font-size:12pt;color:#000000;font-family:Calibri,Helvetica,sans-serif;" dir="ltr">\r\n' +
		'<p></p>\r\n' +
		'<div style="font-family: Verdana; font-size: 12px;">Sehr geehrte Damen und Herren,<br><br></div>\r\n' +
		'<div style="font-family: Verdana; font-size: 12px;">Im Anhang finden Sie meine Bewerbungsunterlagen für die von Ihnen ausgeschriebene Stelle.<br></div>\r\n'
		+
		'<div style="font-family: Verdana; font-size: 12px;">Da mein Profil und meine bisherigen Erfahrungen gut zu ihren Anforderungen passen, bin ich davon überzeugt, einen echten Mehrwert leisten zu können.<br></div>\r\n' +
		'<div style="font-family: Verdana; font-size: 12px;">Für Rückfragen stehe ich Ihnen gerne zur Verfügung und freue mich über die Möglichkeit eines persönlichen Gespächs.<br><br></div>\r\n' +
		'<div style="font-family: Verdana; font-size: 12px;">Mit freundlichen Grüßen<br></div>\r\n' +
		'<div style="font-family: Verdana; font-size: 12px;">Sarah Siedler<br><br></div>\r\n' +
		'<div style="font-family: Verdana; font-size: 12px;">Anlagen:<br></div>\r\n' +
		'<div style="font-family: Verdana; font-size: 12px;">Bewerbungsunterlagen</div>\r\n' +
		'<p></p>\r\n' +
		'</div>\r\n' +
		'</body>\r\n' +
		'</html>\r\n',
	attachments: [
		{
			name: 'Bewerbung.docx',
			contentType: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document;\r\n' +
				'name="Bewerbung.docx"',
			inline: false
		},
		{
			name: 'Bewerbung.pdf',
			contentType: 'application/pdf;\r\n' +
				'name="Bewerbung.pdf"',
			inline: false
		}
	],
	//props
	'isPhish': true,
	'isEncrypted': false,
	'isSigned': false,
	'help': 'Achte immer auf die Dateiendung des Anhangs. Könnten Office-Dateien Makros enthalten, um Malware herunterzuladen?',
	'mark': {
		'subj': {
			'susp': false, //not suspicious, therefore no text needed
		},
		'date': {
			'susp': false,
		},
		'sender': {
			'susp': false,
		},
		'email': {
			'susp': false,
		},
		'attachments': [
			{
				'susp': true,
				'text': "Office-Dokumente im Mailanhang (z.B mit Dateiendung .docx,.xls) zu öffnen ist problematisch, da diese Makroviren enthalten können.",
			},
			{
				'susp': false,
			},
		],
		'body': [
			{
				'markTxt': "Damen und Herren",
				'text': "Die Anrede ist unpersönlich",
			},

		],
	}
});
//Daten veraltet
emails.push({
	date: 'Tue, 19 May 2020 11:43:39 +0000 (UTC)',
	subject: 'Ihre Daten sind veraltet',
	from: { name: 'Sparkasse', email: 'sparkasse@service-sk.de' },
	to: { email: 'maxmustermann@web.de' },
	headers: {
		'Return-Path': '<bounces+36625-eb1f-=web.de@putsmail.litmus.com>',
		Received: [
			'from o1.putsmail.litmus.com ([50.31.42.65]) by mx-ha.web.de\r\n' +
			'(mxweb010 [212.227.15.17]) with ESMTPS (Nemesis) id 1MsXvD-1ilOCs2J6R-00txSi\r\n' +
			'for <maxmustermann@web.de>; Tue, 19 May 2020 13:43:41 +0200',
			'by filter0135p3las1.sendgrid.net with SMTP id filter0135p3las1-26609-5EC3C66B-107\r\n' +
			'2020-05-19 11:43:39.987099584 +0000 UTC m=+310851.558897816',
			'from putsmail.com (unknown)\r\n' +
			'by ismtpd0004p1iad1.sendgrid.net (SG)\r\n' +
			'with ESMTP id sQNTOoudTTSTRMUYYLOiTg\r\n' +
			'for <maxmustermann@web.de>;\r\n' +
			'Tue, 19 May 2020 11:43:39.775 +0000 (UTC)'
		],
		'DKIM-Signature': 'v=1; a=rsa-sha256; c=relaxed/relaxed; d=litmus.com;\r\n' +
			'h=from:subject:mime-version:to:content-type:content-transfer-encoding;\r\n' +
			's=s1; bh=g8SjhmNsOh9T55GOktrDzbAtvOqrAyeoNcpkEUaRhg8=;\r\n' +
			'b=Oq8tYwVpBRMufH9U6Tha2Pi6MizDQYN4WEJMv1fMk6QfK8QIyt+K8kmb1Cjo3l75PdLD\r\n' +
			't70pH9usArUESqAekfITvglTIv3KKbId2zq7fDbx3aK+Vc7QYmuNvhjr2QfdNpG8weSbvh\r\n' +
			'qg2EsmDrtbmZfgHPtVgbNa+LELZAc8PzI=',
		Date: 'Tue, 19 May 2020 11:43:39 +0000 (UTC)',
		From: 'sparkasse@service-sk.de',
		'Message-ID': '<5ec3c66bb6b85_43fc75e0149005956f0@fc3507a4-d330-4d08-9a39-d6310181cf1a.mail>',
		Subject: 'Ihre Daten sind veraltet',
		'Mime-Version': '1.0\r\n' +
			'=?us-ascii?Q?Rz3Ii86sRfqa7ZIB5m+y1K5F1txMnwcycjLBt5hUtfaNsHdE1o6V4ddHCrmKTe?=\r\n' +
			'=?us-ascii?Q?fo7bw9bs8FYTqIwJ166ptb04wEHP5+qJM+5RZnv?=\r\n' +
			'=?us-ascii?Q?EReHqEo+NJKtDm4BhPH5uAMf2mGsqUqUIkaZh6W?=\r\n' +
			'=?us-ascii?Q?Ip7gQYpZAKs4hojsLzNkRcKeOAemUpatIM3RthZ?=\r\n' +
			'=?us-ascii?Q?z8=2FBXAzgNwZk3Soq3wqihrAfwy4igvERGKA=3D=3D?=',
		To: 'maxmustermann@web.de',
		'Content-Type': 'multipart/alternative;\r\n' +
			'boundary="--==_mimepart_5ec3c66bb633e_43fc75e0149005955b3";\r\n' +
			'charset=UTF-8',
		'Content-Transfer-Encoding': '7bit',
		'Envelope-To': '<maxmustermann@web.de>',
		'X-UI-Filterresults': 'unknown:2;V03:K0:ZGuB42xFvLU=:ye8A+SV3Px0CSkKC1HtCK4FGvZ\r\n' +
			'+eTush4TBAEIgVj2bgiwh0mOuX7/F0vRFrDD53kqvhXLdTcMigg0McTgorTKYCJf5nRIcsMOP\r\n' +
			'JYBn81begVLVK3YaKgCGN2+jaOIDrGldRlCEldrqU8PsRpf7oT+rs2NHsOXgDlRex0VOw0F17\r\n' +
			'cw3xwLdak1u7bi9U9OWcX191d2iUWzoSwuZmekWaxLIPamqEznapDq4+vLRqiHxqDVj7vTlsO\r\n' +
			'K7qFgI30lbQoZZCrCbfVNsYtsXr7Q50ZGUGsZvFhn18kfUMVaRxO/k0ulCtlKC7sXCMUy1GOn\r\n' +
			'NyLwGapNfIZtYtiggrb5JeP+iePSRoW84kpgo8MCHY1kubzC/qGkvUkDRe4OD2zgxd9v9vZO8\r\n' +
			'L2UY8SGftifg3WgS3F1ntgmF5wolm3mr20bNQQlYl2OOvutFgKTWEMyWxS/PG5O/jeckBBEai\r\n' +
			'mDDX1CTtFJ5ZAIhLXwwljQ0tXf8YEpiiiN5CFw4Mpswp/isbcMXm61b1Jv5Q5M6rBKbNZe4Fw\r\n' +
			'iGRJtzeg2Bu3c/dhf52PWMwVfQu1Kw3wKwuRMjzfuc99KGBDQbsJePrzv2DxqTAn+w1SQaiEk\r\n' +
			'gX6I4ppKAuL2cWzQVcfZg3bzHAAuVPTl96AtVUoVyFDpSdjyMnlzokez5htRlpY+CZ3Tgh+x3\r\n' +
			'OjvX7x/H179lZtQDzPNre6Uot1KXc1XfhoZ/8ghln8GAx+gGJXlZwZIF/jtYWIJXc5uQcldSn\r\n' +
			'iP4B/lyYB3njqe4i3MzmZ3ftpHzv8m9Mu/h20opeQ2kPb1oONpeBDg1cAY0Xd7gkgyiCTRAoS\r\n' +
			'vzL2TeI6wnCee4Og8E3YwvBTTs4t9OWJMX4/j/+P5BKY1rey9d4qctxm6uSX0V0Ee5vS38k+T\r\n' +
			'uhnEwa+g1gbvcJcFVfrge7liKjbAdUNU9S4zYzkWmHerneJtBcx9ujotPxXp1HRSmtiFnBG3y\r\n' +
			'wXzACFL+1ZSm1IzLLlR+qKQu0v0z2P7xR0gD7ERNpN/UXl5yKNCcwGiST+bpOpctOr/auB3nO\r\n' +
			'CJ/cpKvH8/LOBnEzyZZuG0nSJUi47282ayzARezaPmDj2HNFmVYm42NF8Db6hngkHNPnJomAv\r\n' +
			'de7VVt/BsIFtPsp4IrMAcCaCKVEEJ9NYnoRXHObVqb8YIC/5CYKEzZXd/zyTbpOJREV0sXzti\r\n' +
			'428vGPghMykG6Oy8d86zeBWGIvz2dp7/lXlMA95xLdN9Uf6qR27T/TGLnpqZ6wlT3Y4RLFfVj\r\n' +
			'T+4dTIhk+aPFskl5JKuSbESopP1mAKbzwLhuCZCuSAaYbkzZuwdBCXkHgWuXoywcO9bzOhUHz\r\n' +
			'XA7SXZciiu2sRJxieCItlqOy6wZon3GVCE0X+xvbN8FJXxSirXD6ZJXfO6WT0oMmy8opjhKFA\r\n' +
			'raTTD2JUz6E/uSyky22ZE11NDCwjjyMhq5zPPx+tM2dtNHZLBYENEGdLlcJ6FN5Hit8hU7GXQ\r\n' +
			'ShupCBIC1YWPED4roCgHroX8vo80E9KxBSEcsxoJysT8LTOQAmpY6DlepKYrY3AMliVNLMeLP\r\n' +
			'L7/iEmBeUgv8gbuNZuEz1OzyK4g6FNWVoSsv8yVjc9YjaQeRGq/jhnSuzuLUQAzRiZAGtuO2U\r\n' +
			'MQKc0X/LCBuVh+PXh2sYwh1lu/FICjmX2WLYMzwnJqUm1XL+82oFjJbUhqlAbnpLL/X7ulQFp\r\n' +
			'4jJ4WAoEfwjW5ss5vCo8jRFrU1pb7o7APnN7e6FqT1kHvysEw2AJDU9S6RVW6qb6JeIbqCGYV\r\n' +
			'OrumEfo6Q5JFsF9Ot/Rn97GeatK1Lg9wyScGhJTj/qHe0upS++U3BfSqQKaatnAkY9exx7ywv\r\n' +
			'1jV/R1ToKnEdWgeMiYV+zDkbrGI9+JR3XLRRQgVOYGYVgoFKqX8Wwmnn1NYe95kORC5yK/hFy\r\n' +
			'Og+329mv+3Crc62TuUD6tSDv1PjIFzOGPkQdbgklwMSKfq4Ae7H36329rVoHOOp+8jBav4cHF\r\n' +
			'bHtldc60dU5OVGn4AY92CVGusKo0s8gA3Dw0J4VzSgODHMQ1bqR/jveuIxqgciE5E9IwBbVWx\r\n' +
			'i3CGaIrU2PEsg5NTrVfi5Nv39UInjuhDp5ll/U6KdJsi/SD/tNGBZ0+LtmEDjE1AoTr9+LhqG\r\n' +
			'm1U4i/hGva5hQ7ivSX5hK11xMG8cXl62iTvA7KCehEV3ZkD+Mqz4/NkjzWLlboufexQ/GVTdY\r\n' +
			'61sxonu+1g678XjND1A6UmIN+/kzNYJYG2w6B5wk1+Bldnt2gk3bqWfWCcsUnKVMeN3tcr5Ph\r\n' +
			'IrqkhEwbWMRg6tGhNr+U+RlcAK+e0gaqG0zfk7LVs15uWJA8OECfM/PWPXFik0Xnckn8L57qR\r\n' +
			'fkGkOHrC0xWEF8/1lkdeSq5O8OjLbF6nqyZ6i0lZafBH/YV9aArvSMHfsDzETKBWSsIKnOQmJ\r\n' +
			'hIuMZ4CkiMiZVkAwbZ2XLuG6416d0lBKdV7nWvMTmbDX5pYFkV+/6K2qp9orFm1KkiYl015oC\r\n' +
			'UWBicSYQUsh8Y2Ol0nwuf5ts6A/4W6BkVJJv1VnGMSaVSYI0F5/InbyN4MsyMCO9aGLfe+KNm\r\n' +
			'dcUxUgyo3L0Vm/8/wmBhyXHPj6g9zCOTACmY+EvTsOQjzwqr2oTn80wmsnCX4TtchILFnghdP\r\n' +
			'h8/8LhuaUheSLGzgXudcmSjeW8mOo87niDSMwPHYC1WIwDiZHpd4h5dEe3HMLRpG3zib5FLnh\r\n' +
			'VMIutfVKoQEB8e0AZ+9nxDk0vC6Yx3FYuisw3b+6tDAyC/SR7D/F172U6MBv4OQ5CtlR0KtnJ\r\n' +
			'W2K/Vfluot7fR6bcenzLzYpM3mmMJu1F1ZsYPlr4mIZuXv/NO0Xs96+AEN93XyRiZy6g79C7z\r\n' +
			'6mCD58VX3cO6uMI5YEu8rviWg7MlYXgqSVhsrCLv0='
	},
	text: '',
	html: 'Sehr geehrter Kunde,<br><br>\r\n' +
		'Ihre persönlichen Daten bedürfen der Aktualisierung. Sie sind verpflichtet, Ihre Daten bei Aufforderung zu aktualisieren. Sollten Sie Ihre Daten nicht aktualisiert haben, werden wir Sie schriftlich per Einschreiben zur Aktualisierung auffordern. Hierbei kann ein Entgeld von 2,56 Euro entstehen.<br><br>\r\n' +
		'<a href="https://www.sparkasse.de.service-sk.de">Zur Aktualisierung</a><br><br>\r\n' +
		'Freundlich grüßt,<br>\r\n' +
		'Ihre Sparkasse\r\n',

	'isPhish': true,
	'isEncrypted': true,
	'isSigned': false,
	'help': 'Wie lautet die Domain in der E-Mail-Adresse und der URL?\n' +
		'Ruft die E-Mail ein Gefühl der Bedrohung auf?',
	'mark': {
		'subj': {
			'susp': false, //not suspicious
		},
		'date': {
			'susp': false,
		},
		'sender': {
			'susp': false,
		},
		'email': {
			'susp': true,
			'text': "Die Absenderdomäne lautet 'service-sk.de' und nicht 'sparkasse.de'",
		},
		'body': [
			{
				'markTxt': "Sehr geehrter Kunde",
				'text': "Die Anrede ist unpersönlich",
			},
			{
				'markTxt': "Entgeld von 2,56 Euro",
				'text': "Hier kommt es zu einer Drohung. Wenn man der Forderung nicht nachkommt, kann man mit einer Strafe rechnen.",
			},
			{
				'markTxt': "Zur Aktualisierung",
				'text': "Der Link führt nicht zu 'sparkasse.de', sondern zu 'service-sk.de'. Dieser Trick kommt häufig vor. Hier ist 'sparkasse.de' nur eine Subdomain.",
			},
		],
	}
});
//Konto eingeschränkt
emails.push({
	date: 'Tue, 19 May 2020 09:19:26 +0000 (UTC)',
	subject: 'Ihr Konto wurde eingeschränkt!',
	from: { name: 'Amazon', email: 'no-reply@arnazon.de' },
	to: { email: 'duygu98@web.de' },
	headers: {
		'Return-Path': '<bounces+36625-eb1f-duygu98=web.de@putsmail.litmus.com>',
		Received: [
			'from o1.putsmail.litmus.com ([50.31.42.65]) by mx-ha.web.de\r\n' +
			'(mxweb011 [212.227.15.17]) with ESMTPS (Nemesis) id 1MQO2f-1jNaPu1qtt-00MKA1\r\n' +
			'for <duygu98@web.de>; Tue, 19 May 2020 11:19:27 +0200',
			'by filterdrecv-p3iad2-8ddf98858-7qsdm with SMTP id filterdrecv-p3iad2-8ddf98858-7qsdm-19-5EC3A49E-F\r\n' +
			'2020-05-19 09:19:26.146027058 +0000 UTC m=+4695120.828284595',
			'from putsmail.com (unknown)\r\n' +
			'by ismtpd0002p1iad1.sendgrid.net (SG)\r\n' +
			'with ESMTP id NN3TaUA1TVqwcs4qgx-z5Q\r\n' +
			'for <duygu98@web.de>;\r\n' +
			'Tue, 19 May 2020 09:19:26.122 +0000 (UTC)'
		],
		'DKIM-Signature': 'v=1; a=rsa-sha256; c=relaxed/relaxed; d=litmus.com;\r\n' +
			'h=from:subject:mime-version:to:content-type:content-transfer-encoding;\r\n' +
			's=s1; bh=dUS+2BU3wEytc/xx5OGV0OX/HoMFSO1H6if/3U3PvBg=;\r\n' +
			'b=jTs+lVph+GeuWcpKnIV30w3YtTSTTq1grSQ7cL0yfVtxbRhZoafh+VhSAN6XncvO109Y\r\n' +
			'BV0NeALpCqsGBEICRRTafYhC6O1OmJjEySblRi2K/bmmaxNizfL7vpeTTQiuBtMNj6ApRE\r\n' +
			'G7IywI2lhRW3iOlEiNX5YzOP2gNPFP91M=',
		Date: 'Tue, 19 May 2020 09:19:26 +0000 (UTC)',
		From: 'no-reply@arnazon.de',
		'Message-ID': '<5ec3a49e14def_43fc75c0135d43570a6@fc3507a4-d330-4d08-9a39-d6310181cf1a.mail>',
		Subject: 'Ihr Konto wurde =?UTF-8?Q?eingeschr=C3=A4nkt!?=',
		'Mime-Version': '1.0\r\n' +
			'=?us-ascii?Q?Rz3Ii86sRfqa7ZIB5m+y1K5F1txMnwcycjLBt5hUtfaNsHdE1o6V4ddHCrmKTe?=\r\n' +
			'=?us-ascii?Q?fo4ffqfWvr=2Fpbkjqt77avrLQ2zYQU8hoKZZC+7y?=\r\n' +
			'=?us-ascii?Q?ZBCXO=2FPsqo+QfhGDpzExZhbSTC=2FbnE+xQaRAgeU?=\r\n' +
			'=?us-ascii?Q?leFn05JTmi+ZHBxKMZbsvHHoGlDCu1DvDXITmDu?=\r\n' +
			'=?us-ascii?Q?3FKEFEmQ11y67yi4VoyyFjOivAvS6fiXzLOF4DH?=\r\n' +
			'=?us-ascii?Q?t6zLH22YI=2Frx07GmY=3D?=',
		To: 'duygu98@web.de',
		'Content-Type': 'multipart/alternative;\r\n' +
			'boundary="--==_mimepart_5ec3a49e14299_43fc75c0135d435692e";\r\n' +
			'charset=UTF-8',
		'Content-Transfer-Encoding': '7bit',
		'Envelope-To': '<duygu98@web.de>',
		'X-UI-Filterresults': 'unknown:2;V03:K0:iwhPeI7Y2Hg=:wRm/8ZjxMgzbvV/3sZYlTBIGWH\r\n' +
			'J3UWxKtF3OyLxA0xnnklqT4voO2bBogPdFrLWUjwLlPs+4moCM+pBIjeF3PnREe3/a98k0iU+\r\n' +
			'zHIMIvUc16yhze6Wm7WbWelTxPLRKuPIEkQIuscRWCQbmWJuv8Lqc/uXP3T9YYuMLGwbqtuUV\r\n' +
			'qx6Wm7Zj8vHuYl64xGxRER4FfPNy7sltJBZSAF3N109oMmZcwLWE5W0Vy4dy5SixqaG+R1+KG\r\n' +
			'RdTmW4SHlHR6Hk5asE2XAESOR7X3xgJvRCIn5YiWyleaNGNRsEGlnSJjfWv62yMZtxDLIErUb\r\n' +
			'xpDJvcb5QKkh48MqqKpL9bfrBdrm7OnOEFJbia7xbUN1q2kP8ISh68XS8h3PCjBgtp6RPFD3U\r\n' +
			'JenXsYOjTTOuaCETgYSh6B1pFyhWTEu/1GJl89oPGGI+iUKOmvwm++fazt6vFaZLLzKgnaVh3\r\n' +
			'ybVfZG7WdF+tblXFyLj0AGvaVgbGY3UXvXxh/HRBfh3KEbdmaUDUlDtL0y8NednRunoavx7el\r\n' +
			'5GCNB+thlnlznFqD583vws7vqZ81rYTZtsLhzRYhE11NyPnqKYb6nmHaCOyPfGtiZNB3GfKEw\r\n' +
			'9QwNtVKyI6hhMj6FH/X1zQGvA49y8k8IVtPSINYhAVoSUHn3amAEUzNqK2IIoAmrcIu44BArL\r\n' +
			'tRev/c7fnbAw8MHUurwnIoLEw+f+gXcuy3Zu4Ssz7bVjs1L7XZXemYIo5ymLsG4Gec5wIBdcq\r\n' +
			'hJ66vMgR3qXn78OhNjWtUWjyLE2qRQH8OELslmSG0dKRi4nXDeN1vhaA4JbJD6WdNQJGdBmyd\r\n' +
			'z7RxzX6sJZOsz6R+6hFuC3FkGjfndigKkX5zz548eHOOuiKnqFRgWjlB/T6WStkUrxkQ087OZ\r\n' +
			'2MpN8HH6Z35wQCLMgiC/3HnyBuN2RugQrY8dnlmfvHYCvsSAHdiXV5KwDwbveijfBjp3D7jYM\r\n' +
			'tfPCyzZL0KVlExelvMLMKoQAQug4vGu2Onw1uDqwBfwHqWbVR8f1fOGHjE/E6FG30sEjJjAJh\r\n' +
			'JlN36mCi3nfKCw4yAMRebzNQHS5bLgboJKIYPcZ187BjqeV6pmt/RboPH+pJsBPBCpRFSSCTD\r\n' +
			'MobqUp8lzORuaPOsIwtVImm+/tnnHVkPLUjQCVCt4IjWJlm26J0lnd8uReA+bpIwP821ZlEih\r\n' +
			'8TMPh/vfrDtQHqeEw1FtEaGGGJTL5z3YQmJGpEI3wQjI7FmUPwQDRgmawYjNnwXviTZ+6pavF\r\n' +
			'Qq/YvfZUHuqq0LmHsWagdYRUL/jdV9GbzZC65uQn48wOJFYlVlbmAlqdn7APKjuwNR0iCF0To\r\n' +
			'S4nIDQA2gB9fLZmCStW+sTymNSqCIWG9uo3dCj7jR6TjBzEah7njWAcJXpr3Gnwn/RSjs7+eL\r\n' +
			'xJPWYdSDk2IE7TxHRoApKqysdbLeMqkmg9h5zJ+A2b5Vx8QcYJZySlphMZP6nT5gZ+j1PR0KI\r\n' +
			'4OiBKddYVLucf+NIlW6Zv/jkxQMPNbIyRqPKmVYlwXfFMUztg6Xa5we5Ie8OHeIZNUrqVYRT/\r\n' +
			'8L0L6aIiMWwooCRygxxRq5mtg5+CJzsUe9cAi4D7xrPPW6aMzlBcQyeiFIAsIcBDmNTORKqxg\r\n' +
			'EaDZc+89e0mTd0jBk7YBY+y31shvcxW7WKAajxUdhVwQ92u9OXb6AeZT7j2HolKYiDCUxWr7A\r\n' +
			'I49yJBJm4GqdYcrX0vCPfN4EXqAJ9z/aVBWJ7baw/G5w1vb9hNXwp+ZpoMQ1qu+6eK4pApcbI\r\n' +
			'PcPXrigIzKckpaFTmvjYK3DSgWZstoxIQplj6z1ba2V5tlx/uvi15GtUCg/gbm7iY13NCrAfk\r\n' +
			'Viq9X9tXQYp8ZnDKPS3w/8Qh0UYTmG0QHp/jE1Nx9X4nEpc9aOum1YVWW2qdqVYBM5hgpmOmr\r\n' +
			'ez0BFRUspZoGbzAIBxzldiEQHG6WnkWiUbsPbBL1+IXRcNfiYAJPjhilVB/+6VonKR2KiDJEz\r\n' +
			'A0OxUHL7MekT7C6aUis04Z+eiyO1zOw3hhYKsg+X6Qp5sEl0jC7D2yVTUkdY+bncph9Gr27Gv\r\n' +
			'9cJNSub9eBfT6oGHXeIa+a8L7E8cGguAJLnb7oOYphjmcmm6f8LOXU3L6kcsEFjdoWtCiqwjF\r\n' +
			'7V7Jptv66uhWmvCkSQZBCj4SFh4lssP3w9K4RY7HsiL94rpLTdFky66pwUbHqhYEJmpVd2G05\r\n' +
			'YZMFMYykUtU9PgU0XZc27IaMxXFruYIDVhv+JykPV6hjbFTgs3rusqZXLlEB1DRzO5ZhOuutt\r\n' +
			'kp6mbIdnBrz6qa1pveLNdA+rBgSDc8+3i3wltyWGAxCPqQIz0RzMT5P1GnSAsq+VZVVv1CR+H\r\n' +
			'lxI89LzJettkYQOOjAKZZ7YMvXWhtzY+t0ZlDtqs7WyM4D8C7kQ0lpZa7LOgePuBPCqMUqk3L\r\n' +
			'gOgykI9BQ3t5o52njY77TPmX50ks9Us3rTKjL/4EJ3zFqQhd5LEQON6+M5uIzqhCA9y5ZwjQ5\r\n' +
			'sHPv/9QabSBZQpjvVToqkYxjzKhk6n263kzOY4GVpYjAm4xbefsPYl0uD2DaL8MSwSo/1DYBy\r\n' +
			'YoWEn9QgBrcdXAzOa2FOmO7rEyvs0FPrn1Mvtnolyf/dDIUiY9tFTDR3NPQ0bkwJnoEi3KSp5\r\n' +
			'FR48ZtIOxH3YrfX4319tAq6xCJjGyQaWj4+lKkZiy0aythcwIq6a6gQpbiSHh1N33SAMXMxKp\r\n' +
			'h77rjc1t3ra5X1Up/S/WCVEqYO8prAExyWK5+z6p3Sw4LplvZfup2USx8xPyZPBu2eUZngpmi\r\n' +
			'bxUHjdMiSqmvq6X5Tw1G7JbKLxSoWOUzJYb5R7nULqAAgV2JRdUjCgsxdcvlLpjJMDObaGtek\r\n' +
			'FXOlANdaiXSjEdDp8F2L+QEeUn2X7iCkGxEfvrbGuA5MIpkD9eoV6p//PQHNnsluFGW5pSM3g\r\n' +
			'HeWS6UCDOX4QLabrjeZw=='
	},
	text: '',
	html: 'Sehr geehrter Kunde,<br><br>\r\n' +
		'\r\n' +
		'wir, das Kundenservice-Team, nehmen ihre Sicherheit sehr ernst.<br>\r\n' +
		'Aus diesem Grund ist es vonnöten eine routinemäßige Sicherheitskontrolle durchzuführen.<br><br>\r\n' +
		'\r\n' +
		'Der Datenabgleich dient dazu, dass Ihre persönlichen Daten fortdauernd aktuell sind.<br>\r\n' +
		'So können wir Sie unter anderem vor Missbrauch durch Dritte schützen.<br><br>\r\n' +
		'\r\n' +
		'Sollte einer Abweichung der hinterlegten Daten vom System erkannt werden, werden Sie temporär gesperrt und von einem Mitarbeiter schriftlich informiert.<br><br>\r\n' +
		'\r\n' +
		'Wir bitten Sie Ihre Daten binnen 48 Stunden vollständig zu verfizieren.<br><br>\r\n' +
		'\r\n' +
		'<a href="https://www.arnazon.de/verify">Weiter zur Verifizierung</a><br><br>\r\n' +
		'\r\n' +
		'mit freundlichen Grüßen,<br>\r\n' +
		'Ihr Amazon-Kundenservice\r\n',

	'isPhish': true,
	'isEncrypted': true,
	'isSigned': true,
	'help': 'Wie lautet die Domain in der E-Mail-Adresse und der URL?\n' +
		'Ruft die E-Mail ein Gefühl der Bedrohung auf? Wirst du aufgefordert dringend zu handeln?',
	'mark': {
		'subj': {
			'susp': false, //not suspicious
		},
		'date': {
			'susp': false,
		},
		'sender': {
			'susp': false,
		},
		'email': {
			'susp': true,
			'text': "Die Absenderdomäne lautet 'arnazon.com' und nicht 'amazon.com'",
		},
		'body': [
			{
				'markTxt': "Sehr geehrter Kunde",
				'text': "Die Anrede ist unpersönlich",
			},
			{
				'markTxt': "temporär gesperrt",
				'text': "Hier kommt es zu einer Drohung. Wenn man seine Daten nicht korrekt eingibt, wird man bestraft. Echte E-Mails würden dir nicht drohen.",
			},
			{
				'markTxt': "binnen 48 Stunden",
				'text': "Hier wird ein dringender Handlungsbedarf gefordert. Das führt dazu, dass Empfänger sich nicht mit den Sicherheitsindikatoren auseinadersetzen.",
			},
			{
				'markTxt': "Weiter zur Verifizierung",
				'text': "Der Link führt nicht zu 'amazon.de', sondern 'arnazon.de'",
			},
		],
	}
});
