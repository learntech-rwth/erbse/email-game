//xAPI functions

function generateActor(){
  // request userId;
  var userId = _pseudonymRequester.pseudonym;

  return {
      objectType: "Agent",
      id: "mailto:"+ userId +"@creation-game.de",
      name: userId
  }
}

function sendStatement(stmt, successCallback = (stmtID) => console.log(stmtID),failureCallback= (error) => console.log(error.message)){
  if(MTLG.getOptions().logging == "xAPI"){
      console.log(MTLG.xapidatacollector);
      console.log(stmt);
      MTLG.xapidatacollector.sendStatement(stmt).then(successCallback).catch(failureCallback);
  }
}

/**
 * This function records a button press in an xAPI statement and sends it
 * @function
 * @param {string} buttonName Name of the button that was pressed. Used as object name
 * @param {number} userId Unique ID of the user that pressed the button during the game
 * @param {string} stageName Name of the stage that was started
 * @param {string} levelName Name of the level that was started
 */
function xapiButton(buttonName,userId,stageName="currently not selected",levelName="currently not selected"){
  // xAPI-Statement
  let stmt = {
      actor: generateActor(),
      verb: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/multitouch/verbs/clicked",
          display : {en : "clicked", de: "klickte"}
      },
      object: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/multitouch/activities/button",
          name: {en : " ", de: " "},
      },
      context:{
        extensions:{
          "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/stage":stageName,
          "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/level":levelName
        }
      }
  };
  stmt.object.name = buttonName;

  sendStatement(stmt);
}

/**
 * This function records an option "drop" (moveable choice option) in an xAPI statement and sends it
 * @function
 * @param {string} draggableName Name of the option that was pressed. Used as object name
 * @param {number} userId Unique ID of the user that pressed the button during the game
 * @param {string} stageName Name of the stage that was started
 * @param {string} levelName Name of the level that was started
 */
function xapiDraggable(draggableName,userId,stageName="currently not selected",levelName="currently not selected"){
  // xAPI-Statement
  let stmt = {
      actor: generateActor(),
      verb: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/multitouch/verbs/dragged",
          display : {en : "dragged", de: "zog"}
      },
      object: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/multitouch/activities/draggable",
          name: {en : " ", de: " "}
      },
      context:{
        extensions:{
          "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/stage":stageName,
          "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/level":levelName
        }
      }
  };
  stmt.object.name = draggableName;

  sendStatement(stmt);
}


function xapiGameStart(gameName = "E-Mail Game"){
  let stmt = {
      actor: generateActor(),
      verb: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/verbs/initialized",
          display : {en : "initialized", de: "initialisierte"}
      },
      object: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/game",
          name: {en: gameName, de: gameName},
      }
  };

  sendStatement(stmt);
}

function xapiGameEnd(gameName = "E-Mail Game"){
  let stmt = {
      actor: generateActor(),
      verb: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/verbs/ended",
          display : {en : "ended", de: "beendete"}
      },
      object: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/game",
          name: {en: gameName, de: gameName},
      }
  };

  sendStatement(stmt);
}

function xapiGameLoaded(gameName = "E-Mail Game"){
  let stmt = {
      actor: generateActor(),
      verb: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/verbs/loaded",
          display : {en : "loaded", de: "lud"}
      },
      object: {
        id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/game",
        name: {en: gameName, de: gameName},
      }
  };

  sendStatement(stmt);
}


/**
 * This function records the start of a level in an xAPI statement and sends it
 * @function
 * @param {string} levelName Name of the level that was started. Used as object name
 * @param {number} userId Unique ID of the user that started the level
 */
function xapiLevelStart(levelName,userId){
  // xAPI-Statement
  let stmt = {
      actor: generateActor(),
      verb: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/verbs/started",
          display : {en : "started", de: "startete"}
      },
      object: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/level",
          name: {en: levelName}
      }
  };

  sendStatement(stmt);
}

/**
 * This function records the end of a level in an xAPI statement and sends it
 * @function
 * @param {string} levelName Name of the level that was finished. Used as object name
 * @param {number} userId Unique ID of the user that finised the level
 * @param {number} score Amount of points the user scored in this level. Default is set to -1 if no points could be gathered (if timers were deactivated)
 */
function xapiLevelEnd(levelName,userId,score=-1){
  // xAPI-Statement
  let stmt = {
      actor: generateActor(),
      verb: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/verbs/finished",
          display : {en : "finished", de: "schloss ab"}
      },
      object: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/level",
          name: {en: levelName}
      },
      result:{
        score:{
          raw:score
        }
      },
  };

  sendStatement(stmt)
}

/**
 * This function records the cancelation of a level in an xAPI statement and sends it
 * @function
 * @param {string} levelName Name of the level that was canceled. Used as object name
 * @param {number} userId Unique ID of the user that canceled the level
 */
function xapiLevelCancel(levelName,userId){
  // xAPI-Statement
  let stmt = {
      actor: generateActor(),
      verb: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/verbs/canceled",
          display : {en : "canceled", de: "brach ab"}
      },
      object: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/level",
          name: {en: levelName}
      }
  };

  sendStatement(stmt)
}

/**
 * This function records when the a user fails to complete a level in an xAPI statement and sends it
 * @function
 * @param {string} levelName Name of the level that wasn't completed successfully. Used as object name
 * @param {number} userId Unique ID of the user that didn't finish the level successfully
 */
function xapiLevelFailure(levelName,userId){
  // xAPI-Statement
  let stmt = {
      actor: generateActor(),
      verb: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/generic/verbs/failed",
          display : {en : "failed", de: "scheiterte"}
      },
      object: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/level",
          name: {en: levelName}
      }
  };

  sendStatement(stmt);
}

/**
 * This function records the start of a stage in an xAPI statement and sends it
 * @function
 * @param {string} stageName Name of the stage that was started. Used as object name
 * @param {number} userId Unique ID of the user that started the stage
 * @param {string} levelName Name of the level the stage belongs to
 */
function xapiStageStart(stageName,userId,levelName="currently "){
  // xAPI-Statement
  let stmt = {
      actor: generateActor(),
      verb: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/verbs/started",
          display : {en : "started", de: "startete"}
      },
      object: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/stage",
          name: {en: stageName}
      },
      context:{
        extensions:{
          "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/level":levelName
        }
      }
  };

  sendStatement(stmt);
}

/**
 * This function records the end of a stage in an xAPI statement and sends it
 * @function
 * @param {string} stageName Name of the stage that was finished. Used as object name
 * @param {number} userId Unique ID of the user that finished the stage
 * @param {string} levelName Name of the level the stage belongs to
 */
function xapiStageEnd(stageName,userId,levelName){
  // xAPI-Statement
  let stmt = {
      actor: generateActor(),
      verb: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/verbs/finished",
          display : {en : "finished", de: "schloss ab"}
      },
      object: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/stage",
          name: {en: stageName}
      },
      context:{
        extensions:{
          "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/level":levelName
        }
      }
  };
  
  sendStatement(stmt);
}

/**
 * This function records the cancelation of a stage in an xAPI statement and sends it
 * @function
 * @param {string} stageName Name of the stage that was canceled. Used as object name
 * @param {number} userId Unique ID of the user that canceled the stage
 * @param {string} levelName Name of the level the stage belongs to
 */
function xapiStageCancel(stageName,userId,levelName){
  // xAPI-Statement
  let stmt = {
      actor: generateActor(),
      verb: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/verbs/canceled",
          display : {en : "canceled", de: "brach ab"}
      },
      object: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/stage",
          name: {en: stageName}
      },
      context:{
        extensions:{
          "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/level":levelName
        }
      }
  };

  sendStatement(stmt);
}

/**
 * This function records when the a user fails to complete a stage in an xAPI statement and sends it
 * @function
 * @param {string} levelName Name of the stage that wasn't completed successfully. Used as object name
 * @param {number} userId Unique ID of the user that didn't finish the stage successfully
 * @param {string} levelName Name of the level the stage belongs to
 */
function xapiStageFailure(stageName,userId,levelName){
  // xAPI-Statement
  let stmt = {
      actor: generateActor(),
      verb: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/generic/verbs/failed",
          display : {en : "failed", de: "scheiterte"}
      },
      object: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/stage",
          name: {en: stageName}
      },
      context:{
        extensions:{
          "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/level":levelName
        }
      }
  };

  sendStatement(stmt);
}

/**
 * This function records when the a user completes a correct match (during the matching game) in an xAPI statement and sends it
 * @function
 * @param {string} matchName Name of the 2 matching option. Used as object name
 * @param {number} userId Unique ID of the user that did the matching
 * @param {string} stageName Name of the stage the match belongs to (this is pretty redundant)
 * @param {string} levelName Name of the level the stage belongs to
 */
function xapiMatched(matchName,userId,stageName,levelName){
  // xAPI-Statement
  let stmt = {
      actor: generateActor(),
      verb: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/generic/verbs/connected",
          display : {en : "connected", de: "verband"}
      },
      object: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/multitouch/activities/draggable",
          name: {en : " ", de: " "}
      },
      context:{
        extensions:{
          "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/stage":stageName,
          "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/level":levelName
        }
      }
  };
  stmt.object.name = matchName;

  sendStatement(stmt);
}

/**
 * This function records when the a user completes a wrong match (during the matching game) in an xAPI statement and sends it
 * @function
 * @param {string} matchName Name of the 2 matching option. Used as object name
 * @param {number} userId Unique ID of the user that did the matching
 * @param {string} stageName Name of the stage the match belongs to (this is pretty redundant)
 * @param {string} levelName Name of the level the stage belongs to
 */
function xapiNotMatched(matchName,userId,stageName,levelName){
  // xAPI-Statement
  let stmt = {
      actor: generateActor(),
      verb: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/generic/verbs/failed",
          display : {en : "failed to match", de: "scheiterte an Zuordnung"}
      },
      object: {
          id : "https://xapi.elearn.rwth-aachen.de/definitions/multitouch/activities/draggable",
          name: {en : " ", de: " "}
      },
      context:{
        extensions:{
          "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/stage":stageName,
          "https://xapi.elearn.rwth-aachen.de/definitions/seriousgames/activity/level":levelName
        }
      }
  };
  stmt.object.name = matchName;

  sendStatement(stmt);
}
