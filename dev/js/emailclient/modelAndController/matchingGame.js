// matching game

/**
 * This module comprises all functions relevant to the matching game.
 * It is build based on the revealing module pattern.
 * @function
 */
var matchingGameModule = function(){

  /**
   * This function initializes the matching game. It resets all indexes, buttons in the matching pane, etc.
   * It also call loadMatchingOptions() to put the different text passages and phishing features into the buttons and sets the level timer (if activated)
   * @function
   */
  function initMatchingGame(){
    let path = this.folderPaneView.levelView.newEmailPaneView;
    this.progData.currently = "matching";
    this.folderPaneView.buttonTextView.text = l("back");
    // console.log(this.progData.createdEmail);

    //reset mistakes and scores
    this.progData.currentMatchingScore = 0;
    this.progData.currentMatchingMistakes = 0;
    this.progData.problematicTopics.clear();

    //reactivate mouse clicks
    for (let i = 0; i < path.newEmailMatchView.textOptions.length; i++){
      path.newEmailMatchView.textOptions[i].mouseEnabled = true;
      path.newEmailMatchView.featureOptions[i].mouseEnabled = true;
    }

    this.manageDialogBox(l('dialog-matching-instructions'));

    //place options in the foreground and reset their positions
    this.stage.setChildIndex(path.newEmailMatchView.newEmailMatchViewContainer, this.stage.numChildren - 1);
    if (this.progData.currentPhishingEmail.infos.id == this.progData.firstMatchingLevel){
      path.newEmailMatchView.matchingTutorialContainer.visible = true;
      this.stage.setChildIndex(path.newEmailMatchView.matchingTutorialContainer, this.stage.numChildren - 1);
    }
    else{
      path.newEmailMatchView.matchingTutorialContainer.visible = false;
    }
    this.stage.setChildIndex(path.newEmailMatchView.newEmailMatchLogoView.newEmailMatchLogoContainer, this.stage.numChildren - 1);
    this.stage.setChildIndex(path.newEmailMatchView.newEmailMatchLiveView.newEmailMatchLiveContainer, this.stage.numChildren - 1);
    for(let i = 0; i < path.newEmailMatchView.textOptions.length; i++){
      this.stage.setChildIndex(path.newEmailMatchView.textOptions[i], this.stage.numChildren - 1);
      this.stage.setChildIndex(path.newEmailMatchView.featureOptions[i], this.stage.numChildren - 1);
      resetMatchingOption.bind(this)(path.newEmailMatchView.textOptions[i],true,1);
      resetMatchingOption.bind(this)(path.newEmailMatchView.featureOptions[i],true,1);
    }

    //change visibilities
    this.setNewEmailGameContainerVisibility(false,false,false,true,true,true,false,false,true,false); //levelView, email creation view (newEmailView), email creation view buttons, matching game view, matching game buttons, review view, continue button, matching game lives, tutorial

    //make buttons draggable
    this.dragButtonListener(path.newEmailMatchView.textOptions,true);
    this.dragButtonListener(path.newEmailMatchView.featureOptions,true);

    loadMatchingOptions.bind(this)();

    //add a timer for the level
    if (this.progData.matchingTimerEnabled == true){
      this.progData.matchingGameTimePerOption = this.progData.difficultyOptions[this.progData.difficulty]; //in case the difficulty has changed
      this.progData.timer.set(this.progData.matchingGameTimePerOption*(this.folderPaneView.levelView.newEmailPaneView.newEmailMatchView.textOptions.length),() => { //*(this.folderPaneView.levelView.newEmailPaneView.newEmailMatchView.textOptions.length-1)
        //all of this will only trigger if the time runs out
        if (this.progData.difficulty != "easy" && this.progData.matchingTimerEnabled == true){
          this.manageDialogBox(l('dialog-too-slow2')); //tell the players that they can still lower the difficulty to get more time
        }
        else{
          this.manageDialogBox(l('dialog-too-slow1'));
        }
        xapiStageFailure(this.progData.currently, this.progData.userId, this.progData.currentPhishingEmail.infos.name); //xapi call
        xapiLevelFailure(this.progData.currentPhishingEmail.infos.name,this.progData.userId); //xapi call
        this.controlPaneView.continueButton.visible = true; //make continue Button visible so the players can return to the selection screen
        this.progData.timeUp = true;
        //reset current matching options location (all other option will be at the right position already except for the one that could be dragged right now)
        if (this.progData.currentMatchingText != null){
          createjs.Tween.get(this.progData.currentMatchingText)
            .call(resetMatchingOption.bind(this),[this.progData.currentMatchingText,false,0.5])
        }
        if (this.progData.currentMatchingFeature != null){
          createjs.Tween.get(this.progData.currentMatchingFeature)
            .call(resetMatchingOption.bind(this),[this.progData.currentMatchingFeature,false,0.5])
        }
        for (let i = 0; i < this.folderPaneView.levelView.newEmailPaneView.newEmailMatchView.textOptions.length; i++){ //make buttons transparent so the players know that they can't choose anything anymore
          removeMouseInteractionMatchingBox.bind(this)(this.folderPaneView.levelView.newEmailPaneView.newEmailMatchView.textOptions[i]);
          removeMouseInteractionMatchingBox.bind(this)(this.folderPaneView.levelView.newEmailPaneView.newEmailMatchView.featureOptions[i]);
          createjs.Tween.get(this.folderPaneView.levelView.newEmailPaneView.newEmailMatchView.textOptions[i].children[0])
            .to({alpha:0.5},350) //fade out option
          createjs.Tween.get(this.folderPaneView.levelView.newEmailPaneView.newEmailMatchView.featureOptions[i].children[0])
            .to({alpha:0.5},350) //fade out option
        }
        // console.log(this.folderPaneView.levelView.newEmailPaneView.newEmailMatchView.featureOptions);
      });
      this.manageTimerDisplay(this.progData.matchingGameTimePerOption*(this.folderPaneView.levelView.newEmailPaneView.newEmailMatchView.textOptions.length),true); //activate the display timer
      this.progData.timer.start();
      this.progData.timeUp = null;
    }
    else{ //make timer invisible if we don't need it here
      this.manageTimerDisplay(this.progData.matchingGameTimePerOption*(this.folderPaneView.levelView.newEmailPaneView.newEmailMatchView.textOptions.length),false); //deactivate the display timer
    }
  }

  /**
   * This function positions a matching game option either at its starting location or in the middle of the matching game where the logos are
   * @function
   * @param {event} e The mouseup event that got triggered. Used for the location of the mouse / button center and the whole button container
   */
  function positionMatchingOption(e){
    let path = this.folderPaneView.levelView.newEmailPaneView.newEmailMatchView;
    let p = e.currentTarget.localToLocal(e.localX, e.localY, this.stage); //get current pointer location
    let area = path.newEmailMatchLogoView;
    let btnWidth = path.buttonWidth;
    let btnHeight = path.buttonHeight;

    //check if we hit the matching area (around the logos, in the middle of the matching game screen) with an option
    if (p.x > area.x && p.x < area.x + area.width && p.y > area.y && p.y < area.y + area.height && (this.progData.matchingTimerEnabled == false || this.progData.timer.timeUp() == false)){
      let affilation = null;
      e.currentTarget.children[0].alpha = 1; //make option opaque agaain

      //make tutorial overlay invisible
      if (path.matchingTutorialContainer.visible == true){
        createjs.Tween.get(path.matchingTutorialContainer)
          .to({alpha:0},350) //fade out the whole shape
          .wait(1000)
          .to({visible:false})
          .to({alpha:1},1) //fade in the whole shape, so we don't have to do this at the level start
      }

      //check if the option contains a feature or text from the created PE
      for (let i = 0; i < path.textOptions.length; i++){
        if (e.currentTarget.id == path.textOptions[i].id){
          affilation = "text";

          //check if we need to reset another text option that was previously selected and afterwards position the new option
          if (this.progData.currentMatchingText != null && this.progData.currentMatchingText.id != e.currentTarget.id){
            resetMatchingOption.bind(this)(this.progData.currentMatchingText,false,1);
          }
          this.progData.currentMatchingText = e.currentTarget;

          //place the option at the right position
          this.progData.currentMatchingText.children[0].x = path.newEmailMatchLogoView.newEmailMatchBinaryLine1.graphics._instructions[1].x - path.buttonWidth/2;
          this.progData.currentMatchingText.children[0].y = path.newEmailMatchLogoView.newEmailMatchBinaryLine1.graphics._instructions[1].y - path.buttonHeight;
          this.progData.currentMatchingText.children[1].realignButtonText();
          xapiDraggable({en:"text option " + this.progData.currentMatchingText.children[1].optionId.toString(), de:"Text-Option " + this.progData.currentMatchingText.children[1].optionId.toString()},this.progData.userId, this.progData.currently, this.progData.currentPhishingEmail.infos.name); //xapi call
          break;
        }
      }
      //if the option didn't contain text it contains a feature
      if (affilation === null){

        //check if we need to reset another option that was previously selected and afterwards position the new option
        if (this.progData.currentMatchingFeature != null && this.progData.currentMatchingFeature.id != e.currentTarget.id){ //reset current matching feature later !!!! TODO
          resetMatchingOption.bind(this)(this.progData.currentMatchingFeature,false,1);
        }
        this.progData.currentMatchingFeature = e.currentTarget;
        this.progData.currentMatchingFeature.children[0].x = path.newEmailMatchLogoView.newEmailMatchBinaryLine2.graphics._instructions[2].x - path.buttonWidth/2;
        this.progData.currentMatchingFeature.children[0].y = path.newEmailMatchLogoView.newEmailMatchBinaryLine2.graphics._instructions[2].y;
        this.progData.currentMatchingFeature.children[1].realignButtonText();
        xapiDraggable({en:"feature option " + this.progData.currentMatchingFeature.children[1].optionId.toString(), de:"Merkmal-Option " + this.progData.currentMatchingFeature.children[1].optionId.toString()},this.progData.userId, this.progData.currently, this.progData.currentPhishingEmail.infos.name); //xapi call
      }

      //if both matching fields are filled, check if the selection makes sense
      if (this.progData.currentMatchingText != null && this.progData.currentMatchingFeature != null){
        manageMatchingOptions.bind(this)();
      }

    }
    else{ //the option was placed somewhere else on the screen but not in the matching area or the time is up -> reset it for better visibility
      if (this.progData.matchingTimerEnabled == true && this.progData.timer.timeUp() == true){
        resetMatchingOption.bind(this)(e.currentTarget,false,0.5);
      }
      else{
        resetMatchingOption.bind(this)(e.currentTarget,false,1);
      }
    }
  }

  /**
   * This helper function sets the position of a button during the matchign game (used for tween)
   * @function
   * @param {button} button The button container we want to move
   * @param {number} x The new x coordinate of the button
   * @param {number} y The new y coordinate of the button
   */
  function setButtonCoordinates(button,x,y){
    button.children[0].set({ x: x, y: y});
  }

  /**
   * This function resets a matching game option's position
   * @function
   * @param {button} button The button container we want to reset
   * @param {bool} first If this function is called for the first time (true) we don't need to fade in the options, otherwise we want to do that (false)
   * @param {number} alpha Transparency of the option after the reset
   */
  function resetMatchingOption(button,first=false,alpha=1){
    let path = this.folderPaneView.levelView.newEmailPaneView.newEmailMatchView;
    let btnWidth = path.buttonWidth;
    let btnHeight = path.buttonHeight;
    let x,y;
    let btn;

    //find the correct starting position for the button
    for (let i = 0; i < path.textOptions.length; i++){
      if (button.id == path.CONSTmatchTextOptionsPositions[i][2]){
        x = path.CONSTmatchTextOptionsPositions[i][0];
        y = path.CONSTmatchTextOptionsPositions[i][1];
        break;
      }
      else if (button.id == path.CONSTmatchFeatureOptionsPositions[i][2]){
        x = path.CONSTmatchFeatureOptionsPositions[i][0];
        y = path.CONSTmatchFeatureOptionsPositions[i][1];
        break;
      }
    }
    if (first == true){ //we don't want to wait for the buttons to show up when we start the matching level
      setButtonCoordinates.bind(this)(button,x,y);
      button.children[1].realignButtonText();
    }
    else{
      createjs.Tween.get(button)
        .to({alpha:0},350) //fade out the whole option
        .call(setButtonCoordinates.bind(this),[button,x,y])
        .call(this.realignButtonText,[button])
        .to({alpha:1},350) //fade in the whole option
      createjs.Tween.get(button.children[0])
        .wait(360) //wait until the whole option is faded out
        .to({alpha:alpha},350) //fade out the background of the option
    }

    //reset the affilation between the option and the matching game (if there is one)
    if (this.progData.currentMatchingText != null){
      if (button.id == this.progData.currentMatchingText.id){
        this.progData.currentMatchingText = null;
      }
    }
    else if (this.progData.currentMatchingFeature != null){
      if (button.id == this.progData.currentMatchingFeature.id){
        this.progData.currentMatchingFeature = null;
      }
    }
  }

  /**
   * This function checks both matching options after two of them were chosen.
   * If the options match the players see positive feedback and the options are made more transparent.
   * If the options don't match a wobble animation is played, a live is lost, feedback is displayed and the option's phishing features are saved.
   * In both cases the options are reset to their original positions
   * @function
   */
  function manageMatchingOptions(){
    let path = this.folderPaneView.levelView.newEmailPaneView.newEmailMatchView;
    //check if we got a match in the matching area
    if (this.progData.currentMatchingText.children[1].corresponding == this.progData.currentMatchingFeature.children[1].text){ //note that this.currentMatchingText and this.currentMatchingFeature are buttons!
      //let the answers fade out if the players found a match
      this.playSounds("feedback/correct");
      xapiMatched({en:"text option " + this.progData.currentMatchingText.children[1].optionId.toString() + " and " + "feature option " + this.progData.currentMatchingFeature.children[1].optionId.toString(), de:"Text-Option " + this.progData.currentMatchingText.children[1].optionId.toString() + " und " + "Merkmal-Option " + this.progData.currentMatchingFeature.children[1].optionId.toString()},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
      createjs.Tween.get(this.progData.currentMatchingText)
        .wait(100) //wait a bit so transition is smoother
        .call(resetMatchingOption.bind(this),[this.progData.currentMatchingText,false,0.5]) //let the option fade out and move it to its original position. then fade it in to 50% alpha
        .call(removeMouseInteractionMatchingBox.bind(this),[this.progData.currentMatchingText]); //deactivate mouse interaction so the player can't use it again for now
      createjs.Tween.get(this.progData.currentMatchingFeature)
        .wait(100)
        .call(resetMatchingOption.bind(this),[this.progData.currentMatchingFeature,false,0.5])
        .call(removeMouseInteractionMatchingBox.bind(this),[this.progData.currentMatchingFeature]);
      //give the user positive feedback in the dialog box
      this.manageDialogBox(l('dialog-compliment'+this.progData.currentMatchingScore.toString()),l('dialog-matching-instructions'));
      this.progData.currentMatchingScore++;
      if (this.progData.currentMatchingScore == this.progData.matchingPairs.length || this.progData.currentMatchingScore == 4){ //the last pair was found
        if (this.progData.currentMatchingMistakes <= this.progData.maxMatchingMistakes){ //the players did a maximum of 2 mistakes
          // this.progData.passedMailsSet.add()
          this.manageDialogBox(l('dialog-continue')); //let them continue
        }
        else{ //they tried to pass the level after they did 3 mistakes or more
          this.manageDialogBox(l('dialog-cheating')); //poke a little fun at them
        }
      }
    }
    else { //no match has been found, indicate the wrong match and reset the options
      this.progData.currentMatchingMistakes++;
      this.playSounds("feedback/fail");
      xapiNotMatched({en:"text option " + this.progData.currentMatchingText.children[1].optionId.toString() + " and " + "feature option " + this.progData.currentMatchingFeature.children[1].optionId.toString(), de:"Text-Option " + this.progData.currentMatchingText.children[1].optionId.toString() + " und " + "Merkmal-Option " + this.progData.currentMatchingFeature.children[1].optionId.toString()},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
      if (this.progData.currentMatchingMistakes <= this.progData.maxMatchingMistakes){
        //let one live disapper
        createjs.Tween.get(path.newEmailMatchLiveView.lives[path.newEmailMatchLiveView.lives.length-this.progData.currentMatchingMistakes])
          .to({alpha:0},350) //fade out option
          .call(function (target,index){
            target[index].visible = false;
          },[path.newEmailMatchLiveView.lives,path.newEmailMatchLiveView.lives.length-this.progData.currentMatchingMistakes])
          .to({alpha:1},350);
          //tell the user he did something wrong in the dialog box
          this.manageDialogBox(l('dialog-wrong-answer'+((this.progData.currentMatchingMistakes-1)%3).toString()),l('dialog-matching-instructions'));
      }
      else { //player has made 3 matching mistakes and thus has to redo the level
        this.controlPaneView.continueButton.visible = true;
        this.progData.timer.stop();
        this.progData.timeUp = false;
        clearInterval(this.progData.displayClockTimer);
        this.manageDialogBox(l('dialog-wrong-answer'+((this.progData.currentMatchingMistakes-1)%3).toString()),l('dialog-gameover'));
      }

      //indicate a wrong match
      this.progData.currentMatchingText.animateWobble(10);
      this.progData.currentMatchingFeature.animateWobble(-10);

      //hide the logos while the wobble goes on
      createjs.Tween.get(path.newEmailMatchLogoView.letterImg)
        .to({alpha:0},20) //fade out option
        .wait(760)
        .to({alpha:1},20);

      createjs.Tween.get(path.newEmailMatchLogoView.questionImg)
        .to({alpha:0},20) //fade out option
        .wait(760)
        .to({alpha:1},20);

      //save the topics that were falsely matched
      this.progData.problematicTopics.add(this.progData.currentMatchingFeature.children[1].text);
      this.progData.problematicTopics.add(this.progData.currentMatchingText.children[1].corresponding);

      //reset the options
      createjs.Tween.get(this.progData.currentMatchingText)
        .wait(800) //wait 800s until the wobble animation is finished
        .call(resetMatchingOption.bind(this),[this.progData.currentMatchingText,false,1])

      createjs.Tween.get(this.progData.currentMatchingFeature)
        .wait(800) //wait 800s until the wobble animation is finished
        .call(resetMatchingOption.bind(this),[this.progData.currentMatchingFeature,false,1])
    }

    this.progData.currentMatchingText = null;
    this.progData.currentMatchingFeature = null;

    //allow the player to see the results pane
    if (this.progData.currentMatchingScore == this.progData.matchingPairs.length ||this.progData.currentMatchingScore == 4){
      if (this.progData.matchingTimerEnabled == true && this.progData.timer.timeUp() == false){ //deactivate the timers
        this.progData.timer.stop();
        this.progData.timeUp = false;
        this.progData.timeLeft += this.progData.time; //safe the time that was left for the scoring
        clearInterval(this.progData.displayClockTimer);
      }
      this.controlPaneView.continueButton.visible = true;
    }
  }

  /**
   * This helper function changes the background's alpha of a matching box. Used in the matching game for resets and to indicate correct matches (for tween)
   * @function
   * @param {button} button The button container we want to move
   * @param {number} alpha New transparency value
   */
  function changeMatchingBoxAlpha(box,alpha){
    box.children[0].alpha = alpha;
  }

  /**
   * This helper function disables the mouse interaction from buttons. Used in the matching game resets
   * @function
   * @param {button} box The button container we want to move
   */
  function removeMouseInteractionMatchingBox(box){
    //box.removeAllEventListeners();
    box.mouseEnabled = false;
  }

  /**
   * This function loads the content of the matching options from the created email.
   * First the (to the current level) important phishing features are collected.
   * Then the list is filled up with the remaining phishing features (lowPriorityOptions) and if necessary text passages that don't contain phishing (fillOptions).
   * Lastly the text passages and phishing features are put in place as button texts for the matching game.
   * @function
   */
  function loadMatchingOptions(){
    let path = this.folderPaneView.levelView.newEmailPaneView.newEmailMatchView;
    let lowPriorityOptions = [];
    let fillOptions = [];
    let texts = [];
    let features = [];
    this.progData.matchingPairs = [];

    //generate a list of text parts from the created email
    outer: for (let i = 0; i < this.progData.createdEmail.length; i++){
      inner: for (let j = 0; j < Object.keys(this.progData.currentPhishingEmail.infos.priorities).length; j++){
        if (this.progData.createdEmail[i].type == this.progData.currentPhishingEmail.infos.priorities[j]){ //important features
          this.progData.matchingPairs.push([this.progData.createdEmail[i].text,l(this.progData.createdEmail[i].type)]);
          continue outer;
        }
      }
      if (this.progData.createdEmail[i].phishing == true){ //PFs that don't have a high priority in this level
          lowPriorityOptions.push([this.progData.createdEmail[i].text,l(this.progData.createdEmail[i].type)]);
      }
      else{ //filler text passages
        fillOptions.push([this.progData.createdEmail[i].text,l("pf-no-phishing")]);
      }
    }

    //shuffle the content of the low priority options list and the filler list
    shuffleList(lowPriorityOptions);
    shuffleList(fillOptions);

    //if there are not enough high priority phishing features in the created email, fill up the game with normal phishing features
    for (let i = 0; this.progData.matchingPairs.length < path.textOptions.length && i < lowPriorityOptions.length; i++){
        this.progData.matchingPairs.push([lowPriorityOptions[i][0],lowPriorityOptions[i][1]]);
    }
    //if there are not enough phishing features in the created email, fill up the game with normal text passages
    for (let i = 0; this.progData.matchingPairs.length < path.textOptions.length && i < fillOptions.length; i++){
        this.progData.matchingPairs.push([fillOptions[i][0],fillOptions[i][1]]);
    }

    //shuffleList(this.matchingPairs);

    //split the matching pairs in two so we can shuffle them seperately
    for (let i = 0; i < this.progData.matchingPairs.length && i < path.numberButtons; i++){
      texts.push(this.progData.matchingPairs[i]); //add the whole sub array so we can also safe which matching option corresponds to this text
      features.push(this.progData.matchingPairs[i][1]);
    }

    //shuffe options so the pairs aren't always on the opposite sides of the game board
    shuffleList(texts);
    shuffleList(features);

    //fill in the buttons with features or text passages and reset their opacity
    for (let i = 0; i < 4; i++){
      path.textOptions[i].children[1].text = texts[i][0];
      path.textOptions[i].children[1].corresponding = texts[i][1];
      path.textOptions[i].children[1].optionId = i;
      path.textOptions[i].children[0].graphics._fill.style = this.blue;
      path.textOptions[i].children[0].alpha = 1;
      path.textOptions[i].children[1].realignButtonText();
      path.featureOptions[i].children[1].text = features[i];
      path.featureOptions[i].children[1].optionId = i;
      path.featureOptions[i].children[0].graphics._fill.style = this.blue;
      path.featureOptions[i].children[0].alpha = 1;
      path.featureOptions[i].children[1].realignButtonText();
    }
  }

  /**
   * This function shuffles the content of a list/array around (Durstenfeld shuffle)
   * @function
   * @param {array} list The list that gets shuffled
   */
  function shuffleList(list) {
    for (let i = list.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [list[i], list[j]] = [list[j], list[i]];
    }
  }

  return{
    initMatchingGame: initMatchingGame,
    positionMatchingOption: positionMatchingOption
  };

}();
