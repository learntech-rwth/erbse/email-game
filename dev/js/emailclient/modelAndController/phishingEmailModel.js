/*
  model for phishing emails and tutorials, based on the email model of the original game by Duygu
  note that sometimes phishing features weren't marked as such if they were not introduced before
  look at the lang.js file to know how the phishing types should be called
*/

var phishingLevels = [];

//phishing
phishingLevels.push({
  id:0,
  name:'name-phishing',
  requirement:{0:0},
  dialog:{
    start:"supervisor",
    "en":{
      0:"In order to create your own test phishing emails I'm going to introduce you to the name today. Let’s get started, shall we?",
      1:"Yes, thank you.",
      2:"So, Phishing is a threat to everybody from private individuals to high-ranking government officals.",
      3:"Ok, but what exactly is phishing and how can I protect myself from it?",
      4:"Let’s first talk about the definition. Phishing is a crime that employs social engineering and technical subterfuge in order to steal a victim's personal data.",
      5:"So in essence phishers try to manipulate people into giving up things such as login information?",
      6:"Exactly, phishers aim to obtain personal data such as login credentials or banking information. Now that we got that out of the way let's take a look at our first phishing feature.",
      7:"Ok, that sounds good.",
    },
    "de":{
      0:"Um deine eigenen Test-Phishing-Mails zu erstellen, werde ich dir heute eine Einführung in das Thema geben. Fangen wir also an, ok?",
      1:"Ja, gerne.",
      2:"Also, Phishing stellt eine Bedrohung für jeden dar, von Privatpersonen bis hin zu hochrangigen Personen des öffentlichen Lebens.",
      3:"Ok, aber was genau ist Phishing und wie kann ich mich davor schützen?",
      4:"Lass uns erstmal über die Definition sprechen. Phishing ist ein Verbrechen, bei dem Social Engineering und technische Tricks eingesetzt werden, um die persönlichen Daten eines Opfers zu stehlen.",
      5:"Phisher versuchen also im Wesentlichen, Menschen zu manipulieren, damit sie Dinge wie Anmeldedaten preisgeben?",
      6:"Genau, Phisher zielen darauf ab, persönliche Daten wie z.B. Passwörter oder Bankverbindungen zu erlangen. Da wir das aus dem Weg geräumt haben, lass uns einen Blick auf unser erstes Phishing-Merkmal werfen.",
      7:"Ok, das hört sich gut an.",
    },
  }
});

//scarcity
phishingLevels.push({
  id:1,
  name:'name-scarcity',
  requirement:{0:0},
  dialog:{
    start:"supervisor",
    "en":{
      0:"Let's start with 'scarcity'. It's an easier phishing feature. In general people tend to value things more the less they're available.",
      1:"Like limited stock for certain advertised products?",
      2:"Yes, phishers borrowed some of the strategies advertisers use. So, a phishing email that employs 'scarcity' tries to presure recipients by claiming that they only have a limited amount of time before they will face negative consequences.",
      3:"What exactly do you mean with negative consequences?",
      4:"The phishers could potentionally claim that the victims only have 24h to respond to the email before their accounts get suspended.",
      5:"Ok, so if an email tells me that I only have a limited amount of time to respond that could mean phishers are using 'scarcity'?", // means that the phishing email tries to enforce certain actions by limiting things such as the response time or quantities of stock.",
      6:"Exactly! Why don't you try to integrate 'scarcity' into your first phishing email?",
      7:"Sure, I'll get right to it."
    },
    "de":{
      0:"Beginnen wir mit dem einfacheren Phishing-Merkmal 'Knappheit'. Im Allgemeinen schätzen Menschen Dinge umso mehr, je schlechter sie verfügbar sind.",
      1:"Wie zum Beispiel bei begrenzten Stückzahlen für beworbene Produkte?",
      2:"Ja, Phisher haben sich einiges von Werbefirmen abgeschaut. Phishing-E-Mails, die 'Knappheit' einsetzen, versuchen die Empfänger unter Druck zu setzen. Sie behaupten, dass die Empfänger innerhalb einer begrenzten Zeit tätig werden müssen, um negativen Konsequenzen zu entgehen.",
      3:"Was genau meinst du mit negativen Konsequenzen?",
      4:"Die Phisher könnten z.B. behaupten, dass die Opfer nur 24 Stunden Zeit haben, um auf die E-Mail zu reagieren, bevor ihr Konto gesperrt wird.",
      5:"Ok, wenn in einer E-Mail steht, dass ich nur eine begrenzte Zeit zum Antworten habe, könnte das bedeuten, dass Phisher 'Knappheit' nutzen?",
      6:"Ganz genau! Warum versuchst du nicht mal 'Knappheit' in eine erste Phishing-E-Mail zu integrieren?",
      7:"Sicher, ich fange sofort an."
    },
  },
});
phishingLevels.push({
  date:'',
  subject:'',
  from:{name:'',email:''},
  to:{email:''},
  header:{
    'Return-Path':'',
    Received:[''],
    Date:'',
    From:'',
    Subject:'',
    To:'',
  },
  text:'',
  html:'',
  'isPhish':false,
  'isEncrypted':false,
  'isSigned':false,
  'help':'',
  'mark':{
    'subj': {
      'susp': false, //not suspicious
    },
    'date': {
      'susp': false,
    },
    'sender': {
      'susp': false,
    },
    'email': {
      'susp': true,
      'text': '',
    },
    'body': [
      {
        'markTxt': '',
        'text': '',
      },
    ],
  },
  creation:{
    infos: {id:2,matching:false,priorities:{0:"pf-scarcity"},requirement:{0:0},name:"name-scarcity",dialog:'dialog-scarcity',topic:{en:"the purchase of an app called Tribal War",de:"der Kauf der App Tribal War"},}, //MATCHING IS FALSE
    //Dear Customer Your Apple ID was used to purchase Tribal War App on a computer or device that had not previously been associated with this Apple ID. The download was initiated from AR , United States. If you initiated this download, you can disregard this email. It was only sent to alert you in case you did not initiate the download yourself. If you did not make this purchase, we recommend that you go to https://itunes.apple.com/ref-082736</a></span> to cancel the transaction, Confirm that you're the owner of the account and then follow the instructions. Regards, Apple
    "en":{
      "to": "Antonia Nur <a.nur@company.com>",
      "from":{  "option0":{text:"Service <service@company.com>", phishing:false, type:""},
                "option1":{text:"Support <support@company.com>", phishing:false, type:""},
                "option2":{text:"Bernd <bernd59@gmail.com>", phishing:false, type:""} //TODO
              },
      "subject":{ "option0":{text:"Tribal War purchase", phishing:false, type:""},
                  "option1":{text:"Apple payment Notification", phishing:false, type:""},
                  "option2":{text:"About your last transaction", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Dear customer,", phishing:false, type:""},
                  "option1":{text:"Hello,", phishing:false, type:""},
                  "option2":{text:"Greetings,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"Your Apple ID was used to purchase Tribal War App on a device that had not previously been associated with this ID.", phishing:false, type:""},
                  "option1":{text:"Recently the Tribal War App was purchased on a device previously unknown to us.", phishing:false, type:""},
                  "option2":{text:"The Tribal War App has been purchased over your Apple account.", phishing:false, type:""} //TODO
            },
      "choice3":{ "option0":{text:"In case you didn't carry out this transaction please send us your credit card information in the next 48h so we can reimburse you.", phishing:true, type:"pf-scarcity"},
                  "option1":{text:"This notification was only sent to alert you in case you did not initiate the download yourself.", phishing:false, type:""},
                  "option2":{text:"If you're using a new device please ignore this email.", phishing:false, type:""} //TODO
            },
      "choice4":{ "option0":{text:"Perhaps a member of your family had access to your account and carried out the transaction.", phishing:false, type:""},
                  "option1":{text:"You should be careful who you grant access to your Apple account.", phishing:false, type:""},
                  "option2":{text:"Regardless we recommend that you respond with your banking information until tommorow to verify your ownership.", phishing:true, type:"pf-scarcity"} //TODO
                  },
      "choice5":{ "option0":{text:"Regards, Apple", phishing:false, type:""},
                  "option1":{text:"Your Apple service team", phishing:false, type:""},
                  "option2":{text:"Kind regards", phishing:false, type:""} //TODO
            },
      },
    "de":{
      "to": "Antonia Nur <a.nur@company.com>",
      "from":{  "option0":{text:"Service <service@company.com>", phishing:false, type:""},
                "option1":{text:"Support <support@company.com>", phishing:false, type:""},
                "option2":{text:"Bernd <bernd59@gmail.com>", phishing:false, type:""} //TODO
              },
      "subject":{ "option0":{text:"Ihre Tribal War Bestellung", phishing:false, type:""},
                  "option1":{text:"Apple Pay Benachrichtigung", phishing:false, type:""},
                  "option2":{text:"Bezüglich ihrer letzten Transaktion", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Sehr geehrter Kunde,", phishing:false, type:""},
                  "option1":{text:"Hallo,", phishing:false, type:""},
                  "option2":{text:"Grüße,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"Ihre Apple ID wurde verwendet, um die Tribal War App auf einem Gerät zu kaufen, das zuvor nicht mit dieser ID verknüpft war.", phishing:false, type:""},
                  "option1":{text:"Kürzlich wurde die Tribal War App auf einem uns bisher unbekannten Gerät gekauft.", phishing:false, type:""},
                  "option2":{text:"Die Tribal War App wurde über Ihr Apple-Konto gekauft.", phishing:false, type:""} //TODO
            },
      "choice3":{ "option0":{text:"Sollten Sie diese Transaktion nicht autorisiert haben, senden Sie uns bitte in den nächsten 48h Ihre Bankdaten, damit wir Ihnen den Betrag erstatten können.", phishing:true, type:"pf-scarcity"},
                  "option1":{text:"Diese Benachrichtigung wurde nur versendet, um Sie zu benachrichtigen, falls Sie den Download nicht selbst initiiert haben.", phishing:false, type:""},
                  "option2":{text:"Wenn Sie ein neues Gerät verwenden, ignorieren Sie diese E-Mail bitte.", phishing:false, type:""} //TODO
            },
      "choice4":{ "option0":{text:"Möglicherweise hatte ein Mitglied Ihrer Familie Zugang zu Ihrem Konto und hat die Transaktion durchgeführt.", phishing:false, type:""},
                  "option1":{text:"Sie sollten sorgfältig darauf achten, wem Sie Zugriff auf Ihr Apple-Konto gewähren.", phishing:false, type:""},
                  "option2":{text:"Unabhängig davon empfehlen wir Ihnen, bis morgen mit Ihren Bankdaten zu antworten, um Ihre Eigentümerschaft zu verifizieren.", phishing:true, type:"pf-scarcity"} //TODO
                  },
      "choice5":{ "option0":{text:"Mit freundlichen Grüßen, Apple", phishing:false, type:""},
                  "option1":{text:"Ihr Apple Service Team", phishing:false, type:""},
                  "option2":{text:"Mit freundlichen Grüßen", phishing:false, type:""} //TODO
            },
      },
  },
});

//authority
phishingLevels.push({
  id:3,
  name:'name-authority',
  requirement:{0:2},
  dialog:{
    start:"supervisor",
    "en":{
      0:"Ok, you understood scarcity then, let's continue with 'authority'. People tend to not question others that are in a higher-ranking positions.",
      1:"You mean because of negative consequences, right?",
      2:"Correct, just as with scarcity people fear for negative consequences. Although respect could also explain this behaviour.",
      3:"So, phishers could act as if they are government officals to gain the upper hand on me so to speak?",
      4:"Yes, phishers could impersonate government officals, employers or perhaps high-ranking employees of service providers such as administrators.",
      5:"Got it, I should be wary of emails from people that emphasize a higher than normal standing.",
      6:"Then why don't you write another phishing email that contains 'authority'? In the meantime I'll prepare a short test for you, so you can practice the phishing features I already taught you about.",
      7:"Sure, sounds good.",
    },
    "de":{
      0:"Ok, jetzt hast du Knappheit verstanden. Dann lass uns mit 'Autorität' fortfahren. Menschen neigen dazu Autoritätspersonen nicht zu hinterfragen.",
      1:"Du meinst wegen möglicher negativer Konsequenzen, richtig?",
      2:"Richtig, genau wie bei Knappheit fürchten die Menschen negative Konsequenzen. Obwohl auch Respekt dieses Verhalten erklären könnte.",
      3:"Phisher könnten also so tun, als ob sie Regierungsbeamte wären, um sozusagen die Oberhand über mich zu gewinnen?",
      4:"Ja, Phisher könnten sich als Regierungsbeamte, Arbeitgeber oder vielleicht auch als hochrangige Mitarbeiter von Dienstleistern, wie Administratoren, ausgeben.",
      5:"Verstanden, man sollte also bei E-Mails von Leuten, die ihren Rang betonen, vorsichtig sein.",
      6:"Dann verfass doch mal eine neue Phishing-E-Mail, die 'Autorität' enthält. In der Zwischenzeit bereite ich einen kleinen Test für dich vor. Dann kannst du die Phishing Merkmale, die ich dir bisher erklärt habe, ein wenig üben.",
      7:"Alles klar.",
    },
  },
});
phishingLevels.push({
  date:'',
  subject:'',
  from:{name:'',email:''},
  to:{email:''},
  header:{
    'Return-Path':'',
    Received:[''],
    Date:'',
    From:'',
    Subject:'',
    To:'',
  },
  text:'',
  html:'',
  'isPhish':false,
  'isEncrypted':false,
  'isSigned':false,
  'help':'',
  'mark':{
    'subj': {
      'susp': false, //not suspicious
    },
    'date': {
      'susp': false,
    },
    'sender': {
      'susp': false,
    },
    'email': {
      'susp': true,
      'text': '',
    },
    'body': [
      {
        'markTxt': '',
        'text': '',
      },
    ],
  },
  creation:{
    infos: {id:4,matching:true,priorities:{0:"pf-authority"},requirement:{0:2},name:'name-authority',dialog:'dialog-authority',topic:{en:"a job offer",de:"ein Jobangebot"},}, //MATCHING IS FALSE

    /*I am Edward R. Carr a geographer and anthropologist whose career and research focus on exploring alternative ways of achieving meaningful and enduring improvements to human well-being.
    I worked in rural sub-Saharan Africa on issues of development and global change among various rural communities.
    My scholarship and teaching encourage students and development professionals to reexamine the sometimes-damaging preconceptions embedded in development theory;
    in my book Delivering Development: Globalization's Shoreline and the Road to a Sustainable Future,
    I demonstrates how these commonly held beliefs about globalization and development have failed the global poor, and how to redefine what assistance to developing communities really means in an ever-changing world.
    You have been selected to work with me through the UCLA HR departments as my PA while I attend to a United Nations Environment Programme in Australia.
    You will only Mail letters, Reply emails and run errands on my behalf at €500/wk.
    I will meet you on campus upon my arrival by the last week to September for one on one interview and possibilities of making it a long term employment if you impress me while I am away.
    In addition , I am the author of more than 50 publications on issues of development, adaptation to climate change, and the changing global environment.
    Of particular note, I was the lead author of two global environmental assessments (the Millennium Ecosystem Assessment and the The United Nations Environment Programme's Fourth Global Environment Outlook)
    and was review editor for a chapter of the Intergovernmental Panel on Climate Change?
    Fifth Assessment Report. So you understand you are working with a very busy person. To apply for this position, Please send your Full Name, E-mail, Alternate E-mail and Mobile here.
    Note: I will communicate via email frequently and you must be able to check your emails every 10 minutes for important employment update or tasks.",
    */
    "en":{
      "to": "Lisbet Gulmira <l.gulmira@company.com>",
      "from":{  "option0":{text:"Carr <carr@office.com>", phishing:false, type:""},
                "option1":{text:"E.R. Carr <e.r.carr@yahoo.com>", phishing:false, type:""},
                "option2":{text:"HR <info@humanresources.com>", phishing:true, type:"pf-authority"} //TODO
              },
      "subject":{ "option0":{text:"An opportunity for you", phishing:false, type:""},
                  "option1":{text:"I am Edward R. Carr", phishing:false, type:""},
                  "option2":{text:"Please respond", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Dear Lisbet,", phishing:false, type:""},
                  "option1":{text:"Greetings,", phishing:false, type:""},
                  "option2":{text:"Hello Lisbet,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"I can offer you a new job opportunity as my assistant.", phishing:false, type:""},
                  "option1":{text:"I am Edward R. Carr a geographer and anthropologist whose research focus is on improving human well-being.", phishing:false, type:""},
                  "option2":{text:"My name is Edward R. Carr and I work as an anthropologist and psychic.", phishing:false, type:""} //TODO
            },
      "choice3":{ "option0":{text:"You have been selected through the UCLA HR departments as my PA while I attend to a UN environment program.", phishing:true, type:"pf-authority"},
                  "option1":{text:"My work in this field is rather busy.", phishing:false, type:""},
                  "option2":{text:"I'm in need of your assistance for an environment program.", phishing:false, type:""} //TODO
            },
      "choice4":{ "option0":{text:"Please respond as fast as possible or I'll have to find somebody else.", phishing:true, type:"pf-scarcity"},
                  "option1":{text:"You will only mail letters, reply to emails and run errands at $500/wk.", phishing:false, type:""},
                  "option2":{text:"If you would like to work for me please send me your personal information such as name and banking details.", phishing:true, type:"pf-authority"} //TODO
                  },
      "choice5":{ "option0":{text:"Kind regards, Edward R. Carr", phishing:false, type:""},
                  "option1":{text:"Edward R. Carr", phishing:false, type:""},
                  "option2":{text:"Best regards", phishing:false, type:""} //TODO
            },
    },
    "de":{
      "to": "Lisbet Gulmira <l.gulmira@company.com>",
      "from":{  "option0":{text:"Carr <carr@office.com>", phishing:false, type:""},
                "option1":{text:"E.R. Carr <e.r.carr@yahoo.com>", phishing:false, type:""},
                "option2":{text:"Personalabteilung <info@Personalabteilung.com>", phishing:true, type:"pf-authority"} //TODO
              },
      "subject":{ "option0":{text:"Eine Gelegenheit für Sie", phishing:false, type:""},
                  "option1":{text:"Ich bin Edward R. Carr", phishing:false, type:""},
                  "option2":{text:"Bitte antworten Sie", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Liebe Lisbet,", phishing:false, type:""},
                  "option1":{text:"Grüße,", phishing:false, type:""},
                  "option2":{text:"Hallo Lisbet,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"Ich kann Ihnen eine neue Beschäftigung als meine Assistentin anbieten.", phishing:false, type:""},
                  "option1":{text:"Ich bin Edward R. Carr, ein Geograph und Anthropologe, dessen Forschungsschwerpunkt die Verbesserung des menschlichen Wohlbefindens ist.", phishing:false, type:""},
                  "option2":{text:"Mein Name ist Edward R. Carr und ich arbeite als Anthropologe und Hellseher.", phishing:false, type:""} //TODO
            },
      "choice3":{ "option0":{text:"Sie wurden durch die Personalabteilung der UCLA als mein PA ausgewählt, während ich an einem UN-Umweltprogramm teilnehme.", phishing:true, type:"pf-authority"},
                  "option1":{text:"Meine Tätigkeit in diesem Bereich ist recht arbeitsreich.", phishing:false, type:""},
                  "option2":{text:"Ich benötige Ihre Unterstützung für ein Umweltprogramm.", phishing:false, type:""} //TODO
            },
      "choice4":{ "option0":{text:"Bitte antworten Sie so schnell wie möglich, sonst muss ich mich nach jemand anderem umsehen.", phishing:true, type:"pf-scarcity"},
                  "option1":{text:"Für 500 €/Woche werden Sie Briefe verschicken, E-Mails beantworten und Botengänge durchführen.", phishing:false, type:""},
                  "option2":{text:"Wenn Sie für mich arbeiten möchten, senden Sie mir bitte Ihre persönlichen Daten wie Name und Bankverbindung.", phishing:true, type:"pf-authority"} //TODO
                  },
      "choice5":{ "option0":{text:"Mit freundlichen Grüßen, Edward R. Carr", phishing:false, type:""},
                  "option1":{text:"Edward R. Carr", phishing:false, type:""},
                  "option2":{text:"MfG", phishing:false, type:""} //TODO
            },
    },
  },
});

//email addresses
phishingLevels.push({
  id:5,
  name:'name-email',
  requirement:{0:4},
  dialog:{
    start:"supervisor",
    "en":{
      0:"You did really well so far, but before we can continue we need to talk a bit about email addresses. Why don't you give me an example address?",
      1:"How about 'john.smith@yahoo.com'?",
      2:"Ok, an email address has a local part before and a domain part after the '@'.",
      3:"So 'john.smith' is the local and 'yahoo.com' is the domain part?",
      4:"Yes, it's also important to note that the domain is read from right to left. So 'com' is the top level domain or TLD. These TLDs usually belong to countries.", // but generic ones such as 'com' exist as well.",
      5:"Sure, but what about 'yahoo'?",
      6:"Well, 'yahoo' is the hostname in the 'com' top level domain. Usually the hostname and the name of the company that owns this domain are identical. Furthermore, there could also be other sub-level domains such as 'support' in 'support.yahoo.com'. Sub-level domains are basically single departments of the main company.",
      7:"So, the local part 'john.smith' is the person in the company that receives the email?",
      8:"Correct, I think that should be enough for now, we can get back to this later.",
    },
    "de":{
    0:"Gute Arbeit, aber bevor wir weitermachen können, müssen wir noch ein wenig über E-Mail-Adressen sprechen. Gib mir doch mal eine Beispieladresse.",
    1:"Wie wäre es mit 'john.smith@yahoo.com'?",
    2:"Ok, eine E-Mail-Adresse hat einen lokalen vor und einen Domänen-Teil nach dem '@'.",
    3:"Also ist 'john.smith' der lokale und 'yahoo.com' der Domänen-Teil?",
    4:"Ja, wichtig ist auch, dass die Domäne von rechts nach links gelesen wird. Also ist 'com' die Top-Level-Domäne oder TLD. Diese TLDs gehören meist zu Ländern.",
    5:"Ok, aber was ist 'yahoo'?",
    6:"Nun, 'yahoo' ist der Hostname in der 'com' Top-Level-Domäne. Normalerweise sind der Hostname und der Name der Firma, die diese Domäne besitzt, identisch. Darüber hinaus kann es auch andere Sub-Level-Domänen geben, wie z. B. 'support' in 'support.yahoo.com'. Sub-Level-Domänen sind quasi einzelne Abteilungen des Hauptunternehmens.",
    7:"Damit, ist der lokale Teil 'john.smith' die Person im Unternehmen, die die E-Mail erhält?",
    8:"Korrekt, ich glaube das sollte erstmal reichen.",
    },
  },
});

//similar domains
phishingLevels.push({
  id:6,
  name:'name-similar-domains',
  requirement:{0:4},
  dialog:{
    start:"supervisor",
    "en":{
      0:"Now that you understood email addresses let's get back to phishing.",
      1:"Sure, what's next?",
      2:"Phishers can use 'similar domains' that are close to real domains of companies to lure the recipients into a false sense of security.",
      3:"You mean by adding safety related terms such as 'security' or 'support' as sub-level domains of email addresses?",
      4:"Yes, essentially there are two ways for phishers to use 'similar domains'. First they can add terms to sub-level domains of email addresses or in the same vein switch the local with the domain part. The other approach is to use similar looking characters such as rn instead of m.",
      5:"I think I got it.",
      6:"Don't worry it's actually a lot easier than you may think. Basically look if the sender address was written correctly and if the domain actually belongs to the company from which the email claims to be.",
      7:"Then I guess it's time to write another phishing email.",
    },
    "de":{
      0:"Jetzt wo du E-Mail-Adressen verstanden hast, können wir zum Phishing zurückkehren.",
      1:"Ok, wie geht es weiter?",
      2:"Phisher können 'ähnliche Domänen' verwenden, die den echten Domänen von Unternehmen sehr ähnlich sehen, um Leute auszutricksen.",
      3:"Meinst du indem Wörter wie 'security' oder 'support' als Sub-Level-Domänen von E-Mail-Adressen verwendet werden?",
      4:"Ja, im Wesentlichen existieren zwei Möglichkeiten für 'ähnliche Domänen'. Zum einen können der E-Mail-Adresse neue Terme als Sub-Level-Domänen hinzugefügt werden oder man vertauscht den lokalen und den Domänen Teil. Zum anderen können Zeichen verwendet werden, die sich stark ähneln, wie z.B. rn anstatt eines m.",
      5:"Ok, ich glaube ich habe es verstanden.",
      6:"Keine Bange, es ist leichter als es zuerst den Anschein hat. Achte einfach darauf, dass die E-Mail-Adresse korrekt geschrieben ist und tatsächlich zum entsprechenden Unternehmen gehört.",
      7:"Ich schätze dann ist es wieder Zeit für mich eine neue Phishing-E-Mail zu schreiben.",
    },
  }
});
phishingLevels.push({
  date:'',
  subject:'',
  from:{name:'',email:''},
  to:{email:''},
  header:{
    'Return-Path':'',
    Received:[''],
    Date:'',
    From:'',
    Subject:'',
    To:'',
  },
  text:'',
  html:'',
  'isPhish':false,
  'isEncrypted':false,
  'isSigned':false,
  'help':'',
  'mark':{
    'subj': {
      'susp': false, //not suspicious
    },
    'date': {
      'susp': false,
    },
    'sender': {
      'susp': false,
    },
    'email': {
      'susp': true,
      'text': '',
    },
    'body': [
      {
        'markTxt': '',
        'text': '',
      },
    ],
  },
  creation:{
    infos: {id:7,matching:true,priorities:{0:"pf-similar-domain"},requirement:{0:4},name:'name-similar-domains',dialog:'dialog-similar-domains',topic:{en:"the unauthorized access of an account",de:"ein unautorisierter Kontozugriff"},}, //MATCHING IS FALSE
    /*Dear Customer,We believe that an unauthorized party may have accessed your account. To protect your privacy, we have: Deactivate your account until you have completed the review.
    Undo any changes made by this party.Outstanding orders cancelled.You cannot access your account until you have verified your ${{serviceName}} account.
    We urge you to complete validation as soon as possible. Failure to validate your information within 24 hours may result in the termination of your ${{serviceName}} account to protect our system.
    We apologize for the inconvenience. We work hard to protect our customers. Log in to your ${{serviceName}} account and update your account information.
    Log-In Best,,${{serviceName}}-Support.Wагnіng: Тhіѕ еmаіl wаѕ ѕеnt fгom аn е-mаіl notіfісаtіon thаt doеѕ not ассеpt іnсomіng mаіl. Рlеаѕе do not геѕond to thіѕ mеѕѕаgе."
    */
    "en":{
      "to": "Paxton Adone <p.adone@company.com>",
      "from":{  "option0":{text:"Support <support@arnazon.com>", phishing:true, type:"pf-similar-domain"},
                "option1":{text:"Help <help@you.com>", phishing:false, type:""},
                "option2":{text:"Amazon <amazon@securty.com>", phishing:true, type:"pf-similar-domain"} //TODO
              },
      "subject":{ "option0":{text:"Somebody logged into your account", phishing:false, type:""},
                  "option1":{text:"Unauthorized access", phishing:false, type:""},
                  "option2":{text:"Be careful", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Dear Paxton Adone,", phishing:false, type:""},
                  "option1":{text:"Mr. Adone,", phishing:false, type:""},
                  "option2":{text:"Hello Paxton,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"Somebody is trying to steal your account.", phishing:false, type:""},
                  "option1":{text:"I'm a senior administrator and supervise all amazon customer accounts.", phishing:true, type:"pf-authority"},
                  "option2":{text:"We believe that an unauthorized party may have accessed your account.", phishing:false, type:""} //TODO
            },
      "choice3":{ "option0":{text:"To protect your privacy, we have deactivated your account until you have completed the review.", phishing:false, type:""},
                  "option1":{text:"You cannot access your account until you verified it.", phishing:false, type:""},
                  "option2":{text:"Failure to validate your information within 24 hours via email may result in the termination of your amazon account.", phishing:true, type:"pf-scarcity"} //TODO
            },
      "choice4":{ "option0":{text:"We apologize for the inconvenience.", phishing:false, type:""},
                  "option1":{text:"We work hard to protect our customers.", phishing:false, type:""},
                  "option2":{text:"Please immediately respond with your account information via email, so we can verify it.", phishing:true, type:"pf-scarcity"} //TODO
                  },
      "choice5":{ "option0":{text:"Amazon support", phishing:false, type:""},
                  "option1":{text:"All the best", phishing:false, type:""},
                  "option2":{text:"Have a nice day Mr. Adone", phishing:false, type:""} //TODO
            },
    },
    "de":{
      "to": "Paxton Adone <p.adone@company.com>",
      "from":{  "option0":{text:"Support <support@arnazon.com>", phishing:true, type:"pf-similar-domain"},
                "option1":{text:"Hilfe <help@you.com>", phishing:false, type:""},
                "option2":{text:"Amazon <amazon@securty.com>", phishing:true, type:"pf-similar-domain"} //TODO
              },
      "subject":{ "option0":{text:"Jemand hat sich in Ihr Konto eingeloggt", phishing:false, type:""},
                  "option1":{text:"Unautorisierter Zugriff", phishing:false, type:""},
                  "option2":{text:"Seien Sie vorsichtig", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Lieber Paxton Adone,", phishing:false, type:""},
                  "option1":{text:"Hr. Adone,", phishing:false, type:""},
                  "option2":{text:"Hallo Paxton,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"Jemand versucht, Ihr Konto zu hacken.", phishing:false, type:""},
                  "option1":{text:"Ich bin Senior Administrator und betreue alle Amazon-Kundenkonten.", phishing:true, type:"pf-authority"},
                  "option2":{text:"Wir glauben, dass ein Unbefugter auf Ihr Konto zugegriffen haben könnte.", phishing:false, type:""} //TODO
            },
      "choice3":{ "option0":{text:"Um Ihre Privatsphäre zu schützen, haben wir Ihr Konto daher deaktiviert, bis Sie eine Überprüfung abschließen.", phishing:false, type:""},
                  "option1":{text:"Sie können erst dann auf Ihr Konto zugreifen, wenn Sie sich verifiziert haben.", phishing:false, type:""},
                  "option2":{text:"Wenn Sie Ihre Angaben nicht innerhalb von 24 Stunden per E-Mail bestätigen, kann dies zur Kündigung Ihres Amazon-Kontos führen.", phishing:true, type:"pf-scarcity"} //TODO
            },
      "choice4":{ "option0":{text:"Wir entschuldigen uns für die Unannehmlichkeiten.", phishing:false, type:""},
                  "option1":{text:"Wir arbeiten hart, um unsere Kunden zu schützen.", phishing:false, type:""},
                  "option2":{text:"Bitte teilen Sie uns Ihre Kontodaten sofort per E-Mail mit, damit wir diese überprüfen können.", phishing:true, type:"pf-scarcity"} //TODO
                  },
      "choice5":{ "option0":{text:"Amazon Support", phishing:false, type:""},
                  "option1":{text:"Alles Gute", phishing:false, type:""},
                  "option2":{text:"Einen schönen Tag noch Hr. Adone", phishing:false, type:""} //TODO
            },
    },
  },
});

//email spoofing
phishingLevels.push({
  id:8,
  name:'name-spoofing',
  requirement:{0:7},
  dialog:{
    start:"supervisor",
    "en":{
      0:"Ok, while 'similar domains' are at least somewhat easy to spot the next feature is a bit more problematic.",
      1:"I'm all ears.",
      2:"Basically, nothing in emails gets verified. So anybody could easily send emails on the behalf of official email addresses such as 'service@paypal.com'. This is called 'email spoofing'.",
      3:"Wow, that seems like a huge oversight. Did they fix it yet?",
      4:"Yes, there are ways to authenticate emails but these largely depend on the email providers. So you can't be sure they are employed.",
      5:"So, in essence it's best not to trust an email even if the shown email address is actually from a reputable company?",
      6:"Yeah, that's correct! And remember that email addresses are read from right to left. Otherwise you might end up using a 'similar domain' instead of 'email spoofing'.",//Sadly as of now that's the best approach to deal with 'email spoofing'.",
      7:"Ok, I'll remember that!", //even if the address looks legitimate the email could still be phishing, got it!",
    },
    "de":{
      0:"Ok, während 'ähnliche Domains' zumindest halbwegs leicht zu erkennen sind, ist das nächste Merkmal ein wenig problematischer.",
      1:"Ich bin ganz Ohr.",
      2:"Im Wesentlichen wird in E-Mails nichts verifiziert. Jeder kann also E-Mails im Namen offizieller E-Mail-Adressen wie z.B. 'service@paypal.com' versenden. Dies bezeichnen wir dann als 'E-Mail-Spoofing'.",
      3:"Wow, das scheint ein gewaltiges Versäumnis zu sein. Wurde es schon behoben?",
      4:"Ja, es gibt Wege E-Mails zu authentifizieren, aber diese hängen von den E-Mail-Anbietern ab. Es ist also nicht garantiert, dass sie eingesetzt werden.",
      5:"Im Grunde ist es also am besten, einer E-Mail erstmal nicht direkt zu vertrauen, selbst wenn die angezeigte E-Mail-Adresse tatsächlich von einem seriösen Unternehmen stammt?",
      6:"Ja, das stimmt. Und denk daran, dass du E-Mail-Adressen von rechts nach links liest. Sonst verwendest du eventuell eine 'ähnliche Domäne' und kein 'E-Mail Spoofing'.", //ist das im Moment die beste Methode, um mit 'E-Mail-Spoofing' umzugehen
      7:"Ok, das werde ich mir merken!", //selbst wenn die Adresse legitim aussieht, könnte die E-Mail immer noch Phishing sein, verstanden!",
    },
  }
});
phishingLevels.push({
  date:'',
  subject:'',
  from:{name:'',email:''},
  to:{email:''},
  header:{
    'Return-Path':'',
    Received:[''],
    Date:'',
    From:'',
    Subject:'',
    To:'',
  },
  text:'',
  html:'',
  'isPhish':false,
  'isEncrypted':false,
  'isSigned':false,
  'help':'',
  'mark':{
    'subj': {
      'susp': false, //not suspicious
    },
    'date': {
      'susp': false,
    },
    'sender': {
      'susp': false,
    },
    'email': {
      'susp': true,
      'text': '',
    },
    'body': [
      {
        'markTxt': '',
        'text': '',
      },
    ],
  },
  creation:{
    infos: {id:9,matching:true,priorities:{0:"pf-spoofing"},requirement:{0:7},name:'name-spoofing',dialog:'dialog-spoofing',topic:{en:"a full Gmail mailbox",de:"ein volles Gmail-Postfach"},}, //MATCHING IS FALSE


    /*E-mail Spam and Fraudulent Survey your mailbox is almost full. Dear ${{recipientMail}} 3840MB 4096MB Current size Maximum size Please reduce your mailbox size.
    Delete any items you don't need from your mailbox and empty your Deleted Items folder. Click here to do reduce size automatically.
    Thanks, Mail System Administrator This notification was sent to ${{recipientMail}}; Don't want occasional updates about subscription preferences and friendly suggestions?
    Change what email Google+ sends you.Google Inc., 1600 Amphitheatre Pkwy, Mountain View, CA 94043 USA
    */
    "en":{
      "to": "Rhiannan Molloy <r.molloy@company.com>",
      "from":{  "option0":{text:"Gmail <gmail@accounts.com>", phishing:true, type:"pf-similar-domain"},
                "option1":{text:"<support@awiehzgfz08aw.com>", phishing:false, type:""},
                "option2":{text:"Account management <no-reply@accounts.google.com>", phishing:true, type:"pf-spoofing"} //TODO
              },
      "subject":{ "option0":{text:"Attention user", phishing:false, type:""},
                  "option1":{text:"E-mail spam and fraudulent surveys, your mailbox is almost full.", phishing:false, type:""},
                  "option2":{text:"You exceeded your storage limit", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Mrs. Molloy,", phishing:false, type:""},
                  "option1":{text:"Dear,", phishing:false, type:""},
                  "option2":{text:"Good day,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"3840MB/4096MB current size of maximum size, please reduce your mailbox size.", phishing:false, type:""},
                  "option1":{text:"It appears you have almost reached your storage limit.", phishing:false, type:""},
                  "option2":{text:"You should be aware that your mailbox is almost full.", phishing:false, type:""} //TODO
            },
      "choice3":{ "option0":{text:"If you don't want occasional updates just let us know.", phishing:false, type:""},
                  "option1":{text:"Reply to this email and send us your login credentials so we can increase the size of your mailbox in the next 72h.", phishing:true, type:"pf-scarcity"},
                  "option2":{text:"Delete any items you don't need from your mailbox and empty your Deleted Items folder.", phishing:false, type:""} //TODO
            },
      "choice4":{ "option0":{text:"You can respond to this email with your login credentials to have our first-class administrators reduce the size for you.", phishing:true, type:"pf-authority"},
                  "option1":{text:"Nontheless we hope you have a pleasant day.", phishing:false, type:""},
                  "option2":{text:"You should always be careful who you grant access to your account.", phishing:false, type:""} //TODO
                  },
      "choice5":{ "option0":{text:"Kind regards your Gmail Team", phishing:false, type:""},
                  "option1":{text:"Google Inc., 1600 Amphitheatre Pkwy, Mountain View, CA 94043 USA", phishing:false, type:""},
                  "option2":{text:"Thanks, Mail System Administrator", phishing:true, type:"pf-authority"} //TODO
            },
      },
    "de":{
      "to": "Rhiannan Molloy <r.molloy@company.com>",
      "from":{  "option0":{text:"Gmail <gmail@accounts.com>", phishing:true, type:"pf-similar-domain"},
                "option1":{text:"<support@awiehzgfz08aw.com>", phishing:false, type:""},
                "option2":{text:"Account Verwaltung <no-reply@accounts.google.com>", phishing:true, type:"pf-spoofing"} //TODO
              },
      "subject":{ "option0":{text:"Achtung Benutzer", phishing:false, type:""},
                  "option1":{text:"Spam und betrügerische Umfrage haben Ihr Postfach fast komplett gefüllt", phishing:false, type:""},
                  "option2":{text:"Sie haben Ihr Speicherlimit erreicht", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Fr. Molloy,", phishing:false, type:""},
                  "option1":{text:"Liebe,", phishing:false, type:""},
                  "option2":{text:"Guten Tag,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"3840MB/4096MB aktuelle Größe der maximalen Größe, bitte reduzieren Sie den verbrauchten Speicherplatz Ihres Postfachs.", phishing:false, type:""},
                  "option1":{text:"Es scheint, dass Sie Ihr Speicherlimit fast erreicht haben.", phishing:false, type:""},
                  "option2":{text:"Sie sollten wissen, dass Ihr Postfach fast voll ist.", phishing:false, type:""} //TODO
            },
      "choice3":{ "option0":{text:"Wenn Sie keine regelmäßigen Updates wünschen, lassen Sie es uns einfach wissen.", phishing:false, type:""},
                  "option1":{text:"Antworten Sie auf diese E-Mail und senden Sie uns Ihre Anmeldedaten, damit wir Ihr Postfach in den nächsten 72h vergrößern können.", phishing:true, type:"pf-scarcity"},
                  "option2":{text:"Löschen Sie nicht benötigte Elemente aus Ihrem Postfach und leeren Sie den Ordner 'Gelöschte Objekte'.", phishing:false, type:""} //TODO
            },
      "choice4":{ "option0":{text:"Sie können auf diese E-Mail mit Ihren Anmeldedaten antworten, damit unsere erklassigen Administratoren ihren Papierkorb für sie leeren.", phishing:true, type:"pf-authority"},
                  "option1":{text:"Nichtsdestotrotz wünschen wir Ihnen einen angenehmen Tag.", phishing:false, type:""},
                  "option2":{text:"Sie sollten immer vorsichtig sein, wem Sie Zugriff auf Ihr Konto gewähren.", phishing:false, type:""} //TODO
                  },
      "choice5":{ "option0":{text:"Mit freundlichen Grüßen Ihr Gmail Team", phishing:false, type:""},
                  "option1":{text:"Google Inc., 1600 Amphitheatre Pkwy, Mountain View, CA 94043 USA", phishing:false, type:""},
                  "option2":{text:"Danke, E-Mail System Administrator", phishing:true, type:"pf-authority"} //TODO
            },
      },
  },
});

//URLs
phishingLevels.push({
  id:10,
  name:'name-url-obfuscation1',
  requirement:{0:9},
  dialog:{
    start:"supervisor",
    "en":{
      0:"You did really well so far, but before we can continue we need to talk a bit about URLs since the email responses you used aren't that common anymore. URLs, also knowns as links, are used to locate resources on the internet. Why don't you give me an example URL?",
      1:"Thank you, how about 'https://www.about.gitlab.com'?",
      2:"Ok, so the 'https' refers to the protocol that is used to communicate between the website and your pc. Basically it ensures that data is transmitted in a way such that nobody can listen to what is being send.", // Without going to much into detail just make sure that links use HTTPS.",
      3:"Sure. The 'www.about.gitlab.com' is the domain, right?",
      4:"Yes, domains and email addresses adhere to the same rules.",
      5:"Got it, 'gitlab' is the main company that uses the 'com' top-level domain to be accessible. Then 'www' and 'about' are sub-domains or departments of 'gitlab'.",
      6:"Correct. Furthermore, there're a lot of different 'URL obfuscation' strategies. For example phishers can use 'similar domains' just as in emails. Apart from that they could also trick you with using IP-addresses instead of a domain. Most companies will never send out IP-based links such as 'https://142.250.199.78' since domains were specifically designed to wrap IPs into something better readable.",
    },
    "de":{
      0:"Du hast dich bisher gut geschlagen, aber bevor wir fortfahren können, müssen wir ein wenig über URLs sprechen. URLs, auch Links genannt, werden verwendet, um Ressourcen im Internet zu lokalisieren. Warum gibst du mir nicht eine Beispiel URL?",
      1:"Danke, wie wäre es mit 'https://www.about.gitlab.com'?",
      2:"Ok, das 'https' bezieht sich auf das Protokoll, das für die Kommunikation zwischen der Website und dem PC verwendet wird. Im Grunde sorgt es dafür, dass die Daten so übertragen werden, dass niemand mithören kann, was gesendet wird.",
      3:"Dann ist 'www.about.gitlab.com' die Domäne, richtig?",
      4:"Ja, Domänen und E-Mail-Adressen folgen den gleichen Regeln.",
      5:"Verstanden, dann ist 'gitlab' das Unternehmen das die 'com' Top-Level-Domäne nutzt. Und damit sind 'www' und 'about' die Sub-Domänen oder Abteilungen von 'gitlab'.",
      6:"Richtig. Außerdem gibt es eine Menge von Strategien zur 'URL-Verschleierung'. Zum Beispiel können Phisher 'ähnliche Domänen' wie in E-Mails verwenden. Außerdem können sie auch mit IP-Adressen statt einer Domäne tricksen. Die meisten Unternehmen werden niemals IP-basierte Links wie 'https://142.250.199.78' verschicken, da Domänen speziell dafür entwickelt wurden, IPs in etwas lesbares zu verpacken.",
    },
  }
});
phishingLevels.push({
  date:'',
  subject:'',
  from:{name:'',email:''},
  to:{email:''},
  header:{
    'Return-Path':'',
    Received:[''],
    Date:'',
    From:'',
    Subject:'',
    To:'',
  },
  text:'',
  html:'',
  'isPhish':false,
  'isEncrypted':false,
  'isSigned':false,
  'help':'',
  'mark':{
    'subj': {
      'susp': false, //not suspicious
    },
    'date': {
      'susp': false,
    },
    'sender': {
      'susp': false,
    },
    'email': {
      'susp': true,
      'text': '',
    },
    'body': [
      {
        'markTxt': '',
        'text': '',
      },
    ],
  },
  creation:{
    infos: {id:11,matching:true,priorities:{0:"pf-url-obfuscation"},requirement:{0:9},name:'name-url-obfuscation1',dialog:'dialog-url-obfuscation',topic:{en:"a UN grant",de:"eine UN Subvention"},}, //MATCHING IS FALSE


    /*
    Dear payment recipient. We would like to draw your attention to the fact that after a series of discussions with our Board of Directors of the Telephone Banking System (TBS / TDO),
    Boa, your outstanding payment of (USD 25 million) is held by security companies / IMF / IRS, has been signed and approved for payment by the United Nations.
    To achieve our goal of releasing your funds, we recommend that you reconfirm the following information so that we can complete this transaction with you before the end of the months.
    Your full name. Your full address.Direct phonenumber. Mobile. Profession. Nationality.
    Finally, your response to this email should be made immediately before it will be too late for you to receive your total funds (Twenty Five Million Dollars by Telephone Banking System of payment.
    Waiting for your immediate response. Regards UN Foreign Payment Officer Mobile ; + 1 312 724 6088
    */
    "en":{
      "to": "Samiya Norman <s.norman@company.com>",
      "from":{  "option0":{text:"United Nations <info-un@icloud.com>", phishing:true, type:"pf-similar-domain"},
                "option1":{text:"United Nations <relief-fund@un.org>", phishing:true, type:"pf-spoofing"},
                "option2":{text:"John <john18924@yahoo.com>", phishing:false, type:""} //TODO
              },
      "subject":{ "option0":{text:"Urgent message", phishing:false, type:""},
                  "option1":{text:"A huge opportunity awaits you", phishing:false, type:""},
                  "option2":{text:"A message from your UN ambassador", phishing:true, type:"pf-authority"} //TODO
                },
      "choice1":{ "option0":{text:"Dear payment recipient,", phishing:false, type:""},
                  "option1":{text:"Samiya Norman,", phishing:false, type:""},
                  "option2":{text:"Hello sir or madam,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"The outstanding sum of 25 million USD has been approved.", phishing:false, type:""},
                  "option1":{text:"It appears that today is your lucky day.", phishing:false, type:""},
                  "option2":{text:"Your outstanding payment of (USD 25 million) that is held by the IRS, has been signed and approved for payment by us the United Nations.", phishing:true, type:"pf-authority"} //TODO
            },
      "choice3":{ "option0":{text:"To achieve our goal of releasing your funds, we recommend that you confirm your information so we can complete this transaction.", phishing:false, type:""},
                  "option1":{text:"Just follow this link https://234.31.248.93 and follow the instrucitons.", phishing:true, type:"pf-url-obfuscation"},
                  "option2":{text:"This large sum of money is part of a model project and an investment into the future of this great nation.", phishing:false, type:""} //TODO
            },
      "choice4":{ "option0":{text:"Please enter your full name, full address, phone number, profession and nationality under https://un.service.funding.com.", phishing:true, type:"pf-url-obfuscation"},
                  "option1":{text:"Waiting for your response.", phishing:false, type:""},
                  "option2":{text:"Finally, your response to this email should be made immediately before it will be too late for you to receive your total funds.", phishing:true, type:"pf-scarcity"} //TODO
                  },
      "choice5":{ "option0":{text:"Regards your UN ambassador", phishing:true, type:"pf-authority"},
                  "option1":{text:"We hope to hear from you", phishing:false, type:""},
                  "option2":{text:"Please inform us about your decision", phishing:false, type:""} //TODO
            },
      },
    "de":{
      "to": "Samiya Norman <s.norman@company.com>",
      "from":{  "option0":{text:"Vereinte Nationen <info-un@icloud.com>", phishing:true, type:"pf-similar-domain"},
                "option1":{text:"Vereinte Nationen <relief-fund@un.org>", phishing:true, type:"pf-spoofing"},
                "option2":{text:"John <john18924@yahoo.com>", phishing:false, type:""} //TODO
              },
      "subject":{ "option0":{text:"Wichtige Mitteilung", phishing:false, type:""},
                  "option1":{text:"Eine große Chance wartet auf Sie", phishing:false, type:""},
                  "option2":{text:"Eine Nachricht von Ihrem UN-Botschafter", phishing:true, type:"pf-authority"} //TODO
                },
      "choice1":{ "option0":{text:"Sehr geehrter Zahlungsempfänger,", phishing:false, type:""},
                  "option1":{text:"Samiya Norman,", phishing:false, type:""},
                  "option2":{text:"Guten Tag Empfänger,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"Die ausstehende Summe von 25 Mio. € wurde genehmigt.", phishing:false, type:""},
                  "option1":{text:"Es scheint, dass heute Ihr Glückstag ist.", phishing:false, type:""},
                  "option2":{text:"Ihre ausstehende Zahlung in Höhe von (25 Mio. €), die sich im Besitz des Finanzamts befindeen, wurde von den Vereinten Nationen unterzeichnet und zur Zahlung freigegeben.", phishing:true, type:"pf-authority"} //TODO
            },
      "choice3":{ "option0":{text:"Um die Freigabe Ihres Geldes zu erreichen, empfehlen wir Ihnen, Ihre Daten zu bestätigen, damit wir die Transaktion abschließen können.", phishing:false, type:""},
                  "option1":{text:"Folgen Sie einfach diesem Link https://234.31.248.93 und befolgen Sie die Anweisungen.", phishing:true, type:"pf-url-obfuscation"},
                  "option2":{text:"Diese große Summe ist Teil eines Modellprojekts und eine Investition in die Zukunft ihres Landes.", phishing:false, type:""} //TODO
            },
      "choice4":{ "option0":{text:"Bitte geben Sie unter https://un.service.funding.com Ihren vollständigen Namen, Ihre vollständige Adresse, Telefonnummer, Beruf und Nationalität an.", phishing:true, type:"pf-url-obfuscation"},
                  "option1":{text:"Wir warten auf Ihre Antwort.", phishing:false, type:""},
                  "option2":{text:"Abschließend sollten Sie sofort auf diese E-Mail antworten, bevor es für Sie zu spät ist, Ihr gesamtes Geld zu erhalten.", phishing:true, type:"pf-scarcity"} //TODO
                  },
      "choice5":{ "option0":{text:"Mit freundlichen Grüßen Ihr UN-Botschafter", phishing:true, type:"pf-authority"},
                  "option1":{text:"Wir hoffen, von Ihnen zu hören", phishing:false, type:""},
                  "option2":{text:"Bitte teilen Sie uns Ihre Entscheidung mit", phishing:false, type:""} //TODO
            },
    },
  },
});
phishingLevels.push({
  id:12,
  name:'name-url-obfuscation2',
  requirement:{0:11},
  dialog:{
    start:"supervisor",
    "en":{
      0:"Ok, that's it for the first part of 'URL obfuscation'.",
      1:"Are there even more possibilities to phish somebody with URLs?",
      2:"Yes, actually two more obfuscation schemes come to mind. For example an URL such as 'http://www.ebay.de' can be embeded in a link text such as 'click here'. This would look like: <a href= 'http://www.ebay.com'> click here </a>. However, email clients would only show the 'click here' part in the email.",
      3:"And 'click here' would actually lead to 'http://www.ebay.com'. But how do I know when another link is embedded and where a link is actually leading?",
      4:"That's actually really easy. You just have to hover your mouse over the link without clicking on it. In every normal email client the embedded URL will then be shown on the screen.",
      5:"Ok, so I just have to hover over the link without clicking on it to see where it is actually leading. This embedding is just there to save line space, right?",
      6:"Yeah, that was the original idea. The other scheme is 'redirection'. You can use a normal URL such as 'http://www.aol.com' and then append '@phish.org' or '/url?=http://www.phish.org'. The link will then lead to the website 'http://www.phish.org'. However, modern browsers should inform users about redirections. But it's best to also look out for it yourself.",
    },
    "de":{
      0:"Gut, damit hätten wir schon ein wenig über 'URL-Verschleierung' gelernt.",
      1:"Gibt es noch mehr Möglichkeiten, jemanden mit URLs zu phishen?",
      2:"Ja, mir fallen da noch zwei weitere Verschleierungen ein. Zum Beispiel kann eine URL wie 'http://www.ebay.de' in einen Linktext wie 'klicke hier' eingebettet werden. Das würde dann so aussehen: <a href= 'http://www.ebay.de'> klicke hier </a>. E-Mail-Clients würden dem Nutzer aber nur 'klicke hier' anzeigen.",
      3:"Und 'klicke hier' würde in diesem Beispiel zu 'http://www.ebay.de' führen. Aber wie finde ich heraus ob ein anderer Link eingebettet wurde und wohin ein Link tatsächlich führt?",
      4:"Das ist eigentlich ganz einfach. Du musst nur mit der Maus über den Link fahren, ohne ihn anzuklicken. In gängigen E-Mail-Clients wird dann die eingebettete URL angezeigt.",
      5:"Ok, ich muss nur mit der Maus über den Link fahren, ohne ihn anzuklicken. Diese Einbettung ist vermutlich da, um Platz zu sparen, oder?",
      6:"Ja, das ist die Idee dahinter. Dann gibts es da noch die 'Umleitung'. Du kannst eine URL wie 'http://www.aol.com' verwenden und etwas wie '@phish.org' oder '/url?=http://www.phish.org' anhängen. Das führt dann auf die Seite 'http://www.phish.org'. Moderne Browser sollten solche Umleitungen aber in der Regel melden. Du solltest aber auch selbst darauf achten.",
    },
  }
});
phishingLevels.push({
  date:'',
  subject:'',
  from:{name:'',email:''},
  to:{email:''},
  header:{
    'Return-Path':'',
    Received:[''],
    Date:'',
    From:'',
    Subject:'',
    To:'',
  },
  text:'',
  html:'',
  'isPhish':false,
  'isEncrypted':false,
  'isSigned':false,
  'help':'',
  'mark':{
    'subj': {
      'susp': false, //not suspicious
    },
    'date': {
      'susp': false,
    },
    'sender': {
      'susp': false,
    },
    'email': {
      'susp': true,
      'text': '',
    },
    'body': [
      {
        'markTxt': '',
        'text': '',
      },
    ],
  },
  creation:{
    infos: {id:13,matching:true,priorities:{0:"pf-url-obfuscation"},requirement:{0:11},name:'name-url-obfuscation2',dialog:'dialog-url-obfuscation',topic:{en:"a tax refund",de:"eine Steuerrückzahlung"},}, //MATCHING IS FALSE

    /*Dear taxpayer, On your last tax return, we noticed a discrepancy.
    You have paid more taxes than our department was owed, and as such have to be afforded a refund.
    Please visit ${{phishingURL}} soon to provide your information and receive the refund within 14 days.
    Sincerely, Internal Revenue Service",
    */
    "en":{
      "to": "Izabela Ali <i.ali@company.com>",
      "from":{  "option0":{text:"Werner <werner@safdoiuh.de>", phishing:false, type:""},
                "option1":{text:"IRS <irs@gov.to>", phishing:true, type:"pf-similar-domain"},
                "option2":{text:"Ed Merrit <ed@merritt.uk>", phishing:false, type:""} //TODO
              },
      "subject":{ "option0":{text:"Taxes", phishing:false, type:""},
                  "option1":{text:"We took too much from you", phishing:false, type:""},
                  "option2":{text:"Tax discrepancy", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Hello,", phishing:false, type:""},
                  "option1":{text:"Greetings,", phishing:false, type:""},
                  "option2":{text:"Dear taxpayer,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"It came to our attention that you paid too many taxes.", phishing:false, type:""},
                  "option1":{text:"Our automated system discovered that you're owed a certain amount of money (5000$).", phishing:false, type:""},
                  "option2":{text:"On your last tax return, we noticed a discrepancy.", phishing:false, type:""} //TODO
            },
      "choice3":{ "option0":{text:"Please visit 'https://www.irs.gov@241.192.4.1' to provide your information.", phishing:true, type:"pf-url-obfuscation"},
                  "option1":{text:"If you reply to this email and immediately send us your login credentials we can help you.", phishing:true, type:"pf-scarcity"},
                  "option2":{text:"Even though this seems rather odd we ensure you that the issue was on our side.", phishing:false, type:""} //TODO
            },
      "choice4":{ "option0":{text:"We are sorry for the inconvenience.", phishing:false, type:""},
                  "option1":{text:"Regardless we hope to make it as easy for you as possible to get your money back.", phishing:false, type:""},
                  "option2":{text:"Just click <a href= 'http://www.awer3.jtz5.com'> here </a>.", phishing:true, type:"pf-url-obfuscation"} //TODO
                  },
      "choice5":{ "option0":{text:"Sincerely, Internal Revenue Service", phishing:false, type:""},
                  "option1":{text:"Best wishes your IRS agent E. Merritt", phishing:true, type:"pf-authority"},
                  "option2":{text:"Your IRS team", phishing:true, type:"pf-authority"} //TODO
            },
      },
    "de":{
      "to": "Izabela Ali <i.ali@company.com>",
      "from":{  "option0":{text:"Werner <werner@safdoiuh.de>", phishing:false, type:""},
                "option1":{text:"IRS <irs@gov.to>", phishing:true, type:"pf-similar-domain"},
                "option2":{text:"Ed Merrit <ed@merritt.uk>", phishing:false, type:""} //TODO
              },
      "subject":{ "option0":{text:"Steuern", phishing:false, type:""},
                  "option1":{text:"Wir haben Ihnen zu viel berechnet", phishing:false, type:""},
                  "option2":{text:"Steuerdiskrepanz", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Hallo,", phishing:false, type:""},
                  "option1":{text:"Grüße,", phishing:false, type:""},
                  "option2":{text:"Sehr geehrter Steuerzahler,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"Es wurde festgestellt, dass Sie zu viele Steuern gezahlt haben.", phishing:false, type:""},
                  "option1":{text:"Unser automatisiertes System hat festgestellt, dass Ihnen ein bestimmter Geldbetrag (5000$) zusteht.", phishing:false, type:""},
                  "option2":{text:"Bei Ihrer letzten Steuererklärung ist uns eine Diskrepanz aufgefallen.", phishing:false, type:""} //TODO
            },
      "choice3":{ "option0":{text:"Bitte besuchen Sie 'https://www.irs.gov@241.192.4.1', um Ihre Daten zu bestätigen.", phishing:true, type:"pf-url-obfuscation"},
                  "option1":{text:"Wenn Sie sofort auf diese E-Mail antworten und uns Ihre Anmeldedaten zukommen lassen, können wir Ihnen helfen.", phishing:true, type:"pf-scarcity"},
                  "option2":{text:"Auch wenn dies etwas merkwürdig erscheint, versichern wir Ihnen, dass das Problem auf unserer Seite lag.", phishing:false, type:""} //TODO
            },
      "choice4":{ "option0":{text:"Wir bitten um Entschuldigung für die Unannehmlichkeiten.", phishing:false, type:""},
                  "option1":{text:"Unabhängig davon hoffen wir, es Ihnen so einfach wie möglich zu machen, Ihr Geld zurückzubekommen.", phishing:false, type:""},
                  "option2":{text:"Klicken sie einfach <a href= 'http://www.awer3.jtz5.com'> hier </a>.", phishing:true, type:"pf-url-obfuscation"} //TODO
                  },
      "choice5":{ "option0":{text:"Mit freundlichen Grüßen, Internal Revenue Service", phishing:false, type:""},
                  "option1":{text:"Alles Gute Ihr IRS Agent E. Merritt", phishing:true, type:"pf-authority"},
                  "option2":{text:"Ihr IRS Team", phishing:true, type:"pf-authority"} //TODO
            },
    },
  },
});

//generic salutation, poor spelling, poor grammar
phishingLevels.push({
  id:14,
  name:'name-mixed',
  requirement:{0:13},
  dialog:{
    start:"supervisor",
    "en":{
      0:"Nice work so far. Let's talk about some easier phishing features next, ok?",
      1:"Thank you and yeah sure.",
      2:"Ok, let's see. You already did it instinctively but phishers often use 'generic salutations' simply because they don't know their victims' names.",
      3:"Yeah, that makes sense, if they send out thousands of emails how could they know all of the victims' names.",
      4:"Exactly! Apart from 'generic salutations' 'poor spelling' and 'poor grammar' can be found in some phishing emails.",
      5:"Right, a legitimate company would have their employees proof read something before sending out emails to potential customers.",
      6:"Yes, in general companies want to appear professional and trustworthy. Therefore poor language can be an indicator for phishing.",
      7:"Alright, I'll try to integrate these things into my next phishing email. Let's see if someone falls for it."
    },
    "de":{
      0:"Bisher machst du gute Arbeit. Wie wäre es jetzt mit einem leichteren Merkmal?",
      1:"Danke und ja das klingt gut.",
      2:"Ok, schauen wir mal. Du hast es schon instinktiv getan, aber Phisher verwenden oft 'generische Anreden', einfach weil sie die Namen ihrer Opfer nicht kennen.",
      3:"Ja, das ergibt Sinn, wenn sie Tausende von E-Mails verschicken, wie könnten sie alle Namen der Opfer kennen.",
      4:"Ganz genau! Neben 'generischen Anreden' finden sich in manchen Phishing-E-Mails auch 'schlechte Rechtschreibung' und 'schlechte Grammatik'.",
      5:"Richtig, ein seriöses Unternehmen würde seine Mitarbeiter veranlassen, etwas Korrektur zu lesen, bevor es E-Mails an potenzielle Kunden verschickt.",
      6:"Ja, im Allgemeinen wollen Unternehmen professionell und vertrauenswürdig erscheinen. Daher kann schlechte Sprache ein Indikator für Phishing sein.",
      7:"Na gut, ich werde versuchen, diese Dinge in meine nächste Phishing-E-Mail zu integrieren. Mal sehen, ob jemand darauf hereinfällt."
    },
  }
});
phishingLevels.push({
  date:'',
  subject:'',
  from:{name:'',email:''},
  to:{email:''},
  header:{
    'Return-Path':'',
    Received:[''],
    Date:'',
    From:'',
    Subject:'',
    To:'',
  },
  text:'',
  html:'',
  'isPhish':false,
  'isEncrypted':false,
  'isSigned':false,
  'help':'',
  'mark':{
    'subj': {
      'susp': false, //not suspicious
    },
    'date': {
      'susp': false,
    },
    'sender': {
      'susp': false,
    },
    'email': {
      'susp': true,
      'text': '',
    },
    'body': [
      {
        'markTxt': '',
        'text': '',
      },
    ],
  },
  creation:{
    infos: {id:15,matching:true,priorities:{0:"pf-poor-spelling",1:"pf-poor-grammar",2:"pf-generic-salutation"},requirement:{0:13},name:'name-mixed',dialog:'dialog-poor-language',topic:{en:"compensation for the financial crisis",de:"Schadensersatz für die Finanzkrise"},}, //MATCHING IS FALSE

    /*"NATWEST BANK PLC, 10 Southwark Street , London Bridge , London - UK SE1 1TT RC: 121878 Our Ref: NTB/WESTMIN/INTER-15 Your Ref: Affidavit: AFX 076GD7B24 Tell=+447035969549 Fax+448435643403
    please kindly reply with this email (Natwstbakplc@outlook.com.au)  REF:- INSTRUCTION TO CREDIT YOUR ACCOUNT WITH THE SUM OF (US$10Million)
    In fight against corruption in the Banking system and in pursuit to re-build a good relationship with the British Government and the foreigners by the British Prime Minister.
    This is the second time we are notifying you about this said fund.
    After due vetting and evaluation of your file that was sent to us by the Nigerian Government in conjunction with the Ministry of Finance and Central Bank of the Federal Republic of Nigeria.
    The British Government, World Bank and United Nation  has approved a compensation payment of US$10,000,000.00 in your name which shall be transfer to your bank account through an online,
    we will create an online bank in your name and you will transfer your fund by yourself through our online to your bank account.
    We chose to transfer your fund through an online banking so that no Agency will notice or stop your fund.
    Sir, you shall receive this compensation fund within 3 working days, if you comply with our instructions and adhere to our directives.
    We were meant to understand from our findings that you have been going through hard ways by Paying a lot of charges to see to the release of your fund (US$10Million),
    this has been the handwork of some miscreant elements from that Country.
    We advise that you stop further communication with any correspondence from any bank, or anywhere Concerning your funds as you will receive your fund from this bank if you follow our instruction.
    Do not go through anybody again but through this Bank if you really want your fund.
    Finally, you are advice to re-confirm these to us,  Your Full Name, Contact address, Occupation Telephone and Fax Number for easy communication.
    We need your second email gmail or hotmail for security and private reasons.  Yours sincerely, Mr Anthony M. Smith Head Of Account Department, NatWest Bank, London United Kingdom.
    Here is Customers care private! Email address (Natwstbakplc@outlook.com.au)",
    */
    "en":{
      "to": "Zunaira Gomez <z.gomez@company.com>",
      "from":{  "option0":{text:"<etrhsaq@90afna.au>", phishing:false, type:""},
                "option1":{text:"HSBC Support <support@hsbc.com>", phishing:true, type:"pf-spoofing"},
                "option2":{text:"Bank <Natwstbakplc@outlook.com.au>", phishing:true, type:"pf-similar-domain"} //TODO
              },
      "subject":{ "option0":{text:"In eff0rt of mendlng a broken rel4tinship", phishing:true, type:"pf-poor-spelling"},
                  "option1":{text:"We make right again, wish for good partners", phishing:true, type:"pf-poor-grammar"},
                  "option2":{text:"In hopes of setting things right", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Dear sir or madame,", phishing:true, type:"pf-generic-salutation"},
                  "option1":{text:"Mrs. Gomez,", phishing:false, type:""},
                  "option2":{text:"Hello Zunaira Gomez,", phishing:false, type:""} //TODO
                },
      "choice2":{ "option0":{text:"We desire a good relationship with the British Governend and the British Preime Minister.", phishing:true, type:"pf-poor-spelling"},
                  "option1":{text:"This is the second time we are notifying you about this fund.", phishing:false, type:""},
                  "option2":{text:"We are aware of the hardships the financial sector put the general public through.", phishing:false, type:""} //TODO
                },
      "choice3":{ "option0":{text:"After evaluation of your file that was sent to us by the Nigerian Government we want to compensate you.", phishing:false, type:""},
                  "option1":{text:"The World Bank has approved a compensation payment of US$10,000,000.00 in your name.", phishing:false, type:""},
                  "option2":{text:"In fight against bad evil in the Banking system give money you we want.", phishing:true, type:"pf-poor-grammar"} //TODO
                },
      "choice4":{ "option0":{text:"Do not go through anybody but this <a href= 'http://www.banking.90afna.au'> bank </a>.", phishing:true, type:"pf-url-obfuscation"},
                  "option1":{text:"VVe chose to transfr your fund through an online banking so that no 4gency can stop your fun.", phishing:true, type:"pf-poor-spelling"},
                  "option2":{text:"We understand from our findings that you have been going through hard ways by paying a lot of charges.", phishing:false, type:""} //TODO
                  },
      "choice5":{ "option0":{text:"We advise that you stop further communication with any other bank until you receive your funds.", phishing:false, type:""},
                  "option1":{text:"You will receive your fund from this bank if you follow our instruction.", phishing:false, type:""},
                  "option2":{text:"Finally, you are advice personal information to re-confirm these to us.", phishing:true, type:"pf-poor-grammar"} //TODO
                  },
      "choice6":{ "option0":{text:"Kind regards, Zunaira Gomez", phishing:false, type:""},
                  "option1":{text:"Hopefully your reply will be swift", phishing:false, type:""},
                  "option2":{text:"Yours sincerely, Mr Anthony M. Smith Head Of Account Department, NatWest Bank, London UK", phishing:true, type:"pf-authority"} //TODO
                },
    },
    "de":{
      "to": "Zunaira Gomez <z.gomez@company.com>",
      "from":{  "option0":{text:"<etrhsaq@90afna.au>", phishing:false, type:""},
                "option1":{text:"HSBC Support <support@hsbc.com>", phishing:true, type:"pf-spoofing"},
                "option2":{text:"Bank <Natwstbakplc@outlook.com.au>", phishing:true, type:"pf-similar-domain"} //TODO
              },
      "subject":{ "option0":{text:"Um 1 zerbrochene Beziehung zu rep4rieren", phishing:true, type:"pf-poor-spelling"},
                  "option1":{text:"Wir machen richtig, wünschen für gute Partnerschaft", phishing:true, type:"pf-poor-grammar"},
                  "option2":{text:"In der Hoffnung, die Dinge richtig zu stellen", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Sehr geehrte Damen und Herren,", phishing:true, type:"pf-generic-salutation"},
                  "option1":{text:"Fr. Gomez,", phishing:false, type:""},
                  "option2":{text:"Hallo Zunaira Gomez,", phishing:false, type:""} //TODO
                },
      "choice2":{ "option0":{text:"Wir wünschen uns ein gutes Verhältnis zur britischen Regirung und dem britischen Preimminister.", phishing:true, type:"pf-poor-spelling"},
                  "option1":{text:"Dies ist das zweite Mal, dass wir Sie bezüglich dieses Geldbetrages benachrichtigen.", phishing:false, type:""},
                  "option2":{text:"Wir sind uns der Strapazen bewusst, die der Finanzsektor der Allgemeinheit auferlegt.", phishing:false, type:""} //TODO
                },
      "choice3":{ "option0":{text:"Nach Auswertung Ihrer Akte, die uns von der nigerianischen Regierung zugesandt wurde, möchten wir Sie entschädigen.", phishing:false, type:""},
                  "option1":{text:"Die Weltbank hat eine Entschädigungszahlung in Höhe von 10.000.000,00 US$ auf Ihren Namen genehmigt.", phishing:false, type:""},
                  "option2":{text:"Im Kampf gegen das Böse im Bankensystem geben Sie Geld, das wir wollen.", phishing:true, type:"pf-poor-grammar"} //TODO
                },
      "choice4":{ "option0":{text:"Schalten Sie keine andere außer dieser <a href= 'http://www.banking.90afna.au'> Bank </a> ein.", phishing:true, type:"pf-url-obfuscation"},
                  "option1":{text:"VVir haben uns entschiedn, Ihr Geld rnittels Online-Banking zu überweisen, damit keine Institution Ihr Glück verhindern kann.", phishing:true, type:"pf-poor-spelling"},
                  "option2":{text:"Wir verstehen aus unseren Untersuchungen, dass Sie eine Menge Gebühren gezahlt haben.", phishing:false, type:""} //TODO
                  },
      "choice5":{ "option0":{text:"Wir raten Ihnen, die weitere Kommunikation mit jeder anderen Bank einzustellen, bis Sie Ihr Geld erhalten.", phishing:false, type:""},
                  "option1":{text:"Sie erhalten Ihr Geld von dieser Bank, wenn Sie unseren Anweisungen folgen.", phishing:false, type:""},
                  "option2":{text:"Schließlich werden Sie persönliche Informationen beraten, um diese an uns zurück zu bestätigen.", phishing:true, type:"pf-poor-grammar"} //TODO
                  },
      "choice6":{ "option0":{text:"Mit freundlichen Grüßen, Zunaira Gomez", phishing:false, type:""},
                  "option1":{text:"Wir hoffen auf eine baldige Antwort", phishing:false, type:""},
                  "option2":{text:"Hochachtungsvoll, Hr Anthony M. Smith Head Of Account Department, NatWest Bank, London UK", phishing:true, type:"pf-authority"} //TODO
                },
    },
  },
});

//malicious attachments
phishingLevels.push({
  id:16,
  name:'name-malicious-attachment',
  requirement:{0:15},
  dialog:{
    start:"supervisor",
    "en":{
      0:"The next phishing feature I'm going to show you are 'malicious attachments'.",
      1:"You mean like viruses?",
      2:"Yes, in general you shouldn't click on any attachment that you didn't request yourself.",
      3:"I thought this only applied to Word documents.",
      4:"Everything from Microsoft Office documents, PDFs to video files can contain malicious software that could try to steal your potentionally valuable data.",
      5:"Wow, I wasn't aware that there are so many ways to trick me into installing malicious software on my system. Then I'll try to attach something to my next phishing email and see what happens.",
    },
    "de":{
      0:"Lass uns als nächstes über 'bösartige Anhänge' sprechen.",
      1:"Meinst du sowas wie Viren?",
      2:"Ja, im Allgemeinen solltest du auf keinen Anhang klicken, den du nicht selbst angefordert hast.",
      3:"Ich dachte das würde nur für Word-Dokumente gelten.",
      4:"Alles, von Microsoft Office-Dokumenten über PDFs bis hin zu Videodateien, kann bösartige Software enthalten. Sobald sie auf deinem Rechner ist könnte diese Software versuchen deine Daten zu stehlen.",
      5:"Wow, mir war nicht bewusst, dass es so viele Möglichkeiten gibt, um mich auszutricksen, damit ich bösartige Software installiere. Dann werde ich an meine nächste Phishing E-Mail mal etwas anhängen und schauen was passiert.",
    },
  }
});
phishingLevels.push({
  date:'',
  subject:'',
  from:{name:'',email:''},
  to:{email:''},
  header:{
    'Return-Path':'',
    Received:[''],
    Date:'',
    From:'',
    Subject:'',
    To:'',
  },
  text:'',
  html:'',
  'isPhish':false,
  'isEncrypted':false,
  'isSigned':false,
  'help':'',
  'mark':{
    'subj': {
      'susp': false, //not suspicious
    },
    'date': {
      'susp': false,
    },
    'sender': {
      'susp': false,
    },
    'email': {
      'susp': true,
      'text': '',
    },
    'body': [
      {
        'markTxt': '',
        'text': '',
      },
    ],
  },
  creation:{
    infos: {id:17,matching:true,priorities:{0:"pf-malicious-attachment",},requirement:{0:15},name:'name-malicious-attachment',dialog:'dialog-malicious-attachment',topic:{en:"a new payroll software",de:"eine neue Lohnabrechnungssoftware"},}, //MATCHING IS FALSE

    /*ACTION REQUIRED : Staff & Faculty Administrator's  New payroll directory adjustment notification ,
    Please kindly  download the attached user verification routine and complete the required directive to be added to the new payroll directory as requested.
    Thank you, ${{serviceName}}, Payroll  Department.
    */
    "en":{
      "to": "John Connor <j.connor@company.com>",
      "from":{  "option0":{text:"Harvard faculty <harvard@faculty.fr>", phishing:true, type:"pf-similar-domain"},
                "option1":{text:"Robert Patrick <robert.patrick@sky.net>", phishing:false, type:""},
                "option2":{text:"Harvard <administration@harvard.edu>", phishing:true, type:"pf-spoofing"} //TODO
              },
      "subject":{ "option0":{text:"ACTION REQUIRED", phishing:false, type:""},
                  "option1":{text:"This is important", phishing:false, type:""},
                  "option2":{text:"Staff & faculty administrator's new payroll directory adjustment notification", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"John Connor,", phishing:false, type:""},
                  "option1":{text:"Dear faculty member,", phishing:true, type:"pf-generic-salutation"},
                  "option2":{text:"Mr Connor,", phishing:false, type:""} //TODO
                },
      "choice2":{ "option0":{text:"Due to a change in the Iegislation, the faculty had to change their acounting softwar.", phishing:true, type:"pf-poor-spelling"},
                  "option1":{text:"The faculty has transitioned to a new payroll software.", phishing:false, type:""},
                  "option2":{text:"Enclosed you will find a user verification routine.", phishing:true, type:"pf-malicious-attachment"} //TODO
                },
      "choice3":{ "option0":{text:"Since this will affect when you will receive your salary please act immediately.", phishing:true, type:"pf-scarcity"},
                  "option1":{text:"Afterwards, follow the self-explanatory instructions in the software to be added to the new payroll directory.", phishing:false, type:""},
                  "option2":{text:"A lot of great have been certified about this software and so do we agree.", phishing:true, type:"pf-poor-grammar"} //TODO
                },
      "choice4":{ "option0":{text:"With best regards", phishing:false, type:""},
                  "option1":{text:"Thank you, Harvard, Payroll Department", phishing:false, type:""},
                  "option2":{text:"We hope you have a pleasant day", phishing:false, type:""} //TODO
                },
      },
    "de":{
      "to": "John Connor <j.connor@company.com>",
      "from":{  "option0":{text:"Harvard Fakultät <harvard@faculty.fr>", phishing:true, type:"pf-similar-domain"},
                "option1":{text:"Robert Patrick <robert.patrick@sky.net>", phishing:false, type:""},
                "option2":{text:"Harvard <administration@harvard.edu>", phishing:true, type:"pf-spoofing"} //TODO
              },
      "subject":{ "option0":{text:"AKTION ERFORDERLICH", phishing:false, type:""},
                  "option1":{text:"Das ist wichtig", phishing:false, type:""},
                  "option2":{text:"Benachrichtigung über das neue Abrechnungsverzeichnis für Mitarbeiter und Administratoren der Fakultät", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"John Connor,", phishing:false, type:""},
                  "option1":{text:"Lieber Fakultätsmitarbeiter,", phishing:true, type:"pf-generic-salutation"},
                  "option2":{text:"Mr Connor,", phishing:false, type:""} //TODO
                },
      "choice2":{ "option0":{text:"Aufgrund einer Änderung der Gesetzgebung musste die FakuItät ihre Buchhaltungssoftwar umstellen.", phishing:true, type:"pf-poor-spelling"},
                  "option1":{text:"Die Fakultät hat auf eine neue Lohnabrechnungssoftware umgestellt.", phishing:false, type:""},
                  "option2":{text:"Anbei finden Sie eine Verifizierungsroutine für Ihre Nutzerdaten.", phishing:true, type:"pf-malicious-attachment"} //TODO
                },
      "choice3":{ "option0":{text:"Da dies Auswirkungen darauf hat, wann Sie Ihr Gehalt erhalten, handeln Sie bitte sofort.", phishing:true, type:"pf-scarcity"},
                  "option1":{text:"Folgen Sie anschließend den selbsterklärenden Anweisungen in der Software, um in das neue Abrechnungsverzeichnis aufgenommen zu werden.", phishing:false, type:""},
                  "option2":{text:"Über diese Software wurde viel Gutes schon berichtet und wir damit sind.", phishing:true, type:"pf-poor-grammar"} //TODO
                },
      "choice4":{ "option0":{text:"Mit freundlichen Grüßen", phishing:false, type:""},
                  "option1":{text:"Vielen Dank, Harvard, Abteilung Lohnbuchhaltung", phishing:false, type:""},
                  "option2":{text:"Hoffentlich haben Sie einen angenehmen Tag", phishing:false, type:""} //TODO
                },
    },
  },
});

//consistency
phishingLevels.push({
  id:18,
  name:'name-consistency',
  requirement:{0:17},
  dialog:{
    start:"supervisor",
    "en":{
      0:"'Consistency' is the next phishing feature I'm going to tell you about.",
      1:"I'm all ears.",
      2:"People tend to desire consistent behaviour in themselves and in others. Phishers can exploit this by stating that the victim actually requested the malicious attachment.",
      3:"That makes a lot of sense. If somebody tells me that he did something for me because I asked him to do it I would be less inclined to question him.",
      4:"In general, staying consistent with past behaviour can save us some time since we don't have to think things through again. However, it also can make us vulnerable.",
      5:"Then let's see how I can integrate this into a phishing email.",
    },
    "de":{
      0:"'Beständigkeit' ist das nächste Phishing Merkmal über das ich mit dir sprechen möchte.",
      1:"Ich bin ganz Ohr.",
      2:"Menschen neigen dazu, sich ein konsistentes Verhalten bei sich selbst und bei anderen zu wünschen. Phisher können dies ausnutzen, indem sie z.B. behaupten, dass das Opfer einen bösartigen Anhang selbst angefordert hat.",
      3:"Das ergibt Sinn. Wenn mir jemand sagt, dass er etwas für mich getan hat, weil ich ihn darum gebeten habe, würde ich das vermutlich weniger hinterfragen.",
      4:"Generell spart uns das Festhalten an früheren Entscheidungen etwas Zeit, weil wir Dinge nicht mehrmals durchdenken müssen. Dieses Verhalten kann uns aber leider auch angreifbar machen.",
      5:"Dann schaue ich Mal, wie ich 'Beständigkeit' in eine Phishing-E-Mail integrieren kann.",
    },
  }
});
phishingLevels.push({
  date:'',
  subject:'',
  from:{name:'',email:''},
  to:{email:''},
  header:{
    'Return-Path':'',
    Received:[''],
    Date:'',
    From:'',
    Subject:'',
    To:'',
  },
  text:'',
  html:'',
  'isPhish':false,
  'isEncrypted':false,
  'isSigned':false,
  'help':'',
  'mark':{
    'subj': {
      'susp': false, //not suspicious
    },
    'date': {
      'susp': false,
    },
    'sender': {
      'susp': false,
    },
    'email': {
      'susp': true,
      'text': '',
    },
    'body': [
      {
        'markTxt': '',
        'text': '',
      },
    ],
  },
  creation:{
    infos: {id:19,matching:true,priorities:{0:"pf-consistency",},requirement:{0:17},name:'name-consistency',dialog:'dialog-consistency',topic:{en:"a series of vacation pictures",de:"eine Reihe von Urlaubsfotos"},}, //MATCHING IS FALSE

    /* Holiday photos
    Hey Danny,
    remember me from your last vacation?
    Well, I remembered that you wanted me to share some of the scenery photos I took.
    I uploaded the photos <a href= 'http://myphotos.afsdj892.uk'> here </a>.
    I hope everything is working out for you.
    Hit me up if you want to talk.
    Simon Skinner
    */

    "en":{
      "to": "Danny Butterman <d.butterman@company.com>",
      "from":{  "option0":{text:"Simon Skinner <simon.skinner@yahoo.com>", phishing:false, type:""},
                "option1":{text:"<dr6i@gmail.com>", phishing:false, type:""},
                "option2":{text:"Simon Skinner <5im0n@aol.com>", phishing:false, type:""} //TODO
              },
      "subject":{ "option0":{text:"Some photos for you", phishing:false, type:""},
                  "option1":{text:"Holiday photos", phishing:false, type:""},
                  "option2":{text:"Long time not seen", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Howdy,", phishing:true, type:"pf-generic-salutation"},
                  "option1":{text:"Dear recipient,", phishing:true, type:"pf-generic-salutation"},
                  "option2":{text:"Hey Danny,", phishing:false, type:""} //TODO
                },
      "choice2":{ "option0":{text:"How do you do?", phishing:false, type:""},
                  "option1":{text:"Rernember me from your last vactation?", phishing:true, type:"pf-poor-spelling"},
                  "option2":{text:"I'm sorry I didn't reply sooner, but I've been quite a busy man.", phishing:true, type:"pf-consistency"} //TODO
                },
      "choice3":{ "option0":{text:"I hope everything is working out for you.", phishing:false, type:""},
                  "option1":{text:"It has been a long time, hasn't it?", phishing:false, type:""},
                  "option2":{text:"Well, I remembered that you wanted me to share some of the scenery photos I took.", phishing:true, type:"pf-consistency"} //TODO
                },
      "choice4":{ "option0":{text:"Hit me up if you want to talk.", phishing:false, type:""},
                  "option1":{text:"I uploaded the photos <a href= 'http://myphotos.afsdj892.uk'> here </a>.", phishing:true, type:"pf-url-obfuscation"},
                  "option2":{text:"Hopefully the attached photots are up to your standards.", phishing:true, type:"pf-malicious-attachment"} //TODO
                },
      "choice5":{ "option0":{text:"All the best Simon", phishing:false, type:""},
                  "option1":{text:"Wishes best for your", phishing:true, type:"pf-poor-grammar"},
                  "option2":{text:"Kind regards", phishing:false, type:""} //TODO
                },
      },
    "de":{
      "to": "Danny Butterman <d.butterman@company.com>",
      "from":{  "option0":{text:"Simon Skinner <simon.skinner@yahoo.com>", phishing:false, type:""},
                "option1":{text:"<dr6i@gmail.com>", phishing:false, type:""},
                "option2":{text:"Simon Skinner <5im0n@aol.com>", phishing:false, type:""} //TODO
              },
      "subject":{ "option0":{text:"Einige Fotos für Sie", phishing:false, type:""},
                  "option1":{text:"Urlaubsfotos", phishing:false, type:""},
                  "option2":{text:"Lange nicht mehr gesehen", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Servus,", phishing:true, type:"pf-generic-salutation"},
                  "option1":{text:"Lieber Empfänger,", phishing:true, type:"pf-generic-salutation"},
                  "option2":{text:"Hey Danny,", phishing:false, type:""} //TODO
                },
      "choice2":{ "option0":{text:"Guten Tag, wie geht es dir?", phishing:false, type:""},
                  "option1":{text:"Erinners du dich an rnich aus deinem letzten Urlaub?", phishing:true, type:"pf-poor-spelling"},
                  "option2":{text:"Es tut mir leid, dass ich dir nicht früher geantwortet habe, aber ich war sehr beschäftigt.", phishing:true, type:"pf-consistency"} //TODO
                },
      "choice3":{ "option0":{text:"Ich hoffe, alles läuft gut bei dir.", phishing:false, type:""},
                  "option1":{text:"Es ist schon lange her, nicht wahr?", phishing:false, type:""},
                  "option2":{text:"Nun, ich habe mich daran erinnert, dass du einige der Landschaftsfotos, die ich gemacht habe, wolltest.", phishing:true, type:"pf-consistency"} //TODO
                },
      "choice4":{ "option0":{text:"Melde dich bei mir, wenn du reden willst.", phishing:false, type:""},
                  "option1":{text:"Ich habe die Fotos <a href= 'http://myphotos.afsdj892.uk'> hier </a> hochgeladen.", phishing:true, type:"pf-url-obfuscation"},
                  "option2":{text:"Ich hoffe, dass die beigefügten Fotos deinen Ansprüchen genügen.", phishing:true, type:"pf-malicious-attachment"} //TODO
                },
      "choice5":{ "option0":{text:"Alles Gute Simon", phishing:false, type:""},
                  "option1":{text:"Nur Beste für das dich", phishing:true, type:"pf-poor-grammar"},
                  "option2":{text:"Mit freundlichen Grüßen", phishing:false, type:""} //TODO
                },
    },
  },
});

//liking principle
phishingLevels.push({
  id:20,
  name:'name-liking',
  requirement:{0:17},
  dialog:{
    start:"supervisor",
    "en":{
      0:"Did you ever notice that you like others that are similar to you?",
      1:"Yeah sure, my friends and me have a lot of things in common.",
      2:"Believe it or not, but phishers can exploit this as well. It is called the 'liking principle'. Basically humans are more inclined to comply with people or things that they perceive as either attractive or credible.",
      3:"But how does an email appear attractive to me?",
      4:"It sounds a bit weird, I have to agree with you there. Simply put, phishers can imitate benign emails such as security alerts or outright copy them. Then they will tamper with the links in the email.",
      5:"So people should focus on the content of the email and not the optic, got it.",
      6:"Yes, this and the fact that phishers can appear similar/friendly to us by pretending to enjoy the same things we do. This requires more research on the phishers' part but can be highly effective if they want to target specific individuals.",
      7:"That makes sense, I guess an email would appear more credilbe to me if it states my true name, address and other things about me."
    },
    "de":{
      0:"Dir ist bestimmt aufgefallen, das du Leute magst die dir ähneln, oder?",
      1:"Ja klar, meine Freunde und ich haben viel gemeinsam.",
      2:"Phisher können dies tatsächlich ausnutzen. Es nennt sich das 'Liking-Prinzip'. Grundsätzlich sind Menschen eher dazu geneigt, anderen Menschen oder Dingen zu folgen, die sie als attraktiv oder glaubwürdig wahrnehmen.",
      3:"Aber wie wirkt denn eine E-Mail attraktiv auf mich?",
      4:"Es klingt ein bisschen seltsam, da muss ich dir zustimmen. Einfach ausgedrückt: Phisher können gutartige E-Mails wie z.B. Sicherheitswarnungen imitieren oder sie einfach kopieren und nur die Links manipulieren.",
      5:"Ok, also sollte man auf den Inhalt und weniger auf die Optik achten, verstanden.",
      6:"Ja, Phisher können uns außerdem ähnlich oder freundlich erscheinen, indem sie vorgeben, die gleichen Dinge wie wir zu mögen. Das kostet sie zwar auch etwas Zeit, kann aber sehr effektiv sein, wenn sie bestimmte Personen ansprechen wollen.",
      7:"Das ergibt Sinn, ich denke, eine E-Mail würde mir glaubwürdiger erscheinen, wenn sie z.B. Dinge wie meinen echten Namen und meine Adresse enthält."
    },
  }
});
phishingLevels.push({
  date:'',
  subject:'',
  from:{name:'',email:''},
  to:{email:''},
  header:{
    'Return-Path':'',
    Received:[''],
    Date:'',
    From:'',
    Subject:'',
    To:'',
  },
  text:'',
  html:'',
  'isPhish':false,
  'isEncrypted':false,
  'isSigned':false,
  'help':'',
  'mark':{
    'subj': {
      'susp': false, //not suspicious
    },
    'date': {
      'susp': false,
    },
    'sender': {
      'susp': false,
    },
    'email': {
      'susp': true,
      'text': '',
    },
    'body': [
      {
        'markTxt': '',
        'text': '',
      },
    ],
  },
  creation:{
    infos: {id:21,matching:true,priorities:{0:"pf-liking",},requirement:{0:17},name:'name-liking',dialog:'dialog-liking',topic:{en:"a pending payment for a costume",de:"eine offene Zahlung für ein Kostüm"},}, //MATCHING IS FALSE

    /*"Dear Shopper, we are sorry to inform you that your wire transfer to ${{serviceName}} could not be booked.
    We hereby request you pay the remaining amount by ${{date}}. If we do not receive payment by this date, we will be forced to take your order to pay to a court.
    All related costs are at your expense. The complete claim form no. 607368141, from which you can see all positions, is attached.
    Due to the existing errors, you are also obliged to pay the incurred fees of 4.68 Euro.
    In order to prevent further costs, we ask you to transfer the outstanding amount to our account.
    All bookings up to ${{date}} will be accepted. If there are any questions or uncertainties, we expect to be contacted within three days.
    Best regards ${{serviceName}} Limited liability company
    */

    "en":{
      "to": "Mable Pines <m.pines@company.com>",
      "from":{  "option0":{text:"Gideon <gideon2012@yahoo.com>", phishing:false, type:""},
                "option1":{text:"Costume Shipping <service@costume-shipping.com>", phishing:false, type:""},
                "option2":{text:"Security Administration <security@paypal.com>", phishing:true, type:"pf-spoofing"} //TODO
              },
      "subject":{ "option0":{text:"We are sorry to inform you", phishing:false, type:""},
                  "option1":{text:"From one dress up artist to another", phishing:true, type:"pf-liking"},
                  "option2":{text:"Your transaction didn't go through", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Dear shopper,", phishing:true, type:"pf-generic-salutation"},
                  "option1":{text:"Hello Mable,", phishing:false, type:""},
                  "option2":{text:"Mrs Pines,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"The complete claim form no. 607368141, from which you can see all positions, is attached.", phishing:true, type:"pf-malicious-attachment"},
                  "option1":{text:"Due to the existing errors, you are obIiged to pay the incurred fees of 4.6B Euro.", phishing:true, type:"pf-poor-spelling"},
                  "option2":{text:"We hereby request you pay the remaining amount for your costume.", phishing:false, type:""} //TODO
            },
      "choice3":{ "option0":{text:"If we do not receive payment by next week, we will be forced to take your order to pay to a court.", phishing:false, type:"pf-scarcity"},
                  "option1":{text:"All related costs are at your expense.", phishing:false, type:""},
                  "option2":{text:"We know that you're a huge costume fan, just like we are, but please transfer the remaning amount.", phishing:true, type:"pf-liking"} //TODO
                },
      "choice4":{ "option0":{text:"Have a nice day", phishing:false, type:""},
                  "option1":{text:"Best wishes", phishing:false, type:""},
                  "option2":{text:"We hope to see you shop at our store again", phishing:false, type:""} //TODO
                },
    },
    "de":{
      "to": "Mable Pines <m.pines@company.com>",
      "from":{  "option0":{text:"Gideon <gideon2012@yahoo.com>", phishing:false, type:""},
                "option1":{text:"Kostümversand <service@costume-shipping.com>", phishing:false, type:""},
                "option2":{text:"Security Administration <security@paypal.com>", phishing:true, type:"pf-spoofing"} //TODO
              },
      "subject":{ "option0":{text:"Es tut uns leid, Sie darüber informieren zu müssen", phishing:false, type:""},
                  "option1":{text:"Von einem Verkleidungskünstler zum anderen", phishing:true, type:"pf-liking"},
                  "option2":{text:"Ihre Transaktion konnte nicht abgewickelt werden", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Lieber Kunde,", phishing:true, type:"pf-generic-salutation"},
                  "option1":{text:"Hallo Mable,", phishing:false, type:""},
                  "option2":{text:"Fr. Pines,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"Das vollständige Antragsformular Nr. 607368141, aus dem Sie alle Positionen ersehen können, ist beigefügt.", phishing:true, type:"pf-malicious-attachment"},
                  "option1":{text:"Aufgrund der vorhandenen Fehler sind Sie verpfIichtet, die angefallenen Gebühren in Höhe von 4,6B Euro zu zahlen.", phishing:true, type:"pf-poor-spelling"},
                  "option2":{text:"Wir fordern Sie hiermit auf, den Restbetrag für Ihr Kostüm zu begleichen.", phishing:false, type:""} //TODO
            },
      "choice3":{ "option0":{text:"Sollten wir das Geld nicht bis nächste Woche bekommen, müssen Sie mit rechtlichen Konsequenzen rechnen.", phishing:false, type:"pf-scarcity"},
                  "option1":{text:"Sie tragen alle damit verbundenen Kosten.", phishing:false, type:""},
                  "option2":{text:"Wir wissen, dass Sie ein großer Kostümfan sind, genau wie wir, aber bitte überweisen Sie den Restbetrag.", phishing:true, type:"pf-liking"} //TODO
                },
      "choice4":{ "option0":{text:"Einen schönen Tag", phishing:false, type:""},
                  "option1":{text:"Alles Gute", phishing:false, type:""},
                  "option2":{text:"Wir hoffen, Sie wieder bei uns begrüßen zu dürfen", phishing:false, type:""} //TODO
                },
      },
  },
});

//reciprocation
phishingLevels.push({
  id:22,
  name:'name-reciprocation',
  requirement:{0:17},
  dialog:{
    start:"supervisor",
    "en":{
      0:"The next phishing feature I want to tell you about is 'reciprocation'.",
      1:"I can't say that I've heard that one before.",
      2:"Don't worry, it's really not that difficult to understand. In essence, people are inclined to return favors they received.",
      3:"I guess, but how can phishers abuse this?",
      4:"Well, they can tell their vicitms that they protected them from other fictitious attacks of phishers or hackers.",
      5:"Ok, this makes a lot of sense. So 'reciprocation' could occur when I get emails that tell me that somebody stopped a third person from stealing my account?",
      6:"Exactly, that's it.",
    },
    "de":{
      0:"Das nächste Phishing Merkmal, über das ich mit dir sprechen möchte, ist 'Reziprozität'.",
      1:"Klingt komisch, ich kann nicht sagen, dass ich davon schon mal etwas gehört habe.",
      2:"Kein Sorge, es ist einfacher als es klingt. Im Wesentlichen sind wir bemüht Gefallen zurückzuzahlen.",
      3:"Das kann sein, aber wie können Phisher das denn missbrauchen?",
      4:"Naja, sie können z.B. vorgeben, dass sie ihre Opfer vor anderen fiktiven Angriffen von Phishern oder Hackern geschützt haben.",
      5:"Ok, das ergibt Sinn. Also könnte 'Reziprozität' vorliegen, wenn eine E-Mail mir mitteilt, dass mich jemand vor dem Hackingversuch einer anderen Person bewahrt hat?",
      6:"Genau, da sollte man immer aufpassen.",
    },
  }
});
phishingLevels.push({
  date:'',
  subject:'',
  from:{name:'',email:''},
  to:{email:''},
  header:{
    'Return-Path':'',
    Received:[''],
    Date:'',
    From:'',
    Subject:'',
    To:'',
  },
  text:'',
  html:'',
  'isPhish':false,
  'isEncrypted':false,
  'isSigned':false,
  'help':'',
  'mark':{
    'subj': {
      'susp': false, //not suspicious
    },
    'date': {
      'susp': false,
    },
    'sender': {
      'susp': false,
    },
    'email': {
      'susp': true,
      'text': '',
    },
    'body': [
      {
        'markTxt': '',
        'text': '',
      },
    ],
  },
  creation:{
    infos: {id:23,matching:true,priorities:{0:"pf-reciprocation",},requirement:{0:17},name:'name-reciprocation',dialog:'dialog-reciprocation',topic:{en:"the unauthorized access of an account",de:"ein unautorisierter Kontozugriff"},}, //MATCHING IS FALSE

    /*Dear American Express Member,
    Due to a suspicious attempt on your account from an unusual location,We recommend that you change your password.
    How to change your password?Proceed with the link below and log in.
    We will take it from there.
    Change PasswordThis notification will only last for 24 hours and your account may be subject toSuspension if no action is taken by you.
    Thank You.American Express Team.
    */
    "en":{
      "to": "Connie Travis <c.travis@company.com>",
      "from":{  "option0":{text:"AMEX Service <am3x@aol.com>", phishing:true, type:"pf-similar-domain"},
                "option1":{text:"Security <security@americanexpress.com>", phishing:true, type:"pf-spoofing"},
                "option2":{text:"Ian Moody <m00dy@gmail.com>", phishing:false, type:""} //TODO
              },
      "subject":{ "option0":{text:"Cyber criminals may be at work", phishing:false, type:""},
                  "option1":{text:"Security notification", phishing:false, type:""},
                  "option2":{text:"Suspicious login detected", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"To whom it may concern,", phishing:true, type:"pf-generic-salutation"},
                  "option1":{text:"Hey Connie,", phishing:false, type:""},
                  "option2":{text:"Dear American Express Member,", phishing:true, type:"pf-generic-salutation"} //TODO
            },
      "choice2":{ "option0":{text:"It is possible that a third party tried to hack into your banking account.", phishing:false, type:""},
                  "option1":{text:"Due to a suspicious attempt to log into your account from an unusual location, we recommend that you change your password.", phishing:false, type:""},
                  "option2":{text:"Someone may have tried to gain access to your account but don't worry we kept it secure.", phishing:true, type:"pf-reciprocation"} //TODO
            },
      "choice3":{ "option0":{text:"To change your password proceed with this <a href= 'https://www.amx.servise.org'> link </a>.", phishing:true, type:"pf-url-obfuscation"},
                  "option1":{text:"We have and will protect your account.", phishing:true, type:"pf-reciprocation"},
                  "option2":{text:"This notification will only last for 24 hours and your account may be subject to suspension if no action is taken by you.", phishing:true, type:"pf-scarcity"} //TODO
                },
      "choice4":{ "option0":{text:"We are so sorry for the inconvenience.", phishing:false, type:""},
                  "option1":{text:"If you want to sort this out via email please use the attached document to confirm your account information.", phishing:true, type:"pf-malicious-attachment"},
                  "option2":{text:"We recommend you visit https://www.amx.servise.org and follow the procedure.", phishing:true, type:"pf-url-obfuscation"} //TODO
                },
      "choice5":{ "option0":{text:"Your AMEX administration team", phishing:true, type:"pf-authority"},
                  "option1":{text:"Yours sincerely", phishing:false, type:""},
                  "option2":{text:"Thank You, American Express Team.", phishing:false, type:""} //TODO
                },
      },
    "de":{
      "to": "Connie Travis <c.travis@company.com>",
      "from":{  "option0":{text:"AMEX Service <am3x@aol.com>", phishing:true, type:"pf-similar-domain"},
                "option1":{text:"Security <security@americanexpress.com>", phishing:true, type:"pf-spoofing"},
                "option2":{text:"Ian Moody <m00dy@gmail.com>", phishing:false, type:""} //TODO
              },
      "subject":{ "option0":{text:"Cyber-Kriminelle könnten es auf Sie abgesehen haben", phishing:false, type:""},
                  "option1":{text:"Sicherheitswarnungen", phishing:false, type:""},
                  "option2":{text:"Verdächtige Anmeldung erkannt", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"An alle, die es betrifft,", phishing:true, type:"pf-generic-salutation"},
                  "option1":{text:"Hey Connie,", phishing:false, type:""},
                  "option2":{text:"Sehr geehrter American Express Kunde,", phishing:true, type:"pf-generic-salutation"} //TODO
            },
      "choice2":{ "option0":{text:"Es ist möglich, dass ein Unbefugter versucht hat, sich in Ihr Bankkonto zu hacken.", phishing:false, type:""},
                  "option1":{text:"Auf Grund eines Versuchs, sich von einem ungewöhnlichen Ort in Ihr Konto einzuloggen, empfehlen wir Ihnen, Ihr Passwort zu ändern.", phishing:false, type:""},
                  "option2":{text:"Jemand könnte versucht haben, sich Zugang zu Ihrem Konto zu verschaffen, aber keine Sorge, wir haben es verhindert.", phishing:true, type:"pf-reciprocation"} //TODO
            },
      "choice3":{ "option0":{text:"Für einen Passwortwechsel klicken Sie bitte <a href= 'https://www.amx.servise.org'> hier </a>.", phishing:true, type:"pf-url-obfuscation"},
                  "option1":{text:"Wir haben Ihren Account geschützt und werden dies auch weiterhin tun.", phishing:true, type:"pf-reciprocation"},
                  "option2":{text:"Diese Benachrichtigung gilt nur für 24 Stunden und Ihr Konto kann gesperrt werden, wenn Sie keine Maßnahmen ergreifen.", phishing:true, type:"pf-scarcity"} //TODO
                },
      "choice4":{ "option0":{text:"Wir entschuldigen uns ausdrücklich für die Unannehmlichkeiten.", phishing:false, type:""},
                  "option1":{text:"Wenn Sie dies per E-Mail erledigen möchten, verwenden Sie bitte das angehängte Dokument, um Ihre Kontoinformationen zu bestätigen.", phishing:true, type:"pf-malicious-attachment"},
                  "option2":{text:"Wir empfehlen Ihnen, https://www.amx.servise.org zu besuchen und die Prozedur zu befolgen.", phishing:true, type:"pf-url-obfuscation"} //TODO
                },
      "choice5":{ "option0":{text:"Ihr AMEX Administrations Team", phishing:true, type:"pf-authority"},
                  "option1":{text:"Hochachtungsvoll", phishing:false, type:""},
                  "option2":{text:"Vielen Dank, American Express Team.", phishing:false, type:""} //TODO
                },
    },
  },
});

//social proof
phishingLevels.push({
  id:24,
  name:'name-social-proof',
  requirement:{0:17},
  dialog:{
    start:"supervisor",
    "en":{
      0:"We're almost done, good work for your first day.",
      1:"Thank you.",
      2:"Our next phishing feature is called 'social proof'. You might have heard about it in another context before.",
      3:"Do you mean that people want their peers respect?",
      4:"Yeah, somewhat in that regard. 'Social proof' refers to people being influenced by their peers. If other people act in a certain way, we are inclined to do the same.",
      5:"So phishers could address their victims as part of a group. And then make their victims believe that everybody in this group reveals his personal data.",
      6:"Yes, that's right.",
    },
    "de":{
      0:"Wir sind fast fertig, gute Arbeit für deinen ersten Tag.",
      1:"Danke schön.",
      2:"Unser nächstes Phishing Merkmal ist der 'soziale Beweis'. Du hast davon vielleicht schon in einem etwas anderen Kontext gehört.",
      3:"Meinst du, dass Leute gerne von ihren Mitmenschen respektiert werden wollen?",
      4:"Ja, sowas in der Art. Der 'soziale Beweis' bezieht sich darauf wie wir von unseren Mitmenschen beeinflusst werden. Wenn andere auf eine bestimmte Art handeln, dann sind wir geneigt dies ebenfalls zu tun.",
      5:"Also könnten Phisher ihre Opfer als Teil einer größeren Gruppe ansprechen. Und natürlich würden die Phisher ihren Opfern vorgaukeln, dass jeder in dieser Gruppe seine Daten preisgibt.",
      6:"Ja genau, du hast es verstanden.",
    },
  }
});
phishingLevels.push({
  date:'',
  subject:'',
  from:{name:'',email:''},
  to:{email:''},
  header:{
    'Return-Path':'',
    Received:[''],
    Date:'',
    From:'',
    Subject:'',
    To:'',
  },
  text:'',
  html:'',
  'isPhish':false,
  'isEncrypted':false,
  'isSigned':false,
  'help':'',
  'mark':{
    'subj': {
      'susp': false, //not suspicious
    },
    'date': {
      'susp': false,
    },
    'sender': {
      'susp': false,
    },
    'email': {
      'susp': true,
      'text': '',
    },
    'body': [
      {
        'markTxt': '',
        'text': '',
      },
    ],
  },
  creation:{
    infos: {id:25,matching:true,priorities:{0:"pf-social-proof",},requirement:{0:17},name:'name-social-proof',dialog:'dialog-social-proof',topic:{en:"a cyber security training",de:"ein Kurs über Cybersicherheit"},}, //MATCHING IS FALSE

    /*Good morning!
    You have been assigned a new cybersecurity course.
    This training is mandatory for all Employees, and must be completed within 24hrs.
    Please complete the course ${{phishingURL}} It should only take about 20 minutes.
    And make sure to pay close attention; we will be testing your phish-spotting skills over the next couple months!
    PLEASE NOTE: Many employees have to take multiple awareness courses like this per year.
    This is NOT the same as any standart training that you’ve already taken.
    If you believe you have already taken this exact course, please login and double check that all training is complete.
    Here are the steps to complete this course: Click this link: ${{phishingURL}}\r\n2.
    Login with your username/Email and password.
    Take each module in order (top to bottom).
    Once you've finished all of the modules, a notice will appear at the top of the page letting you know everything is complete.
    That's it! Thank you for taking this course, and for doing your part to help keep our Email safe and secure!
    Cybersecurity Trainer, ITS
    */
    "en":{
      "to": "Leena Galvan <l.galvan@company.com>",
      "from":{  "option0":{text:"Account management <no-reply@accounts.google.com>", phishing:true, type:"pf-spoofing"}, //TODO
                "option1":{text:"Cyber Security Trainer <microsoft.security@trainer.au>", phishing:true, type:"pf-similar-domain"},
                "option2":{text:"Oskar Quinn <oskar.quinn87@icloud.com>", phishing:false, type:""},
              },
      "subject":{ "option0":{text:"You have been assigned a new cyber security course.", phishing:false, type:""},
                  "option1":{text:"I'm your new cyber security supervisor", phishing:true, type:"pf-authority"},
                  "option2":{text:"A cyber security training awaits you", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Hello Leena,", phishing:false, type:""},
                  "option1":{text:"Whats up,", phishing:true, type:"pf-generic-salutation"},
                  "option2":{text:"How are you Mrs Galvan,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"Please complete the course at https://www.microsoft.trainer.au, it should only take about 20 minutes.", phishing:true, type:"pf-url-obfuscation"},
                  "option1":{text:"Recently the IT department noticed a lot of phishing attempts.", phishing:false, type:""},
                  "option2":{text:"This training is mandatory for all employees.", phishing:true, type:"pf-social-proof"} //TODO
            },
      "choice3":{ "option0":{text:"Just click on this <a href= 'https://www.microsoft.trainer.au'> link </a> to start.", phishing:true, type:"pf-url-obfuscation"},
                  "option1":{text:"This is NOT the same as any standard training that you’ve already taken.", phishing:false, type:""},
                  "option2":{text:"Make sure to pay close attention, we will be testing your phish-spotting skills over the next couple months!", phishing:false, type:""} //TODO
                },
      "choice4":{ "option0":{text:"That's it! Thank you for taking this course", phishing:false, type:""},
                  "option1":{text:"Thank you for doing your part to help keeping the employees email service safe", phishing:true, type:"pf-social-proof"},
                  "option2":{text:"Cyber security Trainer, ITS", phishing:false, type:""} //TODO
                },
      },
    "de":{
      "to": "Leena Galvan <l.galvan@company.com>",
      "from":{  "option0":{text:"Account Management <no-reply@accounts.google.com>", phishing:true, type:"pf-spoofing"}, //TODO
                "option1":{text:"Cyber Security Trainer <microsoft.security@trainer.au>", phishing:true, type:"pf-similar-domain"},
                "option2":{text:"Oskar Quinn <oskar.quinn87@icloud.com>", phishing:false, type:""},
              },
      "subject":{ "option0":{text:"Sie haben einen neuen Cybersicherheitskurs zugewiesen bekommen.", phishing:false, type:""},
                  "option1":{text:"Ich bin Ihr neuer Vorgesetzter für Cybersicherheit", phishing:true, type:"pf-authority"},
                  "option2":{text:"Eine Cybersicherheitsschulung wartet auf Sie", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Hallo Leena,", phishing:false, type:""},
                  "option1":{text:"Alles locker?", phishing:true, type:"pf-generic-salutation"},
                  "option2":{text:"Wie geht es Ihnen Fr. Galvan,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"Bitte schließen Sie den 20-minütigen Kurs auf https://www.microsoft.trainer.au ab.", phishing:true, type:"pf-url-obfuscation"},
                  "option1":{text:"In letzter Zeit hat die IT-Abteilung eine Vielzahl von Phishing-Versuchen festgestellt.", phishing:false, type:""},
                  "option2":{text:"Diese Schulung ist für alle Mitarbeiter verpflichtend.", phishing:true, type:"pf-social-proof"} //TODO
            },
      "choice3":{ "option0":{text:"Klicken Sie einfach auf diesen <a href= 'https://www.microsoft.trainer.au'> Link </a>.", phishing:true, type:"pf-url-obfuscation"},
                  "option1":{text:"Dies ist NICHT dasselbe wie irgendein Standardtraining, das Sie bereits absolviert haben.", phishing:false, type:""},
                  "option2":{text:"Passen Sie gut auf, denn in den nächsten Monaten werden wir Ihre Fähigkeiten zur Erkennung von Phishing testen!", phishing:false, type:""} //TODO
                },
      "choice4":{ "option0":{text:"Das war schon alles! Danke, dass Sie diesen Kurs belegt haben", phishing:false, type:""},
                  "option1":{text:"Vielen Dank, dass Sie Ihren Teil dazu beitragen, dass der E-Mail-Dienst der Mitarbeiter sicher bleibt", phishing:true, type:"pf-social-proof"},
                  "option2":{text:"Cyber Security Trainer, ITS", phishing:false, type:""} //TODO
                },
      },
  },
});

//everything combined
phishingLevels.push({
  id:26,
  name:'name-bonus',
  requirement:{0:19,1:21,2:23,3:25},
  dialog:{
    start:"supervisor",
    "en":{
      0:"That's it, I think you have accquired a lot of phishing knowledge today. Good job!",
      1:"Thank you very much for teaching me all about it.",
      2:"You're welcome.",
      3:"So, what am I going to do tomorrow?",
      4:"Well, I have got a meeting tomorrow morning, so why don't you use everything you've learned so far and write some more phishing emails until I'm done. Maybe we could even make it into a little challenge. Try to use as many phishing features as possible. After all, we have to keep our employees vigilant.",
      5:"Alright, I'll do that! See you tomorrow.",
      6:"Until tommorow.",
    },
    "de":{
      0:"So, fertig. Ich denke du hast für heute genug über Phishing gelernt. Gute Arbeit!",
      1:"Vielen Dank, dass du dir die Zeit dafür genommen hast mir das alles beizubringen.",
      2:"Kein Problem.",
      3:"Was steht denn morgen an?",
      4:"Ich habe morgen früh ein Meeting. Warum nutzt du also nicht alles, was du bisher gelernt hast, und schreibst noch ein paar Phishing-E-Mails, bis ich fertig bin. Vielleicht könnten wir daraus auch eine kleine Herausforderung machen. Versuch einfach mal so viele Phishing Merkmale wie möglich zu verwenden. Schließlich müssen wir unsere Mitarbeiter dazu bringen stets wachsam zu sein.",
      5:"Alles klar, werde ich machen! Dann bis morgen.",
      6:"Bis morgen.",
    },
  }
});
phishingLevels.push({
  date:'',
  subject:'',
  from:{name:'',email:''},
  to:{email:''},
  header:{
    'Return-Path':'',
    Received:[''],
    Date:'',
    From:'',
    Subject:'',
    To:'',
  },
  text:'',
  html:'',
  'isPhish':false,
  'isEncrypted':false,
  'isSigned':false,
  'help':'',
  'mark':{
    'subj': {
      'susp': false, //not suspicious
    },
    'date': {
      'susp': false,
    },
    'sender': {
      'susp': false,
    },
    'email': {
      'susp': true,
      'text': '',
    },
    'body': [
      {
        'markTxt': '',
        'text': '',
      },
    ],
  },
  creation:{
    infos: {id:27,matching:true,priorities:{},requirement:{0:26},name:'name-bonus1',dialog:'dialog-everything',topic:{en:"a session about retirement benefits",de:"ein Gespräch über Altersvorsorge"},}, //MATCHING IS FALSE

    /*
    Employee ${{name}}, Each year, as an employee of ${{serviceName}} you are eligible to schedule a phone call, teleconference,
    or in-person meeting off campus with a representative for answers to your specific state, federal and individual retirement benefit questions.
    At your consultation you will be provided with information on what your expected income will be from retirement programme when you retire, and how much longer you will have to work.
    You will also receive advice on the best ways to utilize your  options with your retirement programme and/or Social Security benefits.
    Please be sure to indicate which type of appointment you prefer (off-campus, phone call, or teleconference) in the notes section while scheduling.
    Please also include your direct cell phone number. Appointments fill up quickly. Secure your spot by clicking on the link below or simply reply “yes” to this email.
    ${{phishingURL}} All licensed representatives are not employees of the college or retirement programme.To opt out of future mailings, click on the following link:${{phishingURL}}
    */
    "en":{
      "to": "Tarik Tanner <t.tanner@company.com>",
      "from":{  "option0":{text:"Administration <apple@administrator.com>", phishing:true, type:"pf-similar-domain"}, //TODO
                "option1":{text:"Jordan Fletcher <jordan.fletcher@gmail.com>", phishing:false, type:""},
                "option2":{text:"Fletcher <fletcher@yahoo.com>", phishing:false, type:""},
              },
      "subject":{ "option0":{text:"Your yearly retirment questions", phishing:false, type:""},
                  "option1":{text:"Retirement benefits", phishing:false, type:""},
                  "option2":{text:"Have you figured your retirement out yet?", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Dear employee,", phishing:true, type:"pf-generic-salutation"},
                  "option1":{text:"Tarik,", phishing:false, type:""},
                  "option2":{text:"It's time Mr Tanner", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"Your annual retirement benefit questioning session is available for scheduling in the next 72h.", phishing:true, type:"pf-scarcity"},
                  "option1":{text:"Please use the attached document to schedule your retirement question appointment.", phishing:true, type:"pf-malicious-attachment"},
                  "option2":{text:"As an employee of our company you are eligible to schedule your annual phone call for answers to your specific retirement benefit questions.", phishing:true, type:"pf-social-proof"} //TODO
            },
      "choice3":{ "option0":{text:"At your consultation you will be provided with information on what your expected pension and how much longer you will have to work.", phishing:false, type:""},
                  "option1":{text:"Since you requested to be among the first employees to use the service this time, we are contacting you first.", phishing:true, type:"pf-consistency"},
                  "option2":{text:"Our consultants will give advice on the best ways to utilize your options.", phishing:false, type:""} //TODO
                },
      "choice4":{ "option0":{text:"Please also make sure that you have additional data at hand during the call.", phishing:false, type:""},
                  "option1":{text:"To opt out of future mailings please reply with 'no' to this email.", phishing:false, type:""},
                  "option2":{text:"Secure your spot by clicking this <a href= 'http://www.apple.administrator.com'> link </a>.", phishing:true, type:"pf-url-obfuscation"} //TODO
                },
      "choice5":{ "option0":{text:"Thank you for your time", phishing:false, type:""},
                  "option1":{text:"Kind regards", phishing:false, type:""},
                  "option2":{text:"Your HR team", phishing:true, type:"pf-authority"} //TODO
                },
      },
    "de":{
      "to": "Tarik Tanner <t.tanner@company.com>",
      "from":{  "option0":{text:"Administration <apple@administrator.com>", phishing:true, type:"pf-similar-domain"}, //TODO
                "option1":{text:"Jordan Fletcher <jordan.fletcher@gmail.com>", phishing:false, type:""},
                "option2":{text:"Fletcher <fletcher@yahoo.com>", phishing:false, type:""},
              },
      "subject":{ "option0":{text:"Ihre jährlichen Fragen zur Altersvorsorge", phishing:false, type:""},
                  "option1":{text:"Ruhestandsleistungen", phishing:false, type:""},
                  "option2":{text:"Haben Sie Ihren Ruhestand schon geplant?", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Lieber Mitarbeiter,", phishing:true, type:"pf-generic-salutation"},
                  "option1":{text:"Tarik,", phishing:false, type:""},
                  "option2":{text:"Es ist wieder Zeit Hr. Tanner", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"Die Terminvergabe für ihr persönliches Gespräch bezüglich Ihrer Altersvorsorge steht in den nächsten 72h zu Verfügung.", phishing:true, type:"pf-scarcity"},
                  "option1":{text:"Bitte verwenden Sie das beigefügte Dokument, um einen Termin für Ihr Gespräch zu vereinbaren.", phishing:true, type:"pf-malicious-attachment"},
                  "option2":{text:"Als Mitarbeiter unseres Unternehmens haben Sie die Möglichkeit, Ihren jährlichen Termin zum Thema Altersvorsorge zu vereinbaren.", phishing:true, type:"pf-social-proof"} //TODO
            },
      "choice3":{ "option0":{text:"Bei Ihrem Beratungsgespräch erhalten Sie Informationen darüber, welche Rente Sie erwarten können und wie lange Sie noch arbeiten müssen.", phishing:false, type:""},
                  "option1":{text:"Da Sie darum gebeten haben, diesmal zu den ersten Mitarbeitern zu gehören, die den Service nutzen, kontaktieren wir Sie zuerst.", phishing:true, type:"pf-consistency"},
                  "option2":{text:"Unsere Berater geben Ihnen Tipps, wie Sie Ihre Möglichkeiten am besten ausschöpfen können.", phishing:false, type:""} //TODO
                },
      "choice4":{ "option0":{text:"Bitte stellen Sie auch sicher, dass Sie während des Gesprächs Ihre Unterlagen zur Hand haben.", phishing:false, type:""},
                  "option1":{text:"Um sich von zukünftigen E-Mails abzumelden, antworten Sie bitte mit 'nein' auf diese E-Mail.", phishing:false, type:""},
                  "option2":{text:"Sichern Sie sich Ihren Termin <a href= 'http://www.apple.administrator.com'> hier </a>.", phishing:true, type:"pf-url-obfuscation"} //TODO
                },
      "choice5":{ "option0":{text:"Vielen Dank für Ihre Zeit", phishing:false, type:""},
                  "option1":{text:"Mit freundlichen Grüßen", phishing:false, type:""},
                  "option2":{text:"Ihr Personalabteilungs-Team", phishing:true, type:"pf-authority"} //TODO
                },
    },
  },
});
phishingLevels.push({
  date:'',
  subject:'',
  from:{name:'',email:''},
  to:{email:''},
  header:{
    'Return-Path':'',
    Received:[''],
    Date:'',
    From:'',
    Subject:'',
    To:'',
  },
  text:'',
  html:'',
  'isPhish':false,
  'isEncrypted':false,
  'isSigned':false,
  'help':'',
  'mark':{
    'subj': {
      'susp': false, //not suspicious
    },
    'date': {
      'susp': false,
    },
    'sender': {
      'susp': false,
    },
    'email': {
      'susp': true,
      'text': '',
    },
    'body': [
      {
        'markTxt': '',
        'text': '',
      },
    ],
  },
  creation:{
    infos: {id:28,matching:true,priorities:{},requirement:{0:26},name:'name-bonus2',dialog:'dialog-everything',topic:{en:"a population census",de:"eine Volkszählung"},}, //MATCHING IS FALSE

    /*
    "Dear recipient, as usual, we are conducting our annual government census.
    You are required to provide your statistical information to help us assess the state of the country.
    If you do not fill out the form, you will lose several forms of tax benefits.
    Please <a href='${{phishingURL}}'>click here</a> to participate.
    Ministry of Finance",
    */
    "en":{
      "to": "Elysha Duffy <e.duffy@company.com>",
      "from":{  "option0":{text:"Peter Tungsten <tungsten58@icloud.com>", phishing:false, type:""}, //TODO
                "option1":{text:"IRS <irs@gov.to>", phishing:true, type:"pf-similar-domain"},
                "option2":{text:"Census <johnson@census.gov>", phishing:true, type:"pf-spoofing"},
              },
      "subject":{ "option0":{text:"Census", phishing:false, type:""},
                  "option1":{text:"We are counting on you", phishing:false, type:""},
                  "option2":{text:"The governemnt requires your assistance", phishing:true, type:"pf-authority"} //TODO
                },
      "choice1":{ "option0":{text:"Hello there,", phishing:true, type:"pf-generic-salutation"},
                  "option1":{text:"Mrs Duffy,", phishing:false, type:""},
                  "option2":{text:"Dear recipient,", phishing:true, type:"pf-generic-salutation"} //TODO
            },
      "choice2":{ "option0":{text:"Just like last year we are conducting our annual government census and we require your help once more.", phishing:true, type:"pf-consistency"},
                  "option1":{text:"We are aware that you're a hard working, important individual just like the people that work here.", phishing:true, type:"pf-liking"},
                  "option2":{text:"You are required to provide your address, phone number and salary for statistical purposes.", phishing:false, type:""} //TODO
            },
      "choice3":{ "option0":{text:"Please <a href= 'http://www.census.gow.com'> click here </a> to participate.", phishing:true, type:"pf-url-obfuscation"},
                  "option1":{text:"If you do not fill out the attached form, you will lose several forms of tax benefits.", phishing:true, type:"pf-malicious-attachment"},
                  "option2":{text:"Keeping track of the state of the country is of utmost importance.", phishing:false, type:""} //TODO
                },
      "choice4":{ "option0":{text:"Ministry of Finance", phishing:true, type:"pf-authority"},
                  "option1":{text:"Yours sincerely", phishing:false, type:""},
                  "option2":{text:"Godspeed", phishing:false, type:""} //TODO
                },
      },
    "de":{
      "to": "Elysha Duffy <e.duffy@company.com>",
      "from":{  "option0":{text:"Peter Tungsten <tungsten58@icloud.com>", phishing:false, type:""}, //TODO
                "option1":{text:"IRS <irs@gov.to>", phishing:true, type:"pf-similar-domain"},
                "option2":{text:"Zensus <johnson@census.gov>", phishing:true, type:"pf-spoofing"},
              },
      "subject":{ "option0":{text:"Zensus", phishing:false, type:""},
                  "option1":{text:"Wir zählen auf Sie", phishing:false, type:""},
                  "option2":{text:"Die Regierung benötigt Ihre Hilfe", phishing:true, type:"pf-authority"} //TODO
                },
      "choice1":{ "option0":{text:"Hallo zusammen,", phishing:true, type:"pf-generic-salutation"},
                  "option1":{text:"Fr. Duffy,", phishing:false, type:""},
                  "option2":{text:"Sehr geehrter Empfänger,", phishing:true, type:"pf-generic-salutation"} //TODO
            },
      "choice2":{ "option0":{text:"Wie schon im letzten Jahr führen wir unsere jährliche Volkszählung durch und benötigen auch diesmal Ihre Hilfe.", phishing:true, type:"pf-consistency"},
                  "option1":{text:"Wir sind uns bewusst, dass Sie, genau wie wir, eine hart arbeitende, wichtige Person sind.", phishing:true, type:"pf-liking"},
                  "option2":{text:"Zu statistischen Zwecken geben Sie bitte Ihre Adresse, Telefonnummer und Ihr Gehalt an.", phishing:false, type:""} //TODO
            },
      "choice3":{ "option0":{text:"Bitte <a href= 'http://www.census.gow.com'> klicken Sie hier </a> um teilzunehmen.", phishing:true, type:"pf-url-obfuscation"},
                  "option1":{text:"Wenn Sie das angehängte Formular nicht ausfüllen, verlieren Sie mehrere verschiedene Steuervorteile.", phishing:true, type:"pf-malicious-attachment"},
                  "option2":{text:"Es ist von größter Wichtigkeit, den Überblick über den Zustand des Landes zu behalten.", phishing:false, type:""} //TODO
                },
      "choice4":{ "option0":{text:"Finanzministerium", phishing:true, type:"pf-authority"},
                  "option1":{text:"Hochachtungsvoll", phishing:false, type:""},
                  "option2":{text:"Viel Erfolg", phishing:false, type:""} //TODO
                },
    },
  },
});
phishingLevels.push({
  date:'',
  subject:'',
  from:{name:'',email:''},
  to:{email:''},
  header:{
    'Return-Path':'',
    Received:[''],
    Date:'',
    From:'',
    Subject:'',
    To:'',
  },
  text:'',
  html:'',
  'isPhish':false,
  'isEncrypted':false,
  'isSigned':false,
  'help':'',
  'mark':{
    'subj': {
      'susp': false, //not suspicious
    },
    'date': {
      'susp': false,
    },
    'sender': {
      'susp': false,
    },
    'email': {
      'susp': true,
      'text': '',
    },
    'body': [
      {
        'markTxt': '',
        'text': '',
      },
    ],
  },
  creation:{
    infos: {id:29,matching:true,priorities:{},requirement:{0:26},name:'name-bonus3',dialog:'dialog-everything',topic:{en:"an upgrade of a banking account",de:"die Aktuallisierung eines Bankkontos"},}, //MATCHING IS FALSE

    /*
    Good day
    We have reached our digital NEXT, and the old Online Banking site has been discontinued.
    Experience the new Online Banking site with a two-step sign in feature.
    Click Here to sign in and Upgrade, you will be required to first enter your email address and then your password on the next screen.
    Registered phone number we have on our record must be confirmed as we will verify by sending One-Time-PIN to validate your details.
    The small-but-very-important print:Everything in this email and any attachments relating to the official business of ${{serviceName}} Group Limited is proprietary to the group.
    It is confidential, legally privileged and protected by law. We do not own and endorse any other content. The person addressed in the email is the sole authorised recipient.
    Received it by mistake? Please notify the sender immediately and do not read disclose or use the content in any way.
    Remember, we will never send you any communication asking you to update or provide confidential information about you or your account.
    If there’s anything strange about this email or other emails you receive claiming to be from ${{serviceName}}, please forward it to phishing@${{serviceDomain}} and we’ll take it from there.
    We also cannot assume the integrity of this communication has been maintained or that it is free of errors, virus, interception or interference.
    You can read more about our privacy policy on our website at www.${{serviceDomain}}Copyright ${{serviceName}} Group Limited. All rights reserved.
    The ${{serviceName}} of South Africa Limited (Reg. No. 1962/000738/06). We are an authorised financial services provider and registered credit provider (NCRCP15).
    Moving Forward is a trademark of The ${{serviceName}} of South Africa Limited",
    */
    "en":{
      "to": "Anoushka Aguirre <a.Aguirre@company.com>",
      "from":{  "option0":{text:"Next Banking <accounting@hsbc.com>", phishing:true, type:"pf-spoofing"}, //TODO
                "option1":{text:"Guy Drake <guy.drake@aol.com>", phishing:false, type:""},
                "option2":{text:"Next Banking <b4nk1ng@protonmail.com>", phishing:true, type:"pf-similar-domain"},
              },
      "subject":{ "option0":{text:"We have reached our digital NEXT", phishing:false, type:""},
                  "option1":{text:"The future be now", phishing:true, type:"pf-poor-grammar"},
                  "option2":{text:"We are not like other banks", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Good day,", phishing:true, type:"pf-generic-salutation"},
                  "option1":{text:"Mrs Anoushka Aguirre", phishing:false, type:""},
                  "option2":{text:"Hello Anoushka,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"Experience the new online banking site with a two-step sign in feature.", phishing:false, type:""},
                  "option1":{text:"The future hoIds many uncertainties but be asured we at NEXT Banking vvill assist you in every possibIe way.", phishing:true, type:"pf-poor-spelling"},
                  "option2":{text:"We have reached our digital NEXT, and the old online banking site has been discontinued.", phishing:false, type:""} //TODO
            },
      "choice3":{ "option0":{text:"This email is confidential, legally privileged and protected by law.", phishing:false, type:""},
                  "option1":{text:"Registered phone numbers we have on our record must confirm as we will verify by sending One-Time-PIN to validate details.", phishing:true, type:"pf-poor-grammar"},
                  "option2":{text:"Go to https://www.b4nk1ng.bu551ne5.org to sign in and upgrade, you will be required to first enter your email address and then your password.", phishing:true, type:"pf-url-obfuscation"} //TODO
                },
      "choice4":{ "option0":{text:"Remember, we will never send you any communication asking you to update or provide confidential information about you or your account.", phishing:false, type:""},
                  "option1":{text:"Be assured, we take very good care of our customers, and as you know no NEXT Banking spam has ever reached you.", phishing:true, type:"pf-reciprocation"},
                  "option2":{text:"You can read more about our privacy policy on our website or in the attached file.", phishing:true, type:"pf-malicious-attachment"} //TODO
                },
      "choice5":{ "option0":{text:"Yours sincerely", phishing:false, type:""},
                  "option1":{text:"Kind regards", phishing:false, type:""},
                  "option2":{text:"Your NEXT Banking team", phishing:false, type:""} //TODO
                },
      },
    "de":{
      "to": "Anoushka Aguirre <a.Aguirre@company.com>",
      "from":{  "option0":{text:"Next Banking <accounting@hsbc.com>", phishing:true, type:"pf-spoofing"}, //TODO
                "option1":{text:"Guy Drake <guy.drake@aol.com>", phishing:false, type:""},
                "option2":{text:"Next Banking <b4nk1ng@protonmail.com>", phishing:true, type:"pf-similar-domain"},
              },
      "subject":{ "option0":{text:"Wir haben das digitale NEXT erreicht", phishing:false, type:""},
                  "option1":{text:"Die Zukunft jetzt beginnt", phishing:true, type:"pf-poor-grammar"},
                  "option2":{text:"Wir sind nicht wie andere Banken", phishing:false, type:""} //TODO
                },
      "choice1":{ "option0":{text:"Guten Tag,", phishing:true, type:"pf-generic-salutation"},
                  "option1":{text:"Fr. Anoushka Aguirre", phishing:false, type:""},
                  "option2":{text:"Hallo Anoushka,", phishing:false, type:""} //TODO
            },
      "choice2":{ "option0":{text:"Erleben Sie die neue Online-Banking-Seite mit zweistufiger Anmeldefunktion.", phishing:false, type:""},
                  "option1":{text:"Die Zukunft birgt vieIe Unsicherheiten, aber wir von NEXT Banking werden Sie auf jede erdenkIiche VVeise unterstützen.", phishing:true, type:"pf-poor-spelling"},
                  "option2":{text:"Wir haben unser digitales NEXT erreicht, und die alte Online-Banking-Seite wurde abgeschaltet.", phishing:false, type:""} //TODO
            },
      "choice3":{ "option0":{text:"Diese E-Mail ist vertraulich, rechtlich privilegiert und durch das Gesetz geschützt.", phishing:false, type:""},
                  "option1":{text:"Um Einweg-PINs erhalten zu können, müssen bestätigen Sie Ihre hinterlegte Telefonnummer.", phishing:true, type:"pf-poor-grammar"},
                  "option2":{text:"Um Ihr Konto zu aktuallisieren, müssen Sie Ihre E-Mail-Adresse und Ihr Passwort auf https://www.b4nk1ng.bu551ne5.org eingeben.", phishing:true, type:"pf-url-obfuscation"} //TODO
                },
      "choice4":{ "option0":{text:"Denken Sie immer daran, wir werden Sie niemals dazu aufgefordert vertrauliche Informationen über Sie oder Ihr Konto preiszugeben.", phishing:false, type:""},
                  "option1":{text:"Seien Sie versichert, dass wir uns sehr gut um unsere Kunden kümmern, weshalb Sie noch nie ein NEXT Banking Spam-Nachricht erreicht hat.", phishing:true, type:"pf-reciprocation"},
                  "option2":{text:"Informationen bezüglich unserer Datenschutzbestimmungen können Sie unserer Website oder der angehängten Datei entnehmen.", phishing:true, type:"pf-malicious-attachment"} //TODO
                },
      "choice5":{ "option0":{text:"Hochachtungsvoll", phishing:false, type:""},
                  "option1":{text:"Mit freundlichen Grüßen", phishing:false, type:""},
                  "option2":{text:"Ihr NEXT Banking Team", phishing:false, type:""} //TODO
                },
    },
  },
});
