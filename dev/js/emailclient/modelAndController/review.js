//review

/**
 * This module comprises all functions relevant to the game's review section.
 * It is build based on the revealing module pattern.
 * @function
 */
var reviewModule = function(){

  /**
   * This function manages the points the players have earned. gets used in the game review
   * @function
   */
  function manageScoring(){
    this.folderPaneView.scoreText.textbox.visible = true;
    this.folderPaneView.scoreNumber.textbox.visible = true;
    this.folderPaneView.highscoreText.textbox.visible = true;
    this.folderPaneView.highscoreNumber.textbox.visible = true;
    this.progData.scoringMultiplier = this.progData.difficultyOptions["easy"]/this.progData.difficultyOptions[this.progData.difficulty] + this.progData.difficultyMultiplierBonus[this.progData.difficulty]; //recalculate the scoring in case the difficulty has changed
    let score = this.progData.timeLeft * this.progData.scoringMultiplier; //calculate how well the players did
    let alreadyScored = false;
    for (let i = 0; i < this.progData.scoreMemory.length; i++){ //check if the score in the current level is higher than the old highscore
      if (this.progData.scoreMemory[i].length > 1 && this.progData.activeLevel == this.progData.scoreMemory[i][0]){ //when an old highscore for this level exists
        if (score >= this.progData.scoreMemory[i][1]){ //when we beat the existing highscore
          this.folderPaneView.scoreNumber.textbox.children[1].text = score.toString();
          this.folderPaneView.highscoreNumber.textbox.children[1].text = score.toString();
          this.progData.scoreMemory[i][1]=score;
          this.manageDialogBox(l('dialog-review-instructions')+ " " + l('dialog-review-highscore'));
        }
        else{
          this.folderPaneView.scoreNumber.textbox.children[1].text = score.toString();
          this.folderPaneView.highscoreNumber.textbox.children[1].text = this.progData.scoreMemory[i][1];
        }
        alreadyScored = true;
        break;
      }
    }
    if (alreadyScored == false){ //level hasn't been scored yet
      this.progData.scoreMemory.push([this.progData.activeLevel,score]);
      this.folderPaneView.scoreNumber.textbox.children[1].text = score.toString();
      this.folderPaneView.highscoreNumber.textbox.children[1].text = score.toString();
      this.manageDialogBox(l('dialog-review-instructions')+ " " + l('dialog-review-highscore'));
    }
    this.progData.save();
  }

  /**
   * This function initializes the review view after the continue button has been pressed (in the matching game). It mostly changes visibilities
   * @function
   */
  function initReview(){
    let path = this.folderPaneView.levelView.newEmailPaneView;
    this.progData.currently = "review";
    this.folderPaneView.buttonTextView.text = l("back");
    this.manageDialogBox(l('dialog-review-instructions'));
    if (this.progData.matchingTimerEnabled == true || this.progData.creationTimerEnabled == true){ //disable timers if they are enabled
      this.manageTimerDisplay.bind(this)(0,false);
      this.progData.timer.stop();
    }
    this.tickerWrapperParticipantHovers = createjs.Ticker.on("tick", highlightsModule.participantHoverOnMouseover.bind(this));
    this.tickerWrapperURLHovers = createjs.Ticker.on("tick", this.tickerWrapperURLHovers);
    this.stage.setChildIndex(path.newEmailViewContainer, this.stage.numChildren - 1);
    this.stage.setChildIndex(path.newEmailReviewView.newEmailReviewContainer, this.stage.numChildren - 1);
    manageReview.bind(this)();
    this.setNewEmailGameContainerVisibility(false,true,false,false,false,false,true,true,false,false); //levelView, email creation view (newEmailView), email creation view buttons, matching game view, matching game buttons, review view, continue button, matching game lives, tutorial
    if (this.progData.currentPhishingEmail.infos.matching == true){ //make matching game review box visible if the matching game has been played
      path.newEmailReviewView.bgReviewMatching.visible = true;
      path.newEmailReviewView.matchingImg.visible = true;
      path.newEmailReviewView.newEmailReviewMatchingTextbox.textbox.visible = true;
      path.newEmailReviewView.borderReviewMachting.visible = true;
    }
    else{
      path.newEmailReviewView.bgReviewMatching.visible = false;
      path.newEmailReviewView.matchingImg.visible = false;
      path.newEmailReviewView.newEmailReviewMatchingTextbox.textbox.visible = false;
      path.newEmailReviewView.borderReviewMachting.visible = false;
    }
  }

  /**
   * This function manages which text is shown in the review textboxes. It also calls the highlighting function of the phishing features for the mouseover events in the review
   * @function
   */
  function manageReview(){
    let path = this.folderPaneView.levelView.newEmailPaneView.newEmailReviewView;
    let multiple = (Object.keys(this.progData.currentPhishingEmail.infos.priorities).length>1)? true:false; //one or multiple features?
    this.progData.activeLevelPassed = true; //if the player failed somewhere we set it back to false

    //comment on the usage of the current phishing feature(s) of this level
    if (this.progData.featureOfCurrentLevelUsed == true){ //a phishing feature of the current level was used
      path.newEmailReviewEmailTextbox.txt.text = (multiple == true)? l('review-current-features-used-start')+l(this.progData.currentPhishingEmail.infos.dialog)+l('review-current-features-used-end')+'\n':l('review-current-feature-used-start')+l(this.progData.currentPhishingEmail.infos.dialog)+l('review-current-feature-used-end')+'\n';
      path.bgReviewEmail.graphics._fill.style = this.blue;//make background blue
    }
    else if (Object.keys(this.progData.currentPhishingEmail.infos.priorities).length == 0 && this.progData.amountOfUsedPhishingFeature < 4){ //the level didn't require anything specific and the palyers integrated something into the email
      path.newEmailReviewEmailTextbox.txt.text = l('review-no-specific-feature-required-passed1') + '\n';
      path.bgReviewEmail.graphics._fill.style = this.yellow;//make background yellow
    }
    else if (Object.keys(this.progData.currentPhishingEmail.infos.priorities).length == 0 && this.progData.amountOfUsedPhishingFeature >= 4){ //the level didn't require anything specific and the palyers integrated a lot of phishing features into the email
      path.newEmailReviewEmailTextbox.txt.text = l('review-no-specific-feature-required-passed2') + '\n';
      path.bgReviewEmail.graphics._fill.style = this.blue;//make background blue
    }
    else if (Object.keys(this.progData.currentPhishingEmail.infos.priorities).length == 0 && this.progData.amountOfUsedPhishingFeature == 0){ //the level didn't require anything specific and the palyers didn't integrated something into the email
      path.newEmailReviewEmailTextbox.txt.text = l('review-no-specific-feature-required-not-passed') + '\n';
      path.bgReviewEmail.graphics._fill.style = this.orange;//make background orange
    }
    else{
      path.newEmailReviewEmailTextbox.txt.text = (multiple == true)? l('review-current-features-not-used-start')+l(this.progData.currentPhishingEmail.infos.dialog)+l('review-current-features-not-used-end')+'\n':l('review-current-feature-not-used-start')+l(this.progData.currentPhishingEmail.infos.dialog)+l('review-current-feature-not-used-end')+'\n';
      path.bgReviewEmail.graphics._fill.style = this.orange;//make background orange
      this.progData.activeLevelPassed = false;
    }

    //comment on the amount of phishing features used (in the non-bonus Levels)
    if (Object.keys(this.progData.currentPhishingEmail.infos.priorities).length > 0){
      if (this.progData.amountOfUsedPhishingFeature > 3 ){ //a lot of phishing feature were used
        path.newEmailReviewEmailTextbox.txt.text += l('review-too-many-features');
        if (path.bgReviewEmail.graphics._fill.style != this.orange){
          path.bgReviewEmail.graphics._fill.style = this.yellow;//make background yellow
        }
      }
      else{ //a moderate amount of phishing features was
        if (this.progData.featureOfCurrentLevelUsed == true || (Object.keys(this.progData.currentPhishingEmail.infos.priorities).length == 0 && this.progData.amountOfUsedPhishingFeature > 0)){
          path.newEmailReviewEmailTextbox.txt.text += l('review-good-amount-of-features-passed');
        }
        else if (this.progData.amountOfUsedPhishingFeature > 0){ //the player didn't use the current feature but at least the email contains only a small amount of features
            path.newEmailReviewEmailTextbox.txt.text += l('review-good-amount-of-features-not-passed');
        }
        else if (Object.keys(this.progData.currentPhishingEmail.infos.priorities).length != 0){ //the email doesn't contain phishing at all and there are no requirements
          path.newEmailReviewEmailTextbox.txt.text += l('review-no-features');
        }
      }
    }

    //comment on how many mistakes the players did in the matching game
    let textMatchingText = "";
    if (this.progData.timeUp != true){ //matching timer not up
      if (this.progData.currentMatchingMistakes == 0){
        textMatchingText = l('review-matching-mistakes0');
        path.bgReviewMatching.graphics._fill.style = this.blue;//make background blue //#92D050 green
      }
      else if (this.progData.currentMatchingMistakes == 1){
        textMatchingText = l('review-matching-mistakes1');
        path.bgReviewMatching.graphics._fill.style = this.yellow;//make background yellow
      }
      else if (this.progData.currentMatchingMistakes == 2){
        textMatchingText = l('review-matching-mistakes2');
        path.bgReviewMatching.graphics._fill.style = this.yellow;//make background yellow
      }
      else if (this.progData.currentMatchingMistakes >= 3){
        textMatchingText = l('review-matching-mistakes3');
        for (let pair of this.progData.matchingPairs){ //add all of the topics that were in the matching game to the list of problematic topics
          this.progData.problematicTopics.add(pair[1]);
        }
        path.bgReviewMatching.graphics._fill.style = this.orange;//make background orange // #FF3300 red
        this.progData.activeLevelPassed = false;
      }
    }
    else{ //matching timer up
      textMatchingText = this.progData.amountOfUsedPhishingFeature > 0? l('review-matching-time1') : l('review-matching-time2');
      for (let pair of this.progData.matchingPairs){ //add all of the topics that were in the matching game to the list of problematic topics
        this.progData.problematicTopics.add(pair[1]);
      }
      path.bgReviewMatching.graphics._fill.style = this.orange;//make background orange // #FF3300 red
      this.progData.activeLevelPassed = false;
    }

    //add topics the user had a hard time with during the matching game to the review
    if (this.progData.currentMatchingMistakes > 0 || this.progData.timeUp == true){
      for (let topic of [...this.progData.problematicTopics]){
        if (topic != l('pf-no-phishing')){
          textMatchingText += "\n - " + topic;
        }
      }
    }
    path.newEmailReviewMatchingTextbox.txt.text = textMatchingText; //write text to matching review textbox


    if (this.progData.activeLevelPassed == true && (this.progData.creationTimerEnabled == true || (this.progData.matchingTimerEnabled == true && this.progData.currentPhishingEmail.infos.matching == true))){
      manageScoring.bind(this)(true);
    }

    //highlight the used phishing features in the email
    highlightsModule.managePhishingHighlights.bind(this)();

  }

  return{
    initReview:initReview
  };

}();
