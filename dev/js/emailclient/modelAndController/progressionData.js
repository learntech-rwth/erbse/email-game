
/**
 * This class holds the game progress such as the level that is currently played,
 * the email that just got created and the wrongfully matched topic in the matching game
 * @class
 * @param {number} userId The ID of the user that is playing the game. Used for xAPI statements
*/
function progressionData (userId){
  //general level progression
  this.activeLevel = 0; //currently active level
  this.activeLevelPassed = false;
  this.passedMailsSet = new Set();
  this.passedLevelsSet = MTLG.getOptions().debug == true? new Set([0,2,4,7,9,11,13,15,17,19,21,23,25,26,27,28]): new Set([0]); //set that will contain all finished levels, displays how far the player got // [0,2,4,7,9,11,13,15,17,19,21,23,25,26]
  this.gameplayLevelsSet = new Set([2,4,7,9,11,13,15,17,19,21,23,25,26,27,28,29]); //set that contains all gameplay levels, used to check if the game is finished in initLevelSelection()
  this.gameOverScreenShown = false; //indicates if the gameOver screen was already shown before, if true don't show it again
  this.currently = null; //tells us if we're choosing a level, or creating an email, playing the matching game or reviewing something
  this.featureOfCurrentLevelUsed = false; //used to decide whether a level was passed
  this.amountOfUsedPhishingFeature = 0; //features used in this level
  this.firstMatchingLevel = null; //contains the first level that features a matching game. used for the tutorial overlay of the matching game
  for (let i = 0; i < phishingLevels.length; i++){
    if (phishingLevels[i].creation != undefined && phishingLevels[i].creation.infos.matching == true){
      this.firstMatchingLevel = phishingLevels[i].creation.infos.id;
      break;
    }
  }
  this.firstCreationLevel = null; //contains the first level that features a creation game. used for the tutorial overlay of the creation game
  for (let i = 0; i < phishingLevels.length; i++){
    if (phishingLevels[i].creation != undefined && phishingLevels[i].creation.infos != undefined){
      this.firstCreationLevel = phishingLevels[i].creation.infos.id;
      break;
    }
  }

  //tutorial
  this.activeTutorial = null; //contains the currently active tutorial
  this.activeTutorialIndex = 0; //currently displayed tutorial parts

  //email creation game
  this.activePartIndex = 0; //tells us where we are in the PE (0 = to, 1 = from, 2 = subject, 3 = choice1, ...)
  this.createdEmail = []; //the created email is safed in here in form of the choosen entries from the phishingEmailModel. an entry looks as follows {text,phishing,type,explanation}
  this.currentPhishingEmail = null; //this will contain the creation part of this level's phishingEmailModel.js
  this.lastChoice = false; //will be true when all creation game choices have been made (after each level)
  this.clicksEnabled = MTLG.getOptions().clicksEnabled; //will be true if the player has selected to click control for the creation game and false if he wants to use drag n drop, can be set in the game interface

  //matching game
  this.currentMatchingFeature = null; //currently selected phishing feature in the matching game
  this.currentMatchingText = null; //currently selected text passage in the matching game
  this.maxMatchingMistakes = 2; //maximum amount of mistakes the players are allowed to commit
  this.currentMatchingMistakes = 0; //current amount of matching mistakes in this level
  this.currentMatchingScore = 0; //current amount of correctly matched text passages and features in this level
  this.matchingPairs = []; //contains entries of a text passage [0] with their corresponding phishing feature [1]. some entries are perhaps filler passages
  this.problematicTopics = new Set([]); //will contain the specific topics that players failed to match correctly during the matching game; is used in the review

  //hover textbox data and highlights
  this.highlights = []; //contains highlight entries that indicate text passages that contain phishing features during the review. highlight entry is defined in highlight.js
  this.participantHovers = []; //contains highlight entries that consist of a participant textbox and the not shortened text this box should originaly contain. highlight entry is defined in highlight.js
  this.URLHovers = []; //contains higlight entries that consist of the shape of a textbox and the URL that is embedded in the link text.

  //scoring
  this.difficulty = MTLG.getOptions().difficulty; //see manifest/game.config.js
  this.newDifficulty = this.difficulty; //we only want to change the difficulty at the start of each creation level not during a level (or the players could cheat by starting with easy and finishing with hard)
  this.difficultyOptions = {"easy":40000,"medium":20000,"hard":10000}; //difficulty levels of the game (time in ms for each option)
  this.difficultyMultiplierBonus = {"easy":0,"medium":1,"hard":2}; //bonus multiplier that gets added when a higher than easy difficulty is selected
  this.scoringMultiplier = this.difficultyOptions["easy"]/this.difficultyOptions[this.difficulty] + this.difficultyMultiplierBonus[this.difficulty]; //gets multiplied with the remaing number of seconds to get the score for the level
  this.scoreMemory = []; //will contain entries consisting of level id (entry [0]) and the score (based on the remaining time) in the creation game and matching game (entry [1])
  this.score = 0; //holds the score, based on the score memory. will be displayed in the interface

  //timer
  this.creationTimerEnabled = MTLG.getOptions().creationTimerEnabled; //will be true if the creation game is supposed to have a timer or false otherwise
  this.matchingTimerEnabled = MTLG.getOptions().matchingTimerEnabled; //will be true if the matching game is supposed to have a timer or false otherwise
  this.creationGameTimePerOption = this.difficultyOptions[this.difficulty];
  this.matchingGameTimePerOption = this.difficultyOptions[this.difficulty];
  this.timer = new MTLG.utils.timer(); //timer for creation and/or matching game
  this.time = 0; //safes the current time of the displayClockTimer
  this.timeUp = null; //since this.timer.timeUp() only tells you if the original time from the start settings of the timer is up, we need to log that we have finished a level in time (will be false if the level got finished in time)
  this.timeLeft = 0; //safes how much time the players have left after creation (and matching) game. used for scoring
  this.displayClockTimer = null //display timer

  //sound
  this.sounds = MTLG.getOptions().soundEnabled; //will be true if the player has selected to hear sounds, can be set in the game interface

  //xAPI
  this.userId = userId; //the randomly (in startView.js) generated  user ID for xAPI statements


  // Saving/Loading
  this.save = function(){
    _saver.save({
      scoreMemory: this.scoreMemory,
      passedLevelsSet: Array.from(this.passedLevelsSet),
      passedMailsSet: Array.from(this.passedMailsSet)
    });
  }

  this.load = function(saveData){
    if(saveData != null && saveData != {}){
      if("scoreMemory" in saveData){
        this.scoreMemory = saveData.scoreMemory;
      }
      if("passedLevelsSet" in saveData){
        this.passedLevelsSet = new Set(saveData.passedLevelsSet);
      }
      if("passedMailsSet" in saveData){
        this.passedMailsSet = new Set(saveData.passedMailsSet);
      }
    }
  }

  this.load(_saver.load())
}
