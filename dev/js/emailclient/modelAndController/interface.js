/**
 * Email interface
 *
 */

function interface(stage,userId) {
  this.progData = new progressionData(userId); //hold most of the data for level progression and things such as the created email or the current phishing email of the active level

  MTLG.setBackgroundColor("#f2f2f2");
  this.stage = stage;
  this.mails = MTLG.emailClient.getEmails();

  // Remove the saved mails from the list of possible mails to select from
  let sortedData = Array.from(this.progData.passedMailsSet);
  sortedData = sortedData.sort((a, b) => {return b - a;})
  for(let passedIndex of sortedData){
    this.mails.splice(passedIndex, 1)
  }

  var showHeader = MTLG.emailClient.getOptions().showHeader;
  var currentIndex = 0; //Index of the currently displayed email
  var previousIndex = 0;
  var seeMail = false; //true if an email is displayed currently
  var tickerWrapper; //variable to save wrapper of eventlistener on createjs.Ticker
  this.tickerWrapperParticipantHovers; //variable to save wrapper of eventlistener on createjs.Ticker
  this.tickerWrapperEmailHighlights; //variable to save wrapper of eventlistener on createjs.Ticker
  this.tickerWrapperURLHovers; //variable to save wrapper of eventlistener on createjs.Ticker
  this.gameVisible = 0; // 0 = none, 1=old, 2 = new
  this.tutorialSeen = [];

  //Slice E-Mails-Array//
  // var sliceIndex = (this.mails.length - 4 > 0) ? this.mails.length - 4 : 0; //get maximum of(0, mails.length-4)
  var sliceIndex = Math.max(this.mails.length-4, 0); //get maximum of(0, mails.length-4)
  // TODO: SHOULDNT THIS BE MIN 4, this.mails.length as well?
  this.displayedMails = this.mails.slice(sliceIndex); //maximal 4 displayed
  this.hiddenMails = this.mails.slice(0, sliceIndex);

  //Variables for Positioning of Views
  var maxHeight = MTLG.getOptions().height; //1080
  var maxWidth = MTLG.getOptions().width; //1920
  var folderWidth = 250;
  var msgListWidth = 400;
  var previewWidth = maxWidth - folderWidth - msgListWidth - 30; //30:padding
  var emailclientHeight = 950;

  //frequently used colors
  this.blue = "#4da6ff";
  this.lightBlue = "#AED6FF";
  this.orange = "#ffae3b";
  this.red = "#FF3300";
  this.yellow = "#FFDF00";
  this.green = "#92D050";
  this.grey = "#f2f2f2";
  this.white = "white";
  this.black = "black";


  //------display Views --------------------------------------------------------

  //draw Folder Pane View
  this.folderPaneView = new folderPaneView(0, 10, folderWidth, emailclientHeight, this.stage);
  this.folderPaneView.numUnread = this.displayedMails.length;
  this.folderPaneView.numUnreadBox.txt.text = this.folderPaneView.numUnread;

  //progress data and some constants
  this.maxLevel = this.folderPaneView.levelView.levelButtons.length-1;
  this.standardPadding = 5; //standard padding for boxes

  //draw View that is dislayed, when no Email is selected
  this.noSelecView = new noSelectionView(900, 200, this.stage);

  //add 'New-Message' Pop-Up and Method for animation
  this.newMsgNotification = new newMsgNotificationView(this.stage);

  //display Message List and show preview on click
  drawMsgList.bind(this)();

  //draw View for Control Pane
  this.controlPaneView = new controlPaneView(0, maxHeight - 110, maxWidth, 110, this.stage);

  //add resultbox to stage. But now it's invisible
  this.resultBoxView = new resultBoxView(0, 0, 665, emailclientHeight + 20, this.stage);

  //-----------------------------------------------------------------------------

  this.drawMsgList = drawMsgList;

  //display message List and show preview on click
  function drawMsgList() {
    if(this.messageListView != null){
      this.mails = MTLG.emailClient.getEmails();
      this.displayedMails = this.mails.slice(sliceIndex); //maximal 4 displayed
      this.hiddenMails = this.mails.slice(0, sliceIndex);
      this.messageListView._messageContainer.visible = false;
      this.messageListView.remove();
      this.seeMail = false;
      MTLG.emailClient.updateClientViews(this.stage);
    }

    this.folderPaneView.numUnread = this.displayedMails.length;
    this.folderPaneView.numUnreadBox.txt.text = this.folderPaneView.numUnread;

    this.messageListView = new messageListView(260, 10, msgListWidth, maxHeight - 110, this.stage, this.displayedMails); //create Messagelist View

    this.msgList = this.messageListView._messageContainer; //Array containing Messagebox Views
    if(this.emailViews != null){
      for(let emailView of this.emailViews){
        emailView.emailContainer.visible = false
        this.stage.removeChild(emailView);
      }
    }
    this.emailViews = []; //Array containing all email Views (Header+Preview)
    MTLG.emailClient.updateClientViews(this.stage);

    for (var i = 0; i < this.msgList.length; i++) { //Fill emailViews array and add click events that lead to the display of the associated email when Messagebox is clicked

      this.stage.addChild(this.msgList[i]); //display Messagebox in stage

      this.emailViews[i] = new emailView(folderWidth + msgListWidth + 20, 10, previewWidth, emailclientHeight, this.stage, this.displayedMails[i], showHeader); //assign Email View
      //(function (i) {
      addMsgListener.bind(this)(i); //add on click events
      // }.bind(this))(i);

    }

    MTLG.emailClient.updateClientViews(this.stage);
  }

  //Email Pop-Up with animation
  function notification() {
    createjs.Tween.get(this.newMsgNotification.box.textbox, { override: true }) //
      .label("newMail")
      .to({ x: -340 }, 500) //width of pop-up is 335 and the popup is currently not visible because it is far right.
      .wait(2000) //wait 2 seconds before pop up disappears again
      .to({ x: 0 }, 500); //back to previous position
  }

  //add onClick event for Msg with index i in MsgList.
  function addMsgListener(i) {
    this.msgList[i].on('click', function (evt) {
      if (!this.folderPaneView.tutorialModalView.tutorialModalViewContainer.visible && !this.folderPaneView.settingsView.settingsViewContainer.visible) {
        previousIndex = currentIndex;
        currentIndex = i; //index of currently clicked messagebox or email
        seeMail = true; //true if an email is displayed currently

        if(this.msgList[previousIndex] != null){
          this.msgList[previousIndex].children[0].alpha = 1; //children[0] contains Background of Messagebox.
        }
        this.msgList[currentIndex].children[0].alpha = 0.5; //change BG opacity to indicate a successful click

        this.emailViews[previousIndex].emailContainer.visible = false; //last displayed email is now invisible
        this.emailViews[currentIndex].emailContainer.visible = true; //display email View that belongs to clicked Messagebox
        this.stage.setChildIndex(this.emailViews[currentIndex].emailContainer, this.stage.numChildren - 2); //dont't add to last index, because pop-up should not be overlayed by email View, when user clicks

        if (!this.emailViews[currentIndex].wasSeen) { //email should be marked as "seen" and decrease number of unread mails
          this.emailViews[currentIndex].wasSeen = true;
          this.msgList[currentIndex].children[1].color = "black"; //children[1] is sendername in Messagebox. Change to black, when it is already clicked
          this.folderPaneView.numUnread--; //decrease counter which shows how many emails are still unread
          if (this.folderPaneView.numUnread > 0) { //if all emails are read, the counter should not be displayed
            this.folderPaneView.numUnreadBox.txt.text = this.folderPaneView.numUnread;
          }
          else {
            this.folderPaneView.numUnreadBox.textbox.visible = false;
          }
        }
      }
    }.bind(this));
  }

  //display stamps and verify button and add belonging event listeners
  function displayStamp() {

    this.controlPaneView.stamp.visible = true; //display stamp
    this.stage.setChildIndex(this.controlPaneView.stamp, this.stage.numChildren - 1);
    this.controlPaneView.stamp.removeAllEventListeners(); //remove Eventlisteners to avoid unexpected behaviour
    this.controlPaneView.stamp.on('pressmove', draggable.bind(this));
    this.controlPaneView.stamp.on('pressup', displayPrint.bind(this));

    this.controlPaneView.verifyButton.visible = true; //display verify button
    this.controlPaneView.verifyButton.removeAllEventListeners(); //remove Eventlisteners to avoid unexpected behaviour
    this.controlPaneView.verifyButton.on('click', verify.bind(this));

  }

  //display red stamp print when stamp is dropped, and remove when dblclick
  function displayPrint(e) {

    let imgH = this.controlPaneView.stamp.imgH;
    let imgW = this.controlPaneView.stamp.imgW;

    let p = e.currentTarget.localToLocal(e.localX, e.localY, this.stage);

    if (p.x >= folderWidth + msgListWidth + 20 && p.y <= maxHeight - 130) { //stamp is dropped in accepted area (preview)

      var print = MTLG.assets.getBitmap('img/stamp-red.png'); //display stamp print
      print.scaleX = 0.15;
      print.scaleY = 0.15;
      print.x = p.x - imgW / 3;
      print.y = p.y - imgH / 4;

      this.controlPaneView.printContainer.addChild(print);
      this.stage.setChildIndex(this.controlPaneView.printContainer, this.stage.numChildren - 1);

      print.on('dblclick', function () { //remove print on dblclick
        this.controlPaneView.printContainer.removeChild(print);
      }.bind(this));

    }

    e.currentTarget.set({ x: 850, y: MTLG.getOptions().height - 100 }); //After drop stamp is again at previous position
  }

  //stamp follows mouse on 'pressmove'
  function draggable(e) {
    let imgH = this.controlPaneView.stamp.imgH;
    let imgW = this.controlPaneView.stamp.imgW;

    let p = e.currentTarget.localToLocal(e.localX, e.localY, stage);
    e.currentTarget.set({ x: p.x - imgW / 3, y: p.y - imgH / 2 });
  }

  //test if stamp print hits targets, highlight correct ones yellow and forgotten ones purple, replace print with icon
  function verify() {

    this.controlPaneView.verifyButton.visible = false; //verifyButton invisible
    this.controlPaneView.stamp.visible = false; //stamp invisible
    this.resultBoxView.tutImg.visible = false; //Tutrial Img invisible

    let imgH = this.controlPaneView.stamp.imgH;
    let imgW = this.controlPaneView.stamp.imgW;

    let emailView = this.emailViews[currentIndex]; //view of currently displayed Email
    let targets = emailView.targets; //Array containing suspicious passages

    let prints = this.controlPaneView.printContainer.children; //array containing all stamp prints

    this.suspSet = markSuspPassages.bind(this)(targets); //mark all suspicious passages purple and return a set containing indices of all suspicious passages
    this.foundSet = new Set(); // Set containing the indices of the suspicous targets that the user marked (with stamp)

    for (var i = 0; i < prints.length; i++) { //test if stamp print hits a suspicious passage
      var match = false; //true if print hits a suspicious passage
      for (var j = 0; j < targets.length; j++) {

        //check if x-Position and y-Position of print are on suspicious target. children[0] is Text of suspicious passage.
        let markXR = prints[i].x + imgW / 3 + 5 + 15;
        let markXL = prints[i].x + imgW / 3 + 5 - 15;
        let markYT = prints[i].y + imgH / 4 + 5 + 15;
        let markYB = prints[i].y + imgH / 4 + 5 - 15;
        // console.log(targets[j]);
        let hitTarget = targets[j].checkMarkXY(markXR, markXL, markYT, markYB);

        if (hitTarget) { //if there is a stamp print on target 
          match = true; //true if print hits a suspicious passage
          targets[j].setBgColor("yellow"); //change BG of found passage yellow. children[0] is Background Shape.

          var icon = MTLG.assets.getBitmap('img/correct.png'); //replace stamp print with 'correct' icon
          icon.x = prints[i].x + imgW / 4;
          icon.y = prints[i].y;
          icon.scaleX = 0.08;
          icon.scaleY = 0.08;
          this.controlPaneView.correctIconContainer.addChild(icon);
          this.stage.addChild(this.controlPaneView.correctIconContainer);

          this.foundSet.add(j); //add index to set of found passages

        }
      }

      if (!match) { //drop target is not suspicious. replace print with 'incorrect' icon
        var icon = MTLG.assets.getBitmap('img/incorrect.png');
        icon.x = prints[i].x + imgW / 4;
        icon.y = prints[i].y + imgH / 8;
        icon.scaleX = 0.08;
        icon.scaleY = 0.08;
        this.controlPaneView.correctIconContainer.addChild(icon);
        this.stage.addChild(this.controlPaneView.correctIconContainer);
      }
    }

    this.controlPaneView.printContainer.removeAllChildren(); //remove all prints

    displayResults.bind(this)(); //display results on left side

  }

  //display result on left side
  function displayResults() {

    this.forgotSet = new Set( //contains indices of all suspicious passages that are not marked as suspicious (with stamp)
      [...this.suspSet].filter(x => !this.foundSet.has(x)));

    this.controlPaneView.points.txt.text = this.foundSet.size + "/" + this.suspSet.size + " " + l('points'); //display number of found passages in relation to existing suspicious passages
    this.controlPaneView.points.fixWidth(); //adjust Width of the Textbox that displays the points
    this.controlPaneView.points.textbox.visible = true;
    this.stage.setChildIndex(this.controlPaneView.points.textbox, this.stage.numChildren - 1);


    if (this.foundSet.size == 0) { //player found no suspicious passage
      this.resultBoxView.reasonTxt.text = ""; //don't need to display found passages and reasons, because player found none.
      this.resultBoxView.resultTxt.text = "";
      this.resultBoxView.hackerImg.visible = true; //show image of a hacker to fill empty space
      this.stage.setChildIndex(this.resultBoxView.hackerImg, this.stage.numChildren - 1);
    }
    else { //player found at least one suspicious passage
      this.resultBoxView.resultTxt.color = "black";
      this.resultBoxView.resultTxt.text = l('evaluation'); //Headline 'Evaluation'

      this.resultBoxView.reasonTxt.text = "";

      this.resultBoxView.legendCont.visible = true; //yellow Box as legend
      this.stage.setChildIndex(this.resultBoxView.legendCont, this.stage.numChildren - 1);

      let targets = this.emailViews[currentIndex].targets;
      this.reasonCont = new createjs.Container(); //contains marked texts and resaon Texts that are displaed in resultbox on the left side
      let height = 20; //for y position of reason Texts

      for (let i of this.foundSet) { //list found suspicious passages and the reasons for this classification
        var markedText = new Textbox({ //Text of supicious passage marked with yellow BG
          text: targets[i].getSuspiciousText(),
          x: this.resultBoxView.reasonTxt.x,
          y: this.resultBoxView.reasonTxt.y + height,
          colorBg: "yellow",
        });
        this.reasonCont.addChild(markedText.textbox);
        height += markedText.bg.bgSize.h + 10;

        var reasonText = new Textbox({ //Reason for the classification
          text: targets[i].getReason(),
          x: this.resultBoxView.reasonTxt.x,
          y: this.resultBoxView.reasonTxt.y + height,
          fixwidth: 650,
        });
        reasonText.fixHeight(); //adjust Height, because reason  Text could be more than one line
        this.reasonCont.addChild(reasonText.textbox);
        height += reasonText.bg.bgSize.h + 30;

        this.stage.addChild(this.reasonCont);
      }
    }

    if (this.forgotSet.size !== 0) { //there are passages that the player does not found
      this.resultBoxView.firstMouseover.visible = true; //display prompt to hover over marked passages first before continuing
      this.stage.setChildIndex(this.resultBoxView.firstMouseover, this.stage.numChildren - 1);
    }

    tickerWrapper = createjs.Ticker.on("tick", resultOnMouseover.bind(this)); //dislay result box on mouseover over target
  }

  //display result on mouseover over marked passages, display continue-button after each explanation was read
  function resultOnMouseover() {
    var forgotSet = this.forgotSet;

    let targets = this.emailViews[currentIndex].targets; //suspicious passages

    for (let index = 0; index < targets.length; index++) {

      // This does not work here any longer => check has to be moved to the components instead
      let hoverData = targets[index].globalToLocal(MTLG.getStage().mouseX, MTLG.getStage().mouseY); //mouse position

      if(Array.isArray(hoverData)) { // is a MultiTextbox
        // Showing the right reason text on mouseover 
        // Need: reference to reason textbox that is to be shown, reason textboxes that are to be hidden
        let reasonTextData = targets[index].showReasonText(hoverData);
        if(reasonTextData.toShow.length > 0){
          for(let textbox of reasonTextData.toShow){
            textbox.reason.textbox.visible = true; //display reason underneath suspicious passage
            this.stage.setChildIndex(textbox.reason.textbox, this.stage.numChildren - 1);
          }
          
          if (forgotSet.has(index)) { //first mouseover over all forgotten passages. Afterwards display of Continue Button
            forgotSet.delete(index); //remove all indices belonging to passages where player already hovered over.
            targets[index].setAlpha(0.5); //decrease opacity of Background of mousovered passage, to indicate that player already hovered over it
          }
        }
        for(let textbox of reasonTextData.toHide){
          textbox.reason.textbox.visible = false;
        }
        
      }
      else { // is a simple Textbox
        if (targets[index].hitTest(hoverData.x, hoverData.y)) { //if mouse hovers over suspicious passage
          targets[index].reason.textbox.visible = true; //display reason underneath suspicious passage
          this.stage.setChildIndex(targets[index].reason.textbox, this.stage.numChildren - 1);
  
          if (forgotSet.has(index)) { //first mouseover over all forgotten passages. Afterwards display of Continue Button
            forgotSet.delete(index); //remove all indices belonging to passages where player already hovered over.
            targets[index].setAlpha(0.5); //decrease opacity of Background of mousovered passage, to indicate that player already hovered over it
          }
        }
        else {
          targets[index].reason.textbox.visible = false;
        }
      }     
    }
    this.controlPaneView.inactiveContinueButton.visible = true;
    if (forgotSet.size == 0) { //display continue Button after Mouseover over all forgotten targets
      this.controlPaneView.inactiveContinueButton.visible = false
      this.controlPaneView.continueButton.visible = true; //visible after classification of an email
    }
  }

  //mark all suspicious passages purple
  function markSuspPassages(targets) {
    // this.emailViews[currentIndex].headerView._senderView.showMail(); //show hidden email-adress too so that the player can see if this adress is also suspicious
    var suspSet = new Set();
    for (var i = 0; i < targets.length; i++) {
      suspSet.add(i);
      //targets[i].children[0].bgColor.style = "#09fbd3"; //light blue
      targets[i].setBgColor("#fe53bb"); //pink/purple
    }
    return suspSet;
  }

  //shift position on boxes in messageList. Add new on click events
  function adjustMsgList(index) {
    this.stage.removeChild(this.emailViews[index].emailContainer); //remove email preview
    this.emailViews.splice(index, 1); //remove email View with specified index from EmailViews Array. This Mail was already classified.
    previousIndex = 0; //reset indices, because after an email was classified, no email is displayed
    currentIndex = 0;

    this.stage.removeChild(this.msgList[index]); //remove Messagebox that belongs to classified email
    this.msgList[index].removeAllEventListeners();
    this.msgList.splice(index, 1);


    for (var i = 0; i < this.msgList.length; i++) { //add new on click event. needed because indices are now shifted
      this.msgList[i].removeAllEventListeners(); //remove old event listeners
      if (this.addedNew) { //if new mail is added than shift messages boxes down. But only the ones above the previously deleted one are shifte down. The others remain in same position
        if (i < index && i !== 0) this.msgList[i].y += 170;
      }
      else { //if no mail added than shift up. But only the boxes under the previously deleted are shifted up. The others remain in previous position
        if (i >= index) this.msgList[i].y -= 170;
      }

      //(function (i) {
      addMsgListener.bind(this)(i); //add on click events
      //}.bind(this))(i);

    }


  }

  //add new Mail to displayed Mails Array, increase folder counter, show notification
  function addNewMail() {
    if (this.hiddenMails.length > 0) { //if a new Mail could be displayed
      this.addedNew = true; //true if new Mails was added
      var newMail = this.hiddenMails.pop(); //get last Mail of hiddenMails Array
      this.displayedMails.unshift(newMail); //add new Mail to beginning

      var newEmailView = new emailView(folderWidth + msgListWidth + 20, 10, previewWidth, emailclientHeight, this.stage, newMail, showHeader); //View of recenty added Mail
      this.emailViews.unshift(newEmailView); //add to beginning, because Messageboxes of new Mails will be displayed at the very top

      var newMsgBox = this.messageListView.createMsgBox(newMail, 0); //new Messagebox belonging to new Mail. Index 0 leads to a position at the very top
      this.messageListView._messageContainer.unshift(newMsgBox); //this.msgList points to _messageContainer. Add Messagebox to beginning
      this.stage.addChildAt(newMsgBox, 2); //index 2 so that evaluation is not overlapped

      this.folderPaneView.numUnread++; //increase counter
      this.folderPaneView.numUnreadBox.textbox.visible = true; //if it was before invisible, because numRead was 0
      this.folderPaneView.numUnreadBox.txt.text = this.folderPaneView.numUnread;

      this.stage.setChildIndex(this.newMsgNotification.box.textbox, this.stage.numChildren - 1); //set pop up notification to highest index
      notification.bind(this)(); //show popup

      currentIndex++; //because adding new mail caused a shift

    }
  }

  //all emails are classified. show finish View
  function allClassified() {
    //this.stage.removeAllChildren();
    finishView(this.stage);
  }

  //display controlpane buttons and resultbox depending on 'isVisible'. true means buttons are visible and resultbox not
  function toggleControlPaneButtons(isVisible) {
    this.resultBoxView.resultContainer.visible = !isVisible; //when Buttons are visible, than resultbox is always invisible
    this.controlPaneView.classification.visible = isVisible;
    this.controlPaneView.helpModalView.helpModalContainer.visible = false; //help Modal is always invisible after a classification
    this.controlPaneView.infoBox.boxes.visible = false; //Info boxes are always invisible after a classification

    // if (showHeader) { //Header Pop Up could be visible. Make invisible
    //   this.emailViews[currentIndex].headerView._functionsView._headerPopupContainer.visible = false;
    // }
  }

  //dynamically adding an email. Will be called in 'MTLG.emailClient.addEmail(email)'
  this.addEmail = function (email) {
    this.hiddenMails.unshift(email); //add new Mail to beginning
    if (this.displayedMails.length <= 3 && this.hiddenMails.length == 1) { //new email is only displayed directly if it is the only hidden email and if less than 3 mails are currently displayed.
      addNewMail.bind(this)(); //new Mail on top, notification, and counter up in folder

      for (var i = 0; i < this.msgList.length; i++) { //add new on click event. needed because indices are now shifted
        this.msgList[i].removeAllEventListeners(); //remove old event listeners

        if (i !== 0) this.msgList[i].y += 170; //all messageboxes are shifted down except the one from the newly inserted email

        addMsgListener.bind(this)(i); //add on click events
      }
    }

  }

  //************************************* Start Email Creation (And Matching Game) *************************************
  //for the specific games look at creationGame.js and matchingGame.js

  /**
   * This function manages the ingame tutorials for different phishing features
   * @function
   */
  function manageTutorial(){
    path = this.folderPaneView.levelView.tutorialView;
    this.progData.currently = "tutorial";
    this.folderPaneView.buttonTextView.text = l("back");
    this.setNewEmailGameContainerVisibility(false,false,false,false,false,false,false,false,false,true); //levelView, email creation view (newEmailView), email creation view buttons, matching game view, matching game buttons, review view, continue button, matching game lives, tutorial
    this.stage.setChildIndex(path.tutorialViewContainer, this.stage.numChildren - 1);

    //find out how to align the text
    let xPadding = 50;
    let yPadding = 35;
    let xAlign = null;
    let font = "30px Arial";
    let padTextbox = 10;
    let color = null;

    //test text to get alignment
    let testText = new Textbox({
      x: path.x + path.width - xPadding,
      text: this.progData.activeTutorial.dialog[MTLG.lang.getLanguage()][this.progData.activeTutorialIndex],
      rightalign: true,
      font: font,
      fixwidth: path.width*3/4,
    });

    //find out who is speaking for text alignment
    if((this.progData.activeTutorialIndex%2==0 && this.progData.activeTutorial.dialog.start == "supervisor") || (this.progData.activeTutorialIndex%2==1 && this.progData.activeTutorial.dialog.start == "intern")){ //supervisor speaks
      xAlign = path.x + xPadding;
      color = this.orange;
    }
    else if ((this.progData.activeTutorialIndex%2==1 && this.progData.activeTutorial.dialog.start == "supervisor") || (this.progData.activeTutorialIndex%2==0 && this.progData.activeTutorial.dialog.start == "intern")){ //intern speaks
      if (testText.txt.lines > 1){ //multiple lines
        xAlign = path.x + path.width - xPadding - path.width*3/4 - padTextbox*2;
      }
      else{ //single line
        xAlign = path.x + path.width - xPadding - testText.textbox.children[1].getMeasuredWidth() - padTextbox*2;
      }
      color = this.blue;
    }
    let yAlign = path.tutorialDialogTextboxes.length > 0? path.tutorialDialogTextboxes[path.tutorialDialogTextboxes.length-1].textbox.children[1].y + path.tutorialDialogTextboxes[path.tutorialDialogTextboxes.length-1].txt.lines*testText.txt._rectangle.height + yPadding: path.textStartY + yPadding;

    path.tutorialDialogTextboxes.push( new Textbox({
      x: xAlign,
      y: yAlign,
      text: this.progData.activeTutorial.dialog[MTLG.lang.getLanguage()][this.progData.activeTutorialIndex],
      fixwidth: testText.txt.lines > 1? path.width*3/4: null,
      font: font,
      colorBg: color,
      //stroke: "black",
      pad: padTextbox,
    }));
    path.tutorialViewContainer.addChild(path.tutorialDialogTextboxes[path.tutorialDialogTextboxes.length-1].textbox);
  }

  /**
   * This function adds an event listener to the tutorial view that allows the progression of the tutorial conversation by clicking on the tutorialView's background
   * @function
   */
  function addEventListenerTutorial(){
    this.folderPaneView.levelView.tutorialView.bg.on('click', function (evt) {
      if (Object.keys(this.progData.activeTutorial.dialog[MTLG.lang.getLanguage()]).length -1 > this.progData.activeTutorialIndex){
        this.progData.activeTutorialIndex++;
        manageTutorial.bind(this)();
        if (Object.keys(this.progData.activeTutorial.dialog[MTLG.lang.getLanguage()]).length -1 == this.progData.activeTutorialIndex){ //all tutorial parts have been shown
          this.manageDialogBox(l('dialog-tutorial2'));
          this.controlPaneView.continueButton.visible = true; //show continue button
          this.progData.activeLevelPassed = true;
        }
        xapiButton({en:"tutorial button", de:"Tutorial-Button"},this.progData.userId,this.progData.currently,this.progData.activeTutorial.name); //xapi call
      }
    }.bind(this));
  }

  /**
   * This function wraps the MTLG sound function to make the game play sounds only if the sound option (this.progData.sound) is set to true
   * @function
   * @param {string} sound Contains the link to the soundfile that should be played
   */
  this.playSounds = function (sound){
    if (this.progData.sounds == true){
          MTLG.assets.playSound(sound); // e.g. sound = "feedback/button-selected"
    }
  }.bind(this);

  /**
   * This function manages the content of the dialogBox to give the players feedback and instructions
   * @function
   * @param {string} message The first message that should be displayed. If it isn't given the dialog box is set to invisible
   * @param {string} message2 The second message to be displayed after the first one. We wait 1.5s until we display the second message after the first one was shown
   */
  this.manageDialogBox = function (message=null,message2=null,fast=false){
    let path = this.controlPaneView.dialogView.dialogBox;
    if(message != null){
      this.controlPaneView.dialogView.dialogBoxContainer.visible = true;
      var fadeOutTime = 250; //if there is a prior text in the box we fade it out before fading the new one in
      if (path.text == " "){ //no prior text, so basically just fade in the new text
        fadeOutTime = 1;
      }
      if (message2 != null){ //show a second message after the first one. used to show short feedback in the matching game
        createjs.Tween.get(path,{override:true})
          .to({alpha:0},fadeOutTime) //fade out the option
          .call(function (target,messageText,fnc){
            target.text = messageText;
            fnc.fixDialogBoxHeight();
          },[path,message,this.controlPaneView.dialogView])
          .to({alpha:1},fadeOutTime)
          .wait(1500) //wait a bit so the players can actually read the message
          .to({alpha:0},fadeOutTime)
          .call(function (target,messageText,fnc){
            target.text = messageText;
            fnc.fixDialogBoxHeight();
          },[path,message2,this.controlPaneView.dialogView])
          .to({alpha:1},fadeOutTime);
      }
      else{
        createjs.Tween.get(path,{override:true})
          .to({alpha:0},fadeOutTime) //fade out the option
          .call(function (target,messageText,fnc){
            target.text = messageText;
            fnc.fixDialogBoxHeight();
          },[path,message,this.controlPaneView.dialogView])
          .to({alpha:1},250);
      }
    }
    else{ //no text was given so we just make the box invisible
      this.controlPaneView.dialogView.dialogBoxContainer.visible = false;
      path.text = " ";
    }

  }.bind(this);

  /**
  * This function manages which level buttons are clickable/active and therefore which levels can be selected
  * @function
  */
  this.manageLevelProgression = function (){
    let path = this.folderPaneView.levelView;
    outer: for (let i = 0; i < phishingLevels.length; i++){ //check all levels
      let skip = false;
      let values = null;
      let id = null;

      if (phishingLevels[i].creation == undefined){ //tutorial level
        values = Object.values(phishingLevels[i].requirement);
        id = phishingLevels[i].id;
      }
      else { //creation level
        values = Object.values(phishingLevels[i].creation.infos.requirement);
        id = phishingLevels[i].creation.infos.id;
      }

      //levels have to have a smaller requirement id than the current level
      for (let j of values){
        if (this.progData.passedLevelsSet.has(j) == false){
          skip = true;
          break;
        }
      }
      if (skip == false){
        activateLevelButton.bind(this)(id);
      }
    }
  }.bind(this);
  this.manageLevelProgression(); // also call the function immediately

  /**
   * This function manages (activates and deactivates) the timer depiction on the screen
   * @function
   * @param {number} duration How long the timer is supposed to be running in ms
   * @param {bool} activate Whether we want to start the timer (true) or deactivate it and make it invisible (false)
   */
  this.manageTimerDisplay = function (duration,activate){
    if (activate == true){ //start timer
      this.folderPaneView.clockText.textbox.visible = true;
      this.folderPaneView.clockImg.visible = true;
      this.progData.time = duration/1000;
      this.folderPaneView.clockText.txt.text = this.progData.time.toString() + "s";
      this.folderPaneView.clockText.txt.color = this.black;
      this.progData.displayClockTimer = setInterval( () => {
        if (this.progData.time > 0){
          this.progData.time--;
          this.folderPaneView.clockText.txt.text = this.progData.time.toString() + "s";
        }
        if (this.progData.time < 11){
          this.folderPaneView.clockText.txt.color = this.orange;
        }
        else{
          this.folderPaneView.clockText.txt.color = this.black;
        }
      },1000);
    }
    else if (activate == false){ //stop timer and make it invisible
      this.folderPaneView.clockText.textbox.visible = false;
      this.folderPaneView.clockImg.visible = false;
      clearInterval(this.progData.displayClockTimer); //just for good measure
      this.progData.timer.stop();
    }
  }.bind(this);

  /**
   * This function sets the containers of the new email game to visible or invisible
   * @function
   * @param {bool} levelView Sets the levelViewContainer to visible (if true) or invisible (if false)
   * @param {bool} creationView Sets the newEmailViewContainer to visible (if true) or invisible (if false)
   * @param {bool} creationButtons Sets the choice options (the buttons) of the creation game to visible (if true) or invisible (if false)
   * @param {bool} matchingView Sets the newEmailMatchViewContainer to visible (if true) or invisible (if false)
   * @param {bool} matchingButtons Sets the options of the matching game (the buttons) to visible (if true) or invisible (if false)
   * @param {bool} matchingLogos Sets the newEmailMatchLogoContainer to visible (if true) or invisible (if false)
   * @param {bool} reviewView Sets the newEmailReviewContainer to visible (if true) or invisible (if false)
   * @param {bool} continueButton Sets the continue Button to visible (if true) or invisible (if false)
   * @param {bool} matchingLives Sets the lives in the newEmailMatchLiveView to visible (if true) or invisible (if false)
   * @param {bool} tutorialView Sets the tutorialViewContainer to visible (if true) or invisible (if false)
   */
  this.setNewEmailGameContainerVisibility = function (levelView,creationView,creationButtons,matchingView,matchingButtons,matchingLogos,reviewView,continueButton,matchingLives,tutorialView){
    let path = this.folderPaneView.levelView;
    path.levelViewContainer.visible = levelView;
    for (let i = 0; i < path.levelButtons.length; i++){
      path.levelButtons.visible = levelView;
    }
    path.newEmailPaneView.newEmailViewContainer.visible = creationView;
    creationGameModule.setChoiceButtonVisibility.bind(this)(creationButtons);
    path.newEmailPaneView.creationTutorialContainer.visible = creationButtons;
    this.folderPaneView.emailLengthText.textbox.visible = creationView;
    this.folderPaneView.emailLengthNumber.textbox.visible = creationView;
    if(reviewView == true){ //we don't want the email length to show up in the review (the review also uses the creation view since we display the created email)
      this.folderPaneView.emailLengthText.textbox.visible = false;
      this.folderPaneView.emailLengthNumber.textbox.visible = false;
    }
    path.newEmailPaneView.newEmailMatchView.newEmailMatchViewContainer.visible = matchingView;
    path.newEmailPaneView.newEmailMatchView.setMatchingButtonsVisibility(matchingButtons);
    path.newEmailPaneView.newEmailMatchView.newEmailMatchLogoView.newEmailMatchLogoContainer.visible = matchingLogos;
    path.newEmailPaneView.newEmailReviewView.newEmailReviewContainer.visible = reviewView;
    this.controlPaneView.continueButton.visible = continueButton;
    path.newEmailPaneView.newEmailMatchView.newEmailMatchLiveView.lives[0].visible = matchingLives;
    path.newEmailPaneView.newEmailMatchView.newEmailMatchLiveView.lives[1].visible = matchingLives;
    path.newEmailPaneView.newEmailMatchView.newEmailMatchLiveView.livesCaption.textbox.visible = matchingLives;
    path.tutorialView.tutorialViewContainer.visible = tutorialView;
    this.folderPaneView.settingsView.settingsViewContainer.visible = false; //always turn of the settings when you change the view
    this.folderPaneView.finishNewGameView.finishNewGameViewContainer.visible = false; //always turn of the finish view when you change the view (every change of view will make the gameOver view disappear)
  }.bind(this);

  /**
   * This function initializes the level selection screen after the continue button was pressed. Mostly visibilities are changed
   * @function
   */
  this.initLevelSelection = function (){
    if (this.progData.currently != "selecting"){ //we don't want the dialog to reappear if the player mashes the new button, so check if it's already displaying the right dialog
      this.manageDialogBox(l('dialog-level-instructions'));
      this.progData.currently = "selecting";
      this.folderPaneView.buttonTextView.text = l("menu");
    }
    updateAvailableLevels.bind(this)();
    this.stage.setChildIndex(this.folderPaneView.levelView.levelViewContainer, this.stage.numChildren - 1);
    this.stage.setChildIndex(this.folderPaneView.tutorialModalView.tutorialModalViewContainer, this.stage.numChildren - 1);
    if (!this.tutorialSeen.includes(2)) {
      this.tutorialSeen.push(2);
      this.folderPaneView.tutorialModalView.tutorialModalViewContainer.visible = true;
    }
    if (this.progData.matchingTimerEnabled == true || this.progData.creationTimerEnabled == true){ //disable timers if they are enabled
      this.folderPaneView.scoreText.textbox.visible = false;
      this.folderPaneView.scoreNumber.textbox.visible = false;
      this.folderPaneView.highscoreText.textbox.visible = false;
      this.folderPaneView.highscoreNumber.textbox.visible = false;
      this.manageTimerDisplay(0,false);
      this.progData.timer.stop();
    }
    //this.folderPaneView.newButton.children[0].alpha = 0.5; //set alpha to 0.5 to showcase that the button doesn't really do anything during level selection
    this.setNewEmailGameContainerVisibility(true,false,false,false,false,false,false,false,false,false); //levelView, email creation view (newEmailView), email creation view buttons, matching game view, matching game buttons, review view, continue button, matching game lives, tutorial

    //Congratulations view activation
    if (this.progData.passedLevelsSet.has(26) && this.progData.gameOverScreenShown == false){ //the player has reached the bonus stages
      for (let i of this.progData.gameplayLevelsSet){ //check if all gameplay levels have been finished
        if (this.progData.passedLevelsSet.has(i) == false){
          return;
        }
      }
      this.folderPaneView.finishNewGameView.finishNewGameViewContainer.visible = true;
      this.stage.setChildIndex(this.folderPaneView.finishNewGameView.finishNewGameViewContainer, this.stage.numChildren - 1);
      this.folderPaneView.finishNewGameView.speechContainer.visible = true;
      this.stage.setChildIndex(this.folderPaneView.finishNewGameView.speechContainer, this.stage.numChildren - 1);
      this.playSounds("feedback/correct");
      this.folderPaneView.finishNewGameView.moveInContent(2000);
      this.progData.gameOverScreenShown = true; //only show the contratulations screen once
    }
  }.bind(this);

  /**
   * This function adds event listeners to new email game options so they can be dragged and/or pressed
   * @function
   * @param {button[]} buttons The array of buttons that receive the new on click events
   * @param {bool} match Decides whether buttons belong to the creation game and can be either dragged or clicked, or if they belong to the matching game where they can only be dragged
   */
  this.dragButtonListener = function (buttons,match=false){
    //let buttons = this.folderPaneView.levelView.newEmailPaneView.newEmailMatchView;
    for (let i = 0; i < buttons.length; i++){
      buttons[i].removeAllEventListeners();
      if (match == true){
        buttons[i].on('pressmove',function (evt){
          if (this.folderPaneView.settingsView.settingsViewContainer.visible == false){
              dragButton.bind(this)(evt);
          }
        }.bind(this));
        buttons[i].on('pressup',function (evt){
          if (this.folderPaneView.settingsView.settingsViewContainer.visible == false){
              matchingGameModule.positionMatchingOption.bind(this)(evt);
          }
        }.bind(this));
      }
      else{ //email creation buttons
        if (this.progData.clicksEnabled == false){
          buttons[i].on('pressmove',function (evt){
            if (this.folderPaneView.settingsView.settingsViewContainer.visible == false){
                dragButton.bind(this)(evt);
            }
          }.bind(this));
        }
        buttons[i].on('pressup',function (evt) {
              if (this.progData.clicksEnabled == false && this.folderPaneView.settingsView.settingsViewContainer.visible == false){
                creationGameModule.dragNdropEmailCreation.bind(this)(evt,i);
              }
        }.bind(this));
        buttons[i].on('click', function (evt) {
              if (this.progData.clicksEnabled == true && this.folderPaneView.settingsView.settingsViewContainer.visible == false){
                let path = this.folderPaneView.levelView.newEmailPaneView;
                let p = evt.currentTarget.localToLocal(evt.localX, evt.localY, this.stage)
                creationGameModule.manageEmailCreation.bind(this)(i);
                xapiButton({en:"option " + i.toString() + " button", de:"Option " + i.toString() + " Button"},this.progData.userId,this.progData.currently, this.progData.currentPhishingEmail.infos.name); //xapi call
                //}
              }
        }.bind(this));
      }
    }
  }.bind(this);

  /**
   * This function calls the text realignment function for the text of a button (used for tween events)
   * @function
   * @param {button} button The button whose text should be realigned
   */
  this.realignButtonText = function (button){
    button.children[1].realignButtonText();
  }

  this.setNewEmailGameInterface = function (newVisible=true) {
    let levelView = this.folderPaneView.levelView;
    this.folderPaneView.levelView.newEmailPaneView.newEmailMatchView.newEmailMatchViewContainer.visible = false;
    if (this.gameVisible !== 2 && newVisible) {
      this.gameVisible = 2;
      this.stage.setChildIndex(levelView.levelViewContainer, this.stage.numChildren - 1);
      this.initLevelSelection();
      this.manageDialogBox(l("dialog-level-instructions"));
      this.controlPaneView.classification.visible = false;
      this.controlPaneView.task.visible = false;
      this.messageListView._messageContainer.forEach((item, i) => {
        item.visible = false;
      });
      this.messageListView._searchContainer.visible = false;
      this.emailViews.forEach((item, i) => {
        item.emailContainer.visible = false;
      });
      this.folderPaneView._folderContainer.visible = false;
      this.folderPaneView.numUnreadBox.textbox.visible = false;
      this.folderPaneView.moveSettingsButton(true);
    } else if (this.gameVisible !== 1 && !newVisible){
      //this.folderPaneView.newButton.children[0].alpha = 0.5;
      this.gameVisible = 1;
      this.controlPaneView.classification.visible = true;
      this.controlPaneView.task.visible = true;
      this.manageDialogBox();
      this.setNewEmailGameContainerVisibility(false,false,false,false,false,false,false,false,false,false); //levelView, email creation view (newEmailView), email creation view buttons, matching game view, matching game buttons, review view, continue button, matching lives, tutorial
      this.stage.setChildIndex(this.folderPaneView.tutorialModalView.tutorialModalViewContainer, this.stage.numChildren - 1);
      if (!this.tutorialSeen.includes(1)) {
        this.tutorialSeen.push(1);
        this.folderPaneView.tutorialModalView.tutorialModalViewContainer.visible = true;
      }
      this.messageListView._messageContainer.forEach((item, i) => {
        item.visible = true;
      });
      this.messageListView._searchContainer.visible = true;
      for (let i = 0; i < this.progData.highlights.length; i++){ //turn of potentially remaning highlights
        this.progData.highlights[i].shape.visible = false;
      }
      if(seeMail) {
        this.emailViews[currentIndex].emailContainer.visible = true;
      }
      this.manageTimerDisplay(0,false);
      this.folderPaneView._folderContainer.visible = true;
      this.folderPaneView.numUnreadBox.textbox.visible = true;
      this.folderPaneView.moveSettingsButton(false);
      this.folderPaneView.buttonTextView.text = l("menu");
    }
    this.folderPaneView.tutorialModalView.switchGame(this.gameVisible);
  }

  /**
   * This function adds onClick events for level buttons with index i in the levelButtons list.
   * @function
   * @param {number} levelId ID of the level button that will receive an on click event that leads to a new level (either tutorial or playable level).
   *                         The level the button will "open", is taken from the phishingEmailModel by looking for an entry with the same ID as the button
   */
  function addLevelButtonListener(levelId) {
    var levelButtons = this.folderPaneView.levelView.levelButtons;
    levelButtons[levelId].on('click', function (evt) {
      if (this.folderPaneView.settingsView.settingsViewContainer.visible == false && this.folderPaneView.finishNewGameView.finishNewGameViewContainer.visible == false
        && this.folderPaneView.tutorialModalView.tutorialModalViewContainer.visible == false){
        this.playSounds("feedback/button-selected");
        //this.folderPaneView.newButton.children[0].alpha = 1; //set alpha of new button (to showcase that the button can now be used to cancel a level)
        this.setNewEmailGameContainerVisibility(false,true,true,false,false,false,false,false,false,false) //levelView,creationView,creationButtons,matchingView,matchingButtons,matchingLogos,reviewView,continueButton,matchingLives,tutorialView
        this.stage.setChildIndex(this.folderPaneView.levelView.newEmailPaneView.newEmailViewContainer, this.stage.numChildren - 1);
        this.progData.activeLevel = levelId;
        this.progData.activeLevelPassed = false;
        for (let j = 0; j < phishingLevels.length; j++){
          if (phishingLevels[j].creation != undefined && phishingLevels[j].creation.infos.id == levelId){ //creation level
            creationGameModule.initEmailCreation.bind(this)();
            xapiLevelStart(phishingLevels[j].creation.infos.name, this.progData.userId); //xapi call
            xapiStageStart(this.progData.currently, this.progData.userId, this.progData.currentPhishingEmail.infos.name); //xapi call
            break;
          }
          else if (phishingLevels[j].id == levelId){ //tutorial level
            this.progData.activeTutorial = phishingLevels[j];
            xapiStageStart(this.progData.currently, this.progData.userId, this.progData.activeTutorial.name); //xapi call

            //reset old tutorial level
            this.progData.activeTutorialIndex = 0;
            resetListOfTextboxes(this.folderPaneView.levelView.tutorialView.tutorialViewContainer,this.folderPaneView.levelView.tutorialView.tutorialDialogTextboxes);
            this.manageDialogBox(l('dialog-tutorial1'));

            //start new tutorial
            manageTutorial.bind(this)();
            break;
          }
        }
      }
    }.bind(this));
  }

  /**
   * This function deletes old textboxes a list of textboxes from a container (used to reset the newEmailContent / the body of a previously created email)
   * @function
   * @param {container} container The container the list of textboxes is in
   * @param {textbox[]} list The textboxes we want to delete
   */
  function resetListOfTextboxes(container,list){
    for(let i = 0; i < list.length; i++){
      container.removeChild(list[i].textbox);
    }
    list.length = 0;
  }

  /**
   * This function makes buttons draggable. Basically an event is used to drag its target along with the curser
   * @function
   * @param {event} e The event that get triggered by a pressmove event.
   */
  function dragButton(e){
    this.stage.setChildIndex(e.currentTarget, this.stage.numChildren - 1); //make sure the current object is on top

    //get button specifications
    let btnWidth = e.currentTarget.children[0].graphics._instructions[1].w;
    let btnHeight = e.currentTarget.children[0].graphics._instructions[1].h;
    let lineHeight = e.currentTarget.children[1].lineHeight;
    let txtHeight = e.currentTarget.children[1].getBounds().height;
    let p = e.currentTarget.localToLocal(e.localX, e.localY, this.stage); //get current pointer location

    //console.log(e,e.currentTarget);

    //TODO move container not children
    e.currentTarget.children[0].set({ x: p.x - btnWidth/2, y: p.y - btnHeight/2 });
    e.currentTarget.children[1].set({ x: p.x, y: p.y - txtHeight/2 + lineHeight/4});
    e.currentTarget.children[0].alpha = 0.5; //make button a bit transparent so the players can see where they drag something
  }

  /**
   * This function "activatea" the given level button. Allows the players to access new levels.
   * @function
   * @param {number} id The ID of the level button whose level is now playable because the requirements were fulfilled elsewhere
   */
  function activateLevelButton(id){
    let path = this.folderPaneView.levelView;
    path.levelButtons[id].removeAllEventListeners();
    path.levelButtons[id].children[0].alpha = 1;
    addLevelButtonListener.bind(this)(id);
  }

  /**
   * This function updates the currently available levels by checking if the last level was passed and calling activateLevelButton()
   * @function
   */
  function updateAvailableLevels(){
    if (this.progData.activeLevelPassed == true){ //if the last level was passed add it to the passedLevelsSet
      this.progData.passedLevelsSet.add(this.progData.activeLevel);
      this.progData.save();
      for (let i = 2; i < this.folderPaneView.levelView.levelButtons[this.progData.activeLevel].children.length; i++){ //makes the bow icon of the finished level visible on the button
        if (this.folderPaneView.levelView.levelButtons[this.progData.activeLevel].children[i].position == 2){
          this.folderPaneView.levelView.levelButtons[this.progData.activeLevel].children[i].visible = true;
        }
      }

      this.manageLevelProgression(); //alter the selectable levels in the level view
    }
  }

  //----On Click Events for Buttons in Control Pane-------------------------------------------------------------------

  //Legitimate-Button
  this.controlPaneView.legitButton.on('click', function (evt) {
    if (seeMail) { //click has only an effect if an email is currently displayed

      toggleControlPaneButtons.bind(this)(false); //make Buttons invisible and resultbox visible
      var emailProps = this.displayedMails[currentIndex]; //properties of currently displayed Email

      if (emailProps.isPhish) { //Mail is phishing, but user clicked on legitimate button
        this.resultBoxView.resultTxt.text = l('incorrect') + l('phishTxt-incorrect'); //display result, e.g. 'Incorrect. This is a Phishing E-Mail'
        this.resultBoxView.resultTxt.color = "red"
        this.resultBoxView.reasonTxt.text = "";

        //display prompt to hover over marked passages first before continuing
        this.resultBoxView.firstMouseover.visible = true;
        this.stage.setChildIndex(this.resultBoxView.firstMouseover, this.stage.numChildren - 1);

        //display hacker img to fill empty space
        this.resultBoxView.hackerImg.visible = true;
        this.stage.setChildIndex(this.resultBoxView.hackerImg, this.stage.numChildren - 1);

        //mark all forgotten passages and display reason on mouseover
        let targets = this.emailViews[currentIndex].targets;
        this.suspSet = markSuspPassages.bind(this)(targets); //this.suspSet = this.forgotSet = markSuspPassa... leads to side effects
        this.forgotSet = markSuspPassages.bind(this)(targets);

        tickerWrapper = createjs.Ticker.on("tick", resultOnMouseover.bind(this)); //dislay result box on mouseover over target
      }

      else { //Mail is legitimate and user clicked on legitimate button
        this.resultBoxView.resultTxt.text = l('correct') + l('nophishTxt'); //display result
        this.resultBoxView.resultTxt.color = "green";

        this.resultBoxView.reasonTxt.text = emailProps.reason; //list reasond why the classification as legitimate is correct
        this.controlPaneView.continueButton.visible = true; //continue button visible
      }

    }
  }.bind(this));

  
  this.controlPaneView.avatarImg.on('click', function (evt) {
    this.folderPaneView.tutorialModalView.tutorialModalViewContainer.visible = true;
    this.stage.setChildIndex(this.folderPaneView.tutorialModalView.tutorialModalViewContainer, this.stage.numChildren - 1);

    this.folderPaneView.tutorialModalView.pageBack();
  }.bind(this));

  //Phishing-Button
  this.controlPaneView.phishButton.on('click', function (evt) {
    if (seeMail) { //click has only an effect if an email is currently displayed

      toggleControlPaneButtons.bind(this)(false); //make Buttons invisible and resultbox visible
      var emailProps = this.displayedMails[currentIndex];

      if (emailProps.isPhish) { //Email is phishing and player clicked on phishing button
        this.resultBoxView.resultTxt.text = l('correct') + l('phishTxt-correct'); //result , e.g 'Correct. This is a phishing E-Mail'
        this.resultBoxView.resultTxt.color = "green";

        this.resultBoxView.reasonTxt.text = l('PromptToMark'); //prompt to mark suspicious passages

        this.stage.setChildIndex(this.resultBoxView.resultContainer, this.stage.numChildren - 1); //set to highest index, so that it's visible

        this.resultBoxView.tutImg.visible = true; //display Tutorial Image showing how to use the stamp
        this.stage.setChildIndex(this.resultBoxView.tutImg, this.stage.numChildren - 1);

        displayStamp.bind(this)();
      }
      else { //Email is legitimate, but player clicked on phishing button
        this.resultBoxView.resultTxt.text = l('incorrect') + l('nophishTxt'); //result
        this.resultBoxView.resultTxt.color = "red";

        this.resultBoxView.reasonTxt.text = emailProps.reason; //list reason why the E-Mail should be classified as legitimate

        this.controlPaneView.continueButton.visible = true; //Continue Button visible

        this.stage.setChildIndex(this.resultBoxView.resultContainer, this.stage.numChildren - 1); //set to highest index, so that it's visible
      }

    }
  }.bind(this));

  //Help-Button
  this.controlPaneView.helpModalView.helpButtonContainer.on("click", function (evt) {
    if (seeMail) { //click has only an effect if an email is dislayed right now
      var helpView = this.controlPaneView.helpModalView; //View of help Modal (yellow box on left side)
      if (helpView.helpModalContainer.visible) { //if modal is visible make it invisible
        helpView.helpModalContainer.visible = false;
      }
      else { //if modal is invisible make it visible and set to highest index on stage
        helpView.modal.txt.text = this.displayedMails[currentIndex].help[MTLG.lang.getLanguage()];
        helpView.helpModalContainer.visible = true;
        this.stage.setChildIndex(helpView.helpModalContainer, this.stage.numChildren - 1);
      }
    }
  }.bind(this));

  //Continue-Button
  this.controlPaneView.continueButton.on('click', function (evt) {
    this.playSounds("feedback/button-selected");
    createjs.Ticker.off("tick", tickerWrapper); //remove Tick event (results on Mouseover)
    createjs.Ticker.off("tick", this.tickerWrapperEmailHighlights); //remove Tick event (results on Mouseover)
    createjs.Ticker.off("tick", this.tickerWrapperParticipantHovers); //remove Tick event (results on Mouseover)
    createjs.Ticker.off("tick", this.tickerWrapperURLHovers); //remove Tick event (results on Mouseover)
    if (this.gameVisible === 1){ //when we are in the old game we use the button as it was
      toggleControlPaneButtons.bind(this)(true); //make Buttons visible and resultbox invisible
      this.controlPaneView.continueButton.visible = false; //continue button invisible
      this.resultBoxView.legendCont.visible = false; //yellow legend in resultbox invisible

      seeMail = false; //after clicking on continue, no email is shown in preview

      //make hacker img, prompt for hovering and points invisible
      this.resultBoxView.hackerImg.visible = false;
      this.resultBoxView.firstMouseover.visible = false;
      this.controlPaneView.points.textbox.visible = false;

      this.controlPaneView.correctIconContainer.removeAllChildren(); //remove all icons that indicated whether the print hit a suspicious target or not (correct/incorrect-icons)

      this.stage.removeChild(this.reasonCont); // remove marked text and reason text in resultbox

      // Get the index of the mail in the array of mails from emailModel
      let realIndex = this.mails.indexOf(this.displayedMails[currentIndex]) 
      this.displayedMails.splice(currentIndex, 1); //remove recently classified E-Mail from displayedMails Array

      this.addedNew = false;

      
      if (this.displayedMails.length <= 3) { //if less than 3 mails are currently displayed, then a new mail is displayed
        this.progData.passedMailsSet.add(realIndex);
        this.progData.save()
        addNewMail.bind(this)(); //new Mail on top, notification, and counter up in folder
      }

      adjustMsgList.bind(this)(currentIndex); //shift messageboxes and add new on click listeners

      if (this.displayedMails.length === 0) { //when all Emails are classified. Maybe show a finish View
        allClassified.bind(this)();
      }
    }
    //**************************************** continue button code of the whole email creation game starts here ****************************************
    else if (this.gameVisible === 2){
      let path = this.folderPaneView.levelView.newEmailPaneView;

      if (this.progData.currently == "creation"){
        if (this.progData.currentPhishingEmail.infos.matching == true && (this.progData.creationTimerEnabled == false || this.progData.timeUp == false)){ //matching game will be played
          xapiButton({en:"continue button", de:"Weiter-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
          xapiStageEnd(this.progData.currently, this.progData.userId, this.progData.currentPhishingEmail.infos.name); //xapi call
          xapiStageStart("matching", this.progData.userId, this.progData.currentPhishingEmail.infos.name); //xapi call
          matchingGameModule.initMatchingGame.bind(this)();
        }
        else if (this.progData.currentPhishingEmail.infos.matching == false && (this.progData.timeUp == false || this.progData.creationTimerEnabled == false)){ //display results screen if matching isn't supposed to be played
          xapiButton({en:"continue button", de:"Weiter-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
          xapiStageEnd(this.progData.currently, this.progData.userId, this.progData.currentPhishingEmail.infos.name); //xapi call
          xapiStageStart("review", this.progData.userId, this.progData.currentPhishingEmail.infos.name); //xapi call
          reviewModule.initReview.bind(this)();
        }
        else if (this.progData.timeUp == true){ //matching game won't be played because the players were too slow
          xapiButton({en:"continue button", de:"Weiter-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
          xapiStageCancel(this.progData.currently, this.progData.userId, this.progData.currentPhishingEmail.infos.name); //xapi call
          xapiLevelCancel(this.progData.currentPhishingEmail.infos.name, this.progData.userId); //xapi call
          this.initLevelSelection();
        }
      }
      else if (this.progData.currently == "matching"){ //display results screen after matching game was finished
        xapiButton({en:"continue button", de:"Weiter-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
        if ((this.progData.timeUp == false || this.progData.matchingTimerEnabled == false) && (this.progData.currentMatchingScore == this.progData.matchingPairs.length || this.progData.currentMatchingScore == 4)){ //level passed
          xapiStageEnd(this.progData.currently, this.progData.userId, this.progData.currentPhishingEmail.infos.name); //xapi call
        }
        if ((this.progData.timeUp == false || this.progData.matchingTimerEnabled == false) && (this.progData.currentMatchingMistakes > 2)){ //level not passed
          xapiStageFailure(this.progData.currently, this.progData.userId, this.progData.currentPhishingEmail.infos.name); //xapi call
        }
        else if (this.progData.timeUp == true ){ //timer up
          xapiStageFailure(this.progData.currently, this.progData.userId, this.progData.currentPhishingEmail.infos.name); //xapi call
        }
        xapiStageStart("review", this.progData.userId, this.progData.currentPhishingEmail.infos.name); //xapi call
        reviewModule.initReview.bind(this)();
      }
      else if (this.progData.currently == "review"){ //get back to level selection screen || (this.currently == "matching" && this.currentMatchingMistakes > this.maxMatchingMistakes)){
        xapiButton({en:"continue button", de:"Weiter-Button"},this.progData.userId,this.progData.currently,this.progData.currentPhishingEmail.infos.name); //xapi call
        xapiStageEnd(this.progData.currently, this.progData.userId, this.progData.currentPhishingEmail.infos.name); //xapi call
        if (this.progData.score > 0 && this.progData.activeLevelPassed == true){ //if we have a score, save it
          xapiLevelEnd(this.progData.currentPhishingEmail.infos.name, this.progData.userId, this.progData.score); //xapi call
        }
        else if (this.progData.activeLevelPassed == true){ //no score but level got passed
          xapiLevelEnd(this.progData.currentPhishingEmail.infos.name, this.progData.userId); //xapi call
        }
        else if (this.progData.activeLevelPassed == false){ //level not passed
          xapiLevelFailure(this.progData.currentPhishingEmail.infos.name, this.progData.userId); //xapi call
        }

        this.initLevelSelection();
      }
      else if (this.progData.currently == "tutorial"){ //show new tutorial dialog messages until there are none left //tutorial is over so show level selection screen again
        this.initLevelSelection();
        xapiButton({en:"continue button", de:"Weiter-Button"},this.progData.userId,this.progData.currently,this.progData.activeTutorial.name); //xapi call
        xapiStageEnd(this.progData.currently, this.progData.userId, this.progData.activeTutorial.name); //xapi call
      }
    }
  }.bind(this));

  //Info-Button
  this.controlPaneView.infoBox.info.on('click', function (evt) {
    if (this.controlPaneView.infoBox.boxes.visible == true) { //if three boxes are visible, make them invisible
      this.controlPaneView.infoBox.boxes.visible = false;
    }
    else { //if boxes are invisible, make them visible
      this.controlPaneView.infoBox.boxes.visible = true;
      this.stage.setChildIndex(this.controlPaneView.infoBox.boxes, this.stage.numChildren - 1);
    }
  }.bind(this));

  //Inactive Continue-Button
  this.controlPaneView.inactiveContinueButton.on('click', function (evt) { //Clicking on the inactive button causes the 'prompt to mouseover' to change color
    createjs.Tween.get(this.resultBoxView.firstMouseoverBgColor, { loop: 5 })
      .to({ style: 'white' }, 200)
      .to({ style: "#fe97d6" }, 200)
      .wait(300);
  }.bind(this));

  //add event listeners to buttons (see buttonOnClicks.js)
  addEventListenerNewButton.bind(this)();
  addEventListenerSoundOnButton.bind(this)();
  addEventListenerSoundOffButton.bind(this)();
  addEventListenerClickOnButton.bind(this)();
  addEventListenerClickOffButton.bind(this)();
  addEventListenerSettingsCloseButton.bind(this)();
  addEventListenerEasyButton.bind(this)();
  addEventListenerMediumButton.bind(this)();
  addEventListenerHardButton.bind(this)();
  addEventListenerSettingsButton.bind(this)();
  addEventListenerFinishCloseButton.bind(this)();
  //addEventListenerInboxButton.bind(this)();
  addEventListenerMenuButton.bind(this)();

  //add event listener for tutorial
  addEventListenerTutorial.bind(this)();

}
