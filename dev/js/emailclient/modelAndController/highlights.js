// Highlights

/**
 * This module comprises all functions relevant to the game's highlights.
 * It is build based on the revealing module pattern.
 * @function
 */
var highlightsModule = function (){

  /**
   * This function displays phishing features on mouseover over marked passages of the created email
   * @function
   */
  function phishingFeaturesOnMouseover() {
    for (let i = 0; i < this.progData.highlights.length; i++){
      let point = this.progData.highlights[i].shape.globalToLocal(MTLG.getStage().mouseX, MTLG.getStage().mouseY); //mouse position
      if (this.progData.highlights[i].shape.hitTest(point.x, point.y) && this.progData.activeLevelPassed == true) { //if mouse hovers over suspicious passage
        this.progData.highlights[i].textbox.textbox.visible = true; //display reason over suspicious passage
        this.progData.highlights[i].textbox.txt.x = point.x-this.progData.highlights[i].textbox.txt._rectangle.width/2-this.standardPadding;
        this.progData.highlights[i].textbox.bg.x = point.x-this.progData.highlights[i].textbox.bg.width/2-this.standardPadding;
        this.progData.highlights[i].textbox.txt.y = point.y-this.progData.highlights[i].textbox.txt._rectangle.height-20-this.standardPadding;
        this.progData.highlights[i].textbox.bg.y = point.y-this.progData.highlights[i].textbox.bg.height-20; //-20 so we can hover/activate other stuff with the mouse like embedded links
        this.stage.setChildIndex(this.progData.highlights[i].textbox.textbox, this.stage.numChildren - 1);
      }
      else {
        this.progData.highlights[i].textbox.textbox.visible = false;
      }
    }
  }

  /**
   * This function displays participant hovers (if the content of a participant field is to long) on mouseover over participants fields
   * @function
   */
  function participantHoverOnMouseover() {
    let path = this.folderPaneView.levelView.newEmailPaneView.newEmailParticipantsView;
    for (let i = 0; i < this.progData.participantHovers.length; i++){
      let point = this.progData.participantHovers[i].shape.bg.globalToLocal(MTLG.getStage().mouseX, MTLG.getStage().mouseY); //mouse position
      //let point = this.highlights[i][0].globalToLocal(MTLG.getStage().mouseX, MTLG.getStage().mouseY); //mouse position
      if (this.progData.participantHovers[i].shape.bg.hitTest(point.x, point.y)) { //if mouse hovers over specific participant
        this.progData.participantHovers[i].textbox.textbox.visible = true; //display full participant
        this.progData.participantHovers[i].textbox.txt.x = (point.x+this.progData.participantHovers[i].shape.bg.x)-this.progData.participantHovers[i].textbox.txt._rectangle.width/2-this.standardPadding;
        this.progData.participantHovers[i].textbox.bg.x = (point.x+this.progData.participantHovers[i].shape.bg.x)-this.progData.participantHovers[i].textbox.bg.width/2-this.standardPadding;
        this.progData.participantHovers[i].textbox.txt.y = (point.y+this.progData.participantHovers[i].shape.bg.y)+20+this.standardPadding;
        this.progData.participantHovers[i].textbox.bg.y = (point.y+this.progData.participantHovers[i].shape.bg.y)+20; //-20 so we don't cover up the text
        this.stage.setChildIndex(this.progData.participantHovers[i].textbox.textbox, this.stage.numChildren - 1);
      }
      else {
        this.progData.participantHovers[i].textbox.textbox.visible = false;
      }
    }
  }

  /**
   * This function displays embedded URLs in the URL bar at the bottom of the email content view box
   * @function
   */
  function urlHoverOnMouseover() {
    this.folderPaneView.levelView.newEmailPaneView.newEmailContentView.newEmailURLTextbox.textbox.visible = false;
    for (let i = 0; i < this.progData.URLHovers.length; i++){
      let point = this.progData.URLHovers[i].shape.bg.globalToLocal(MTLG.getStage().mouseX, MTLG.getStage().mouseY); //mouse position
      if (this.progData.URLHovers[i].shape.bg.hitTest(point.x, point.y)) { //if mouse hovers over suspicious passage
        this.folderPaneView.levelView.newEmailPaneView.newEmailContentView.newEmailURLTextbox.txt.text = this.progData.URLHovers[i].text;
        this.folderPaneView.levelView.newEmailPaneView.newEmailContentView.newEmailURLTextbox.textbox.visible = true;
      }
    }
  }

  /**
   * This function creates new mouseover textboxes for (phishing) highlights
   * @function
   * @param {number} x The x position where the highlight box should be placed (relation to the mouse is added in phishingFeaturesOnMouseover())
   * @param {number} y The y position where the highlight box should be placed (relation to the mouse is added in phishingFeaturesOnMouseover())
   * @param {string} text The text version of the present phishing feature
   * @param {string} color The background color of the textbox
   */
  function createHighlightTextbox(x,y,text,color=this.white){
    let path = this.folderPaneView.levelView.newEmailPaneView;
    this.box = new Textbox({
      x: x+5,
      y: y-37,
      text: text,
      font: "30px Arial",
      colorBg: color,
      stroke: "black",
      pad: this.standardPadding,
    });
    this.box.textbox.visible = false;
    return this.box;
  }

  /**
   * This function looks for the highlights of text passages that contain phishing (in the game review) and then call createHighlightShape() and createHighlightTextbox() to create the highlights
   * @function
   */
  function managePhishingHighlights(){
    let path = this.folderPaneView.levelView.newEmailPaneView;
    let alpha = 0.35;

    //delete old highlights
    if (this.progData.highlights.length > 0){
      for (let i = 0; i < this.progData.highlights.length; i++){
        this.stage.removeChild(this.progData.highlights[i].shape);
        this.stage.removeChild(this.progData.highlights[i].textbox);
      }
    }
    this.progData.highlights = [];

    //check if email address contains phishing
    if (this.progData.createdEmail[0].type != "" && this.progData.activeLevelPassed == true){
      let text = path.newEmailParticipantsView.newEmailParticipantsContentContainer.children[1].children[1]; //createjs text from the textbox
      this.progData.highlights.push(new highlight(createHighlightShape.bind(this)(text.x,text.y,text.getMeasuredWidth(),text.getMeasuredLineHeight(),alpha,this.yellow),createHighlightTextbox.bind(this)(text.x,text.y,l(this.progData.createdEmail[0].type),this.white),""));
    }

    //check if email subject contains phishing
    if (this.progData.createdEmail[1].type != "" && this.progData.activeLevelPassed == true){
      let text = path.newEmailParticipantsView.newEmailParticipantsContentContainer.children[2].children[1];
      this.progData.highlights.push(new highlight(createHighlightShape.bind(this)(text.x,text.y,text.getMeasuredWidth(),text.getMeasuredLineHeight(),alpha,this.yellow),createHighlightTextbox.bind(this)(text.x,text.y,l(this.progData.createdEmail[1].type),this.white),""));
    }

    //check the mail content for phishing
    outer: for (let i = 2; i < this.progData.createdEmail.length; i++){
      for (let j = 0; j < path.newEmailContentView.emailTextboxes.length; j++){
        if (this.progData.createdEmail[i].type != "" && this.progData.createdEmail[i].text == path.newEmailContentView.emailTextboxes[j][1] && this.progData.activeLevelPassed == true){ //text passage contains phishing
          let text = path.newEmailContentView.emailTextboxes[j][0].textbox.children[1];
          this.progData.highlights.push(new highlight(createHighlightShape.bind(this)(text.x,text.y-3,text.getMeasuredWidth(),text.getMeasuredLineHeight()+7,alpha,this.yellow),createHighlightTextbox.bind(this)(text.x,text.y,l(this.progData.createdEmail[i].type),this.white),"")); //give this line a mouseover with the corresponding phishing feature
        }
      }
    }

    //add highlight shape and hover textbox to the stage
    for (let i = 0; i < this.progData.highlights.length; i++){
      this.stage.addChild(this.progData.highlights[i].shape);
      this.stage.addChild(this.progData.highlights[i].textbox.textbox);
      this.stage.setChildIndex(this.progData.highlights[i].shape, this.stage.numChildren - 1);
    }
    this.tickerWrapperEmailHighlights = createjs.Ticker.on("tick", phishingFeaturesOnMouseover.bind(this)); //dislay result box on mouseover over target
  }

  /**
   * This function creates a highlight shape for the managePhishingHighlights function for the game review
   * @function
   * @param {number} x The x position where the highlight shape starts placed (based on the underlying textbox that contains the phishing feature)
   * @param {number} y The x position where the highlight shape starts placed (based on the underlying textbox that contains the phishing feature)
   * @param {number} width The width of the highlight shape
   * @param {number} height The height of the highlight shape
   * @param {number} alpha The alpha/transparency value of the highlight shape
   * @param {number} color The color of the highlight shape
   */
  function createHighlightShape(x,y,width,height,alpha,color="#ffae3b"){
    this.shape = new createjs.Shape();
    this.shape.graphics.beginFill(color).drawRect(x,y,width,height);
    this.shape.alpha = alpha;
    return this.shape;
  }

  /**
   * This class contains the necessary shape and textbox for a review highlight
   * @class
   * @param {shape} shape The createJS shape we need to localize a mouseover event
   * @param {textbox} textbox The createJS textbox that contains the text
   * @param {string} text The embedded URL that is used by URL hovers
   */
  function highlight(shape,textbox=null,text=null){
    this.shape = shape;
    this.textbox = textbox;
    this.text = text;
  }

  return{
    participantHoverOnMouseover: participantHoverOnMouseover,
    urlHoverOnMouseover: urlHoverOnMouseover,
    managePhishingHighlights: managePhishingHighlights,
    highlight: highlight
  };

}();
