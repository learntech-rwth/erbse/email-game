//creation game

/**
 * This module comprises all functions relevant to the email creation game.
 * It is build based on the revealing module pattern.
 * @function
 */
var creationGameModule = function (){

  /**
   * This function checks if the email creation option hit the email content field, if yes call manageEmailCreation and in both cases reset the option's position
   * @function
   * @param {event} e The mouse up event that was triggered. Used to get the event's target, so the option can be reset
   * @param {number} buttonId Passed to manageEmailCreation() so the correct text passage is added to the email
   */
  function dragNdropEmailCreation(e,buttonId){
    let path = this.folderPaneView.levelView.newEmailPaneView;
    let p = e.currentTarget.localToLocal(e.localX, e.localY, this.stage); //get current pointer location
    let btnWidth = path.buttonWidth;
    let btnHeight = path.buttonHeight;

    //check if we hit the email content area with an option
    //if (p.x > path.newEmailParticipantsView.x - btnWidth/2 && p.x < path.newEmailParticipantsView.x + path.newEmailParticipantsView.width + btnWidth/2 && p.y > path.newEmailParticipantsView.y - btnHeight/2 && p.y < path.newEmailContentView.y + path.newEmailContentView.height + btnHeight/2){
    if (p.x > path.newEmailParticipantsView.x && p.x < path.newEmailParticipantsView.x + path.newEmailParticipantsView.width && p.y > path.newEmailParticipantsView.y && p.y < path.newEmailContentView.y + path.newEmailContentView.height){
      manageEmailCreation.bind(this)(buttonId);
      xapiDraggable({en:"option " + buttonId.toString() + " button", de:"Option " + buttonId.toString() + " Button"},this.progData.userId); //xapi call
    }

    resetCreationOptions.bind(this)(e.currentTarget);
  }

  /**
   * This function resets an email creation button's position
   * @function
   * @param {button} button The button / choice option whose position is to be reset
   */
  function resetCreationOptions(button){
    let path = this.folderPaneView.levelView.newEmailPaneView;
    let btnWidth = path.buttonWidth;
    let btnHeight = path.buttonHeight;
    let time = 350;

    //find the correct starting position for the button
    for (let i = 0; i < path.choiceOptions.length; i++){
      if (button.id == path.CONSTchoiceOptionsPositions[i][2]){
        if (this.progData.lastChoice == false){ //last choice wasn't made yet, rest everything normal
          createjs.Tween.get(button.children[0])
            .to({alpha:0},time) //fade out option
            .to({x:path.CONSTchoiceOptionsPositions[i][0], y: path.CONSTchoiceOptionsPositions[i][1]})
            .to({alpha:1},time);
          createjs.Tween.get(button)
            .wait(time+10)
            .call(this.realignButtonText,[button]);//realign the text
          break;
        }
        else{ //last choice was made, don't fade in the button completely
          createjs.Tween.get(button.children[0])
            .to({alpha:0},time) //fade out option
            .to({x:path.CONSTchoiceOptionsPositions[i][0], y: path.CONSTchoiceOptionsPositions[i][1]})
            .to({alpha:0.5},time);
          createjs.Tween.get(button)
            .wait(time+10)
            .call(this.realignButtonText,[button]);//realign the text
          break;
        }
      }
    }
  }

  /**
   * This function sets the visibility of the choice buttons
   * @function
   * @param {bool} visibility True if buttons should be visible or false if they should be invisible
   */
  function setChoiceButtonVisibility(visibility){
    let path = this.folderPaneView.levelView.newEmailPaneView;
    for (let i = 0; i < path.choiceOptions.length; i++){
      path.choiceOptions[i].visible = visibility;
      this.stage.setChildIndex(path.choiceOptions[i], this.stage.numChildren - 1);
    }
  }

  /**
   * This function manages a multi-line participant textbox. Only the first line is shown, the whole text only shown in a hover textbox
   * @function
   * @param {textbox} box The textbox that contains a string that is longer than the width of the textbox
   * @param {string} color The background color of the box
   */
  function manageTooLongParticipantTextboxes(box,color=this.white){
    let orgText = box.txt.text;
    if (box.textbox.children[1].getMetrics().lines.length > 1){
      let orgFirstLine = box.textbox.children[1].getMetrics().lines[0];
      let i = 1;
      box.textbox.children[1].text = box.textbox.children[1].getMetrics().lines[0] + " ...";
      while (box.textbox.children[1].getMetrics().lines.length > 1){ //check if the addition of "..." results in a second line, if it does slice away the last words until everything fits in the box
        let cutText = orgFirstLine.split(" ");
        box.textbox.children[1].text = cutText.slice(0,cutText.length-i).join(" ") + " ..."; //cutText.slice(0,cutText.length).join(" ") is the original first line of the string
        i++
      }
      //change the height to the shown text
      box.fixHeight(5);
      this.progData.participantHovers.push(new highlightsModule.highlight(box,new Textbox({
        x: box.txt.x+5,
        y: box.txt.y+37, //make the box appear a little bit under the text, but we actually let it follow the mouse position later on in participantHoverOnMouseover()
        text: orgText,
        font: "30px Arial",
        colorBg: color,
        stroke: "black",
        pad: this.standardPadding,
      })));
      this.progData.participantHovers[this.progData.participantHovers.length-1].textbox.textbox.visible = false;
      this.stage.addChild(this.progData.participantHovers[this.progData.participantHovers.length-1].textbox.textbox);
    }
  }

  /**
   * This function initializes the email creation. It resets all textboxes, indexes, buttons in the new email pane, etc. It also inserts the first choices into the choice buttons and sets the level timer (if activated)
   * @function
   */
  function initEmailCreation(){
    var path = this.folderPaneView.levelView.newEmailPaneView;
    this.progData.activePartIndex = 0;
    this.progData.amountOfUsedPhishingFeature = 0;
    this.progData.lastChoice = false;
    this.progData.featureOfCurrentLevelUsed = false;
    this.progData.createdEmail = [];
    this.progData.currently = "creation";
    this.folderPaneView.buttonTextView.text = l("back");
    this.dragButtonListener(path.choiceOptions,false);
    setChoiceButtonVisibility.bind(this)(true);

    //check for the PE of the current level
    if (findCurrentPhishingEmail.bind(this)() == false){
      console.error("There is no phishing email with this id");
    }

    //manage visibility of creation tutorial overlay
    this.stage.setChildIndex(path.creationTutorialContainer, this.stage.numChildren - 1);
    if (this.progData.currentPhishingEmail.infos.id == this.progData.firstCreationLevel){
      path.creationTutorialContainer.visible = true;
      this.stage.setChildIndex(path.creationTutorialContainer, this.stage.numChildren - 1);
    }
    else{
      path.creationTutorialContainer.visible = false;
    }

    //manage text of email length textbox
    this.folderPaneView.emailLengthNumber.txt.text = "0/"+String(Object.keys(this.progData.currentPhishingEmail[MTLG.lang.getLanguage()]).length-1); //-1 since we don't let the players chose the recipient

    //use correct dialog for the current level
    this.manageDialogBox(l('dialog-creation-instructions1') + " " + l('dialog-creation-instructions2') + this.progData.currentPhishingEmail.infos.topic[MTLG.lang.getLanguage()] + l('dialog-creation-instructions3') + "\n" + l('dialog-integrate-start') + l(this.progData.currentPhishingEmail.infos.dialog) + l('dialog-integrate-end'));

    //reset current choosing option highlights
    path.highlightCurrentChoosingOption(this.progData.activePartIndex,this.progData.currentPhishingEmail[MTLG.lang.getLanguage()]);

    //reset participant fields
    let participantFields = path.newEmailParticipantsView.newEmailParticipantsContentContainer;
    participantFields.children[1].children[1].text = " "; //Sender textbox reset
    participantFields.children[2].children[1].text = " "; //Subject textbox reset

    //reset hover text of too long participant fields
    if (this.progData.participantHovers.length > 0){
      for (let i = 0; i < this.progData.participantHovers.length ; i++){
        this.stage.removeChild(this.progData.participantHovers[i].textbox.textbox);
      }
      this.progData.participantHovers.length = 0;
    }

    //reset saved URLs
    this.progData.URLHovers = [];

    //reset attachment icon
    path.newEmailContentView.attachmentImg.visible = false;

    //set receiver textbox to new receiver mail
    participantFields.children[0].children[1].text = this.progData.currentPhishingEmail[MTLG.lang.getLanguage()].to;

    //if the sender box has multiple lines then add "..." and a hover box
    manageTooLongParticipantTextboxes.bind(this)(path.newEmailParticipantsView.contentReceiverTextbox);
    this.tickerWrapperParticipantHovers = createjs.Ticker.on("tick", highlightsModule.participantHoverOnMouseover.bind(this)); //dislay full participant on mouseover over target
    this.tickerWrapperURLHovers = createjs.Ticker.on("tick", highlightsModule.urlHoverOnMouseover.bind(this)); //dislay full participant on mouseover over target

    //reset buttons
    var buttons = path.choiceOptions;
    let options = Object.values(this.progData.currentPhishingEmail[MTLG.lang.getLanguage()]["from"]); //array of options for the next choice
    buttons.forEach((item, i) => {
      let rnd = Math.floor(Math.random()*(buttons.length-i));
      item.children[1].text = options[rnd].text; //get an option based on the random number
      item.children[1].realignButtonText();
      item.optionInfo = options[rnd]; //safe the remaining information of the option such as phishing and the type of phishing
      options.splice(rnd,1); //remove the option object, that we just used, from the list of options
    });

    this.progData.activePartIndex = 1;
    this.progData.activeEmailPart = activeChoice.bind(this)(); //tells us which part of the creation we are at, e.g. "from" or "choice3" (for reference see phishingEmailModel.js), needed to get the right key/value pairs from the model

    //display which part of the email the players has to choose next
    path.highlightCurrentChoosingOption(this.progData.activePartIndex,this.progData.currentPhishingEmail[MTLG.lang.getLanguage()]);

    //reset email text
    for(let i = 0; i < path.newEmailContentView.emailTextboxes.length; i++){
      path.newEmailContentView.newEmailContentContainer.removeChild(path.newEmailContentView.emailTextboxes[i][0].textbox);
    }
    path.newEmailContentView.emailTextboxes.length = 0;

    //display message symbol
    path.newEmailContentView.newLogoView.newEmailLogoContainer.visible = true;

    //reactivate the clickability of the choice buttons and make them opaque
    for (let i = 0; i < buttons.length; i++){
      buttons[i].mouseEnabled = true;
      buttons[i].children[0].alpha = 1;
    }

    //reset time and difficulty settings
    this.progData.timeLeft = 0;
    this.progData.difficulty = this.progData.newDifficulty;
    this.progData.creationGameTimePerOption = this.progData.difficultyOptions[this.progData.difficulty];

    //add a timer for the level
    if (this.progData.creationTimerEnabled == true){
      //reset time for scoring and difficulty value (in case the difficulty has changed)
      this.progData.timer.set(this.progData.creationGameTimePerOption*(Object.keys(this.progData.currentPhishingEmail[MTLG.lang.getLanguage()]).length-1),() => { //*(Object.keys(this.currentPhishingEmail).length-2)
        //all of this will only trigger if the time runs out
        if (this.progData.difficulty != "easy" && this.progData.creationTimerEnabled == true){
          this.manageDialogBox(l('dialog-too-slow2')); //tell the players that they can still lower the difficulty to get more time
        }
        else{
          this.manageDialogBox(l('dialog-too-slow1'));
        }
        xapiStageFailure(this.progData.currently, this.progData.userId, this.progData.currentPhishingEmail.infos.name); //xapi call
        xapiLevelFailure(this.progData.currentPhishingEmail.infos.name, this.progData.userId); //xapi call
        this.progData.lastChoice = true;
        this.progData.timeUp = true;
        this.controlPaneView.continueButton.visible = true; //make continue Button visible so the players can return to the selection screen
        for (let i = 0; i < this.folderPaneView.levelView.newEmailPaneView.choiceOptions.length; i++){ //make buttons transparent so the players know that they can't choose anything anymore
          this.folderPaneView.levelView.newEmailPaneView.choiceOptions[i].mouseEnabled = false;
          createjs.Tween.get(this.folderPaneView.levelView.newEmailPaneView.choiceOptions[i].children[0])
            .to({alpha:0.5},350) //fade out option
        }
      });
      this.manageTimerDisplay(this.progData.creationGameTimePerOption*(Object.keys(this.progData.currentPhishingEmail[MTLG.lang.getLanguage()]).length-1),true); //start display timer
      this.progData.timer.start();
      this.progData.timeUp = null; //reset time up
    }
  }

  /**
   * This function finds the current phishing email json object from the phishingEmailModel. Gets called during initEmailCreation()
   * @function
   */
  function findCurrentPhishingEmail(){
    for (let i = 0; i < phishingLevels.length; i++){
      if (phishingLevels[i].creation != undefined && this.progData.activeLevel == phishingLevels[i].creation.infos.id){
        this.progData.currentPhishingEmail = phishingLevels[i].creation;
        if (MTLG.getOptions().debug == true){
          console.log("Current phishing email: ",this.progData.currentPhishingEmail);
        }
        return true;
      }
    }
    return false; //show that there is no email for this level
  }

  /**
   * This function manages which choice is currently active in the email creation game. Gets called during manageEmailCreation()
   * @function
   */
  function activeChoice(){
    switch (this.progData.activePartIndex){
      case 0:
        return "to";
      case 1:
        return "from";
      case 2:
        return "subject";
      default:
        return "choice"+(this.progData.activePartIndex-2).toString();
    }
  }

  /**
   * This function adds a new textbox to the email creation game for the body of the new email. We need single textboxes to easily highlight single phishing features
   * @function
   * @param {string} message The text passage that we want to include in our email. If the text passage contains an embedded link also add a hover text
   */
  function addTextboxWithEmailContent(message){
    let path = this.folderPaneView.levelView.newEmailPaneView.newEmailContentView;
    let emailTextbox = path.emailTextboxes;
    let height = path.y + path.padding;
    let width = path.width - (path.padding*2);
    let trueMessage = message;
    let link = null;
    let color = this.black;

    //split message in different parts if it contains a link (the split is done in preparation for potential future changes)
    let regex = /\<.*\>/g;
    if (regex.test(message)){
      let part1 = message.match(/.*.(?=\<a)/g); //anything until "<a" is found
      link = message.match(/(?<=\')..*.(?=\')/g); //the http link between the '' characters
      let part2 = message.match(/(?<=\>.)..*.(?=.\<\/a)/g); //the link text
      let part3 = message.match(/(?<=\/a\>)..*/g); //the rest of the string
      trueMessage = [part1,part2,part3].join('');
      color = "blue"; //color the link blue
    }

    //test how many lines the text would actually have
    let test = new Textbox({
          text: trueMessage,
          fixwidth: width,
          font: "30px Arial",
          lineHeight: path.glyphHeight*path.lineMultiplicator,
        });

    //get lines
    let text = test.textbox.children[1].getMetrics().lines;

    //add a textbox for each single line
    for(let i = 0; i < test.txt.lines; i++){
      if(emailTextbox.length > 0){ //get end position of the previous textbox
        height = emailTextbox[emailTextbox.length -1][0].bg.y + path.glyphHeight* Math.ceil(emailTextbox[emailTextbox.length -1][0].txt.lines)*path.lineMultiplicator; //(start + height) of previous box
        if(i == 0){
          height += 10; //add some space between blocks
        }
      }
      testText = new createjs.Text(text[i],"30px Arial");
      emailTextbox.push( [new Textbox({
            x: path.x + path.padding,
            y: height,
            text: text[i],
            fixwidth: testText.getMeasuredWidth(),
            font: "30px Arial",
            colorBg: this.white,
            colorTxt: color,
            lineHeight: path.glyphHeight*path.lineMultiplicator,
          }),message]); //also safe the whole message so the parts can be assigned easily

      if (i == test.txt.lines - 1){ //add an indicator if more lines will follow
        emailTextbox[emailTextbox.length -1].push(false);
      }
      else{
        emailTextbox[emailTextbox.length -1].push(true);
      }

      path.newEmailContentContainer.addChild(emailTextbox[emailTextbox.length -1][0].textbox);
      //add hover text for embedded link
      if (emailTextbox[emailTextbox.length -1][0].textbox.children[1].color == "blue"){
        this.progData.URLHovers.push(new highlightsModule.highlight(emailTextbox[emailTextbox.length -1][0],"",link.join()));
        // console.log(this.progData.URLHovers)
      }
    }
  }

  /**
   * This function manages that text from buttons is inserted into the email creation layout, the created email is stored and new text is put into the buttons
   * @function
   * @param {number} buttonId The ID of the option whose text we use for the email creation. Based on this ID we also save all additional information of the option such as phishing features
   */
  function manageEmailCreation(buttonId){
    let path = this.folderPaneView.levelView.newEmailPaneView;
    let participants = path.newEmailParticipantsView.newEmailParticipantsContentContainer;
    let buttons = path.choiceOptions;
    let buttonText = buttons[buttonId].children[1].text;
    let emailText = path.newEmailContentView.newEmailContentContainer.children[0].children[1];
    this.playSounds("feedback/typing2");

    //stop mouse clicks while the text changes or when there are no more new choices
    for (let i = 0; i < buttons.length; i++){
      buttons[i].mouseEnabled = false;
    }

    if (this.progData.activePartIndex < Object.keys(this.progData.currentPhishingEmail[MTLG.lang.getLanguage()]).length && (this.progData.creationTimerEnabled == false || this.progData.timer.timeUp() == false)){ // -1 because the Id is also in every PE entry in the phishingEmailModel.js
      //create sender name textbox
      if (this.progData.activePartIndex == 1){
        participants.children[1].children[1].text = buttonText;
        manageTooLongParticipantTextboxes.bind(this)(path.newEmailParticipantsView.contentSenderTextbox);
      }

      //create subject textbox
      else if (this.progData.activePartIndex == 2){
        participants.children[2].children[1].text = buttonText;
        manageTooLongParticipantTextboxes.bind(this)(path.newEmailParticipantsView.contentSubjectTextbox);
      }

      //change email text
      else {
        path.newEmailContentView.newLogoView.newEmailLogoContainer.visible = false; //make logo invisible
        addTextboxWithEmailContent.bind(this)(buttonText); //write email text
      }

      //save the choices/the created phishing email for the matching game
      this.progData.createdEmail.push(buttons[buttonId].optionInfo);
      for (let j = 0; j < Object.keys(this.progData.currentPhishingEmail.infos.priorities).length; j++){ //look if the added passage contained one of the level specific phishing features
        if (buttons[buttonId].optionInfo.type == this.progData.currentPhishingEmail.infos.priorities[j]){ //important features
          this.progData.featureOfCurrentLevelUsed = true; //needed to check whether the level was passed in the review
          break;
        }
      }

      //indicate how many parts of the email were already chosen
      this.folderPaneView.emailLengthNumber.txt.text = String(this.progData.activePartIndex) + "/" + String(Object.keys(this.progData.currentPhishingEmail[MTLG.lang.getLanguage()]).length-1); //-1 since we don't let the players chose the recipient

      //display the attachment icon if an attachment has been selected
      if (this.progData.createdEmail[this.progData.createdEmail.length-1].type == "pf-malicious-attachment"){
        path.newEmailContentView.attachmentImg.visible = true;
      }

      //save how many phishing features were used in this email
      if (this.progData.createdEmail[this.progData.createdEmail.length-1].phishing == true){
        this.progData.amountOfUsedPhishingFeature++;
      }



      this.progData.activePartIndex++; //increase to get the next choice options
      this.progData.activeEmailPart = activeChoice.bind(this)(); //get to know which part of the phishing email we can choose now

      //change button text to next choices (if there is a next choice). currently we just randomly take options from the current level's email, we don't check if certain features are used
      if (this.progData.activePartIndex < Object.keys(this.progData.currentPhishingEmail[MTLG.lang.getLanguage()]).length){ //the second last element is the last one to go through since we don't want to show empty buttons afterwards
        let options = Object.values(this.progData.currentPhishingEmail[MTLG.lang.getLanguage()][this.progData.activeEmailPart]); //array of options for the next choice
        buttons.forEach((item, i) => {
          let rnd = Math.floor(Math.random()*(buttons.length-i));
          //reset the choice options / buttons
          createjs.Tween.get(item.children[1])
            .to({alpha:0},350) //fade out option
            .to({text:options[rnd].text}) //get an option based on the random number //  this.currentPhishingEmail[MTLG.lang.getLanguage()][this.activeEmailPart]
            .call(this.realignButtonText,[item])
            .to({alpha:1},350);
          //reactivate mouse clicks for the next choice
          createjs.Tween.get(item)
            .wait(500)
            .to({mouseEnabled:true},1);
          item.optionInfo = options[rnd]; //safe the remaining information of the option such as phishing or the type of phishing
          options.splice(rnd,1); //remove the option object, that we just used, from the list of options
          });

        if (this.progData.activePartIndex == Object.keys(this.progData.currentPhishingEmail[MTLG.lang.getLanguage()]).length -1){ //add an empty line before the regards
          addTextboxWithEmailContent.bind(this)(" ");
        }

        //make creation tutorial overlay invisible
        if (path.creationTutorialContainer.visible == true){
          createjs.Tween.get(path.creationTutorialContainer)
            .to({alpha:0},350) //fade out the whole shape
            .wait(1000)
            .to({visible:false})
            .to({alpha:1},1) //fade in the whole shape, so we don't have to do this at the level start
        }

        //display which part of the email the players has to choose next
        path.highlightCurrentChoosingOption(this.progData.activePartIndex,this.progData.currentPhishingEmail[MTLG.lang.getLanguage()]);
      }
      //last choice was made
      if (this.progData.activePartIndex == Object.keys(this.progData.currentPhishingEmail[MTLG.lang.getLanguage()]).length){
        this.progData.lastChoice = true;
        this.controlPaneView.continueButton.visible = true; //make continue Button visible if the email is complete

        //check wheter the players completed the level in time or not
        if (this.progData.creationTimerEnabled == true){
          this.progData.timer.stop(); //in theory .pause() should also work here but it doesn't for some reason. maybe the documentation isn't correct
          clearInterval(this.progData.displayClockTimer); //deactivate the display timer
          this.progData.timeUp = false;
          if (this.progData.timer.timeUp() == false){
            this.progData.timeLeft += this.progData.time; //safe the time that was left for the scoring
            this.manageDialogBox(l('dialog-continue'));
          }
        }
        else{
          this.manageDialogBox(l('dialog-continue'));
        }
        for (let i = 0; i < buttons.length; i++){ //make buttons transparent so the players know that they can't choose anything anymore
          createjs.Tween.get(buttons[i].children[0])
            .to({alpha:0.5},350) //fade out option
        }
        //make the indicator that displays which part of the email the players has to choose next disappear
        path.highlightCurrentChoosingOption(this.progData.activePartIndex,this.progData.currentPhishingEmail[MTLG.lang.getLanguage()]);
      }
    }
  }

  return{
    initEmailCreation: initEmailCreation,
    dragNdropEmailCreation: dragNdropEmailCreation,
    manageEmailCreation: manageEmailCreation,
    setChoiceButtonVisibility: setChoiceButtonVisibility
  };

}();
