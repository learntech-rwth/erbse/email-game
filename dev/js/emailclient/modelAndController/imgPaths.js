//Object holding the image paths for better configurability
var imgPaths = {
  'hacker': 'img/hacker.png',
  'speech': 'img/speech-bubble.png',
  'speech2': 'img/speech-bubble2.png',
  'login-mail': 'img/login-mail.png',
  'login-pw': 'img/login-pw.png',
  'letter': 'img/letter.png',
  'attach': 'img/attach-icon.png',
  'encrypted': 'img/lock-icon.png',
  'signed': 'img/ribbon-icon.png',
  'user': 'img/user.png',
  'up': 'img/arrow-up.png',
  'down': 'img/arrow-down.png',
  'close': 'img/close-icon.png',
  'doc': 'img/doc.png',
  'jpg': 'img/jpg.png',
  'pdf': 'img/pdf.png',
  'exe': 'img/exe.png',
  'zip': 'img/zip.png',
  'noType': 'img/noType.png',
  'correct': 'img/correct.png',
  'incorrect': 'img/incorrect.png',
  'stamp': 'img/stamp.png',
  'stamp-print': 'img/stamp-red.png',
  'tut_stamp_de': 'img/Tutorial_stamp.png',
  'tut_stamp_en': 'img/Tutorial_stamp_en.png',
  'phishing': 'img/no.png',
  'legit': 'img/yes.png',
  'help': 'img/maybe.png',
  'continue': 'img/continue-arrow.png',
  'question': 'img/help.png',
  'live': 'img/letter2.png',          // source: raphaelsilva             https://pixabay.com/vectors/letter-e-mail-contact-icon-send-2935365/
  'avatar1': 'img/avatar1.png',       // source: coffeebeanworks          https://pixabay.com/illustrations/graphic-design-diversity-people-3514101/
  'avatar2': 'img/avatar2.png',       // source: coffeebeanworks          https://pixabay.com/illustrations/graphic-design-diversity-people-3514101/
  'sound-on': 'img/sound-on.png',     // source: raphaelsilva             https://pixabay.com/vectors/sound-music-audio-icon-player-2935466/
  'sound-off': 'img/sound-off.png',   // source: raphaelsilva             https://pixabay.com/vectors/sound-cancel-music-audio-icon-2935467/
  'book': 'img/book-icon.png',        //source: OpenIcons                 https://pixabay.com/vectors/manual-docs-documentation-help-98728/
  'controller': 'img/controller.png', // source: IO-Images                https://pixabay.com/vectors/controller-gamepad-video-games-1784571/
  'clock': 'img/clock.png',           // source: raphaelsilva             https://pixabay.com/vectors/clock-time-icon-hour-watch-minute-2935430/
  'matching': 'img/matching-icon.png',
  'letter-border': 'img/letter-border.png',
  'flag-eng': 'img/flag-eng.png',     // source: Clker-Free-Vector-Images https://pixabay.com/users/clker-free-vector-images-3736/
  'flag-ger': 'img/flag-ger.png',     // source: Clker-Free-Vector-Images https://pixabay.com/users/clker-free-vector-images-3736/
  'gear': 'img/gear.png',             // source: Memed_Nurrohmad          https://pixabay.com/illustrations/gear-setting-icon-web-idea-6116836/
  'award': 'img/award.png'            // source: OpenClipart-Verctors     https://pixabay.com/vectors/fame-laurel-wreath-victory-wreath-156160/
};
