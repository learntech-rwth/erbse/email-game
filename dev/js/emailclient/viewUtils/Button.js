
/**
 * Container for a button with text or an image
 * @class
 * @param {number} x The X coordinate where the button starts
 * @param {number} y The Y coordinate where the button starts
 * @param {number} width The width of the button
 * @param {number} height The height of the button
 * @param {string} text The text the button should depict
 * @param {string} color The color of the button's background
 * @param {stage} stage The stage on which the button gets drawn (legacy artefact, NOT in use anymore)
 * @param {string} img Path to an image the button should depict
 * @param {string} font The font the button text should use
 * @param {string} textColor The color of the button's text
 * @param {icon[]} icons List consisting of parts neccessary for a button corner icon e.g. [imgPath['x'],visbility,scalingX,scalingY,alpha] used in levelView
 * @param {number} scaleX The X scaling of the button's image. Ranges from 0 to 1
 * @param {number} scaleY The Y scaling of the button's image. Ranges from 0 to 1
 * @param {number{}} imgPos The x and y position of the buttons image
 * @param {number} textPadX The x padding for the text inside the button (e.g. incase image is on the right)
 */
function Button(x, y, width, height, text, color, stage, img = null, font = "25px Arial", textColor = "black", icons = [], scaleX = 0.15, scaleY = 0.15, imgPos = null, textPadX = 0) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.text = text;
    this.color = color;
    this.stage = stage;
    this.font = font;
    this.img = img;
    this.textColor = textColor;
    this.imgScaleX = scaleX;
    this.imgScaleY = scaleY;
    this.imgPos = imgPos;
    this.textPadX = textPadX;

    var buttonContainer = new createjs.Container();
    var paddingText = 15;

    //BG
    var buttonBg = new createjs.Shape();
    this.bgColor = buttonBg.graphics.beginFill(this.color).command; //retain a reference to beginFill command in order to dynamically update the color later.
    buttonBg.graphics.setStrokeStyle(2, 'round').beginStroke('black');
    buttonBg.graphics.drawRoundRect(0, 0, this.width, this.height, 10);
    buttonBg.x = this.x;
    buttonBg.y = this.y;

    buttonContainer.addChild(buttonBg);

    //get glyph size for correct vertical text alignment of the label
    var textSizeTextbox = new createjs.Text("I", this.font, this.textColor);
    this.glyphHeight = textSizeTextbox.getBounds().height;

    //Text
    var buttonTxt = new createjs.Text(l(this.text), this.font, this.textColor);
    buttonTxt.textAlign = 'center';
    buttonTxt.textBaseline = 'hanging';
    buttonTxt.lineWidth = this.width-2*paddingText; //-2*padding because we need space from each side or the box will look stuffed
    buttonTxt.lineHeight = this.glyphHeight*1.25;

    /*
    let the text start from half the button height.
    shift the text upwards by half its box size.
    note that this will push the text too high since the box size includes the space between the lines
    and the last line has this space (under it) as well!
    therefore, shift the text down by half the size of the space.
    */
    buttonTxt.realignButtonText = function (){
        buttonTxt.x = buttonBg.x + this.width / 2 + this.textPadX;
        buttonTxt.y = buttonBg.y + this.height / 2 - buttonTxt.getBounds().height/2 + (buttonTxt.lineHeight - this.glyphHeight)/2 + 2; //magic 2
    }.bind(this);

    if (this.text != ""){
      buttonTxt.realignButtonText();
    }
    buttonContainer.addChild(buttonTxt);

    this.updateLang = function(){
      if(this.text != ""){
        buttonContainer.children[1].text = l(this.text);
        buttonContainer.children[1].realignButtonText();
      }
      
    }
    MTLG.emailClient.subscribers.push(this);


    //Image
    if (img) {
        this.buttonImg = MTLG.assets.getBitmap(this.img);
        this.buttonImg.scaleX = this.imgScaleX;
        this.buttonImg.scaleY = this.imgScaleY;
        if (this.imgPos == null) {
          this.buttonImg.x = buttonBg.x + this.width / 2 - this.buttonImg.image.width*this.buttonImg.scaleX/2;
          this.buttonImg.y = buttonBg.y + this.height / 2 - this.buttonImg.image.height*this.buttonImg.scaleY/2;
        } else {
          if (this.imgPos.hasOwnProperty("x")) {
            this.buttonImg.x = buttonBg.x + this.imgPos["x"];
          } else {
            this.buttonImg.x = buttonBg.x + this.width / 2 - this.buttonImg.image.width*this.buttonImg.scaleX/2;
          }
          
          if (this.imgPos.hasOwnProperty("y")) {
            this.buttonImg.y = buttonBg.y + this.imgPos["y"];
          } else {
            this.buttonImg.y = buttonBg.y + this.height / 2 - this.buttonImg.image.height*this.buttonImg.scaleY/2;
          }
        }
        buttonContainer.addChild(this.buttonImg);
    }

    buttonContainer.moveImg = function() {
      if (this.imgPos == null) {
        this.buttonImg.x = buttonBg.x + this.width / 2 - this.buttonImg.image.width*this.buttonImg.scaleX/2;
        this.buttonImg.y = buttonBg.y + this.height / 2 - this.buttonImg.image.height*this.buttonImg.scaleY/2;
      } else {
        if (this.imgPos.hasOwnProperty("x")) {
          this.buttonImg.x = buttonBg.x + this.imgPos["x"];
        } else {
          this.buttonImg.x = buttonBg.x + this.width / 2 - this.buttonImg.image.width*this.buttonImg.scaleX/2;
        }
        
        if (this.imgPos.hasOwnProperty("y")) {
          this.buttonImg.y = buttonBg.y + this.imgPos["y"];
        } else {
          this.buttonImg.y = buttonBg.y + this.height / 2 - this.buttonImg.image.height*this.buttonImg.scaleY/2;
        }
      }
    }.bind(this);

    //place icons such as bagdes in the 4 corners of the button (cloclwise: upper left, upper right, lower right, lower left)
    buttonContainer.addIcon = function (img,position=0,visiblity,scaleX,scaleY,alpha = 1){
      let pad = 5; // padding to get the icon a bit away from the border
      let scalingX = scaleX;
      let scalingY = scaleY;
      buttonContainer.addChild(MTLG.assets.getBitmap(img));
      let image = buttonContainer.children[buttonContainer.children.length-1];
      if (scalingX > 1 || scalingX < 0){ // make sure that there are no stupid scalings
        scalingX = 0.2;
      }
      if (scalingY > 1 || scalingY < 0){
        scalingY = 0.2;
      }
      image.scaleX = this.width * scalingX / image.image.width;
      image.scaleY = this.height * scalingY / image.image.height;
      switch (position){
        case 0: //upper left
          image.x = this.x + pad;
          image.y = this.y + pad;
          break;
        case 1: //upper right
          image.x = this.x + this.width - image.image.width * image.scaleX - pad;
          image.y = this.y + pad;
          break;
        case 2: //lower right
          image.x = this.x + this.width - image.image.width * image.scaleX - pad;
          image.y = this.y + this.height - image.image.height * image.scaleY - pad;
          break;
        case 3: //lower left
          image.x = this.x + pad;
          image.y = this.y + this.height - image.image.height * image.scaleY - pad;
          break;
      }
      image.visible = visiblity;
      image.position = position;
      image.alpha = alpha;
    }.bind(this)

    for (let i = 0; i < 4 && i < icons.length; i++){
      if (icons[i].length > 0){
        buttonContainer.addIcon(icons[i][0],i,icons[i][1],icons[i][2],icons[i][3],icons[i][4]);
      }
    }

    //shake a button in alternating directions (indicates a wrong choice in the matching game)
    buttonContainer.animateWobble = function (value=10){
      createjs.Tween.get(this,{override: true})
        .to({x:-value,y:-value},50) //up left
        .to({x:value,y:value},100) //down right
        .to({x:0,y:0},50) //back to the middle
        .to({x:-value,y:value},50) //down left
        .to({x:value,y:-value},100) //up right
        .to({x:0,y:0},50) //back to the middle
        .to({x:-value,y:-value},50) //up left
        .to({x:value,y:value},100) //down right
        .to({x:0,y:0},50) //back to the middle
        .to({x:-value,y:value},50) //down left
        .to({x:value,y:-value},100) //up right
        .to({x:0,y:0},50); //back to the middle
    };

    return buttonContainer;
}
