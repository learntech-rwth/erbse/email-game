
/**
 * View for a textbox
 * @class
 * @param {object} boxOptions The options the textbox is supposed to have. Look at the default options for more detailed infos
 */
function Textbox(boxOptions=null) {
    var defaultOptions = {
        text: "", //text of the textbox
        x: 0, //x coordinate where the box starts
        y: 0, //y coordinate where the box starts
        colorTxt: "black", //color of the box's text
        colorBg: "white", //color of the box's background
        stroke: null, //if truthy the box will have a border
        pad: 0, //padding between the text and the horizontal and vertical sides of the box
        font: "30px Arial", //font the text is supposed to use
        rightaligned: false, //if set to true text will be aligned to the rightside instead of to the left
        fixwidth: 0, //if set forces linebreaks if the text is too big for the box
        fixheight: 0,
        lineHeight: 0, //height of each line (usually set to around 1.25 to have space between the lines)
        centered: false, //if set to true the text is centered instead of left aligned
    };

    var options = Object.assign(defaultOptions, boxOptions);

    this.textbox = new createjs.Container();

    if (options.text) {
        //Text
        this.txt = new createjs.Text(options.text, options.font, options.colorTxt);
        this.txt.x = options.x;
        this.txt.y = options.y;

        this.txt.lines = 1; //number of lines (if fixed width is not used)

        if (options.rightaligned) {
            this.txt.textAlign = "right";
        }
        if (options.centered){
          this.txt.textAlign = "center";
        }


        //BG
        var bounds = this.txt.getBounds();
        var pad = options.pad;
        this.bg = new createjs.Shape();

        if (options.fixwidth) { //has fix Width?
            this.bg.width = options.fixwidth + pad*2;
            this.txt.lineWidth = options.fixwidth;
            this.txt.lines = Math.ceil(this.txt.getMetrics().lines.length); //multi line borders
        }
        else {
            this.bg.width = bounds.width + pad * 2;
        }

        if(options.fixheight){ //has fix Height?
            this.bg.height = options.fixheight;
        }
        else{
              this.bg.height = bounds.height*this.txt.lines + pad * 2;
        }

        if(options.lineHeight){ //has line height?
          this.txt.lineHeight = options.lineHeight;
        }

        this.bg.bgColor = this.bg.graphics.beginFill(options.colorBg).command; //retain a reference to beginFill command in order to dynamically update the color later.
        this.bg.bgSize = this.bg.graphics.beginStroke(options.stroke).drawRect(0, 0, this.bg.width, this.bg.height).command; //retain a reference to drawRect command in order to dynamically update the width and height later.
        this.bg.x = (options.rightaligned)? this.txt.x - this.bg.width - pad : this.txt.x - pad;
        this.bg.y = this.txt.y - pad;

        this.textbox.addChild(this.bg, this.txt);

        this.fixHeight = function (pad = 0) { //adjust Height of box after replacing the text
            var bounds = this.txt.getBounds();
            this.bg.bgSize.h = bounds.height + pad*2;
        }.bind(this);

        this.fixWidth = function (pad = 0) { //adjust Width of box after replacing the text
            var bounds = this.txt.getBounds();
            this.txt.lineWidth = bounds.width;
            this.bg.bgSize.w = bounds.width + pad*2;
        }.bind(this);

        this.textbox.checkMarkX = function(markXR, markXL) { // markXR: right side, prints[i].x + imgW / 3 + 5 + 15, markXL: left side, prints[i].x + imgW / 3 + 5 - 15
            return (markXR >= this.children[0].x) && (markXL <= this.children[0].x + this.children[0].width)
        }.bind(this.textbox);

        this.textbox.checkMarkY = function(markYT, markYB) { // 
            return (markYT >= this.children[0].y) && (markYB <= this.children[0].y + this.children[0].height);
        }.bind(this.textbox);

        this.textbox.checkMarkXY = function(markXR, markXL, markYT, markYB) { // 
            return this.checkMarkX(markXR, markXL) && this.checkMarkY(markYT, markYB);
        }.bind(this.textbox);
    
        this.textbox.setBgColor = function(color){
            this.children[0].bgColor.style = color;
        }.bind(this.textbox);

        this.textbox.getSuspiciousText = function(){
            return this.children[1].text;
        }.bind(this.textbox);

        this.textbox.getReason = function(){
            return this.reason.txt.text;
        }.bind(this.textbox)

        this.textbox.setAlpha = function(){
            this.children[0].alpha = 0.5
        }.bind(this.textbox)
    }

}


/**
 * View for a textbox
 * @class
 * @param {object} boxOptions The options the textbox is supposed to have. Look at the default options for more detailed infos
 */
 function MultiTextbox(markers = []) {
    this.textboxes = markers;

    this.fixHeight = function(pad = 0){
        for (let textbox of textboxes){
            textbox.fixHeight(pad);
        }
    }.bind(this);

    this.fixWidth = function(pad = 0){
        for (let textbox of textboxes){
            textbox.fixWidth(pad);
        }
    }.bind(this);

    this.checkMarkXY = function(markXR, markXL, markYT, markYB) { // markXR: right side, prints[i].x + imgW / 3 + 5 + 15, markXL: left side, prints[i].x + imgW / 3 + 5 - 15
        for (let textbox of this.textboxes){
            if(textbox.checkMarkXY(markXR, markXL, markYT, markYB)){
                return true;
            }
        }
        return false;
    }.bind(this);

    this.setBgColor = function(color){
        for (let textbox of this.textboxes){
            textbox.setBgColor(color);
        }
    }.bind(this);

    this.setAlpha = function(value){
        for (let textbox of this.textboxes){
            textbox.setAlpha(value);
        }
    }.bind(this);

    this.getSuspiciousText = function(){
        let suspiciousText = "";
        for (let textbox of this.textboxes){
            suspiciousText += textbox.getSuspiciousText() + " ";
        }
        return suspiciousText;
    }.bind(this);

    this.getReason = function(){
        return this.textboxes[0].getReason();
    }.bind(this);

    this.globalToLocal = function(x, y){
        let positionArray = [];
        for (let textbox of this.textboxes){
            positionArray.push(textbox.globalToLocal(x,y));
        }
        return positionArray;
    }.bind(this)

    this.showReasonText = function(pointArray){
        let reasonData = {
            "toShow": [],
            "toHide": []
        }
        for(let i = 0; i < pointArray.length; i++){
            if(this.textboxes[i].hitTest(pointArray[i].x, pointArray[i].y)){
                reasonData.toShow.push(this.textboxes[i]);
            }
            else {
                reasonData.toHide.push(this.textboxes[i]);
            }
        }
        return reasonData;
    }.bind(this)
}