

function firstlevel_init() {
  // initialize level 1

  console.log("This players are logged in:");
  for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
    console.log(MTLG.getPlayer(i));
  }

  console.log("This are the available game options:");
  console.log(MTLG.getOptions());

  // esit the game.settings.js
  console.log("This are the available game settings:");
  console.log(MTLG.getSettings());

  drawLevel1();
}

// check wether level 1 is choosen or not
function checkLevel1(gameState) {
  if (gameState && gameState.nextLevel && gameState.nextLevel == "level1") {
    return 1;
  }
  return 0;
}


function drawLevel1() {
  var stageCont = MTLG.getStageContainer();

  console.log("Level 1 started.");

  console.log(MTLG);
  
  let numberOfMails = 10;
  for(let i = 0; i < numberOfMails; i++){
    generatorInterface.addMail();
  }

  MTLG.emailClient.init(); 
  MTLG.emailClient.setEmails(generatorInterface.getMails());
  MTLG.emailClient.assignToStage(stageCont);
}