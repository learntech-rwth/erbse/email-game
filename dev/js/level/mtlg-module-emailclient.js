/*Revealing Module Pattern */

// global variable for shortening language calls
var l = MTLG.lang.getString;

//Revealing Module Pattern
window.MTLG = (function (m) {

    var emailClient = (function () {

        var options = null;
        var emails = []; //Array containing email json objects
        var clientView = null;
        var startScreen = null;
        var clientViewChildren = null;
        this.subscribers = []

        function init(clientOptions = null) {

            var defaultOptions = {
                showStartScreen: true,
                showHeader: true,
            };
            options = Object.assign(defaultOptions, clientOptions);
        }

        function updateLang(){
            for(let subscriber of subscribers){
                if(subscriber != null){
                    subscriber.updateLang();
                }
            }
        }

        function goToMenu(stage) {
            clientViewChildren = [...stage.children];
            stage.removeAllChildren();
            stage.addChild(startScreen.container);
        }

        function goToInterface(stage, game) {
            MTLG.stage = stage;
            for(let child of clientViewChildren) {
                stage.addChild(child);
            }
            if(clientView != null && game == 2){
                clientView.drawMsgList();
            }
            clientViewChildren = [...stage.children]
        }

        function updateClientViews(stage){
            console.log("clientViewChildren.length")
            if(clientViewChildren != null){
                console.log(clientViewChildren.length)
            }
            else{
                console.log("Not initialized1")
            }
            clientViewChildren = [...stage.children];
            if(clientViewChildren != null){
                console.log(clientViewChildren.length)
            }
            else{
                console.log("Not initialized2")
            }
        }

        function isMenuVisible(stage) {
            return stage.contains(startScreen.container);
        }

        function assignToStage(stage) {
            if (options == null) { //if init() is not called before
                init();
            }

            if (options.showStartScreen) {
                if (startScreen === null) {
                    startScreen = new startView(stage);
                }

                startScreen.creationGameButton.on('click', function () {
                    startScreen.login.hide();
                    stage.removeChild(startScreen.container);
                    xapiButton({en:"creation-game button", de:"Creation Game Button"},startScreen.userId); //xapi call
                    xapiGameStart("Mail Creation Game")
                    if (clientView === null || clientViewChildren === null) {
                        if(clientViewChildren != null){
                            for(let child of clientViewChildren){
                                child.visible = false;
                            }
                        }
                        clientView = new interface(stage,startScreen.userId);
                        MTLG.interface = clientView;
                        clientView.setNewEmailGameInterface(true);
                    } 
                    else {
                        goToInterface(stage, 1);
                        clientView.setNewEmailGameInterface(true);
                    }
                }.bind(this));

                startScreen.decideGameButton.on('click', function () {
                    stage.removeChild(startScreen.container);
                    startScreen.login.hide();
                    xapiButton({en:"decide-game button", de:"Decide Game Button"},startScreen.userId); //xapi call
                    xapiGameStart("Mail Matching Game")
                    if (clientView === null || clientViewChildren === null) {
                        if(clientViewChildren != null){
                            for(let child of clientViewChildren){
                                child.visible = false;
                            }
                        }
                        clientViewChildren = null;

                        clientView = new interface(stage,startScreen.userId);
                        MTLG.interface = clientView;
                        clientView.setNewEmailGameInterface(false);
                    } 
                    else {
                        goToInterface(stage, 2);
                        clientView.setNewEmailGameInterface(false);
                    }
                }.bind(this));
            }
            else if (clientView === null || clientViewChildren === null) {
                if(clientViewChildren != null){
                    for(let child of clientViewChildren){
                        child.visible = false;
                    }
                }
                clientViewChildren = null;

                clientView = new interface(stage,startScreen.userId);
                MTLG.interface = clientView;
                clientView.setNewEmailGameInterface(false);
            } 
        }

        function getEmails() {
            return emails;
        }

        function setEmails(emailArray) {
            emails = emailArray;
        }

        function addEmail(email) {
            emails.push(email);

            if(clientView !== null){ //interface is already drawn. Add email dynamically
                clientView.addEmail(email);
            }
        }

        function getOptions() {
            return options;
        }

        return {
            init: init,
            assignToStage: assignToStage,
            getEmails: getEmails,
            setEmails: setEmails,
            addEmail: addEmail,
            getOptions: getOptions,
            goToInterface: goToInterface,
            goToMenu: goToMenu,
            isMenuVisible: isMenuVisible,
            updateClientViews: updateClientViews,
            updateLang: updateLang,
            subscribers: this.subscribers
        }

    })();

    // injecting into MTLG
    m.emailClient = emailClient;

    return m;
})(window.MTLG || {});
