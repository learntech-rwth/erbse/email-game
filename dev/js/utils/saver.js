function saver() {
  const STORAGE_KEY = "GAME_STORAGE";
  const SAVE_KEY = "SAVED_GAME";
  const LP_KEY = "LEARNER_PROFILE";
  const ASSOCIATED_UUID = "ASSOCIATED_UUID";
  const ASSOCIATED_UUID_TIMESTAMP = "ASSOCIATED_UUID_TIMESTAMP";
  const RESET_SAVE_TIME = 2 * 60 * 60 * 1000;

  this.gameStateStorage = {};

  this.saveLP = personalizedServices => {
    window.localStorage.setItem(LP_KEY, JSON.stringify(personalizedServices));
  }

  this.getLP = () => {
    if (window.localStorage.hasOwnProperty(LP_KEY)) {
      return JSON.parse(window.localStorage.getItem(LP_KEY));
    }
    else {
      return null;
    }
  }

  this.save = toSave => {
    if(MTLG.getOptions().logging != "normal"){
      this.setEntry(ASSOCIATED_UUID, _pseudonymRequester.pseudonym)
    }
    else {
      this.setEntry(ASSOCIATED_UUID, _logger.getUUID())
    }

    this.setEntry(ASSOCIATED_UUID_TIMESTAMP, Date.now());

    if(typeof toSave === 'object') {
      this.setEntry(SAVE_KEY, toSave);
    } else {
      this.reset();
    }
  }

  this.load = () => {
    let savedValue = {};
    if(this.storageExists()){
      if(this.getEntry(ASSOCIATED_UUID_TIMESTAMP) == null || this.getEntry(ASSOCIATED_UUID_TIMESTAMP) < Date.now() - RESET_SAVE_TIME) {
        console.log("Reset in load")
        this.reset();
      }

      if(this.hasSavedGameState()){
        savedValue = JSON.parse(JSON.stringify(this.gameStateStorage[SAVE_KEY]));
      }
    }
    else {
      this.createStorage();
    }

    return savedValue;
  }

  this.checkUUID = () => {
    if(this.storageExists()){
      // let currentUUID = _logger.getUUID();
      // let currentUUID = _pseudonymRequester.pseudonym;
      let currentUUID = "";
      this.gameStateStorage = JSON.parse(localStorage.getItem(STORAGE_KEY));
      
      if(this.getEntry(ASSOCIATED_UUID_TIMESTAMP) == null || this.getEntry(ASSOCIATED_UUID_TIMESTAMP) < Date.now() - RESET_SAVE_TIME) {
        if(MTLG.getOptions().logging != "normal"){
          currentUUID = _pseudonymRequester.pseudonym;
        }
      }
      else {
        if(MTLG.getOptions().logging != "normal"){
          currentUUID = this.getEntry(ASSOCIATED_UUID);
        }
      }

      if(!(ASSOCIATED_UUID in this.gameStateStorage) || currentUUID != this.gameStateStorage[ASSOCIATED_UUID]){
        this.reset();
      }
      else {
        _pseudonymRequester.setPseudonym(currentUUID);
      }
    }
  }

  this.reset = () => {
    if(this.storageExists()){
      localStorage.removeItem(STORAGE_KEY);
    }
    this.gameStateStorage = {};
  }
  
  this.resetGameStateStorage = () => {
    this.gameStateStorage = {};
    this.setEntry(SAVE_KEY, {});
  }

  this.storageExists = () => {
    if (window.localStorage.hasOwnProperty(STORAGE_KEY)) {
      return true;
    }
    return false;
  }

  this.hasSavedGameState = () => {
    if(this.storageExists() && SAVE_KEY in this.gameStateStorage && this.gameStateStorage[SAVE_KEY] != {}){
      return true;
    }
    return false;
  }

  this.init = () => {
    this.checkUUID();
    if(this.storageExists()){
      this.setEntry(ASSOCIATED_UUID_TIMESTAMP, Date.now());
      this.gameStateStorage = JSON.parse(localStorage.getItem(STORAGE_KEY));
    }
    else {
      this.initStorage();
    }
  }

  this.initStorage = () => {
    if(MTLG.getOptions().logging == "normal"){
      this.setEntry(ASSOCIATED_UUID, _logger.getUUID());
      this.setEntry(ASSOCIATED_UUID_TIMESTAMP, Date.now());
    }
    else {
      this.setEntry(ASSOCIATED_UUID, _pseudonymRequester.pseudonym);
      this.setEntry(ASSOCIATED_UUID_TIMESTAMP, Date.now());
    }  
  }

  this.getEntry = (key) => {
    return this.gameStateStorage[key];
  }

  this.setEntry = (key, value) => {
    this.gameStateStorage[key] = value;
    let gameStateString = JSON.stringify(this.gameStateStorage);
    window.localStorage.setItem(STORAGE_KEY, gameStateString);
  }

  this.createStorage = () => {
    this.gameStateStorage = {};
    let gameStateString = JSON.stringify(this.gameStateStorage);
    window.localStorage.setItem(STORAGE_KEY, gameStateString);
    this.initStorage();
  }

  this.getNextLevel = () => {
    if(this.hasSavedGameState() && "nextLevel" in this.gameStateStorage[SAVE_KEY]){
      return this.gameStateStorage[SAVE_KEY]["nextLevel"]
    }
    return "level1"
  }

  this.init();
}

