function pseudonymRequester() {
    const REQUEST_URL = 'https://mtlg.elearn.rwth-aachen.de/pseudo/pseudonym';

    this.pseudonym = `fallback_${(Date.now() + Math.random()).toString(36).substring(2).replace(".", "")}`;
    this.overwritten = false;

    this.setPseudonym = (newPseudonym) => {
        this.pseudonym = newPseudonym;
        this.overwritten = true;
    }
    
    fetch(REQUEST_URL)
        .then(response => response.json())
        .then(data => {
            if(!this.overwritten) {
                this.pseudonym = data.pseudonym;
                console.log("Pseudonym");
                console.log(this.pseudonym);
            }
        })
        .catch((error) => {
            console.log("Can't catch new pseudonym");
        });
}
