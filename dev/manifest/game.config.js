/**
 * @Date:   2019-03-06T15:07:33+01:00
 * @Last modified time: 2019-07-03T19:32:48+02:00
 */


MTLG.loadOptions({
  "width":1920, //game width in pixels
  "height":1080, //game height in pixels
  "languages":["en","de"], //Supported languages. First language should be the most complete.
  "countdown":180, //idle time countdown
  "fps":"60", //Frames per second
  "playernumber":4, //Maximum number of supported players
  "FilterTouches": true, //Tangible setting: true means a tangible is recognized as one touch event. False: 4 events.
  "webgl": false, //Using webgl enables using more graphical effects
  "overlayMenu": false, //Starting the overlay menu automatically
  "test": true, //add things to the config as you please
  "soundEnabled": true, //enables sound from the start of the game
  "clicksEnabled": true, //enables clicking in the creation game from the start of the game
  "difficulty": "medium", //difficulty of the game (either "easy", "medium" or "hard"), will decide how many seconds the players have per option, if timers are enabled (40s, 20s or 10s per option)
  "creationTimerEnabled": true, //enables the timer in the creation game
  "matchingTimerEnabled": true, //enabled the timer in the matching game
  "debug": false, //enables some reference logs for shapes, texts, etc.
  "logging": "xAPI", //the used logging mode
  "xapi":{//Configurations for the xapidatacollector. Endpoint and auth properties are required for the xapidatacollector module to work.
    "endpoint": "",//"https://lrs.elearn.rwth-aachen.de/data/xAPI/", //The endpoint of the LRS where XAPI statements are send
    "auth": "",//"Basic ZmZiMmE5NzcyZDBlNWZhMGNkNWI2NGVjNWM5MDFhNThmMDZhMWEyOTo0YmQ1MWY3MjEyNzBkYWIzNTJkYmM2NjM4MzQwNzYxNjgyOWVkYjM1", //Authentification token for LRS
    "gamename": "Creation game", //The name of the game can be transmitted as the platform attribute
    "actor": { //A default actor can be specified to be used when no actor is present for a statement
      objectType: "Agent",
      id: "mailto:defaultActor@test.com",
      name: "DefaultActor"
    }
  },
});
