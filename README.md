# More than meets the eye

The game is automatically deployed to https://lernspiele.informatik.rwth-aachen.de/games/email-game/

## Gameplay 

More than meets the eye is an anti-phishing game which provides two different game modes. 

In the first game mode, phishing mails have to be created by the player using parts provided to them. 
The player has to recognize the phishing techniques used in the different parts and use the correct ones to satisfy the specific requirements of the task. 
They also have to match the parts to their respective phishing techniques afterwards to verify their understanding. 
Between the different tasks, tutorials teach the players more about the different phishing techniques.

In the second game mode, the players are confronted with mails for which they have to decide whether they are malicious or benign. 
In case the mail is malicious, they then have to mark the suspicious sections of the mail. 
This is then verified and a short explanation is provided to them. 

## MTLG-Framework 

The game was created using the MTLG-Framework (Multitouch-Learning-Game-Framework). It was developed as an aid to create platform-independent educational games using JavaScript with support for large Multitouch-Displays. The framework can be found [here](https://gitlab.com/mtlg-framework/mtlg-gameframe). 

## Building the game 

A tutorial on how to build the game yourself from this repository can be found [here](https://gitlab.com/mtlg-framework/mtlg-gameframe/-/wikis/mtlg/installation).
